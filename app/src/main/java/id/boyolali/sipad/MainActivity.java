package id.boyolali.sipad;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/
import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.perf.FirebasePerformance;
import com.google.firebase.perf.metrics.Trace;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.readystatesoftware.systembartint.SystemBarTintManager;

import hotchemi.android.rate.AppRate;
import hotchemi.android.rate.OnClickButtonListener;
import hotchemi.android.rate.StoreType;
import id.boyolali.sipad.fragment.BeritaFragment;
import id.boyolali.sipad.fragment.HomeFragment;
import id.boyolali.sipad.fragment.TimelineFragment;
import id.boyolali.sipad.fragment.UserFragment;
import id.boyolali.sipad.fragment.info.InfoPajakFragment;
import id.boyolali.sipad.util.JenisViewPager;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.ViewPagerAdapter;
import io.fabric.sdk.android.Fabric;
import me.majiajie.pagerbottomtabstrip.NavigationController;
import me.majiajie.pagerbottomtabstrip.PageNavigationView;
import me.majiajie.pagerbottomtabstrip.listener.OnTabItemSelectedListener;


public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    public static ViewPager viewPager;
    TimelineFragment timelineFragment;
    InfoPajakFragment infoFragment;
    HomeFragment homeFragment;
    UserFragment userFragment;
    BeritaFragment beritaFragment;
    private FirebaseAnalytics mFirebaseAnalytics;
    private Toolbar mToolbar;
    private int currentPage;
    private boolean isConnected;
    private String conType;
    private String conType2;
    private PrefManager session;
    private View v;
    private BroadcastReceiver mNetworkReceiver;
    NavigationController mNavigationController;
    PageNavigationView pageBottomTabLayout;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        session = new PrefManager(getApplicationContext());
        Trace myTrace = FirebasePerformance.getInstance().newTrace(TAG);
        myTrace.start();
        initStatusBar();

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();

        //mToolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(mToolbar);
        pageBottomTabLayout = findViewById(R.id.tab);
        viewPager = findViewById(R.id.vp_horizontal_ntb);
        //navigationTabBar = findViewById(R.id.ntb_horizontal);

        initUI();


        mNetworkReceiver = new ConnectivityReceiver();
        registerNetworkBroadcastForNougat();
        //rate_me();


    }

    void rate_me(){

        AppRate.with(this)
                .setStoreType(StoreType.GOOGLEPLAY)
                .setInstallDays(3)
                .setLaunchTimes(10)
                .setRemindInterval(2)
                .setShowLaterButton(true)
                .setShowNeverButton(false)
                .setDebug(true)
                .setCancelable(false)
                .setOnClickButtonListener(new OnClickButtonListener() {
                    @Override
                    public void onClickButton(int which) {
                        Log.e(MainActivity.class.getName(), Integer.toString(which));
                    }
                })
                .setTitle("Beri Nilai")
                .setTextLater("Nanti")
                .setTextRateNow("Baik")
                .setMessage("Bantu kami dalam mengembangkan aplikasi ini agar lebih baik.")
                .monitor();

        AppRate.showRateDialogIfMeetsConditions(this);
    }

    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        timelineFragment = new TimelineFragment();
        infoFragment = new InfoPajakFragment();
        homeFragment = new HomeFragment();
        userFragment = new UserFragment();
        beritaFragment = new BeritaFragment();
        adapter.addFragment(homeFragment);
        adapter.addFragment(timelineFragment);
        adapter.addFragment(infoFragment);
        adapter.addFragment(beritaFragment);
        adapter.addFragment(userFragment);

        viewPager.setAdapter(adapter);
    }


    private void initUI() {
        setupViewPager(viewPager);
        mNavigationController = pageBottomTabLayout.material()
                .addItem(R.drawable.ic_home, "Home")
                .addItem(R.drawable.ic_history, "History")
                .addItem(R.drawable.ic_info, "Informasi")
                .addItem(R.drawable.ic_berita, "Berita")
                .addItem(R.drawable.ic_akun, "Akun")
                .setDefaultColor(getResources().getColor(R.color.abu_muda))
                .build();

        mNavigationController.setupWithViewPager(viewPager);
        mNavigationController.addTabItemSelectedListener(new OnTabItemSelectedListener() {
            @Override
            public void onSelected(int index, int old) {
                //Log.i("asd", "selected: " + index + " old: " + old);
                currentPage = index;
            }

            @Override
            public void onRepeat(int index) {
                //Log.i("asd", "onRepeat selected: " + index);
            }
        });

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                siPAD.getInstance().fabric_report(JenisViewPager.cek_page(position),"","");

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        /*
        final ArrayList<NavigationTabBar.Model> models = new ArrayList<>();
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_home),
                        ContextCompat.getColor(this, R.color.colorPrimaryDark))
                        //.selectedIcon(getResources().getDrawable(R.drawable.ic_sixth))
                        .title("Beranda")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_history),
                        ContextCompat.getColor(this, R.color.colorPrimaryDark))
                        //.selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Riwayat")

                        //.badgeTitle("with")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_info),
                        ContextCompat.getColor(this, R.color.colorPrimaryDark))
                        //.selectedIcon(getResources().getDrawable(R.drawable.ic_seventh))
                        .title("Informasi")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_berita),
                        ContextCompat.getColor(this, R.color.colorPrimaryDark))
//                        .selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Berita")
                        .build()
        );
        models.add(
                new NavigationTabBar.Model.Builder(
                        getResources().getDrawable(R.drawable.ic_akun),
                        ContextCompat.getColor(this, R.color.colorPrimaryDark))
                        //.selectedIcon(getResources().getDrawable(R.drawable.ic_eighth))
                        .title("Profil")
                        .build()
        );

        navigationTabBar.setBgColor(ContextCompat.getColor(this, R.color.background));
        navigationTabBar.setActiveColor(ContextCompat.getColor(this, R.color.background));
        navigationTabBar.setInactiveColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        navigationTabBar.setModels(models);
        navigationTabBar.setCornersRadius(5);
        navigationTabBar.setViewPager(viewPager, 0);


        navigationTabBar.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(final int position) {
            }

            @Override
            public void onPageScrollStateChanged(final int state) {

            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        */


    }

    @Override
    public void onBackPressed() {
        //Log.e(TAG,"wwwwwwwwww    "+currentPage);
        if (currentPage != 0) {
            viewPager.setCurrentItem(0);

            } else {

            dialog_exit();
        }


        return;

    }

    private void dialog_exit() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(MainActivity.this).inflate(R.layout.dialog_with_button,
                (ViewGroup) findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText("Anda akan keluar aplikasi?");
        dialog_left.setText("Tidak");
        dialog_right.setText("Ya");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                MainActivity.this.finish();

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


    }

    private void initStatusBar() {
        Window window = getWindow();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        }


    }

    /*
        private void checkConnection() {
            isConnected = ConnectivityReceiver.isConnected();
            conType = ConnectivityReceiver.getConnectivityType(getApplicationContext());
            conType2 = ConnectivityReceiver.getNetworkClass(getApplicationContext());
            showSnack(isConnected, conType, conType2);
        }

        private void showSnack(boolean isConnected, String conType, String conType2) {
            String message;
            int colorbg,colorText;
            if (isConnected) {

                if (conType == "Using Wifi") {
                    message = conType + "On Line";
                    colorbg = R.color.colorPrimaryDark;
                } else {

                    message = conType + "On Line";
                    colorbg = R.color.colorPrimaryDark;

                }


            } else {
                message = conType + "Off Line";
                colorbg = R.color.merah;

            }
            Snackbar sb = Snackbar.make(findViewById(R.id.vp_horizontal_ntb), message, Snackbar.LENGTH_LONG);
            View sbView = sb.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            sbView.setBackgroundColor(ContextCompat.getColor(this, colorbg));
            textView.setTextColor(Color.WHITE);
            sb.show();

        }

        @Override
        public void onNetworkConnectionChanged(boolean isConnected, String conType, String conType2) {
            showSnack(isConnected, conType, conType2);
        }

        @Override
        protected void onResume() {
            super.onResume();

            siPAD.getInstance().setConnectivityListener(this);
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();

            siPAD.getInstance().setConnectivityListener(this);
        }
    */
    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        siPAD.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        siPAD.getInstance().showalert(getApplicationContext(), isConnected);
    }

}
