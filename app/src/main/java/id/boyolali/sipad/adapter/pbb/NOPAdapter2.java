package id.boyolali.sipad.adapter.pbb;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import id.boyolali.sipad.R;


public class NOPAdapter2 extends RecyclerView.Adapter<NOPAdapter2.MyViewHolder> implements Filterable {

    private Context context;
    private List<NOP> NOPList;
    private List<NOP> NOPListFiltered;
    private NOPsAdapterListener listener;
    public static int selectedCount = 0;
    private SparseBooleanArray selected_items;
    private int current_selected_idx = -1;

    private View.OnClickListener onClickListener = null;

    public static List<NOP> selected_nop = new ArrayList<>();

    public NOPAdapter2(Context context, List<NOP> NOPList, NOPsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.NOPList = NOPList;
        this.NOPListFiltered = NOPList;
        selected_items = new SparseBooleanArray();

    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_nop_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final NOP nop = NOPListFiltered.get(position);
        String KD_PROPINSI = nop.getKD_PROPINSI();
        String KD_DATI2 = nop.getKD_DATI2();
        String KD_KECAMATAN = nop.getKD_KECAMATAN();
        String KD_KELURAHAN = nop.getKD_KELURAHAN();
        String KD_BLOK = nop.getKD_BLOK();
        String NO_URUT = nop.getNO_URUT();
        String KD_JNS_OP = nop.getKD_JNS_OP();
        String NM_WP_SPPT = nop.getNM_WP_SPPT();
        final String NOMINAL_PBB = nop.getNOMINAL_PBB();

        String nop_text = KD_PROPINSI + "." + KD_DATI2 + "." + KD_KECAMATAN + "." + KD_KELURAHAN + "." + KD_BLOK + "-" + NO_URUT + "." + KD_JNS_OP;
        holder.txt_nop.setText(nop_text);
        holder.txt_nama.setText(NM_WP_SPPT);
        holder.image_letter.setText(NM_WP_SPPT.substring(0, 1));


    }


    @Override
    public int getItemCount() {
        return NOPListFiltered.size();
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    NOPListFiltered = NOPList;
                } else {
                    List<NOP> filteredList = new ArrayList<>();
                    for (NOP row : NOPList) {

                        if ((row.getNM_WP_SPPT().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    NOPListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = NOPListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                NOPListFiltered = (ArrayList<NOP>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }


    public interface NOPsAdapterListener {
        void onNOPSelected(NOP NOP);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private View view;
        public TextView txt_nop, txt_nama, image_letter;
        public CardView cv_item;
        public ImageView image;
        public RelativeLayout lyt_checked, lyt_image;
        public View lyt_parent;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txt_nop = view.findViewById(R.id.txt_nop);
            txt_nama = view.findViewById(R.id.txt_nama);
            cv_item = view.findViewById(R.id.cv_item);
            image_letter = (TextView) view.findViewById(R.id.image_letter);
            image = (ImageView) view.findViewById(R.id.image);
            lyt_checked = (RelativeLayout) view.findViewById(R.id.lyt_checked);
            lyt_image = (RelativeLayout) view.findViewById(R.id.lyt_image);
            lyt_parent = (View) view.findViewById(R.id.lyt_parent);

        }

    }
}