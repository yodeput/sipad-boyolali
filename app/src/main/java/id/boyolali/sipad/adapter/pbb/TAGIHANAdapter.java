package id.boyolali.sipad.adapter.pbb;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;

import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;


public class TAGIHANAdapter extends RecyclerView.Adapter<TAGIHANAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<TAGIHAN> TAGIHANList;
    private List<TAGIHAN> TAGIHANListFiltered;
    private TAGIHANsAdapterListener listener;
    public static int selectedCount;

    public static ArrayList<TAGIHAN> selected_TAGIHAN = new ArrayList<>();

    public TAGIHANAdapter(Context context, List<TAGIHAN> TAGIHANList, TAGIHANsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.TAGIHANList = TAGIHANList;
        this.TAGIHANListFiltered = TAGIHANList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_pbb_cek_tagihan, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final TAGIHAN TAGIHAN = TAGIHANListFiltered.get(position);

        String NM_WP_SPPT = TAGIHAN.getNM_WP_SPPT();
        String THN_PAJAK_SPPT = TAGIHAN.getTHN_PAJAK_SPPT();
        String PBB_YG_HARUS_DIBAYAR_SPPT = TAGIHAN.getPBB_YG_HARUS_DIBAYAR_SPPT();
        String TGL_JATUH_TEMPO_SPPT= TAGIHAN.getTGL_JATUH_TEMPO_SPPT();
        String PBB_JUMLAH_BULAN_TERLAMBAT= TAGIHAN.getPBB_JUMLAH_BULAN_TERLAMBAT();
        String PBB_SANKSI= TAGIHAN.getPBB_SANKSI();
        String PBB_TOTAL_HARUS_DIBAYAR= TAGIHAN.getPBB_TOTAL_HARUS_DIBAYAR();
        String total= TAGIHAN.gettotal();

        holder.txt_tahun_pajak.setText(THN_PAJAK_SPPT);
        holder.txt_pokok.setText(rupiah_format_string(PBB_YG_HARUS_DIBAYAR_SPPT));
        holder.txt_bln_denda.setText(PBB_JUMLAH_BULAN_TERLAMBAT+" bulan");
        holder.txt_denda.setText(rupiah_format_string(PBB_SANKSI));
        holder.txt_jumlah.setText(rupiah_format_string(PBB_TOTAL_HARUS_DIBAYAR));
        holder.txt_jatuh_tempo.setText(TGL_JATUH_TEMPO_SPPT);


    }

    @Override
    public int getItemCount() {
        return TAGIHANListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    TAGIHANListFiltered = TAGIHANList;
                } else {
                    List<TAGIHAN> filteredList = new ArrayList<>();
                    for (TAGIHAN row : TAGIHANList) {

                        if ((row.getNM_WP_SPPT().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    TAGIHANListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = TAGIHANListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                TAGIHANListFiltered = (ArrayList<TAGIHAN>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface TAGIHANsAdapterListener {
        void onTAGIHANSelected(TAGIHAN TAGIHAN);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_tahun_pajak, txt_pokok,txt_bln_denda,txt_denda,txt_jumlah,txt_jatuh_tempo;
        public MyViewHolder(View view) {
            super(view);

            txt_tahun_pajak = view.findViewById(R.id.txt_kategori);
            txt_pokok = view.findViewById(R.id.txt_pokok);
            txt_bln_denda = view.findViewById(R.id.txt_bln_denda);
            txt_denda = view.findViewById(R.id.txt_denda);
            txt_jumlah = view.findViewById(R.id.txt_jumlah);
            txt_jatuh_tempo = view.findViewById(R.id.txt_jatuh_tempo);
        }
    }
}