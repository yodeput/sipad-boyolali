package id.boyolali.sipad.adapter.user;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.repsly.library.timelineview.TimelineView;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.NamaBank;

import java.util.ArrayList;
import java.util.List;


public class LunasAdapter extends RecyclerView.Adapter<LunasAdapter.MyViewHolder> implements
        Filterable {

    private Context context;
    private List<LUNAS> LUNASList;
    private List<LUNAS> LUNASListFiltered;
    private LUNASsAdapterListener listener;

    public LunasAdapter(Context context, List<LUNAS> LUNASList, LUNASsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.LUNASList = LUNASList;
        this.LUNASListFiltered = LUNASList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_lunas_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final LUNAS LUNAS = LUNASListFiltered.get(position);

        String kd_bayar = LUNAS.getKODE_BAYAR();
        String ms_pajak = LUNAS.getTAHUN_PAJAK()+"-"+LUNAS.getBULAN_PAJAK();
        String jns_pajak = LUNAS.getJENIS_PAJAK();
        String uraian = LUNAS.getURAIAN();
        String jml_pajak = LUNAS.getTAGIHAN_POKOK();
        String tgl_bayar = LUNAS.getTANGGAL_PEMBAYARAN();
        String bank_bayar = NamaBank.cek_bank(LUNAS.getKODE_BANK());

        holder.txt_kd_bayar.setText(LUNAS.getKODE_BAYAR());
        holder.txt_ms_pajak.setText(LUNAS.getTAHUN_PAJAK()+"-"+LUNAS.getBULAN_PAJAK());
        holder.txt_jns_pajak.setText(LUNAS.getJENIS_PAJAK());
        holder.txt_uraian.setText(LUNAS.getURAIAN());
        holder.txt_jml_pajak.setText(LUNAS.getTAGIHAN_POKOK());
        holder.txt_tgl_bayar.setText(LUNAS.getTANGGAL_PEMBAYARAN());
        holder.txt_bank_bayar.setText(NamaBank.cek_bank(LUNAS.getKODE_BANK()));


    }
    @Override
    public int getItemCount() {
        return LUNASListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    LUNASListFiltered = LUNASList;
                } else {
                    List<LUNAS> filteredList = new ArrayList<>();
                    for (LUNAS row : LUNASList) {

                        if ((row.getKODE_BAYAR().toLowerCase().contains(charString.toLowerCase()))||(row.getNOMOR_DOKUMEN().toLowerCase().contains(charString.toLowerCase()))||(row.getNOMOR_DOKUMEN().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    LUNASListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = LUNASListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                LUNASListFiltered = (ArrayList<LUNAS>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface LUNASsAdapterListener {
        void onLUNASSelected(LUNAS LUNAS);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_kd_bayar,txt_ms_pajak,txt_jns_pajak,txt_uraian,txt_jml_pajak,txt_tgl_bayar,txt_bank_bayar;
        public TimelineView timelineView;
        ImageView but_download, but_search;
        public MyViewHolder(View view) {
            super(view);
            txt_kd_bayar = view.findViewById(R.id.txt_kd_bayar);
            txt_ms_pajak = view.findViewById(R.id.txt_ms_pajak);
            txt_jns_pajak = view.findViewById(R.id.txt_jns_pajak);
            txt_uraian = view.findViewById(R.id.txt_uraian);
            txt_jml_pajak = view.findViewById(R.id.txt_jml_pajak);
            txt_tgl_bayar = view.findViewById(R.id.txt_tgl_bayar);
            txt_bank_bayar = view.findViewById(R.id.txt_bank_bayar);

            but_download = view.findViewById(R.id.but_download);
            but_search = view.findViewById(R.id.but_search);

        }
    }
}