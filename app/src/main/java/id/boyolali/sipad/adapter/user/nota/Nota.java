package id.boyolali.sipad.adapter.user.nota;

public class Nota {
    String ID_NOTA;
    String NO_DOKUMEN;
    String NPWPD;
    String URAIAN_TARIF;
    String JUMLAH_HARGA;
    String JUMLAH_PAJAK;
    String TARIF;
    String TGL_MASA_PAJAK;
    String TGL_JATUH_TEMPO_MASA_PAJAK;

    public Nota() {
    }

    public String getID_NOTA() {
        return ID_NOTA;
    }

    public String getNO_DOKUMEN() {
        return NO_DOKUMEN;
    }

    public String getNPWPD() {
        return NPWPD;
    }


    public String getJUMLAH_HARGA() {
        return JUMLAH_HARGA;
    }

    public String getURAIAN_TARIF() {
        return URAIAN_TARIF;
    }

    public String getJUMLAH_PAJAK() {
        return JUMLAH_PAJAK;
    }

    public String getTARIF () {
        return TARIF;
    }

    public String getTGL_MASA_PAJAK () {
        return TGL_MASA_PAJAK;
    }

    public String getTGL_JATUH_TEMPO_MASA_PAJAK () {
        return TGL_JATUH_TEMPO_MASA_PAJAK;
    }

}
