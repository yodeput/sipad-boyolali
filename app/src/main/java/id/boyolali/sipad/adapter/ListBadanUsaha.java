package id.boyolali.sipad.adapter;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

public class ListBadanUsaha {
    String KODE_BADAN_USAHA;
    String DESKRIPSI_BADAN_USAHA;

    public ListBadanUsaha() {
    }

    public String getKODE_BADAN_USAHA() {
        return KODE_BADAN_USAHA;
    }

    public String getDESKRIPSI_BADAN_USAHA() {
        return DESKRIPSI_BADAN_USAHA;
    }

    public void setKODE_BADAN_USAHA (String KODE_BADAN_USAHA) {
        this.KODE_BADAN_USAHA=KODE_BADAN_USAHA;
    }

    public void setDESKRIPSI_BADAN_USAHA (String DESKRIPSI_BADAN_USAHA) {
        this.DESKRIPSI_BADAN_USAHA=DESKRIPSI_BADAN_USAHA;
    }
}
