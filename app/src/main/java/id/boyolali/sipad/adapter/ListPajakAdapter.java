package id.boyolali.sipad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;

public class ListPajakAdapter extends RecyclerView.Adapter<ListPajakAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<ListPajak> pajakList;
    private List<ListPajak> pajakListFiltered;
    private PajaksAdapterListener listener;

    public ListPajakAdapter(Context context, List<ListPajak> contactList, PajaksAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.pajakList = contactList;
        this.pajakListFiltered = contactList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_pajak_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ListPajak contact = pajakListFiltered.get(position);
        holder.name.setText(contact.getName());

    }

    @Override
    public int getItemCount() {
        return pajakListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    pajakListFiltered = pajakList;
                } else {
                    List<ListPajak> filteredList = new ArrayList<>();
                    for (ListPajak row : pajakList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    pajakListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = pajakListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                pajakListFiltered = (ArrayList<ListPajak>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface PajaksAdapterListener {
        void onPajakSelected(ListPajak pajak);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;

        public MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);


            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected contact in callback
                    listener.onPajakSelected(pajakListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
}