package id.boyolali.sipad.adapter.keckel;

public class kelurahan {

    String KD_PROPINSI;
    String KD_DATI2;
    String KD_KECAMATAN;
    String KD_KELURAHAN;
    String NM_KELURAHAN;
    String ID;

    public kelurahan() {
    }

    public String getKD_PROPINSI() {
        return KD_PROPINSI;
    }

    public String getKD_DATI2() {
        return KD_DATI2;
    }

    public String getKD_KECAMATAN() {
        return KD_KECAMATAN;
    }

    public String getKD_KELURAHAN() {
        return KD_KELURAHAN;
    }

    public String getNM_KELURAHAN() {
        return NM_KELURAHAN;
    }

    public String getID() {
        return ID;
    }

    public void setNM_KELURAHAN (String NM_KELURAHAN) {
        this.NM_KELURAHAN=NM_KELURAHAN;
    }

    public void setKD_KELURAHAN (String KD_KELURAHAN) {
        this.KD_KELURAHAN=KD_KELURAHAN;
    }

    public void setID (String ID) {
        this.ID=ID;
    }


}
