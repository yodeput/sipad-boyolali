package id.boyolali.sipad.adapter.bayar.minerba;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemMinerbaDB extends RealmObject {

    @PrimaryKey
    private long id;

    private String jenis_bahan;

    private String harga;

    private String volume;

    private String total_harga;

    private String total_pajak;
    private String kd_rek_1;
    private String kd_rek_2;
    private String kd_rek_3;
    private String kd_rek_4;
    private String kd_rek_5;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getjenis_bahan() {
        return jenis_bahan;
    }

    public void setjenis_bahan(String jenis_bahan) {
        this.jenis_bahan = jenis_bahan;
    }

    public String getharga() {
        return harga;
    }

    public void setharga(String harga) {
        this.harga = harga;
    }

    public String getvolume() {
        return volume;
    }

    public void setvolume(String volume) {
        this.volume = volume;
    }

    public String gettotal_harga() {
        return total_harga;
    }

    public void settotal_harga(String total_harga) {
        this.total_harga = total_harga;
    }

    public String gettotal_pajak() {
        return total_pajak;
    }

    public void settotal_pajak(String total_pajak) {
        this.total_pajak = total_pajak;
    }



    public String getkd_rek_1() {
        return kd_rek_1;
    }
    public String getkd_rek_2() {
        return kd_rek_2;
    }
    public String getkd_rek_3() {
        return kd_rek_3;
    }
    public String getkd_rek_4() {
        return kd_rek_4;
    }
    public String getkd_rek_5() {
        return kd_rek_5;
    }
    public void setkd_rek_1(String kd_rek_1) {
        this.kd_rek_1 = kd_rek_1;
    }
    public void setkd_rek_2(String kd_rek_2) {
        this.kd_rek_2 = kd_rek_2;
    }
    public void setkd_rek_3(String kd_rek_3) {
        this.kd_rek_3 = kd_rek_3;
    }
    public void setkd_rek_4(String kd_rek_4) {
        this.kd_rek_4 = kd_rek_4;
    }
    public void setkd_rek_5(String kd_rek_5) {
        this.kd_rek_5 = kd_rek_5;
    }



}

