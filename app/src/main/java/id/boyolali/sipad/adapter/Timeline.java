package id.boyolali.sipad.adapter;

public class Timeline {
    String URAIAN;
    String ID;
    String DATETIME;
    String COLLOR;

    public Timeline() {
    }

    public String getId() {
        return ID;
    }

    public String getUraian() {
        return URAIAN;
    }

    public String getDatetime() {
        return DATETIME;
    }

    public String getColor () {
        return COLLOR;
    }

}
