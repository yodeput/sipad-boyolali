package id.boyolali.sipad.adapter.user;

import static id.boyolali.sipad.util.siPAD.getInstance;

import android.content.Context;
import android.os.Handler;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.SwipeLayout.DragEdge;
import com.repsly.library.timelineview.TimelineView;
import id.boyolali.sipad.R;
import java.util.ArrayList;
import java.util.List;


public class USAHAAdapter extends RecyclerView.Adapter<USAHAAdapter.MyViewHolder> implements
    Filterable {

    private Context context;
    private List<USAHA> USAHAList;
    private List<USAHA> USAHAListFiltered;
    private USAHAsAdapterListener listener;

    public USAHAAdapter(Context context, List<USAHA> USAHAList, USAHAsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.USAHAList = USAHAList;
        this.USAHAListFiltered = USAHAList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_usaha_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
      final USAHA USAHA = USAHAListFiltered.get(position);

      final String alamat =
          USAHA.getALAMAT() + " RT." + USAHA.getRT() + " RW." + USAHA.getRW() + " \nKEL." + USAHA
              .getDESA() + " - KEC." + USAHA.getKEC() + "\n" + USAHA.getDATI() + "\n" + USAHA
              .getPROPINSI();
      holder.txt_namausaha.setText(USAHA.getNAMA_USAHA());
      holder.txt_bdn_usaha.setText(USAHA.getDESKRIPSI_BADAN_USAHA());
      holder.txt_alamat.setText(alamat);
      holder.txt_telepon.setText(USAHA.getTELPON());
      holder.txt_tglregister.setText(USAHA.getTGL_REGISTER());

      holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
      holder.swipeLayout.findViewById(R.id.ll_copy).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          String textToCopy = ("Nama Usaha: " + USAHA.getNAMA_USAHA() + "\nJenis: " + USAHA
              .getDESKRIPSI_BADAN_USAHA() + "\nAlamat: " + alamat + "/n" + USAHA.getPROPINSI()
              + "\nTelepon: " + USAHA.getTELPON() + "\nTgl Register:" + USAHA.getTGL_REGISTER());
          getInstance().copyText(context, textToCopy);
        }
      });

      holder.swipeLayout.findViewById(R.id.ll_share).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(final View v) {

          getInstance().init_p_dialog(context, v);
          getInstance().showDialog();
          holder.img_logo.setVisibility(View.VISIBLE);
          Handler handler = new Handler();
          handler.postDelayed(new Runnable() {
            public void run() {

              getInstance().hideDialog();
              getInstance().shareImage(getInstance().takeSS(holder.cv_data, USAHA.getNAMA_USAHA()));
              holder.img_logo.setVisibility(View.GONE);
            }
          }, 800);

        }
      });

    }

      @Override
      public int getItemCount() {
        return USAHAListFiltered.size();
      }

      @Override
      public Filter getFilter() {
        return new Filter() {
          @Override
          protected FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            if (charString.isEmpty()) {
              USAHAListFiltered = USAHAList;
            } else {
              List<USAHA> filteredList = new ArrayList<>();
              for (USAHA row : USAHAList) {

                if ((row.getNAMA_USAHA().toLowerCase().contains(charString.toLowerCase()))||(row.getDATI().toLowerCase().contains(charString.toLowerCase()))||(row.getALAMAT().toLowerCase().contains(charString.toLowerCase()))||(row.getTGL_REGISTER().toLowerCase().contains(charString.toLowerCase()))) {
                  filteredList.add(row);
                }

              }

              USAHAListFiltered = filteredList;
            }

            FilterResults filterResults = new FilterResults();
            filterResults.values = USAHAListFiltered;
            return filterResults;
          }

          @Override
          protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            USAHAListFiltered = (ArrayList<USAHA>) filterResults.values;
            notifyDataSetChanged();
          }
        };
      }


    public interface USAHAsAdapterListener {
        void onUSAHASelected(USAHA USAHA);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_namausaha,txt_bdn_usaha,txt_alamat,txt_telepon,txt_tglregister;
        public TimelineView timelineView;
        public SwipeLayout swipeLayout;
        public CardView cv_data;
        public ImageView img_logo;
        public MyViewHolder(View view) {
            super(view);
            txt_namausaha = view.findViewById(R.id.txt_kategori);
            txt_bdn_usaha = view.findViewById(R.id.txt_pokok);
            txt_alamat = view.findViewById(R.id.txt_alamat);
            txt_telepon = view.findViewById(R.id.txt_telepon);
            txt_tglregister = view.findViewById(R.id.txt_tglregister);
            swipeLayout =  view.findViewById(R.id.swipeLayout);
            cv_data = view.findViewById(R.id.cv_data);
            img_logo = view.findViewById(R.id.img_logo);
            swipeLayout.addDrag(DragEdge.Left, view.findViewById(R.id.bottom_wrapper));

          view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              listener.onUSAHASelected(USAHAListFiltered.get(getAdapterPosition()));
            }
          });

        }
    }
}