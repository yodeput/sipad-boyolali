package id.boyolali.sipad.adapter.user.nota;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.List;

import id.boyolali.sipad.R;
import java.util.Locale;

public class NotaAdapter extends RecyclerView.Adapter<NotaAdapter.MyViewHolder> {

    private Context context;
    private List<Nota> NotaList;
    private List<Nota> NotaListFiltered;
    private NotasAdapterListener listener;

    public NotaAdapter(Context context, List<Nota> NotaList, NotasAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.NotaList = NotaList;
        this.NotaListFiltered = NotaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
            .inflate(R.layout.row_nota_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Nota Nota = NotaListFiltered.get(position);

        holder.txt_npwpd.setText(Nota.getNPWPD());
        holder.txt_idnota.setText(Nota.getID_NOTA());
        holder.txt_nodok.setText(Nota.getNO_DOKUMEN());
        holder.txt_uraian.setText(Nota.getURAIAN_TARIF());
        holder.txt_mspajak.setText(Nota.getTGL_MASA_PAJAK());
        holder.txt_jttempo.setText(Nota.getTGL_JATUH_TEMPO_MASA_PAJAK());

        String txt_omzet = NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(Nota.getJUMLAH_HARGA()));
        String txt_jml_pajak = NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(Nota.getJUMLAH_PAJAK()));

        holder.txt_omzet.setText("Rp. "+txt_omzet);
        holder.txt_tarif.setText(Nota.getTARIF()+"%");
        holder.txt_jml_pajak.setText("Rp. "+txt_jml_pajak);



    }

    @Override
    public int getItemCount() {
        return NotaListFiltered.size();
    }

    public interface NotasAdapterListener {

        void onNotaSelected(Nota Nota);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_npwpd,txt_idnota,txt_nodok,txt_uraian,txt_mspajak,txt_jttempo,txt_omzet,txt_tarif,txt_jml_pajak;


        public MyViewHolder(View view) {
            super(view);
            txt_npwpd = view.findViewById(R.id.txt_npwpd);
            txt_idnota = view.findViewById(R.id.txt_idnota);
            txt_nodok = view.findViewById(R.id.txt_nodok);
            txt_uraian = view.findViewById(R.id.txt_uraian);
            txt_mspajak = view.findViewById(R.id.txt_mspajak);
            txt_jttempo = view.findViewById(R.id.txt_jttempo);
            txt_omzet = view.findViewById(R.id.txt_omzet);
            txt_tarif = view.findViewById(R.id.txt_tarif);
            txt_jml_pajak = view.findViewById(R.id.txt_jml_pajak);


        }
    }
}