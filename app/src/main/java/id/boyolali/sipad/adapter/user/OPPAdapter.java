package id.boyolali.sipad.adapter.user;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.repsly.library.timelineview.TimelineView;

import id.boyolali.sipad.activity.NotaPopup;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.boyolali.sipad.R;
import id.boyolali.sipad.util.JenisPajak;


public class OPPAdapter extends RecyclerView.Adapter<OPPAdapter.MyViewHolder> implements
    Filterable {

    private Context context;
    private List<OPP> OPPList;
    private List<OPP> OPPListFiltered;
    private OPPsAdapterListener listener;

    public OPPAdapter(Context context, List<OPP> OPPList, OPPsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.OPPList = OPPList;
        this.OPPListFiltered = OPPList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_opp_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final OPP OPP = OPPListFiltered.get(position);

        holder.txt_no_reg.setText("#ROP"+OPP.getID_OBJEK_PAJAK());

        holder.txt_no_dok.setText(OPP.getNOMOR_DOKUMEN());

        String jns = JenisPajak.cek_pajak(OPP.getJENIS_PAJAK());
        holder.txt_jns_pajak.setText(jns);
        String masa_pajak = OPP.getTGL_PEMBENTUKAN()+" - "+OPP.getTGL_JATUH_TEMPO();
        holder.txt_ms_pajak.setText(masa_pajak);
        String jml_bayar = NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(OPP.getTGL_JUMLAH_PAJAK()));
        holder.txt_jml_pajak.setText("Rp. "+jml_bayar);


        holder.but_download.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                //Log.e("but_download",OPP.getID_OBJEK_PAJAK());
            }
        });


        holder.but_search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, NotaPopup.class);
                i.putExtra("nota_id",OPP.getID_OBJEK_PAJAK());
                i.putExtra("act","opp");
                context.startActivity(i);




            }
        });


    }
    @Override
    public int getItemCount() {
        return OPPListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    OPPListFiltered = OPPList;
                } else {
                    List<OPP> filteredList = new ArrayList<>();
                    for (OPP row : OPPList) {

                        if ((row.getID_OBJEK_PAJAK().toLowerCase().contains(charString.toLowerCase()))||(row.getNOMOR_DOKUMEN().toLowerCase().contains(charString.toLowerCase()))||(row.getTGL_JUMLAH_PAJAK().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    OPPListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = OPPListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                OPPListFiltered = (ArrayList<OPP>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface OPPsAdapterListener {
        void onOPPSelected(OPP OPP);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_no_reg,txt_no_dok , txt_jns_pajak, txt_ms_pajak, txt_jml_pajak;
        public TimelineView timelineView;
        ImageView but_download, but_search;
        public MyViewHolder(View view) {
            super(view);
            txt_no_reg = view.findViewById(R.id.txt_no_reg);
            txt_no_dok = view.findViewById(R.id.txt_no_dok);
            txt_jns_pajak = view.findViewById(R.id.txt_jns_pajak);
            txt_ms_pajak = view.findViewById(R.id.txt_ms_pajak);
            txt_jml_pajak = view.findViewById(R.id.txt_jml_pajak);

            but_download = view.findViewById(R.id.but_download);
            but_search = view.findViewById(R.id.but_search);

        }
    }
}