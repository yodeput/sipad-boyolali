package id.boyolali.sipad.adapter.bayar;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import com.repsly.library.timelineview.LineType;
import com.repsly.library.timelineview.TimelineView;
import id.boyolali.sipad.R;
import java.util.ArrayList;
import java.util.List;

public class SubJenisUsahaAdapter extends RecyclerView.Adapter<SubJenisUsahaAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<SubJenisUsaha> TimelineList;
    private List<SubJenisUsaha> TimelineListFiltered;
    private TimelinesAdapterListener listener;

    public SubJenisUsahaAdapter(Context context, List<SubJenisUsaha> TimelineList, TimelinesAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.TimelineList = TimelineList;
        this.TimelineListFiltered = TimelineList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_timeline_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final SubJenisUsaha Timeline = TimelineListFiltered.get(position);
        }



    @Override
    public int getItemCount() {
        return TimelineListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    TimelineListFiltered = TimelineList;
                } else {
                    List<SubJenisUsaha> filteredList = new ArrayList<>();
                    for (SubJenisUsaha row : TimelineList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        //if (row.getDatetime().toLowerCase().contains(charString.toLowerCase())) {
                          //  filteredList.add(row);
                       // }
                    }

                    TimelineListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = TimelineListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                TimelineListFiltered = (ArrayList<SubJenisUsaha>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface TimelinesAdapterListener {
        void onTimelineSelected(SubJenisUsaha Timeline);
    }

    private LineType getLineType(int position) {
        if (getItemCount() == 1) {
            return LineType.ONLYONE;

        } else {
            if (position == 0) {
                return LineType.BEGIN;

            } else if (position == getItemCount() - 1) {
                return LineType.END;

            } else {
                return LineType.NORMAL;
            }
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date_time, uraian;
        public TimelineView timelineView;

        public MyViewHolder(View view) {
            super(view);
            date_time = view.findViewById(R.id.txt_datetime);
            uraian = view.findViewById(R.id.txt_uraian);
            timelineView = (TimelineView) view.findViewById(R.id.timeline);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected SubJenisUsaha in callback
                    listener.onTimelineSelected(TimelineListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
}