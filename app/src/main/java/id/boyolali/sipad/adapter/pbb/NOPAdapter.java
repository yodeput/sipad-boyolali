package id.boyolali.sipad.adapter.pbb;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;


import static id.boyolali.sipad.util.Tools.generateRandomColor;


public class NOPAdapter extends RecyclerView.Adapter<NOPAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<NOP> NOPList;
    private List<NOP> NOPListFiltered;
    private NOPsAdapterListener listener;
    public static int selectedCount = 0;
    private SparseBooleanArray selected_items;
    private int current_selected_idx = -1;

    private OnClickListener onClickListener = null;

    public static List<NOP> selected_nop = new ArrayList<>();

    public NOPAdapter(Context context, List<NOP> NOPList, NOPsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.NOPList = NOPList;
        this.NOPListFiltered = NOPList;
        selected_items = new SparseBooleanArray();

    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_nop_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final NOP nop = NOPListFiltered.get(position);
        String KD_PROPINSI = nop.getKD_PROPINSI();
        String KD_DATI2 = nop.getKD_DATI2();
        String KD_KECAMATAN = nop.getKD_KECAMATAN();
        String KD_KELURAHAN = nop.getKD_KELURAHAN();
        String KD_BLOK = nop.getKD_BLOK();
        String NO_URUT = nop.getNO_URUT();
        String KD_JNS_OP = nop.getKD_JNS_OP();
        String NM_WP_SPPT = nop.getNM_WP_SPPT();
        final String NOMINAL_PBB = nop.getNOMINAL_PBB();

        String nop_text = KD_PROPINSI + "." + KD_DATI2 + "." + KD_KECAMATAN + "." + KD_KELURAHAN + "." + KD_BLOK + "-" + NO_URUT + "." + KD_JNS_OP;
        holder.txt_nop.setText(nop_text);
        holder.txt_nama.setText(NM_WP_SPPT);
        holder.image_letter.setText(NM_WP_SPPT.substring(0, 1));

        holder.cv_item.setActivated(selected_items.get(position, false));


        holder.cv_item.setTag(position);

        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onClickListener == null) return;
                onClickListener.onItemClick(v, nop, position);
            }
        });

        holder.cv_item.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (onClickListener == null) return false;
                onClickListener.onItemLongClick(v, nop, position);
                return true;
            }
        });

        toggleCheckedIcon(holder, position, nop);
        displayImage(holder, nop);
/*
        holder.cv_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Integer pos = (Integer) holder.cv_item.getTag();
                Log.e("selected item pos", Integer.toString(pos));

                nop.setSelected(!nop.isSelected());
                holder.view.setBackgroundColor(nop.isSelected() ? Color.CYAN : Color.WHITE);

                if (selected_nop.contains(nop)) {
                    NOPListFiltered.get(position).setSelected(false);
                    selected_nop.remove(nop);
                    unhighlightView(holder);
                    //selectedCount = selectedCount-1;
                } else {
                    NOPListFiltered.get(position).setSelected(true);
                    selected_nop.add(nop);
                    highlightView(holder);
                    selectedCount = selectedCount + 1;
                    nop.SelectedCount = Integer.toString(selectedCount);
                    getInstance().toastSuccess(context, nop.getSelectedCount() + " NOP dipilih");

                }


            }
        });
*/
    }

    private void highlightView(MyViewHolder holder) {
        holder.getLayoutPosition();
        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent));
    }

    private void unhighlightView(MyViewHolder holder) {
        holder.getLayoutPosition();
        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
    }


    private void displayImage(MyViewHolder holder, NOP nop) {
        holder.image.setImageResource(R.drawable.shape_circle);
        holder.image.setColorFilter(generateRandomColor());
        holder.image_letter.setVisibility(View.VISIBLE);

    }

    private void toggleCheckedIcon(MyViewHolder holder, int position, NOP nop) {
        if (selected_items.get(position, false)) {
            holder.lyt_image.setVisibility(View.GONE);
            holder.lyt_checked.setVisibility(View.VISIBLE);
            selected_nop.add(nop);
            highlightView(holder);
            selectedCount = selected_items.size();
            nop.SelectedCount = Integer.toString(selectedCount);
            //getInstance().toastSuccess(context, nop.getSelectedCount() + " NOP dipilih");
            if (current_selected_idx == position) resetCurrentIndex();
        } else {
            selected_nop.remove(nop);
            unhighlightView(holder);
            selectedCount = selectedCount - 1;
            holder.lyt_checked.setVisibility(View.GONE);
            holder.lyt_image.setVisibility(View.VISIBLE);
            if (current_selected_idx == position) resetCurrentIndex();
        }
    }

    @Override
    public int getItemCount() {
        return NOPListFiltered.size();
    }

    public NOP getItem(int position) {
        return NOPListFiltered.get(position);
    }

    public void toggleSelection(int pos) {
        current_selected_idx = pos;
        if (selected_items.get(pos, false)) {
            selected_items.delete(pos);
        } else {
            selected_items.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selected_items.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selected_items.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selected_items.size());
        for (int i = 0; i < selected_items.size(); i++) {
            items.add(selected_items.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        NOPListFiltered.remove(position);
        resetCurrentIndex();
    }

    private void resetCurrentIndex() {
        current_selected_idx = -1;
    }

    public interface OnClickListener {
        void onItemClick(View view, NOP obj, int pos);

        void onItemLongClick(View view, NOP obj, int pos);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    NOPListFiltered = NOPList;
                } else {
                    List<NOP> filteredList = new ArrayList<>();
                    for (NOP row : NOPList) {

                        if ((row.getNM_WP_SPPT().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    NOPListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = NOPListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                NOPListFiltered = (ArrayList<NOP>) filterResults.values;
                notifyDataSetChanged();

            }
        };
    }


    public interface NOPsAdapterListener {
        void onNOPSelected(NOP NOP);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View view;
        public TextView txt_nop, txt_nama, image_letter;
        public CardView cv_item;
        public ImageView image;
        public RelativeLayout lyt_checked, lyt_image;
        public View lyt_parent;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            txt_nop = view.findViewById(R.id.txt_nop);
            txt_nama = view.findViewById(R.id.txt_nama);
            cv_item = view.findViewById(R.id.cv_item);
            image_letter = (TextView) view.findViewById(R.id.image_letter);
            image = (ImageView) view.findViewById(R.id.image);
            lyt_checked = (RelativeLayout) view.findViewById(R.id.lyt_checked);
            lyt_image = (RelativeLayout) view.findViewById(R.id.lyt_image);
            lyt_parent = (View) view.findViewById(R.id.lyt_parent);

        }

        @Override
        public void onClick(View v) {

        }
    }
}