package id.boyolali.sipad.adapter.user;

public class KB {

    String KODE_BAYAR;
    String ID_NOTA;
    String JENIS_PAJAK;
    String JML_BAYAR;
    String TGL_PEMBENTUKAN;
    String TGL_JATUH_TEMPO;

    public KB() {
    }

    public String getKODE_BAYAR() {
        return KODE_BAYAR;
    }

    public String getID_NOTA() {
        return ID_NOTA;
    }


    public String getJENIS_PAJAK() {
        return JENIS_PAJAK;
    }

    public String getJML_BAYAR() {
        return JML_BAYAR;
    }

    public String getTGL_PEMBENTUKAN() {
        return TGL_PEMBENTUKAN;
    }

    public String getTGL_JATUH_TEMPO() {
        return TGL_JATUH_TEMPO;
    }
}
