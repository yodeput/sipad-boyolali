package id.boyolali.sipad.adapter.bayar.minerba;

public class ItemMinerba {
    String jn_bahan;
    String ukuran;
    String tarif;
    String kd_rek_1;
    String kd_rek_2;
    String kd_rek_3;
    String kd_rek_4;
    String kd_rek_5;

    public ItemMinerba() {
    }

    public String getjn_bahan() {
        return jn_bahan;
    }

    public String getukuran() {
        return ukuran;
    }

    public String gettarif() {
        return tarif;
    }

    public void setjn_bahan(String jn_bahan) {
        this.jn_bahan=jn_bahan;
    }

    public void setukuran(String ukuran) {
        this.ukuran=ukuran;
    }

    public void settarif(String tarif) {
        this.tarif=tarif;
    }

    public String getkd_rek_1() {
        return kd_rek_1;
    }
    public String getkd_rek_2() {
        return kd_rek_2;
    }
    public String getkd_rek_3() {
        return kd_rek_3;
    }
    public String getkd_rek_4() {
        return kd_rek_4;
    }
    public String getkd_rek_5() {
        return kd_rek_5;
    }
    public void setkd_rek_1(String kd_rek_1) {
        this.kd_rek_1 = kd_rek_1;
    }
    public void setkd_rek_2(String kd_rek_2) {
        this.kd_rek_2 = kd_rek_2;
    }
    public void setkd_rek_3(String kd_rek_3) {
        this.kd_rek_3 = kd_rek_3;
    }
    public void setkd_rek_4(String kd_rek_4) {
        this.kd_rek_4 = kd_rek_4;
    }
    public void setkd_rek_5(String kd_rek_5) {
        this.kd_rek_5 = kd_rek_5;
    }
}
