package id.boyolali.sipad.adapter.pbb;

public class TAGIHAN {


    String THN_PAJAK_SPPT;
    String NM_WP_SPPT;
    String PBB_YG_HARUS_DIBAYAR_SPPT;
    String TGL_JATUH_TEMPO_SPPT;
    String PBB_JUMLAH_BULAN_TERLAMBAT;
    String PBB_SANKSI;
    String PBB_TOTAL_HARUS_DIBAYAR;
    String total;


    public TAGIHAN() {
    }

    public String getTHN_PAJAK_SPPT() {
        return THN_PAJAK_SPPT;
    }

    public String getNM_WP_SPPT() {
        return NM_WP_SPPT;
    }

    public String getPBB_YG_HARUS_DIBAYAR_SPPT() {
        return PBB_YG_HARUS_DIBAYAR_SPPT;
    }

    public String getTGL_JATUH_TEMPO_SPPT() {
        return TGL_JATUH_TEMPO_SPPT;
    }

    public String getPBB_JUMLAH_BULAN_TERLAMBAT() {
        return PBB_JUMLAH_BULAN_TERLAMBAT;
    }

    public String getPBB_SANKSI() {
        return PBB_SANKSI;
    }

    public String getPBB_TOTAL_HARUS_DIBAYAR() {
        return PBB_TOTAL_HARUS_DIBAYAR;
    }

    public String gettotal() {
        return total;
    }



}
