package id.boyolali.sipad.adapter.bayar.minerba;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import id.boyolali.sipad.R;
import id.boyolali.sipad.realmdb.RealmController;
import id.boyolali.sipad.realmdb.RealmRecyclerViewAdapter;
import io.realm.Realm;

import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;

public class ItemMinerbaDBAdapter extends RealmRecyclerViewAdapter<ItemMinerbaDB> {

    final Context context;
    private Realm realm;
    private LayoutInflater inflater;
    private OnClickListener onClickListener = null;
    private ItemMinerbaAdapterListener listener;


    public ItemMinerbaDBAdapter(Context context, List<ItemMinerbaDB> itemList, ItemMinerbaAdapterListener listener) {
        this.context = context;
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }


    // create new views (invoked by the layout manager)
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate a new card view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.minerba_row_pajak_item, parent, false);
        return new CardViewHolder(view);
    }

    // replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        realm = RealmController.getInstance().getRealm();

        // get the article
        final ItemMinerbaDB db = getItem(position);
        // cast the generic view holder to our specific one
        final CardViewHolder holder = (CardViewHolder) viewHolder;

        final Long nomor = db.getId();
        final String jenis_bahan = db.getjenis_bahan();
        final String harga = db.getharga();
        final String volume = db.getvolume();
        final String total_harga = db.gettotal_harga();
        final String total_pajak = db.gettotal_pajak();


        holder.txt_jns_bahan.setText(jenis_bahan);
        holder.txt_harga.setText(rupiah_format_string(harga));
        holder.txt_volume.setText(volume+" m³");
        holder.txt_ttl_harga.setText(rupiah_format_string(total_harga));
        holder.txt_ttl_pajak.setText(rupiah_format_string(total_pajak));
        holder.txt_nomor.setText(Long.toString(db.getId()));
        holder.txt_nomor.setVisibility(View.GONE);
        holder.but_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onClickListener.onItemClick(v, db, position);

            }
        });

    }

    public interface ItemMinerbaAdapterListener {
        void onItemSelected(ItemMinerbaDB db);
    }


    // return the size of your data set (invoked by the layout manager)
    public int getItemCount() {

        if (getRealmAdapter() != null) {
            return getRealmAdapter().getCount();
        }
        return 0;
    }


    public interface OnClickListener {
        void onItemClick(View view, ItemMinerbaDB obj, int pos);

    }

    public static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView txt_jns_bahan, txt_harga,txt_volume,txt_ttl_harga,txt_ttl_pajak,txt_nomor;
        public ImageView but_delete;
        public CardViewHolder(View itemView) {
            // standard view holder pattern with Butterknife view injection
            super(itemView);

            txt_jns_bahan = itemView.findViewById(R.id.txt_jns_bahan);
            txt_harga = itemView.findViewById(R.id.txt_harga);
            txt_volume = itemView.findViewById(R.id.txt_volume);
            txt_ttl_harga=itemView.findViewById(R.id.txt_ttl_harga);
            txt_ttl_pajak=itemView.findViewById(R.id.txt_ttl_pajak);
            txt_nomor=itemView.findViewById(R.id.txt_nomor);
            but_delete=itemView.findViewById(R.id.but_delete);


        }

        @Override
        public void onClick(View v) {

        }
    }
}
