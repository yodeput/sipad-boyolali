package id.boyolali.sipad.adapter.user;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import id.boyolali.sipad.activity.NotaPopup;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import id.boyolali.sipad.R;
import id.boyolali.sipad.util.JenisPajak;

import com.repsly.library.timelineview.TimelineView;


public class KBAdapter extends RecyclerView.Adapter<KBAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<KB> KBList;
    private List<KB> KBListFiltered;
    private KBsAdapterListener listener;
    private String jns_pajak;

    public KBAdapter(Context context, List<KB> KBList, KBsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.KBList = KBList;
        this.KBListFiltered = KBList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_kb_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final KB KB = KBListFiltered.get(position);

        holder.txt_kd_bayar.setText(KB.getKODE_BAYAR());
        jns_pajak = JenisPajak.cek_pajak(KB.getJENIS_PAJAK());
        holder.txt_jns_pajak.setText(jns_pajak);
        String masa_pajak = KB.getTGL_PEMBENTUKAN()+" - "+KB.getTGL_JATUH_TEMPO();
        holder.txt_ms_pajak.setText(masa_pajak);
        String jml_bayar = NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(KB.getJML_BAYAR()));
        holder.txt_jml_pajak.setText("Rp. "+jml_bayar);


        holder.but_download.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                //Log.e("but_download",KB.getID_NOTA());
            }
        });


        holder.but_search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                Intent i = new Intent(context, NotaPopup.class);
                i.putExtra("nota_id",KB.getID_NOTA());
                i.putExtra("act","kbb");
                context.startActivity(i);
                
            }
        });


    }

    @Override
    public int getItemCount() {
        return KBListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    KBListFiltered = KBList;
                } else {
                    List<KB> filteredList = new ArrayList<>();
                    for (KB row : KBList) {

                        if ((row.getKODE_BAYAR().toLowerCase().contains(charString.toLowerCase()))||(jns_pajak.toLowerCase().contains(charString.toLowerCase()))||(row.getJML_BAYAR().toLowerCase().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    KBListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = KBListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                KBListFiltered = (ArrayList<KB>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface KBsAdapterListener {
        void onKBSelected(KB KB);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_kd_bayar, txt_jns_pajak, txt_ms_pajak, txt_jml_pajak;
        public TimelineView timelineView;
        ImageView but_download, but_search;
        public MyViewHolder(View view) {
            super(view);
            txt_kd_bayar = view.findViewById(R.id.txt_kd_bayar);
            txt_jns_pajak = view.findViewById(R.id.txt_jns_pajak);
            txt_ms_pajak = view.findViewById(R.id.txt_ms_pajak);
            txt_jml_pajak = view.findViewById(R.id.txt_jml_pajak);

            but_download = view.findViewById(R.id.but_download);
            but_search = view.findViewById(R.id.but_search);


        }
    }
}