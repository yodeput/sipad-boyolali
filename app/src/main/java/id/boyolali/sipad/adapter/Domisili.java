package id.boyolali.sipad.adapter;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

public class Domisili {

    String id;
    String name;
    String id_provinsi;
    String id_kab;
    String id_kec;
    String id_desa;


    public Domisili() {
    }

    public String getid() {
        return id;
    }

    public String getname() {
        return name;
    }

    public String getid_provinsi() {
        return id_provinsi;
    }

    public String getid_kab() {
        return id_kab;
    }

    public String getid_kec() {
        return id_kec;
    }

    public String getid_desa() {
        return id_desa;
    }


    public void setid (String id) {
        this.id=id;
    }
    public void setname (String name) {
        this.name=name;
    }
    public void setid_provinsi (String id_provinsi) {
        this.id_provinsi=id_provinsi;
    }
    public void setid_kab (String id_kab) {
        this.id_kab=id_kab;
    }
    public void setid_kec (String id_kec) {
        this.id_kec=id_kec;
    }
    public void setid_desa (String id_desa) {
        this.id_desa=id_desa;
    }

}
