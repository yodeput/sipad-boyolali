package id.boyolali.sipad.adapter.pbb.KodeBayarDB;

import android.content.Context;

import id.boyolali.sipad.realmdb.RealmModelAdapter;
import io.realm.RealmResults;

public class RealmKBAdapter extends RealmModelAdapter<KodeBayarDB> {

    public RealmKBAdapter(Context context, RealmResults<KodeBayarDB> realmResults, boolean automaticUpdate) {

        super(context, realmResults, automaticUpdate);
    }
}