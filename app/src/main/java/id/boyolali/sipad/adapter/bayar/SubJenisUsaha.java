package id.boyolali.sipad.adapter.bayar;

public class SubJenisUsaha {
    String KD;
    String kd_rek_1;
    String kd_rek_2;
    String kd_rek_3;
    String kd_rek_4;
    String kd_rek_5;
    String nm_jenis;
    String nm_hiburan;
    String kd_hiburan;
    String tarif;

    public SubJenisUsaha() {
    }

    public String getKD() {
        return KD;
    }

    public String getkd_rek_1() {
        return kd_rek_1;
    }

    public String getkd_rek_2() {
        return kd_rek_2;
    }

    public String getkd_rek_3 () {
        return kd_rek_3;
    }

    public String getkd_rek_4 () {
        return kd_rek_4;
    }

    public String getkd_rek_5 () {
        return kd_rek_5;
    }

    public String getnm_jenis () {
        return nm_jenis;
    }

    public String getkd_hiburan () {
        return kd_hiburan;
    }

    public String getnm_hiburan () {
        return nm_hiburan;
    }

    public String gettarif () {
        return tarif;
    }

    public void setnm_jenis (String nm_jenis) {
        this.nm_jenis=nm_jenis;
    }

    public void setkd_rek_1(String kd_rek_1) {
        this.kd_rek_1=kd_rek_1;
    }

    public void setkd_rek_2(String kd_rek_2) {
        this.kd_rek_2=kd_rek_2;
    }

    public void setkd_rek_3(String kd_rek_3) {
        this.kd_rek_3=kd_rek_3;
    }

    public void setkd_rek_4(String kd_rek_4) {
        this.kd_rek_4=kd_rek_4;
    }

    public void setkd_rek_5(String kd_rek_5) {
        this.kd_rek_5=kd_rek_5;
    }

    public void setKD (String KD) {
        this.KD=KD;
    }

    public void setkd_hiburan (String kd_hiburan) {
        this.kd_hiburan=kd_hiburan;
    }

    public void setnm_hiburan (String nm_hiburan) {
        this.nm_hiburan=nm_hiburan;
    }

    public void settarif (String tarif) {
        this.tarif=tarif;
    }




}
