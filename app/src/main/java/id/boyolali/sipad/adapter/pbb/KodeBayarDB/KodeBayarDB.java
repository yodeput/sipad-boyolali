package id.boyolali.sipad.adapter.pbb.KodeBayarDB;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class KodeBayarDB extends RealmObject {

    @PrimaryKey
    private long id;

    private String kode_bayar;

    private String jumlah;

    private String jenis_bayar;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getkode_bayar() {
        return kode_bayar;
    }

    public void setkode_bayar(String kode_bayar) {
        this.kode_bayar = kode_bayar;
    }

    public String getjumlah() {
        return jumlah;
    }

    public void setjumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getjenis_bayar() {
        return jenis_bayar;
    }

    public void setjenis_bayar(String jenis_bayar) {
        this.jenis_bayar = jenis_bayar;
    }
}

