package id.boyolali.sipad.adapter.user;

public class OPP {

    String ID_OBJEK_PAJAK;
    String NOMOR_DOKUMEN;
    String JENIS_PAJAK;
    String TGL_PEMBENTUKAN;
    String TGL_JATUH_TEMPO;
    String JUMLAH_PAJAK;

    public OPP() {
    }

    public String getID_OBJEK_PAJAK() {
        return ID_OBJEK_PAJAK;
    }

    public String getNOMOR_DOKUMEN() {
        return NOMOR_DOKUMEN;
    }

    public String getJENIS_PAJAK() {
        return JENIS_PAJAK;
    }

    public String getTGL_PEMBENTUKAN() {
        return TGL_PEMBENTUKAN;
    }

    public String getTGL_JATUH_TEMPO() {return TGL_JATUH_TEMPO;}

    public String getTGL_JUMLAH_PAJAK() {return JUMLAH_PAJAK;}

}
