package id.boyolali.sipad.adapter.pbb.KodeBayarDB;

import android.content.Context;
import android.content.Intent;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.pbb.TampilanKBPBB;
import id.boyolali.sipad.realmdb.RealmController;
import id.boyolali.sipad.realmdb.RealmRecyclerViewAdapter;
import io.realm.Realm;

public class KodeBayarDBAdapter extends RealmRecyclerViewAdapter<KodeBayarDB> {

    final Context context;
    private Realm realm;
    private LayoutInflater inflater;

    public KodeBayarDBAdapter(Context context) {

        this.context = context;
    }

    // create new views (invoked by the layout manager)
    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate a new card view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_kb_pbb_db_item, parent, false);
        return new CardViewHolder(view);
    }

    // replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        realm = RealmController.getInstance().getRealm();

        // get the article
        final KodeBayarDB db = getItem(position);
        // cast the generic view holder to our specific one
        final CardViewHolder holder = (CardViewHolder) viewHolder;

        final String kd_bayar = db.getkode_bayar();
        final String jumlah = db.getjumlah();
        final String jenis = db.getjenis_bayar();
        // set the title and the snippet
        holder.txt_kd_bayar.setText(kd_bayar);
        holder.txt_jumlah.setText("Rp. "+jumlah);
        holder.txt_jenis.setText(jenis);

        if (jenis.contains("Kolektif")){

            holder.img_kolektif.setVisibility(View.VISIBLE);
            holder.img_perorangan.setVisibility(View.GONE);
            holder.img_kolektif.setColorFilter(ContextCompat.getColor(context,R.color.hijau));

        } else {

            holder.img_kolektif.setVisibility(View.GONE);
            holder.img_perorangan.setVisibility(View.VISIBLE);
            holder.img_perorangan.setColorFilter(ContextCompat.getColor(context,R.color.blue_700));

        }


        holder.but_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,TampilanKBPBB.class);
                i.putExtra("from","history");
                i.putExtra("kd_bayar",kd_bayar);
                i.putExtra("jenis",jenis);
                context.startActivity(i);

            }
        });


    }

    // return the size of your data set (invoked by the layout manager)
    public int getItemCount() {

        if (getRealmAdapter() != null) {
            return getRealmAdapter().getCount();
        }
        return 0;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_kd_bayar, txt_jumlah,txt_jenis;
        public ImageView but_detail, img_perorangan, img_kolektif;

        public CardViewHolder(View itemView) {
            // standard view holder pattern with Butterknife view injection
            super(itemView);

            txt_kd_bayar = itemView.findViewById(R.id.txt_kd_bayar);
            txt_jumlah = itemView.findViewById(R.id.txt_jumlah);
            txt_jenis = itemView.findViewById(R.id.txt_jenis);
            but_detail=itemView.findViewById(R.id.but_detail);
            img_perorangan=itemView.findViewById(R.id.img_perorangan);
            img_kolektif=itemView.findViewById(R.id.img_kolektif);


        }
    }
}
