package id.boyolali.sipad.adapter.bayar.airtanah;

public class RefAirTanah {
    String kd_kelas;
    String id_usaha;
    String jenis_usaha;
    String nm_kelas;

    public RefAirTanah() {
    }

    public String getkd_kelas() {
        return kd_kelas;
    }

    public String getid_usaha() {
        return id_usaha;
    }

    public String getjenis_usaha() {
        return jenis_usaha;
    }

    public String getnm_kelas() {
        return nm_kelas;
    }

    public void setjenis_usaha(String jenis_usaha) {
        this.jenis_usaha=jenis_usaha;
    }

    public void setid_usaha(String id_usaha) {
        this.id_usaha=id_usaha;
    }

    public void setkd_kelas(String kd_kelas) {
        this.kd_kelas=kd_kelas;
    }

    public void setnm_kelas(String nm_kelas) {
        this.nm_kelas=nm_kelas;
    }
}
