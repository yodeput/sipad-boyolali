package id.boyolali.sipad.adapter;

public class Berita {
  String id,judul,headline,isi,gambar,tanggal;


  public Berita() {
  }

  public String getid() {
    return id;
  }
  public String getjudul() {
    return judul;
  }

  public String getheadline() {
    return headline;
  }

  public String getisi() {
    return isi;
  }

  public String getgambar() {
    return gambar;
  }

  public String gettanggal() {
    return tanggal;
  }
}