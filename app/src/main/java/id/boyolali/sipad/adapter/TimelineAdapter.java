package id.boyolali.sipad.adapter;

import android.content.Context;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.repsly.library.timelineview.LineType;
import com.repsly.library.timelineview.TimelineView;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Timeline> TimelineList;
    private List<Timeline> TimelineListFiltered;
    private TimelinesAdapterListener listener;

    public TimelineAdapter(Context context, List<Timeline> TimelineList, TimelinesAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.TimelineList = TimelineList;
        this.TimelineListFiltered = TimelineList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_timeline_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Timeline Timeline = TimelineListFiltered.get(position);

        holder.date_time.setText(Timeline.getDatetime());
        holder.uraian.setText(Html.fromHtml(Timeline.getUraian()));
        String warna = Timeline.getColor();
        holder.timelineView.setLineType(getLineType(position));
        if (warna.contains("blue")) {

            holder.date_time.setTextColor(ContextCompat.getColor(context,R.color.blue_dot));
            holder.uraian.setTextColor(ContextCompat.getColor(context,R.color.blue_dot));
            holder.timelineView.setMarkerColor(ContextCompat.getColor(context,R.color.blue_dot));
            holder.timelineView.setEndLineColor(ContextCompat.getColor(context,R.color.blue_dot));
            holder.timelineView.setStartLineColor(ContextCompat.getColor(context,R.color.blue_dot));
        }


    }



    @Override
    public int getItemCount() {
        return TimelineListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    TimelineListFiltered = TimelineList;
                } else {
                    List<Timeline> filteredList = new ArrayList<>();
                    for (Timeline row : TimelineList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getDatetime().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    TimelineListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = TimelineListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                TimelineListFiltered = (ArrayList<Timeline>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface TimelinesAdapterListener {
        void onTimelineSelected(Timeline Timeline);
    }

    private LineType getLineType(int position) {
        if (getItemCount() == 1) {
            return LineType.ONLYONE;

        } else {
            if (position == 0) {
                return LineType.BEGIN;

            } else if (position == getItemCount() - 1) {
                return LineType.END;

            } else {
                return LineType.NORMAL;
            }
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date_time, uraian;
        public TimelineView timelineView;

        public MyViewHolder(View view) {
            super(view);
            date_time = view.findViewById(R.id.txt_datetime);
            uraian = view.findViewById(R.id.txt_uraian);
            timelineView = (TimelineView) view.findViewById(R.id.timeline);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected SubJenisUsaha in callback
                    listener.onTimelineSelected(TimelineListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
}