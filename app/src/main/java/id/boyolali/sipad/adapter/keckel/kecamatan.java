package id.boyolali.sipad.adapter.keckel;

public class kecamatan {

    String KD_PROPINSI;
    String KD_DATI2;
    String KD_KECAMATAN;
    String NM_KECAMATAN;
    String ID;

    public kecamatan() {
    }

    public String getKD_PROPINSI() {
        return KD_PROPINSI;
    }

    public String getKD_DATI2() {
        return KD_DATI2;
    }

    public String getKD_KECAMATAN() {
        return KD_KECAMATAN;
    }

    public String getNM_KECAMATAN() {
        return NM_KECAMATAN;
    }

    public String getID() {
        return ID;
    }

    public void setNM_KECAMATAN (String NM_KECAMATAN) {
        this.NM_KECAMATAN=NM_KECAMATAN;
    }

    public void setKD_KECAMATAN (String KD_KECAMATAN) {
        this.KD_KECAMATAN=KD_KECAMATAN;
    }


}
