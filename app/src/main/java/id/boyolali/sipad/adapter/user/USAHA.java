package id.boyolali.sipad.adapter.user;

public class USAHA {

    String USAHA_ID;
    String NASABAH_ID;
    String NAMA_USAHA;
    String DESKRIPSI_BADAN_USAHA;
    String ALAMAT;
    String RT;
    String RW;
    String DESA;
    String KEC;
    String DATI;
    String PROPINSI;
    String TELPON;
    String TGL_REGISTER;
    String VERIFIKASI;
    private String rank;
    public USAHA() {
    }

    public String getUSAHA_ID() {
        return USAHA_ID;
    }

    public void setUSAHA_ID(String USAHA_ID) {
        this.USAHA_ID = USAHA_ID;
    }

    public String getNASABAH_ID() {
        return NASABAH_ID;
    }

    public String getNAMA_USAHA() {
        return NAMA_USAHA;
    }

    public void setNAMA_USAHA(String NAMA_USAHA) {
        this.NAMA_USAHA = NAMA_USAHA;
    }

    public String getDESKRIPSI_BADAN_USAHA() {
        return DESKRIPSI_BADAN_USAHA;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public String getRT() {
        return RT;
    }

    public String getRW() {
        return RW;
    }

    public String getDESA() {
        return DESA;
    }

    public String getKEC() {
        return KEC;
    }

    public String getDATI() {
        return DATI;
    }

    public String getPROPINSI() {
        return PROPINSI;
    }

    public String getTELPON() {
        return TELPON;
    }

    public String getTGL_REGISTER() {
        return TGL_REGISTER;
    }

    public String getVERIFIKASI() {
        return VERIFIKASI;
    }



}
