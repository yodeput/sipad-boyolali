package id.boyolali.sipad.adapter.keckel;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.repsly.library.timelineview.TimelineView;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;


public class kecamatanAdapter extends RecyclerView.Adapter<kecamatanAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<kecamatan> kecamatanList;
    private List<kecamatan> kecamatanListFiltered;
    private kecamatansAdapterListener listener;
    private String jns_pajak;

    public kecamatanAdapter(Context context, List<kecamatan> kecamatanList, kecamatansAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.kecamatanList = kecamatanList;
        this.kecamatanListFiltered = kecamatanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_kb_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final kecamatan kecamatan = kecamatanListFiltered.get(position);


    }

    @Override
    public int getItemCount() {
        return kecamatanListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    kecamatanListFiltered = kecamatanList;
                } else {
                    List<kecamatan> filteredList = new ArrayList<>();
                    for (kecamatan row : kecamatanList) {

                        if (row.getID().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }

                    }

                    kecamatanListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = kecamatanListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                kecamatanListFiltered = (ArrayList<kecamatan>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface kecamatansAdapterListener {
        void onkecamatanSelected(kecamatan kecamatan);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_kd_bayar, txt_jns_pajak, txt_ms_pajak, txt_jml_pajak;
        public TimelineView timelineView;
        ImageView but_download, but_search;
        public MyViewHolder(View view) {
            super(view);
            txt_kd_bayar = view.findViewById(R.id.txt_kd_bayar);
            txt_jns_pajak = view.findViewById(R.id.txt_jns_pajak);
            txt_ms_pajak = view.findViewById(R.id.txt_ms_pajak);
            txt_jml_pajak = view.findViewById(R.id.txt_jml_pajak);

            but_download = view.findViewById(R.id.but_download);
            but_search = view.findViewById(R.id.but_search);


        }
    }
}