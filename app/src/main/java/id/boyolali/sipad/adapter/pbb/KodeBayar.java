package id.boyolali.sipad.adapter.pbb;

public class KodeBayar {

    String KODE_BAYAR;
    String THN_PAJAK_SPPT;
    String NOP;
    String NM_WP_SPPT;
    String NJOP_SPPT;
    String PBB_YG_HARUS_DIBAYAR_SPPT;
    String PBB_SANKSI;
    String PBB_DISKON;
    String PBB_TOTAL_HARUS_DIBAYAR;
    String TGL_PEMBENTUKAN_KODE_BAYAR;
    String PBB_TERHUTANG_SPPT;
    String TGL_JATUH_TEMPO_KODE_BAYAR;
    String STATUS_PEMBAYARAN_SPPT;
    String NAMA_PEMOHON;
    String EMAIL_PEMOHON;
    String ALAMAT_PEMOHON;
    String NO_TELP_PEMOHON;

    public KodeBayar() {
    }

    public String getKODE_BAYAR() {
        return KODE_BAYAR;
    }

    public String getTHN_PAJAK_SPPT() {
        return THN_PAJAK_SPPT;
    }

    public String getNOP() {
        return NOP;
    }

    public String getNM_WP_SPPT() {
        return NM_WP_SPPT;
    }

    public String getNJOP_SPPT() {
        return NJOP_SPPT;
    }

    public String getPBB_YG_HARUS_DIBAYAR_SPPT() {
        return PBB_YG_HARUS_DIBAYAR_SPPT;
    }

    public String getPBB_SANKSI() {
        return PBB_SANKSI;
    }
    public String getPBB_DISKON() {
        return PBB_DISKON;
    }

    public String getPBB_TOTAL_HARUS_DIBAYAR() {
        return PBB_TOTAL_HARUS_DIBAYAR;
    }

    public String getTGL_PEMBENTUKAN_KODE_BAYAR() {
        return TGL_PEMBENTUKAN_KODE_BAYAR;
    }

    public String getTGL_JATUH_TEMPO_KODE_BAYAR() {
        return TGL_JATUH_TEMPO_KODE_BAYAR;
    }

    public String getPBB_TERHUTANG_SPPT() {
        return PBB_TERHUTANG_SPPT;
    }

    public String getSTATUS_PEMBAYARAN_SPPT() {
        return STATUS_PEMBAYARAN_SPPT;
    }

    public String getNAMA_PEMOHON() {
        return NAMA_PEMOHON;
    }

    public String getEMAIL_PEMOHON() {
        return EMAIL_PEMOHON;
    }

    public String getALAMAT_PEMOHON() {
        return ALAMAT_PEMOHON;
    }

    public String getNO_TELP_PEMOHON() {
        return NO_TELP_PEMOHON;
    }


}
