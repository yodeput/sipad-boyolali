package id.boyolali.sipad.adapter.bayar.reklame;

public class RefReklame {
    String kd_reklame;
    String nm_reklame;
    String kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5;

    public RefReklame() {
    }

    public String getkd_reklame() {
        return kd_reklame;
    }

    public String getnm_reklame() {
        return nm_reklame;
    }

    public String getkd_rek_1() {
        return kd_rek_1;
    }

    public String getkd_rek_2() {
        return kd_rek_2;
    }

    public String getkd_rek_3() {
        return kd_rek_3;
    }

    public String getkd_rek_4() {
        return kd_rek_4;
    }

    public String getkd_rek_5() {
        return kd_rek_5;
    }

    public void setkd_reklame(String kd_reklame) {
        this.kd_reklame = kd_reklame;
    }

    public void setnm_reklame(String nm_reklame) {
        this.nm_reklame = nm_reklame;
    }

    public void setkd_rek_1(String kd_rek_1) {
        this.kd_rek_1 = kd_rek_1;
    }

    public void setkd_rek_2(String kd_rek_2) {
        this.kd_rek_2 = kd_rek_2;
    }

    public void setkd_rek_3(String kd_rek_3) {
        this.kd_rek_3 = kd_rek_3;
    }

    public void setkd_rek_4(String kd_rek_4) {
        this.kd_rek_4 = kd_rek_4;
    }

    public void setkd_rek_5(String kd_rek_5) {
        this.kd_rek_5 = kd_rek_5;
    }

}
