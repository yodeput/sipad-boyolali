package id.boyolali.sipad.adapter.bayar.minerba;

import android.content.Context;

import id.boyolali.sipad.realmdb.RealmModelAdapter;
import io.realm.RealmResults;

public class RealmItemMinerbaAdapter extends RealmModelAdapter<ItemMinerbaDB> {

    public RealmItemMinerbaAdapter(Context context, RealmResults<ItemMinerbaDB> realmResults, boolean automaticUpdate) {

        super(context, realmResults, automaticUpdate);
    }
}