package id.boyolali.sipad.adapter.pbb;

import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.pbb.TampilanKBPBB;

import static id.boyolali.sipad.util.CurrencyFormat.currency_format_string;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;


public class KodeBayarPBBAdapter extends RecyclerView.Adapter<KodeBayarPBBAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<KodeBayar> KodeBayarList;
    private List<KodeBayar> KodeBayarListFiltered;
    private KodeBayarsAdapterListener listener;
    public static int selectedCount;

    public static ArrayList<KodeBayar> selected_KodeBayar = new ArrayList<>();

    public KodeBayarPBBAdapter(Context context, List<KodeBayar> KodeBayarList, KodeBayarsAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.KodeBayarList = KodeBayarList;
        this.KodeBayarListFiltered = KodeBayarList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_kb_pbb_item, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final KodeBayar kb = KodeBayarListFiltered.get(position);

        final String KB = kb.getKODE_BAYAR();
        final String nmwp = kb.getNM_WP_SPPT();
        final String nop = kb.getNOP();
        final String tgl_pembentukan = kb.getTGL_PEMBENTUKAN_KODE_BAYAR();
        final String tgl_jt_tempo= kb.getTGL_JATUH_TEMPO_KODE_BAYAR();

        final String terhutang = kb.getPBB_YG_HARUS_DIBAYAR_SPPT();
        final String sanksi = kb.getPBB_SANKSI();
        final String diskon = kb.getPBB_DISKON();
        final String jumlah = kb.getPBB_TOTAL_HARUS_DIBAYAR();

        holder.txt_kd_bayar.setText(KB);
        holder.txt_jumlah.setText(rupiah_format_string(jumlah));
        holder.txt_pembentukan.setText(tgl_pembentukan);
        holder.txt_jt_tempo.setText(tgl_jt_tempo);

        holder.but_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,TampilanKBPBB.class);
                i.putExtra("from","kb");
                i.putExtra("nmwp",nmwp);
                i.putExtra("nop",nop);
                i.putExtra("terhutang",terhutang);
                i.putExtra("jumlah",jumlah);
                i.putExtra("sanksi",sanksi);
                i.putExtra("diskon",diskon);
                i.putExtra("tgl_jt_tempo",tgl_jt_tempo);
                i.putExtra("KB",KB);
                context.startActivity(i);

            }
        });

    }

    @Override
    public int getItemCount() {
        return KodeBayarListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    KodeBayarListFiltered = KodeBayarList;
                } else {
                    List<KodeBayar> filteredList = new ArrayList<>();
                    for (KodeBayar row : KodeBayarList) {

                        if ((row.getTGL_PEMBENTUKAN_KODE_BAYAR().contains(charString.toLowerCase()))||(row.getTGL_JATUH_TEMPO_KODE_BAYAR().contains(charString.toLowerCase()))
                                ||(row.getEMAIL_PEMOHON().contains(charString.toLowerCase()))||(row.getNAMA_PEMOHON().contains(charString.toLowerCase()))) {
                            filteredList.add(row);
                        }

                    }

                    KodeBayarListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = KodeBayarListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                KodeBayarListFiltered = (ArrayList<KodeBayar>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    public interface KodeBayarsAdapterListener {
        void onKodeBayarSelected(KodeBayar KodeBayar);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_kd_bayar, txt_jumlah,txt_pembentukan,txt_jt_tempo;
        public ImageView but_detail;
        public MyViewHolder(View view) {
            super(view);

            txt_kd_bayar = view.findViewById(R.id.txt_kd_bayar);
            txt_jumlah = view.findViewById(R.id.txt_jumlah);
            txt_pembentukan = view.findViewById(R.id.txt_pembentukan);
            txt_jt_tempo = view.findViewById(R.id.txt_jt_tempo);
            but_detail=view.findViewById(R.id.but_detail);
        }
    }
}