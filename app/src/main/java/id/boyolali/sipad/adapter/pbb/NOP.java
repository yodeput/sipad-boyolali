package id.boyolali.sipad.adapter.pbb;

public class NOP {

    String KD_PROPINSI;
    String KD_DATI2;
    String KD_KECAMATAN;
    String KD_KELURAHAN;
    String KD_BLOK;
    String NO_URUT;
    String KD_JNS_OP;
    String NM_WP_SPPT;
    String NOMINAL_PBB;
    String PBB_TERHUTANG_SPPT;
    String PBB_YG_HARUS_DIBAYAR_SPPT;
    String STIMULUS;
    String DISKON;
    String SANKSI;

    String SelectedCount;

    public Integer image = null;
    public int color = -1;

    private boolean isSelected = false;
    int total;

    public String getKD_PROPINSI() {
        return KD_PROPINSI;
    }

    public String getKD_DATI2() {
        return KD_DATI2;
    }

    public String getKD_KECAMATAN() {
        return KD_KECAMATAN;
    }

    public String getKD_KELURAHAN() {
        return KD_KELURAHAN;
    }

    public String getKD_BLOK() {
        return KD_BLOK;
    }

    public String getNO_URUT() {
        return NO_URUT;
    }

    public String getKD_JNS_OP() {
        return KD_JNS_OP;
    }

    public String getNM_WP_SPPT() {
        return NM_WP_SPPT;
    }

    public String getNOMINAL_PBB() {
        return NOMINAL_PBB;
    }

    public String getPBB_TERHUTANG_SPPT() {
        return PBB_TERHUTANG_SPPT;
    }

    public String getPBB_YG_HARUS_DIBAYAR_SPPT() {
        return PBB_YG_HARUS_DIBAYAR_SPPT;
    }

    public String getSTIMULUS() {
        return STIMULUS;
    }
    public String getDISKON() {
        return DISKON;
    }
    public String getSANKSI() {
        return SANKSI;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isSelected() {
        return isSelected;
    }
    public boolean getSelected() {
        return isSelected;
    }

    public String getSelectedCount() {
        return SelectedCount;
    }

    public void setSelectedCount (String SelectedCount) {
        this.SelectedCount=SelectedCount;
    }

    public int getTotal() {


        return total;
    }

}
