package id.boyolali.sipad.adapter.user;

public class LUNAS {

    String ID_SSPD;
    String KODE_BANK;
    String KODE_BAYAR;
    String NO_SSPD;
    String NAMA_PEMILIK;
    String BULAN_PAJAK;
    String TAHUN_PAJAK;
    String TGL_PENETAPAN_PAJAK;
    String TGL_JATUH_TEMPO;
    String TAGIHAN_POKOK;
    String JENIS_PAJAK;
    String URAIAN;
    String TANGGAL_PEMBAYARAN;
    String NOMOR_DOKUMEN;

    public LUNAS() {
    }

    public String getID_SSPD() {
        return ID_SSPD;
    }

    public String getKODE_BANK() {
        return KODE_BANK;
    }

    public String getKODE_BAYAR() {
        return KODE_BAYAR;
    }

    public String getNO_SSPD() {
        return NO_SSPD;
    }

    public String getNAMA_PEMILIK() {
        return NAMA_PEMILIK;
    }

    public String getBULAN_PAJAK() {
        return BULAN_PAJAK;
    }

    public String getTAHUN_PAJAK() {
        return TAHUN_PAJAK;
    }

    public String getTGL_PENETAPAN_PAJAK() {
        return TGL_PENETAPAN_PAJAK;
    }

    public String getTGL_JATUH_TEMPO() {
        return TGL_JATUH_TEMPO;
    }

    public String getTAGIHAN_POKOK() {
        return TAGIHAN_POKOK;
    }

    public String getJENIS_PAJAK() {
        return JENIS_PAJAK;
    }

    public String getURAIAN() {
        return URAIAN;
    }
    public String getTANGGAL_PEMBAYARAN() {
        return TANGGAL_PEMBAYARAN;
    }
    public String getNOMOR_DOKUMEN() {
        return NOMOR_DOKUMEN;
    }
}
