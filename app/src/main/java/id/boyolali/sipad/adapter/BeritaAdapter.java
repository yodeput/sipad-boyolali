package id.boyolali.sipad.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.R;

public class BeritaAdapter extends RecyclerView.Adapter<BeritaAdapter.MyViewHolder>
        implements Filterable {
    private Context context;
    private List<Berita> BeritaList;
    private List<Berita> BeritaListFiltered;
    private BeritasAdapterListener listener;

    public BeritaAdapter(Context context, List<Berita> BeritaList, BeritasAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.BeritaList = BeritaList;
        this.BeritaListFiltered = BeritaList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_berita_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Berita Berita = BeritaListFiltered.get(position);
        holder.title.setText(Berita.getjudul());
        holder.description.setText(Berita.getheadline());
        holder.url.setText(Berita.getjudul());
        holder.content.setText(Berita.getisi());
        Glide.with(context)
                .load(Berita.getgambar())
                .apply(new RequestOptions().
                        transforms(new CenterCrop(), new RoundedCorners(5))
                        .diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(holder.img_thumb);


    }

    @Override
    public int getItemCount() {
        return BeritaListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    BeritaListFiltered = BeritaList;
                } else {
                    List<Berita> filteredList = new ArrayList<>();
                    for (Berita row : BeritaList) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getjudul().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    BeritaListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = BeritaListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                BeritaListFiltered = (ArrayList<Berita>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }



    public interface BeritasAdapterListener {
        void onBeritaSelected(Berita Berita);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, description, url, content;
        public ImageView img_thumb;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            description = view.findViewById(R.id.description);
            url = view.findViewById(R.id.url);
            content = view.findViewById(R.id.content);
            img_thumb = view.findViewById(R.id.img_thumb);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // send selected Berita in callback
                    listener.onBeritaSelected(BeritaListFiltered.get(getAdapterPosition()));
                }
            });
        }
    }
}