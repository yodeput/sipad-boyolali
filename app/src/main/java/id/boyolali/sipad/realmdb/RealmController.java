package id.boyolali.sipad.realmdb;


import android.app.Application;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import id.boyolali.sipad.adapter.bayar.minerba.ItemMinerbaDB;
import id.boyolali.sipad.adapter.pbb.KodeBayarDB.KodeBayarDB;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(AppCompatActivity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    public void refresh() {

        realm.refresh();
    }


    //KODE BAYAR HISTORY
    public RealmResults<KodeBayarDB> getAllKB() {

        return realm.where(KodeBayarDB.class).findAllSorted("id",Sort.DESCENDING);
    }

    public long getCountKB() {

        return realm.where(KodeBayarDB.class).count();
    }


    public KodeBayarDB getKB(String id) {

        return realm.where(KodeBayarDB.class).equalTo("id", id).findFirst();
    }
    //KODE BAYAR HISTORY


    //PAJAK MINERBA
    public RealmResults<ItemMinerbaDB> getAllPerhitunganMinerba() {

        return realm.where(ItemMinerbaDB.class).findAllSorted("id",Sort.DESCENDING);
    }

    public long getCountPerhitunganMinerba() {

        return realm.where(ItemMinerbaDB.class).count();
    }


    public ItemMinerbaDB deleteItem(String id) {

        return realm.where(ItemMinerbaDB.class).equalTo("id", id).findFirst();
    }
    public void clearPerhitunganMinerba() {

        realm.beginTransaction();
        realm.clear(ItemMinerbaDB.class);
        realm.commitTransaction();
    }
    //KODE BAYAR HISTORY

}
