package id.boyolali.sipad.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class PrefManager {
    // Shared preferences file name
    private static final String PREF_NAME = "sipad_pref";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_IS_ACTIVE = "isActive";
    // LogCat tag
    private static String TAG = PrefManager.class.getSimpleName();
    // Shared Preferences
    SharedPreferences pref;
    Editor editor;
    Context _context;
    // Shared pref mode
    int PRIVATE_MODE = 0;

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setLogin(boolean isLoggedIn) {

        editor.putBoolean(KEY_IS_LOGGED_IN, isLoggedIn);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void setActive(boolean isActive) {

        editor.putBoolean(KEY_IS_ACTIVE, isActive);

        // commit changes
        editor.commit();

        Log.d(TAG, "User login session modified!");
    }

    public void addPref(String tag, String value){
        editor.putString(tag, value);
        editor.commit();
    }
    public String getPref(String tag){
        String value="";

        value = pref.getString(tag,"");

        return value;
    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public boolean isActive() {
        return pref.getBoolean(KEY_IS_ACTIVE, false);
    }
}
