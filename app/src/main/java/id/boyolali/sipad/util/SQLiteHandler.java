/**
 * Author: Ravi Tamada
 * URL: www.androidhive.info
 * twitter: http://twitter.com/ravitamada
 */
package id.boyolali.sipad.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;

public class SQLiteHandler extends SQLiteOpenHelper {

    private static final String TAG = SQLiteHandler.class.getSimpleName();

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "sipad_boyolali";

    // Login table name
    private static final String TABLE_USER = "user";

    // Login Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "nama_lengkap";
    private static final String KEY_NPWPD = "npwpd";
    private static final String KEY_NASABAH_ID = "nasabah_id";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_NUMBER = "no_telp";
    private static final String KEY_FOTO = "foto";
    private static final String KEY_STATUS = "npwpd_status";





    private static final String CREATE_LOGIN_TABLE = "CREATE TABLE " + TABLE_USER + "("
            + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_NAME + " TEXT,"
            + KEY_NPWPD + " TEXT,"
            + KEY_NASABAH_ID + " TEXT,"
            + KEY_EMAIL + " TEXT,"
            + KEY_NUMBER + " TEXT,"
            + KEY_FOTO + " TEXT,"
            + KEY_STATUS + " TEXT" + ")";

    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_LOGIN_TABLE);
        Log.d(TAG, "Database tables created");
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        // Create tables again
        onCreate(db);
    }

    /**
     * Storing user details in database
     */
    public void addUser(String nama_lengkap, String npwpd, String nasabah_id, String email, String no_telp, String foto, String npwpd_status) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, nama_lengkap); // Name
        values.put(KEY_NPWPD, npwpd); //npwpd
        values.put(KEY_NASABAH_ID, nasabah_id);
        values.put(KEY_EMAIL, email); // Email
        values.put(KEY_NUMBER, no_telp); // imei
        values.put(KEY_FOTO, foto);
        values.put(KEY_STATUS, npwpd_status);// uid


        // Inserting Row
        long id = db.insert(TABLE_USER, null, values);
        db.close(); // Closing database connection

        Log.d(TAG, "New user inserted into sqlite: " + id);
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> user = new HashMap<String, String>();
        String selectQuery = "SELECT  * FROM " + TABLE_USER;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            user.put("nama_lengkap", cursor.getString(1));
            user.put("npwpd", cursor.getString(2));
            user.put("nasabah_id", cursor.getString(3));
            user.put("email", cursor.getString(4));
            user.put("no_telp", cursor.getString(5));
            user.put("foto", cursor.getString(6));
            user.put("npwpd_status",cursor.getString(7));
        }
        cursor.close();
        db.close();
        // return user
        Log.d(TAG, "Fetching user from Sqlite: " + user.toString());

        return user;
    }


    public void deleteUsers() {
        SQLiteDatabase db = this.getWritableDatabase();
        // Delete All Rows
        db.delete(TABLE_USER, null, null);
        db.close();

        Log.d(TAG, "Deleted all user info from sqlite");
    }

}
