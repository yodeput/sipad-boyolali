package id.boyolali.sipad.util;

public class JenisUsaha {

    public static String cek_usaha(String kd_pajak){
        String nama_pajak="";
        switch (kd_pajak){
            case "1":
                nama_pajak = "Reklame";
                break;
            case "2":
                nama_pajak = "Air Tanah";
                break;
            case "3":
                nama_pajak = "Minerba";
                break;
            case "4":
                nama_pajak = "Restoran";
                break;
            case "5":
                nama_pajak = "Penerangan";
                break;
            case "6":
                nama_pajak = "Burung Walet";
                break;
            case "7":
                nama_pajak = "Hotel";
                break;
            case "8":
                nama_pajak = "Parkir";
                break;
            case "9":
                nama_pajak = "PBB";
                break;
            case "10":
                nama_pajak = "Hiburan";
                break;
            case "11":
                nama_pajak = "Pajak Lingkungan";
                break;
            case "12":
                nama_pajak = "BPHTB";
        }

        return nama_pajak;
    }
}
