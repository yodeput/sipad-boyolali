package id.boyolali.sipad.util;


public class URLConfig {
    
    //local ---> http://192.168.1.99/sipad/
    //local-public---> http://sagrakreasi.ip-dynamic.com:8080/sipad/
    //public--->http://sipad.boyolali.go.id/
    //==================================//
    public static String SERVER = "https://sipad.boyolali.go.id/";
    //==================================//



    //USER LOGIN, DATA
    public static String URL_LOGIN = SERVER+"android/login";
    public static String URL_USER_DATA_PAJAK = SERVER+"android/userdata?NPWPD=";
    public static String URL_USER_TIMELINE= SERVER+"android/timeline?nasabah_id=";
    public static String URL_USER_NOTA= SERVER+"android/nota_hitung?nota_id=";

    //PBB
    public static final String[] tahun_sppt = {"2019","2018","2017","2016","2015","2014", "2013"};
    public static String URL_PBB_CEKNJOP = SERVER+"android/pbb_ceknjop?";
    public static String URL_PBB_CEKTAGIHAN = SERVER+"android/pbb_cektagihan?";
    public static String URL_PBB_CARIKB = SERVER+"android/pbb_cari_kb?";
    public static String URL_PBB_KOLEKTIF2 = SERVER+"android/bayar_kolektif?";

    public static String URL_PBB_NOP_PERDESA =SERVER+"android/pbb_get_nop_desa?";
    public static String URL_PBB_KECAMATAN =SERVER+"android/kecamatan";
    public static String URL_PBB_KELURAHAN =SERVER+"android/kelurahan?kD_KECAMATAN=";
    public static String URL_PBB_PERORANGAN = SERVER+"layanan/pbb/register/perorangan";
    public static String URL_PBB_KOLEKTIF = SERVER+"layanan/pbb/register/kolektif";

    public static String URL_KATEGORI_USAHA = SERVER+"android/sub_jenis_usaha?pajak=";
    public static String URL_ALAMAT_ZONA = SERVER+"get/gettb/t_zona/administrative_area_level_3/";

    //Parkir, Penerangan, Hotel
    public static String URL_HITUNG_DENDA = SERVER+"get/hitungdenda/";
    public static String URL_BAYAR_HOTEL = SERVER+"android/pajak_hotel?";

    //AirTanah
    public static String URL_AIRTANAH_REF = SERVER+"android/ref_airtanah/";
    public static String URL_AIRTANAH_KELAS = SERVER+"android/ref_airtanah_kelas/";
    public static String URL_AIRTANAH_HITUNG = SERVER+"get/airtanah/";
    public static String URL_AIRTANAH_BAYAR = SERVER+"android/bayar_airtanah?";

    //MINERBA
    public static String URL_JENIS_BAHAN_MINERBA = SERVER+"android/jenis_bahan_minerba";
    public static String URL_BAYAR_MINERBA = SERVER+"android/add_minerba?";

    //REKLAME
    public static String URL_REKLAME_PERIODE = SERVER+"get/period/";
    public static String URL_REKLAME_BAYAR= SERVER+"android/add_reklame?";
    public static String URL_REKLAME_DATA = SERVER+"get/gettb/t_reklame/concat(Kd_Lokasi,jenis_reklame,jangka_waktu)/";
    public static String URL_ALAMAT_AREA = SERVER+"get/gettb/t_area/administrative_area_level_3/";

    public static final String[] muka_reklame = {"1. 1 Muka","2. 2 Muka"};
    public static final String[] letak_reklame = {"1. Indoor","2. Outdoor"};
    public static final String[] status_reklame = {"1. Milik Sendiri","2. Milik Negara"};
    public static final String[] produk_reklame = {"1. Produk Bukan Rokok","2. Rokok","3. Minuman Keras","4. Bukan Produk"};

    public static String URL_BERITA = SERVER+"android/berita";
    public static String URL_SLIDESHOW = SERVER+"android/slideshow";
    public static String URL_STATUS_PAJAK = SERVER+"android/status_pajak";
    public static String ALLOWED_URI_CHARS = "[]@#&=*+-_.,:!?()/~'%";

    public static String REGISTER_AKUN = SERVER+"register/post/ppat/";
    public static String REGISTER_USAHA = SERVER+"add/addrow/t_usaha/";
    public static String GET_PROVINSI = SERVER+"get/gettb/css_prov/";
    public static String GET_KOTAKAB = SERVER+"get/gettb/css_dati/id_prov/";
    public static String GET_KEC = SERVER+"get/gettb/css_kec/id_kab/";
    public static String GET_DESA = SERVER+"get/gettb/css_desa/id_kec/";
    public static String GET_BADAN_USAHA = SERVER+"get/gettb/ref_badan_usaha/";



}
