package id.boyolali.sipad.util;

import android.content.Context;

import androidx.appcompat.app.AppCompatActivity;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.pembayaran.BayarAirtanah;
import id.boyolali.sipad.activity.pembayaran.BayarHiburan;
import id.boyolali.sipad.activity.pembayaran.BayarMinerba;
import id.boyolali.sipad.activity.pembayaran.BayarPenerangan;
import id.boyolali.sipad.activity.pembayaran.BayarReklame;

public class JenisPajak {

    public static String cek_pajak(String kd_pajak){
        String nama_pajak="";
        switch (kd_pajak){
            case "1":
                nama_pajak = "Reklame";
                break;
            case "2":
                nama_pajak = "Air Tanah";
                break;
            case "3":
                nama_pajak = "Minerba";
                break;
            case "4":
                nama_pajak = "Restoran";
                break;
            case "44":
                nama_pajak = "Restoran";
                break;
            case "5":
                nama_pajak = "Penerangan";
                break;
            case "6":
                nama_pajak = "Burung Walet";
                break;
            case "7":
                nama_pajak = "Hotel";
                break;
            case "8":
                nama_pajak = "Parkir";
                break;
            case "9":
                nama_pajak = "PBB";
                break;
            case "10":
                nama_pajak = "Hiburan";
                break;
            case "11":
                nama_pajak = "Pajak Lingkungan";
                break;
            case "12":
                nama_pajak = "BPHTB";
        }

        return nama_pajak;
    }

    public static String url_bayar(String kd_pajak){
        String url_bayar="";
        switch (kd_pajak){
            case "1":
                url_bayar = URLConfig.URL_REKLAME_BAYAR;
                break;
            case "2":
                url_bayar = URLConfig.URL_AIRTANAH_BAYAR;
                break;
            case "3":
                url_bayar = URLConfig.URL_BAYAR_MINERBA;
                break;
            case "4":
                url_bayar = URLConfig.URL_BAYAR_HOTEL;
                break;
            case "5":
                url_bayar = URLConfig.URL_BAYAR_HOTEL;
                break;
            case "6":
                url_bayar = "Burung Walet";
                break;
            case "7":
                url_bayar = URLConfig.URL_BAYAR_HOTEL;
                break;
            case "8":
                url_bayar = URLConfig.URL_BAYAR_HOTEL;
                break;
            case "9":
                url_bayar = "PBB";
                break;
            case "10":
                url_bayar =  URLConfig.URL_BAYAR_HOTEL;
                break;
            case "11":
                url_bayar = "Pajak Lingkungan";
                break;
            case "12":
                url_bayar = "BPHTB";
        }

        return url_bayar;
    }

    public static AppCompatActivity finish (String kd_pajak){
        AppCompatActivity aa=null;
        switch (kd_pajak){
            case "1":
                aa=BayarReklame.end;
                break;
            case "2":
                aa=BayarAirtanah.end;
                break;
            case "3":
                aa=BayarMinerba.end;
                break;
            case "4":
                aa=BayarPenerangan.end;
                break;
            case "44":
                aa=BayarPenerangan.end;
                break;
            case "5":
                aa=BayarPenerangan.end;
                break;
            case "6":
                aa=null;
                break;
            case "7":
                aa=BayarPenerangan.end;
                break;
            case "8":
                aa=BayarPenerangan.end;
                break;
            case "9":
                aa=null;
                break;
            case "10":
                aa=BayarHiburan.end;
                break;
            case "11":
                aa=null;
                break;
            case "12":
                aa=null;
        }

        return aa;
    }

    public static int color_pajak(Context context,String kd_pajak){
        int color_pajak=0;
        switch (kd_pajak){
            case "1":
                color_pajak = context.getResources().getColor(R.color.cyan_800);
                break;
            case "2":
                color_pajak = context.getResources().getColor(R.color.cyan_800);
                break;
            case "3":
                color_pajak = context.getResources().getColor(R.color.deep_orange_800);
                break;
            case "4":
                color_pajak = context.getResources().getColor(R.color.green_800);
                break;
            case "5":
                color_pajak = context.getResources().getColor(R.color.green_800);
                break;
            case "6":
                color_pajak = context.getResources().getColor(R.color.green_800);
                break;
            case "7":
                color_pajak = context.getResources().getColor(R.color.red_800);
                break;
            case "8":
                color_pajak = context.getResources().getColor(R.color.teal_700);
                break;
            case "9":
                color_pajak = context.getResources().getColor(R.color.blue_grey_700);
                break;
            case "10":
                color_pajak = context.getResources().getColor(R.color.hiburan);
                break;
            case "11":
                color_pajak = context.getResources().getColor(R.color.teal_700);
                break;
            case "12":
                color_pajak = context.getResources().getColor(R.color.jingga);
        }

        return color_pajak;
    }
}
