package id.boyolali.sipad.util;

public class NamaBank {

  public static String cek_bank(String kd_bank) {
    String nm_bank = "";
    switch (kd_bank) {
      case "002":
        nm_bank = "BANK BRI";
        break;
      case "003":
        nm_bank = "BANK EKSPOR INDONESIA";
        break;
      case "008":
        nm_bank = "BANK MANDIRI";
        break;
      case "009":
        nm_bank = "BANK BNI";
        break;
      case "011":
        nm_bank = "BANK DANAMON";
        break;
      case "013":
        nm_bank = "PERMATA BANK";
        break;
      case "014":
        nm_bank = "BANK BCA";
        break;
      case "016":
        nm_bank = "BANK BII";
        break;
      case "019":
        nm_bank = "BANK PANIN";
        break;
      case "020":
        nm_bank = "BANK ARTA NIAGA KENCANA";
        break;
      case "022":
        nm_bank = "BANK NIAGA";
        break;
      case "023":
        nm_bank = "BANK BUANA IND";
        break;
      case "026":
        nm_bank = "BANK LIPPO";
        break;
      case "028":
        nm_bank = "BANK NISP";
        break;
      case "030":
        nm_bank = "AMERICAN EXPRESS BANK LTD";
        break;
      case "031":
        nm_bank = "CITIBANK N.A.";
        break;
      case "032":
        nm_bank = "JP. MORGAN CHASE BANK, N.A.";
        break;
      case "033":
        nm_bank = "BANK OF AMERICA, N.A";
        break;
      case "034":
        nm_bank = "ING INDONESIA BANK";
        break;
      case "036":
        nm_bank = "BANK MULTICOR TBK.";
        break;
      case "037":
        nm_bank = "BANK ARTHA GRAHA";
        break;
      case "039":
        nm_bank = "BANK CREDIT AGRICOLE INDOSUEZ";
        break;
      case "040":
        nm_bank = "THE BANGKOK BANK COMP. LTD";
        break;
      case "041":
        nm_bank = "THE HONGKONG & SHANGHAI B.C.";

        break;
      case "042":
        nm_bank = "THE BANK OF TOKYO MITSUBISHI UFJ LTD";
        break;
      case "045":
        nm_bank = "BANK SUMITOMO MITSUI INDONESIA";
        break;
      case "046":
        nm_bank = "BANK DBS INDONESIA";

        break;
      case "047":
        nm_bank = "BANK RESONA PERDANIA";
        break;
      case "048":
        nm_bank = "BANK MIZUHO INDONESIA";
        break;
      case "050":
        nm_bank = "STANDARD CHARTERED BANK";

        break;
      case "052":
        nm_bank = "BANK ABN AMRO";
        break;
      case "053":
        nm_bank = "BANK KEPPEL TATLEE BUANA";
        break;
      case "054":
        nm_bank = "BANK CAPITAL INDONESIA, TBK.";

        break;
      case "057":
        nm_bank = "BANK BNP PARIBAS INDONESIA";
        break;
      case "058":
        nm_bank = "BANK UOB INDONESIA";
        break;
      case "059":
        nm_bank = "KOREA EXCHANGE BANK DANAMON";

        break;
      case "060":
        nm_bank = "RABOBANK INTERNASIONAL INDONESIA";
        break;
      case "061":
        nm_bank = "ANZ PANIN BANK";
        break;
      case "067":
        nm_bank = "DEUTSCHE BANK AG.";

        break;
      case "068":
        nm_bank = "BANK WOORI INDONESIA";
        break;
      case "069":
        nm_bank = "BANK OF CHINA LIMITED";
        break;
      case "076":
        nm_bank = "BANK BUMI ARTA";

        break;
      case "087":
        nm_bank = "BANK EKONOMI";
        break;
      case "088":
        nm_bank = "BANK ANTARDAERAH";
        break;
      case "089":
        nm_bank = "BANK HAGA";

        break;
      case "093":
        nm_bank = "BANK IFI";
        break;
      case "095":
        nm_bank = "BANK CENTURY, TBK.";
        break;
      case "097":
        nm_bank = "BANK MAYAPADA";

        break;
      case "110":
        nm_bank = "BANK JABAR";
        break;
      case "111":
        nm_bank = "BANK DKI";
        break;
      case "112":
        nm_bank = "BPD DIY";

        break;
      case "113":
        nm_bank = "BANK JATENG";
        break;
      case "114":
        nm_bank = "BANK JATIM";
        break;
      case "115":
        nm_bank = "BPD JAMBI";

        break;
      case "116":
        nm_bank = "BPD ACEH";
        break;
      case "117":
        nm_bank = "BANK SUMUT";
        break;
      case "118":
        nm_bank = "BANK NAGARI";

        break;
      case "119":
        nm_bank = "BANK RIAU";
        break;
      case "120":
        nm_bank = "BANK SUMSEL";
        break;
      case "121":
        nm_bank = "BANK LAMPUNG";

        break;
      case "122":
        nm_bank = "BPD KALSEL";
        break;
      case "123":
        nm_bank = "BPD KALIMANTAN BARAT";
        break;
      case "124":
        nm_bank = "BPD KALTIM";

        break;
      case "125":
        nm_bank = "BPD KALTENG";
        break;
      case "126":
        nm_bank = "BPD SULSEL";
        break;
      case "127":
        nm_bank = "BANK SULUT";

        break;
      case "128":
        nm_bank = "BPD NTB";
        break;
      case "129":
        nm_bank = "BPD BALI";
        break;
      case "130":
        nm_bank = "BANK NTT";

        break;
      case "131":
        nm_bank = "BANK MALUKU";
        break;
      case "132":
        nm_bank = "BPD PAPUA";
        break;
      case "133":
        nm_bank = "BANK BENGKULU";

        break;
      case "134":
        nm_bank = "BPD SULAWESI TENGAH";
        break;
      case "135":
        nm_bank = "BANK SULTRA";
        break;
      case "145":
        nm_bank = "BANK NUSANTARA PARAHYANGAN";

        break;
      case "146":
        nm_bank = "BANK SWADESI";
        break;
      case "147":
        nm_bank = "BANK MUAMALAT";
        break;
      case "151":
        nm_bank = "BANK MESTIKA";

        break;
      case "152":
        nm_bank = "BANK METRO EXPRESS";
        break;
      case "153":
        nm_bank = "BANK SHINTA INDONESIA";
        break;
      case "157":
        nm_bank = "BANK MASPION";

        break;
      case "159":
        nm_bank = "BANK HAGAKITA";
        break;
      case "161":
        nm_bank = "BANK GANESHA";
        break;
      case "162":
        nm_bank = "BANK WINDU KENTJANA";

        break;
      case "164":
        nm_bank = "HALIM INDONESIA BANK";
        break;
      case "166":
        nm_bank = "BANK HARMONI INTERNATIONAL";
        break;
      case "167":
        nm_bank = "BANK KESAWAN";
        break;
      case "200":
        nm_bank = "BANK TABUNGAN NEGARA (PERSERO)";
        break;
      case "212":
        nm_bank = "BANK HIMPUNAN SAUDARA 1906, TBK .";
        break;
      case "213":
        nm_bank = "BANK TABUNGAN PENSIUNAN NASIONAL";
        break;
      case "405":
        nm_bank = "BANK SWAGUNA";
        break;
      case "422":
        nm_bank = "BANK JASA ARTA";
        break;
      case "426":
        nm_bank = "BANK MEGA";
        break;
      case "427":
        nm_bank = "BANK JASA JAKARTA";
        break;
      case "441":
        nm_bank = "BANK BUKOPIN";
        break;
      case "451":
        nm_bank = "BANK SYARIAH MANDIRI";
        break;
      case "459":
        nm_bank = "BANK BISNIS INTERNASIONAL";
        break;
      case "466":
        nm_bank = "BANK SRI PARTHA";
        break;
      case "472":
        nm_bank = "BANK JASA JAKARTA";
        break;
      case "484":
        nm_bank = "BANK BINTANG MANUNGGAL";
        break;
      case "485":
        nm_bank = "BANK BUMIPUTERA";
        break;
      case "490":
        nm_bank = "BANK YUDHA BHAKTI";
        break;
      case "491":
        nm_bank = "BANK MITRANIAGA";
        break;
      case "494":
        nm_bank = "BANK AGRO NIAGA";
        break;
      case "498":
        nm_bank = "BANK INDOMONEX";
        break;
      case "501":
        nm_bank = "BANK ROYAL INDONESIA";
        break;
      case "503":
        nm_bank = "BANK ALFINDO";
        break;
      case "506":
        nm_bank = "BANK SYARIAH MEGA";
        break;
      case "513":
        nm_bank = "BANK INA PERDANA";
        break;
      case "517":
        nm_bank = "BANK HARFA";
        break;
      case "520":
        nm_bank = "PRIMA MASTER BANK";
        break;
      case "521":
        nm_bank = "BANK PERSYARIKATAN INDONESIA";
        break;
      case "525":
        nm_bank = "BANK AKITA";
        break;
      case "526":
        nm_bank = "LIMAN INTERNATIONAL BANK";
        break;
      case "531":
        nm_bank = "ANGLOMAS INTERNASIONAL BANK";
        break;
      case "523":
        nm_bank = "BANK DIPO INTERNATIONAL";
        break;
      case "535":
        nm_bank = "BANK KESEJAHTERAAN EKONOMI";
        break;
      case "536":
        nm_bank = "BANK UIB";
        break;
      case "542":
        nm_bank = "BANK ARTOS IND";
        break;
      case "547":
        nm_bank = "BANK PURBA DANARTA";
        break;
      case "548":
        nm_bank = "BANK MULTI ARTA SENTOSA";
        break;
      case "553":
        nm_bank = "BANK MAYORA";
        break;
      case "555":
        nm_bank = "BANK INDEX SELINDO";
        break;
      case "566":
        nm_bank = "BANK VICTORIA INTERNATIONAL";
        break;
      case "558":
        nm_bank = "BANK EKSEKUTIF";
        break;
      case "559":
        nm_bank = "CENTRATAMA NASIONAL BANK";
        break;
      case "562":
        nm_bank = "BANK FAMA INTERNASIONAL";
        break;
      case "564":
        nm_bank = "BANK SINAR HARAPAN BALI";
        break;
      case "567":
        nm_bank = "BANK HARDA";
        break;
      case "945":
        nm_bank = "BANK FINCONESIA";
        break;
      case "946":
        nm_bank = "BANK MERINCORP";
        break;
      case "947":
        nm_bank = "BANK MAYBANK INDOCORP";

    }
    return nm_bank;

  }
}
