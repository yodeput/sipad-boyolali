package id.boyolali.sipad.util;

import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyFormat {

    public static  String info_terbilang(int value)
    {
        String [] bilangan ={"","satu","dua","tiga","empat","lima","enam","tujuh","delapan","sembilan","sepuluh","sebelas"};
        String temp=" ";
        if (value<12){
            temp = " " + bilangan[value];
        }
        else if(value<20){
            temp = info_terbilang(value-10) + " belas";
        }
        else if(value<100){
            temp = info_terbilang(value/10) + " puluh" + info_terbilang(value%10);
        }
        else if(value<200){
            temp = " seratus" + info_terbilang(value-100);
        }
        else if(value<1000){
            temp = info_terbilang(value/100) + " ratus" + info_terbilang(value%100);
        }
        else if(value<2000){
            temp = " seribu"+ info_terbilang(value-1000);
        }
        else if(value<1000000){
            temp = info_terbilang(value/1000) + " ribu" + info_terbilang (value%1000);
        }
        else if(value<1000000000){
            temp = info_terbilang(value/1000000) + " juta" + info_terbilang (value%1000000);
        }    else if(value<1000000000000L){

            temp = info_terbilang(value/1000000000) + " milyar" + info_terbilang (value%1000000000);

        }

        return temp;
    }

    public static String rupiah_format_string(String value){

        String temp=("Rp. "+NumberFormat.getNumberInstance(Locale.UK).format(Integer.parseInt(value)));

        return temp;

    }

    public static String rupiah_format_integer(int value){

        String temp=("Rp. "+NumberFormat.getNumberInstance(Locale.UK).format(value));

        return temp;


    }

    public static String currency_format_string(String value){

        String temp=(NumberFormat.getNumberInstance(Locale.UK).format(Integer.parseInt(value)));

        return temp;

    }

    public static String currency_format_integer(int value){

        String temp=(NumberFormat.getNumberInstance(Locale.UK).format(value));

        return temp;


    }

}
