package id.boyolali.sipad.util;

import android.app.Activity;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Environment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.ContentViewEvent;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import id.boyolali.sipad.R;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

import static id.boyolali.sipad.util.URLConfig.URL_JENIS_BAHAN_MINERBA;
import static id.boyolali.sipad.util.URLConfig.URL_KATEGORI_USAHA;
import static id.boyolali.sipad.util.URLConfig.URL_SLIDESHOW;
import static id.boyolali.sipad.util.URLConfig.URL_STATUS_PAJAK;
import static id.boyolali.sipad.util.URLConfig.URL_USER_DATA_PAJAK;
import static id.boyolali.sipad.util.URLConfig.URL_USER_TIMELINE;


public class siPAD extends Application {

    public static final String TAG = siPAD.class.getSimpleName();
    private static siPAD mInstance;
    private RequestQueue mRequestQueue;
    private AlertDialog dialog;
    private PrefManager session;

    public static synchronized siPAD getInstance() {
        return mInstance;
    }

    public static void hideSoftKeyboard(Activity pActivity) {
        if (pActivity == null) {
            return;
        }
        InputMethodManager inputMethodManager = (InputMethodManager)
                pActivity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
        View viewWithCurrentFocus = pActivity.getCurrentFocus();
        if (inputMethodManager != null && viewWithCurrentFocus != null) {
            inputMethodManager.hideSoftInputFromWindow(
                    viewWithCurrentFocus.getWindowToken(), 0);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        session = new PrefManager(this.getApplicationContext());
        mInstance = this;


        RealmConfiguration config =
                new RealmConfiguration.Builder(this)
                        .deleteRealmIfMigrationNeeded()
                        .build();

        Realm.setDefaultConfiguration(config);
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    public void fabric_report(String content, String type, String id) {
        Answers.getInstance().logContentView(new ContentViewEvent()
                .putContentName(content)
                .putContentType(type)
                .putContentId(id));
    }

    public void dialog_alert(Context context, View v, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_one_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText(message);
        dialog_right.setText("Ok");
        builder.setView(viewInflated);
        final AlertDialog dialog_alert = builder.create();

        if (dialog_alert.isShowing()) {
            dialog_alert.dismiss();
            dialog_alert.cancel();
        }else {
            dialog_alert.show();
            dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            WindowManager.LayoutParams wmlp = dialog_alert.getWindow().getAttributes();
            wmlp.gravity = Gravity.CENTER_HORIZONTAL;
        }
        dialog_alert.setCanceledOnTouchOutside(false);
        dialog_alert.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog_alert.dismiss();

            }
        });
    }

    public void dialog_alert_finish(final Context context, View v, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_one_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText(message);
        dialog_right.setText("Ok");
        builder.setView(viewInflated);
        final AlertDialog dialog_alert = builder.create();

        dialog_alert.show();
        dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog_alert.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog_alert.setCanceledOnTouchOutside(false);
        dialog_alert.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog_alert.dismiss();
                ((Activity)context).finish();

            }
        });
    }

    public void dialog_alert_batal_proses(final Context context, View v, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_with_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        titletxt.setText("");
        messagetxt.setText(message);
        dialog_right.setText("Ok");
        builder.setView(viewInflated);
        final AlertDialog dialog_alert = builder.create();

        dialog_alert.show();
        dialog_alert.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog_alert.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog_alert.setCanceledOnTouchOutside(false);
        dialog_alert.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog_alert.dismiss();
                ((Activity)context).finish();

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog_alert.dismiss();

            }
        });
    }

    public void make_file_data(Context context, String fileName, String data){

        try {

            File dataDir = new File(context.getFilesDir(), "dataUSER");
            dataDir.mkdir();

            if (dataDir.exists()) {

                FileWriter file = new FileWriter(dataDir + File.separator + fileName);
                file.write(data);
                file.flush();
                file.close();
                File f = new File(dataDir + File.separator + fileName);
                if (f.exists()) {

                    //Log.e("Sukses", "File berhasil dibuat" + fileName);

                } else {

                    //Log.e("Gagal", "File not exixt" + fileName);

                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public boolean fileExist(Context context, String title){


        File fileSpinner = new File(context.getFilesDir()+ File.separator + "dataUSER"+ File.separator +title);
        if (fileSpinner.exists()) {

            return true;

        } else {

            return false;

        }

    }

    public void delete_file_data(Context context){

        File dataDir = new File(context.getFilesDir(), "dataUSER");
        dataDir.delete();

    }

    public String get_file_data(Context context, String fileName) {
        byte[] buffer = new byte[0];
        try {
            File dataDir = new File(context.getFilesDir(), "dataUSER");
            File f = new File(dataDir + File.separator + fileName);

            if (f.exists()){
                FileInputStream is = new FileInputStream(f);
                int size = is.available();
                buffer = new byte[size];
                is.read(buffer);
                is.close();
            }
            //check whether file exists

            return new String(buffer);

        } catch (IOException e) {

            //Log.e("TAG", "Error in Reading: " + e.getLocalizedMessage());
            return null;
        }
    }

    public void dialog_exit(final Context context, final View v, final String text) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_with_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText(text);
        dialog_left.setText("Tidak");
        dialog_right.setText("Ya");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                 ((Activity)context).finish();

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


    }

    public void dialog_cancel_bayar(final Context context, final View v) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(context).inflate(R.layout.dialog_with_button,
            (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText("Membatalkan proses bayar??");
        dialog_left.setText("Tidak");
        dialog_right.setText("Ya");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                ((Activity)context).finish();

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });


    }


    public void refresh_data_user(Context context){

        siPAD.getInstance().showDialog();

        String nama = session.getPref("nama");
        String npwpd = session.getPref("npwpd");
        String nasabah_id = session.getPref("nasabah_id");

        req_user_data(npwpd, context);
        req_user_timeline(nasabah_id, context);

    }

    public void req_user_data(String npwpd, final Context context) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                URL_USER_DATA_PAJAK+npwpd, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, response.toString());

                try {

                    String dataLUNAS_string = response.getString("dataLUNAS");
                    String dataKB_string = response.getString("dataKB");
                    String dataOPP_string = response.getString("dataOPP");
                    String dataUSAHA_string = response.getString("dataUSAHA");

                    JSONArray dataLUNAS_array = new JSONArray(dataLUNAS_string);
                    JSONArray dataKB_array = new JSONArray(dataKB_string);
                    JSONArray dataOPP_array = new JSONArray(dataOPP_string);
                    JSONArray dataUSAHA_array = new JSONArray(dataUSAHA_string);

                    int a1 = dataLUNAS_array.length();
                    int a2 = dataKB_array.length();
                    int a3= dataOPP_array.length();
                    int a4= dataUSAHA_array.length();




                    session.addPref("dataLUNAS_len",Integer.toString(a1));
                    session.addPref("dataKB_len",Integer.toString(a2));
                    session.addPref("dataOPP_len",Integer.toString(a3));
                    session.addPref("dataUSAHA_len",Integer.toString(a4));


                    make_file_data(context, "dataLUNAS",dataLUNAS_string);
                    make_file_data(context, "dataKB",dataKB_string);
                    make_file_data(context, "dataOPP",dataOPP_string);
                    make_file_data(context, "dataUSAHA",dataUSAHA_string);


                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

        // Adding request to request queue
        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void req_user_timeline(String nasabah_id, final Context context) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,URL_USER_TIMELINE+nasabah_id, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, response.toString());

                try {

                    String timeLine_string = response.getString("timeLine");

                    make_file_data(context, "timeLine",timeLine_string);



                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void req_slideshow_data(final Context context,final View v) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(URL_SLIDESHOW, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, response.toString());

                try {
                    JSONObject json = new JSONObject(response.toString());
                    JSONArray jArray = json.getJSONArray("slideshow");

                    for(int i=0; i<jArray.length(); i++){

                        JSONObject json_data = jArray.getJSONObject(i);
                        session.addPref("img_url"+i,json_data.getString("img_url"));
                        session.addPref("deskipsi"+i,json_data.getString("deskrisi"));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    //dialog_alert(context,v,getString(R.string.error_koneksi));
                    toastError(context,getString(R.string.error_koneksi));
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                toastError(context,getString(R.string.error_koneksi));
            }
        });

        // Adding request to request queue
        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void req_status_pajak(final Context context) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(URL_STATUS_PAJAK, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.e(TAG, response.toString());
                try {
                    JSONObject json = new JSONObject(response.toString());
                    JSONArray jArray = json.getJSONArray("status_pajak");
                        JSONObject json_data = jArray.getJSONObject(0);
                        session.addPref("status_Parkir",json_data.getString("Parkir"));
                        session.addPref("status_Penerangan",json_data.getString("Penerangan"));
                        session.addPref("status_Reklame",json_data.getString("Reklame"));
                        session.addPref("status_Burung Walet",json_data.getString("Burung Walet"));
                        session.addPref("status_Hotel",json_data.getString("Hotel"));
                        session.addPref("status_Restoran",json_data.getString("Restoran"));
                        session.addPref("status_Hiburan",json_data.getString("Hiburan"));
                        session.addPref("status_Air Tanah",json_data.getString("Air Tanah"));
                        session.addPref("status_Minerba",json_data.getString("Minerba"));
                        session.addPref("status_PBB",json_data.getString("PBB"));
                        session.addPref("status_BPHTB",json_data.getString("BPHTB"));
                        session.addPref("status_BPHTB",json_data.getString("BPHTB"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    //dialog_alert(context,v,getString(R.string.error_koneksi));
                    toastError(context,getString(R.string.error_koneksi));
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                toastError(context,getString(R.string.error_koneksi));
            }
        });

        // Adding request to request queue
        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    public String see_status_pajak(String nama_pajak){
        String a = "status_"+nama_pajak;
        String status = "";
        return status=session.getPref(a);
    }


    public void req_slideshow_data_in_splash(final Context context,final View v) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(URL_SLIDESHOW, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, response.toString());

                try {
                    JSONObject json = new JSONObject(response.toString());
                    JSONArray jArray = json.getJSONArray("slideshow");

                    for(int i=0; i<jArray.length(); i++){

                        JSONObject json_data = jArray.getJSONObject(i);
                        session.addPref("img_url"+i,json_data.getString("img_url"));
                        session.addPref("deskipsi"+i,json_data.getString("deskrisi"));

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                    //dialog_alert(context,v,getString(R.string.error_koneksi));
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();
                //dialog_alert(context,v,getString(R.string.error_koneksi));
            }
        });

        // Adding request to request queue
        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void req_sub_jenis_usaha( final Context context, final View v,final String title) {
        showDialog();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,URL_KATEGORI_USAHA+title, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, response.toString());
                hideDialog();
                try {

                    String sub_jenis_usaha = response.getString("_datarek");

                    make_file_data(context, title, sub_jenis_usaha);



                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                hideDialog();
                //dialog_alert(context,v,getString(R.string.error_koneksi));
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }


    public void req_jenis_bahan_minerba(final String title, final Context context) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,URL_JENIS_BAHAN_MINERBA, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                //Log.e(TAG, response.toString());

                try {

                    String jenis_bahan_minerba = response.getString("_datarefminerba");

                    make_file_data(context, title, jenis_bahan_minerba);



                } catch (JSONException e) {
                    e.printStackTrace();
                    //Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                //VolleyLog.d(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }


    public void init_p_dialog(Context context, View v) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View viewInflated = LayoutInflater.from(context).inflate(R.layout.p_dialog,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        AVLoadingIndicatorView avi = viewInflated.findViewById(R.id.avi);
        builder.setView(viewInflated);
        dialog = builder.create();
        avi.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;

    }

    public void showDialog() {
        if (!dialog.isShowing())
            dialog.show();
    }

    public void hideDialog() {
        if (dialog.isShowing())
            dialog.dismiss();
    }


    public String takeSS(View v, String file_name){
        Date now = new Date();
        android.text.format.DateFormat.format("hh:mm:ss", now);
        String a="";
        try {

            File newfile = new File(Environment.getExternalStorageDirectory()+"/Pictures/siPAD/");
            newfile.mkdir();
            String mPath = newfile.toString() +"/"+ file_name +".jpeg";

            // create bitmap screen capture
            v.setDrawingCacheEnabled(true);
            //Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
            Bitmap bitmap1 =loadBitmapFromView(v,v.getWidth(),v.getHeight());

            v.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap1.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //setting screenshot in imageview
            String filePath = imageFile.getPath();

            Bitmap ssbitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            a=filePath;
            return a;
        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
        return a;
    }

    public static Bitmap loadBitmapFromView(View v, int width, int height) {
        Bitmap b = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        v.draw(c);

        return b;
    }

    public void saveSS(View v, String file_name){
        try {

            File newfile1 = new File(Environment.getExternalStorageDirectory()+"/Pictures/");
            newfile1.mkdir();
            File newfile = new File(Environment.getExternalStorageDirectory()+"/Pictures/siPAD/");
            newfile.mkdir();
            String mPath = newfile.toString() +"/"+ file_name +".jpeg";

            // create bitmap screen capture
            v.setDrawingCacheEnabled(true);
            //Bitmap bitmap = Bitmap.createBitmap(v.getDrawingCache());
            Bitmap bitmap1 =loadBitmapFromView(v,v.getWidth(),v.getHeight());
            v.setDrawingCacheEnabled(false);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap1.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Throwable e) {
            // Several error may come out with file handling or DOM
            e.printStackTrace();
        }
    }

  public void download_img(final String file_name, String url){


  }

    public void shareImage(String sharePath){

        File file = new File(sharePath);
        Uri uri = FileProvider.getUriForFile(this, "id.boyolali.sipad.fileprovider",file);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent );

    }


    public void copyText(Context context, String textToCopy) {
      int sdk = android.os.Build.VERSION.SDK_INT;
      if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) context
            .getSystemService(Context.CLIPBOARD_SERVICE);
        clipboard.setText(textToCopy);
        FancyToast
            .makeText(context, "Selesai disalin", FancyToast.LENGTH_SHORT, FancyToast.SUCCESS,
                false).show();

      } else {
        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context
            .getSystemService(Context.CLIPBOARD_SERVICE);
        android.content.ClipData clip = android.content.ClipData.newPlainText("label", textToCopy);
        clipboard.setPrimaryClip(clip);
        FancyToast
            .makeText(context, "Selesai disalin", FancyToast.LENGTH_SHORT, FancyToast.SUCCESS,
                false).show();

      }
    }

    public boolean issEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidPhone(CharSequence target) {
        return android.util.Patterns.PHONE.matcher(target).matches();
    }

    public final static boolean isEditEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }

    public final void toastError(Context context, String msg){

        FancyToast.makeText(context, msg, FancyToast.LENGTH_SHORT, FancyToast.ERROR, false).show();


    }
    public final void toastSuccess(Context context, String msg){

        FancyToast.makeText(context, msg, FancyToast.LENGTH_SHORT, FancyToast.SUCCESS, false).show();

    }

    public final void toastInfo(Context context, String msg){

        FancyToast.makeText(context, msg, FancyToast.LENGTH_SHORT, FancyToast.INFO, false).show();

    }

    public final void txtwatcher(final EditText editText, final EditText editText_next, final int size){

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (editText.getText().toString().length()==size)
                {
                    editText_next.requestFocus();
                    editText_next.setCursorVisible(true);
                    editText.clearFocus();
                }

                //edit_focus(KD_PROPINSI,KD_DATI2,2);
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length()==size)
                {
                    editText_next.requestFocus();
                    editText_next.setCursorVisible(true);
                    editText.clearFocus();

                }

            }
        });


    }


    public final void edit_enable(final EditText editText, final EditText editText_next, final int size){

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (editText.getText().toString().length()==size)
                {
                    editText_next.setEnabled(true);
                }

                //edit_focus(KD_PROPINSI,KD_DATI2,2);
            }
            @Override
            public void afterTextChanged(Editable editable) {

                if (editable.length()==size)
                {
                    editText_next.setEnabled(true);
                }

            }
        });


    }

    public void showalert(Context context, boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            toastSuccess(context,"Connected");
        } else {
            toastError(context,"Disconnected");
        }

    }

}