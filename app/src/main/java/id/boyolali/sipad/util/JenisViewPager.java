package id.boyolali.sipad.util;

import android.app.Activity;
import android.content.Context;

import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.pembayaran.BayarAirtanah;
import id.boyolali.sipad.activity.pembayaran.BayarHiburan;
import id.boyolali.sipad.activity.pembayaran.BayarMinerba;
import id.boyolali.sipad.activity.pembayaran.BayarPenerangan;
import id.boyolali.sipad.activity.pembayaran.BayarReklame;

public class JenisViewPager {

    public static String cek_page(int pos){
        String jns_fragment="";
        switch (pos){
            case 0:
                jns_fragment = "Home";
                break;
            case 1:
                jns_fragment = "History";
                break;
            case 2:
                jns_fragment = "Informasi";
                break;
            case 3:
                jns_fragment = "Berita";
                break;
            case 4:
                jns_fragment = "Akun";

        }

        return jns_fragment;
    }


}
