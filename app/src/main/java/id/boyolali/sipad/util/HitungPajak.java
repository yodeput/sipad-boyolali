package id.boyolali.sipad.util;

import android.util.Log;

import java.util.Calendar;

public class HitungPajak {

    //Parkir, Penerangan, Hotel
    public static String hitung_pajak_3jenis(int masa_pajak, String jenis_pajak, String Omzet){
        String hasil="";
        final Calendar today = Calendar.getInstance();
        int mont = (today.get(Calendar.MONTH)+1);
        String masa_pajak2= today.get(Calendar.YEAR)+""+String.format("%02d", mont);


        int mp2 = Integer.parseInt(masa_pajak2);

        Omzet = Omzet.replace("Rp","");
        Omzet = Omzet.replace(".","");
        int omzet = Integer.parseInt(Omzet);
        int tarif=0;

        Log.e("Hitung Pajak", masa_pajak +" ----- "+ masa_pajak2);

        if (masa_pajak<mp2) {

            if (omzet <= 5000000) {
                if (jenis_pajak.contains("8")) {
                    tarif = 20;
                } else if (jenis_pajak.contains("5")) {
                    tarif = 9;
                } else if (jenis_pajak.contains("7")) {
                    tarif = 10;
                } else if (jenis_pajak.contains("4")) {
                    tarif = 0;
                } else if (jenis_pajak.contains("99")) {
                    tarif = 10;
                }
            } else if (omzet > 5000000 && omzet <= 10000000) {
                if (jenis_pajak.contains("8")) {
                    tarif = 20;
                } else if (jenis_pajak.contains("5")) {
                    tarif = 9;
                } else if (jenis_pajak.contains("7")) {
                    tarif = 10;
                } else if (jenis_pajak.contains("4")) {
                    tarif = 5;
                } else if (jenis_pajak.contains("99")) {
                    tarif = 10;
                }
            } else if (omzet > 10000000 && omzet <= 19999999) {
                tarif = 7;
                if (jenis_pajak.contains("8")) {
                    tarif = 20;
                } else if (jenis_pajak.contains("5")) {
                    tarif = 9;
                } else if (jenis_pajak.contains("7")) {
                    tarif = 10;
                } else if (jenis_pajak.contains("4")) {
                    tarif = 7;
                } else if (jenis_pajak.contains("99")) {
                    tarif = 10;
                }
            } else {
                tarif = 10;
                if (jenis_pajak.contains("8")) {
                    tarif = 20;
                } else if (jenis_pajak.contains("5")) {
                    tarif = 9;
                }
            }

            int pajak = ((omzet * tarif) / 100);
            hasil=Integer.toString(pajak);
            //Log.e("Hitunnnnnnnng", Integer.toString(pajak));

        } else {

            hasil="false";

        }
        return hasil;

    }


    public static String cek_tarif_3jenis(String jenis_pajak, String Omzet){
        String value="";

        Omzet = Omzet.replace("Rp","");
        Omzet = Omzet.replace(".","");
        int omzet = Integer.parseInt(Omzet);
        int tarif=0;



        if (omzet <= 5000000) {
            if (jenis_pajak.contains("8")) {
                tarif = 20;
            } else if (jenis_pajak.contains("5")) {
                tarif = 9;
            } else if (jenis_pajak.contains("7")) {
                tarif = 10;
            } else if (jenis_pajak.contains("4")) {
                tarif = 0;
            } else if (jenis_pajak.contains("99")) {
                tarif = 10;
            }
        } else if (omzet >= 5000000 && omzet <= 10000000) {
            if (jenis_pajak.contains("8")) {
                tarif = 20;
            } else if (jenis_pajak.contains("5")) {
                tarif = 9;
            } else if (jenis_pajak.contains("7")) {
                tarif = 10;
            } else if (jenis_pajak.contains("4")) {
                tarif = 5;
            } else if (jenis_pajak.contains("99")) {
                tarif = 10;
            }
        } else if (omzet > 10000000 && omzet <= 19999999) {
            tarif = 7;
            if (jenis_pajak.contains("8")) {
                tarif = 20;
            } else if (jenis_pajak.contains("5")) {
                tarif = 9;
            } else if (jenis_pajak.contains("7")) {
                tarif = 10;
            } else if (jenis_pajak.contains("4")) {
                tarif = 7;
            } else if (jenis_pajak.contains("99")) {
                tarif = 10;
            }
        } else {
            tarif = 10;
            if (jenis_pajak.contains("8")) {
                tarif = 20;
            } else if (jenis_pajak.contains("5")) {
                tarif = 9;
            }
        }

        value=Integer.toString(tarif);
        return value;

    }


    public static String cek_masa_pajak(int masa_pajak){
        String hasil="";
        final Calendar today = Calendar.getInstance();
        String masa_pajak2 = today.get(Calendar.YEAR)+""+(today.get(Calendar.MONTH)+1);
        int mont = (today.get(Calendar.MONTH)+1);
        if (mont<10){
            masa_pajak2 = today.get(Calendar.YEAR)+"0"+mont;
        }
        int mp2 = Integer.parseInt(masa_pajak2);

        if (masa_pajak<mp2) {

            hasil="true";

        } else {

            hasil="false";

        }
        return hasil;

    }

}
