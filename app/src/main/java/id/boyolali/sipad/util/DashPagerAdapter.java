package id.boyolali.sipad.util;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import id.boyolali.sipad.fragment.BeritaFragment;
import id.boyolali.sipad.fragment.HomeFragment;
import id.boyolali.sipad.fragment.InfoFragment;
import id.boyolali.sipad.fragment.TimelineFragment;
import id.boyolali.sipad.fragment.UserFragment;

/**
 * Created by Sireesha on 3/2/2017.
 */

public class DashPagerAdapter extends FragmentPagerAdapter {
    public final List<Fragment> fragments = new ArrayList<>();


    int mNumOfTabs;

    public DashPagerAdapter(FragmentManager fragmentManager, int numOfTabs) {
        super(fragmentManager);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                return HomeFragment.newInstance("Home");
            }
            case 1: {
                return TimelineFragment.newInstance("SubJenisUsaha");
            }
            case 2: {
                return InfoFragment.newInstance("Informasi");
            }
            case 3: {
                return BeritaFragment.newInstance("Berita");
            }
            case 4: {
                return UserFragment.newInstance("Akun");
            }
            default:
                return null;

        }
    }

    public void addFragment(Fragment fragment) {
        fragments.add(fragment);
        notifyDataSetChanged();
    }

    @Override
    public boolean isViewFromObject(final View view, final Object object) {
        return view == object;
    }

    @Override
    public int getCount() {
        return 4;
    }


}
