package id.boyolali.sipad.activity.pbb;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.CurrencyFormat;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.siPAD.getInstance;

public class TampilanKBPBB extends AppCompatActivity {

    private static final String TAG = TampilanKBPBB.class.getSimpleName();
    private View v;

    private String GET_PARAMETER, kd_bayar;

    @BindView(R.id.txt_kodebayar)
    TextView txt_kodebayar;


    @BindView(R.id.img_share)
    ImageView img_share;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_qrcode)
    ImageView img_qrcode;

    @BindView(R.id.rl_all)
    RelativeLayout rl_all;
    @BindView(R.id.cv_kd_bayar)
    CardView cv_kd_bayar;
    @BindView(R.id.rl_pemkab)
    RelativeLayout rl_pemkab;

    @BindView(R.id.l1)
    TextView l1;
    @BindView(R.id.l2)
    TextView l2;
    @BindView(R.id.l3)
    TextView l3;

    @BindView(R.id.txt_nmwp)
    TextView txt_nmwp;
    @BindView(R.id.txt_nop)
    TextView txt_nop;
    @BindView(R.id.txt_jttempo)
    TextView txt_jttempo;
    @BindView(R.id.txt_terhutangsppt)
    TextView txt_terhutangsppt;
    @BindView(R.id.txt_sanksi)
    TextView txt_sanksi;
    @BindView(R.id.txt_diskon)
    TextView txt_diskon;
    @BindView(R.id.txt_jumlah)
    TextView txt_jumlah;
    @BindView(R.id.txt_t_pembayaran)
    TextView txt_t_pembayaran;
    @BindView(R.id.txt_jenis_pajak)
    TextView txt_jenis_pajak;
    @BindView(R.id.txt_ket)
    TextView txt_ket;

    @BindView(R.id.btn_id)
    CircularProgressButton btn_id;


    @OnClick(R.id.img_share)
    void img_share() {
        img_back.setVisibility(View.GONE);
        img_share.setVisibility(View.GONE);
        getInstance().showDialog();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getInstance().hideDialog();
                getInstance().shareImage(getInstance().takeSS(rl_all, kd_bayar));
                img_back.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }
    @OnClick(R.id.img_back) void back(){
        finish();
    }

    private String from,nmwp,nop,terhutang,sanksi,tgl_jt_tempo,jumlah,jenis,diskon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_cari_kb_hasil);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(TampilanKBPBB.this, v);
        rl_all.setVisibility(View.GONE);
        Intent intent = getIntent();
        from = intent.getStringExtra("from");

        txt_jenis_pajak.setText("PAJAK BUMI dan BANGUNAN");


        if (from.contains("history")){

            kd_bayar= intent.getStringExtra("kd_bayar");
            jenis= intent.getStringExtra("jenis");
            if (jenis.contains("Kolektif")){


                l1.setText("Nama Pemohon");
                l2.setText("Alamat Pemohon");
                cv_kd_bayar.setCardBackgroundColor(ContextCompat.getColor(this,R.color.teal_800));
                txt_jenis_pajak.setTextColor(ContextCompat.getColor(this,R.color.teal_800));
                get_kode_bayar_kolektif(kd_bayar);

            } else {

                cv_kd_bayar.setCardBackgroundColor(ContextCompat.getColor(this,R.color.blue_900));
                txt_jenis_pajak.setTextColor(ContextCompat.getColor(this,R.color.blue_900));
                get_kode_bayar(kd_bayar);
            }


        } else if (from.contains("kb")){
            rl_all.setVisibility(View.VISIBLE);
            nmwp = intent.getStringExtra("nmwp");
            nop = intent.getStringExtra("nop");
            terhutang = intent.getStringExtra("terhutang");
            sanksi = intent.getStringExtra("sanksi");
            diskon = intent.getStringExtra("diskon");
            tgl_jt_tempo = intent.getStringExtra("tgl_jt_tempo");
            jumlah = intent.getStringExtra("jumlah");
            kd_bayar = intent.getStringExtra("KB");
            l3.setText("Jatuh Tempo KB");

            txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
            txt_nmwp.setText(nmwp);
            txt_nop.setText(nop);
            txt_jttempo.setText(tgl_jt_tempo);
            txt_terhutangsppt.setText(CurrencyFormat.currency_format_string(terhutang));
            txt_sanksi.setText(CurrencyFormat.currency_format_string(sanksi));
            txt_diskon.setText(CurrencyFormat.currency_format_string(diskon));
            txt_jumlah.setText(CurrencyFormat.currency_format_string(jumlah));
            txt_t_pembayaran.setText(getString(R.string.bank_bayar));
            Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
            img_qrcode.setImageBitmap(qrcode_Bitmap);

        }


        siPAD.getInstance().fabric_report("PBB Tampilan KB","","");
    }

    private void get_kode_bayar_kolektif(String kb) {
        getInstance().showDialog();
        final String tag_string_req = "req_kode_bayar";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_CARIKB +"KODE_BAYAR="+kb, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                getInstance().hideDialog();

                try {

                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {

                        rl_all.setVisibility(View.VISIBLE);
                        String data = jObj.getString("data");
                        JSONArray data_array = new JSONArray(data);
                        JSONObject dataObject = data_array.getJSONObject(0);
                        kd_bayar = dataObject.getString("KODE_BAYAR");
                        tgl_jt_tempo = dataObject.getString("TGL_JATUH_TEMPO_SPPT");
                        nmwp = dataObject.getString("NAMA_PEMOHON");
                        nop = dataObject.getString("ALAMAT_PEMOHON");
                        terhutang = dataObject.getString("PBB_YG_HARUS_DIBAYAR_SPPT");
                        sanksi = dataObject.getString("PBB_SANKSI");
                        diskon = dataObject.getString("PBB_DISKON");
                        jumlah = dataObject.getString("PBB_TOTAL_HARUS_DIBAYAR");


                        txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                        txt_nmwp.setText(nmwp);
                        txt_nop.setText(nop);
                        txt_jttempo.setText(tgl_jt_tempo);
                        txt_terhutangsppt.setText(CurrencyFormat.currency_format_string(terhutang));
                        txt_sanksi.setText(CurrencyFormat.currency_format_string(sanksi));
                        txt_diskon.setText(CurrencyFormat.currency_format_string(diskon));
                        txt_jumlah.setText(CurrencyFormat.currency_format_string(jumlah));
                        txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                        Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                        img_qrcode.setImageBitmap(qrcode_Bitmap);

                        //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                        txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+dataObject.getString("EMAIL_PEMOHON"));

                    } else {
                        //Log.e(tag_string_req,response);
                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert_finish(TampilanKBPBB.this, v, errorMsg);

                    }
                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(TampilanKBPBB.this, v, TampilanKBPBB.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void get_kode_bayar(String kb) {
        getInstance().showDialog();
        final String tag_string_req = "req_kode_bayar";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_CARIKB +"KODE_BAYAR="+kb, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                getInstance().hideDialog();

                try {

                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {

                        rl_all.setVisibility(View.VISIBLE);
                        String data = jObj.getString("data");
                        JSONArray data_array = new JSONArray(data);
                        JSONObject dataObject = data_array.getJSONObject(0);
                        kd_bayar = dataObject.getString("KODE_BAYAR");
                        tgl_jt_tempo = dataObject.getString("TGL_JATUH_TEMPO_SPPT");
                        nmwp = dataObject.getString("NM_WP_SPPT");
                        nop = dataObject.getString("NOP");
                        terhutang = dataObject.getString("PBB_YG_HARUS_DIBAYAR_SPPT");
                        sanksi = dataObject.getString("PBB_SANKSI");
                        diskon = dataObject.getString("PBB_DISKON");
                        jumlah = dataObject.getString("PBB_TOTAL_HARUS_DIBAYAR");


                        txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                        txt_nmwp.setText(nmwp);
                        txt_nop.setText(nop);
                        txt_jttempo.setText(tgl_jt_tempo);
                        txt_terhutangsppt.setText(CurrencyFormat.currency_format_string(terhutang));
                        txt_sanksi.setText(CurrencyFormat.currency_format_string(sanksi));
                        txt_diskon.setText(CurrencyFormat.currency_format_string(diskon));
                        txt_jumlah.setText(CurrencyFormat.currency_format_string(jumlah));
                        txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                        Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                        img_qrcode.setImageBitmap(qrcode_Bitmap);

                        //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                        txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+dataObject.getString("EMAIL_PEMOHON"));

                    } else {
                        //Log.e(tag_string_req,response);
                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert_finish(TampilanKBPBB.this, v, errorMsg);

                    }
                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(TampilanKBPBB.this, v, getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);


    }


    private void init_view(){
        txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
        txt_nmwp.setText(nmwp);
        txt_nop.setText(nop);
        txt_jttempo.setText(tgl_jt_tempo);
        txt_terhutangsppt.setText(terhutang);
        txt_sanksi.setText(sanksi);
        txt_jumlah.setText(jumlah);
        txt_t_pembayaran.setText(getString(R.string.bank_bayar));
        Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
        img_qrcode.setImageBitmap(qrcode_Bitmap);

    }

}