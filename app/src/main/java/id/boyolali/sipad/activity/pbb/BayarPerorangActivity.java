package id.boyolali.sipad.activity.pbb;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.URLConfig.URL_PBB_PERORANGAN;
import static id.boyolali.sipad.util.URLConfig.tahun_sppt;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;
import static id.boyolali.sipad.util.siPAD.isValidEmail;
import static id.boyolali.sipad.util.siPAD.isValidPhone;

public class BayarPerorangActivity extends AppCompatActivity {

    private static final String TAG = BayarPerorangActivity.class.getSimpleName();
    private View v;

    @BindView(R.id.edit_email)
    EditText edit_email;
    @BindView(R.id.edit_telepon)
    EditText edit_telepon;
    @BindView(R.id.spinner_thnSPPT)
    MaterialSpinner spinner_thnSPPT;
    @BindView(R.id.KD_PROPINSI)
    EditText KD_PROPINSI;
    @BindView(R.id.KD_DATI2)
    EditText KD_DATI2;
    @BindView(R.id.KD_KECAMATAN)
    EditText KD_KECAMATAN;
    @BindView(R.id.KD_KELURAHAN)
    EditText KD_KELURAHAN;
    @BindView(R.id.KD_BLOK)
    EditText KD_BLOK;
    @BindView(R.id.NO_URUT)
    EditText NO_URUT;
    @BindView(R.id.KD_JNS_OP)
    EditText KD_JNS_OP;

    @BindView(R.id.edit_nop)
    EditText edit_nop;

    public static String s_edit_email, s_edit_telepon, s_thnSPPT = "SPPT", s_KD_PROPINSI, s_KD_DATI2, s_KD_KECAMATAN, s_KD_KELURAHAN, s_KD_BLOK, s_NO_URUT, s_KD_JNS_OP;
    private String get_paramenters;
    private int mLastSpinnerPosition = -1;
    private boolean isDeletePressed;
    private ClipboardManager myClipboard;
    private ClipData myClip;
    private String[] get_paramenters_bayar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_perorangan);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarPerorangActivity.this, v);

        edit_email.requestFocus();
        init_spinner();
        init_edit_nop();

        //edit_email.setText("yodeput@gmail.com");
        //edit_telepon.setText("081976324776");


        siPAD.getInstance().fabric_report("Bayar PBB Perorangan","","");
    }

    @OnClick(R.id.img_back)
    void back() {
        finish();
    }

    @OnClick(R.id.but_cari)
    void cari() {


        String s_email = edit_email.getText().toString();
        String s_telepon = edit_telepon.getText().toString();
        if ((s_email == "") || (s_telepon == "")) {

            getInstance().toastError(getApplicationContext(), "Isi Form dengan Benar");
            edit_email.requestFocus();

        } else if (isEditEmpty(edit_email) || isEditEmpty(edit_telepon)) {

            getInstance().toastError(getApplicationContext(), "Isi Form dengan Benar");
            edit_email.requestFocus();

        } else if (!isValidEmail(s_email)) {

            getInstance().toastError(getApplicationContext(), "Format email salah");
            edit_email.requestFocus();

        } else if (!isValidPhone(s_telepon)) {

            getInstance().toastError(getApplicationContext(), "Format nomor telepon salah");
            edit_telepon.requestFocus();

        } else if (s_thnSPPT.contains("SPPT")) {

            getInstance().toastError(getApplicationContext(), "Anda harus pilih tahun SPPT");
            spinner_thnSPPT.requestFocus();

        } else if (isEditEmpty(edit_nop)) {

            getInstance().toastError(getApplicationContext(), "NOP harus diisi");
            edit_nop.requestFocus();

        } else {



            parameter();



        }

    }

    private void parameter() {
        s_edit_email = edit_email.getText().toString();
        s_edit_telepon = edit_telepon.getText().toString();

        String ss = edit_nop.getText().toString();
        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");
        if ((ss.length()<18)||(ss.length()>18)){

            getInstance().toastError(BayarPerorangActivity.this,"Format NOP salah, cek kembali");
            edit_nop.requestFocus();
        } else {
            s_KD_PROPINSI = ss.substring(0,2);
            s_KD_DATI2 = ss.substring(2,4);
            s_KD_KECAMATAN = ss.substring(4,7);
            s_KD_KELURAHAN = ss.substring(7,10);
            s_KD_BLOK = ss.substring(10,13);
            s_NO_URUT =ss.substring(13,17);
            s_KD_JNS_OP = ss.substring(17);

            get_paramenters_bayar = new String[]{ s_KD_PROPINSI , s_KD_DATI2, s_KD_KECAMATAN
                    ,s_KD_KELURAHAN , s_KD_BLOK, s_NO_URUT, s_KD_JNS_OP
                    ,s_thnSPPT, s_edit_email , s_edit_telepon};

            String[] get_paramenters_bayar2 = new String[]{
                    "sppt[KD_PROPINSI]=" + s_KD_PROPINSI + "&sppt[KD_DATI2]=" + s_KD_DATI2 + "&sppt[KD_KECAMATAN]="
                            + s_KD_KECAMATAN + "&sppt[KD_KELURAHAN]=" + s_KD_KELURAHAN + "&sppt[KD_BLOK]=" + s_KD_BLOK
                            + "&sppt[NO_URUT]=" + s_NO_URUT + "&sppt[KD_JNS_OP]=" + s_KD_JNS_OP + "&sppt[THN_PAJAK_SPPT]="
                            + s_thnSPPT + "&uslog[email]=" + s_edit_email + "&usreg[telepon]="
                            + s_edit_telepon};

            cari_data2(s_edit_email,s_edit_telepon,s_thnSPPT,s_KD_PROPINSI,s_KD_DATI2,
                    s_KD_KECAMATAN,s_KD_KELURAHAN,s_KD_BLOK,s_NO_URUT,s_KD_JNS_OP);
        }
    }

    private void cari_data2(final String email, final String telpon, final String tahun_pajak,
                            final String s_KD_PROPINSI,final String s_KD_DATI2, final String s_KD_KECAMATAN,
                            final String s_KD_KELURAHAN, final String s_KD_BLOK,final String s_NO_URUT, final String s_KD_JNS_OP){

        final String tag_string_req = "req_cek_njop";
        StringRequest postRequest = new StringRequest(Request.Method.POST, URL_PBB_PERORANGAN, new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Response", response);
                        try {
                            final JSONObject jObj = new JSONObject(response);
                            final String status = jObj.getString("status");


                            if (status.equals("true")) {
                                String sanksi = jObj.getString("sanksi");
                                String jumlah = jObj.getString("jumlah");
                                String diskon = jObj.getString("diskon");
                                String stimulus = jObj.getString("stimulus");
                                String data = jObj.getString("data");
                                JSONArray data_array = new JSONArray(data);
                                JSONObject dataObject = data_array.getJSONObject(0);
                                String THN_PAJAK_SPPT = dataObject.getString("THN_PAJAK_SPPT");
                                String NM_WP_SPPT = dataObject.getString("NM_WP_SPPT");
                                String JLN_WP_SPPT = dataObject.getString("JLN_WP_SPPT");
                                String RW_WP_SPPT = dataObject.getString("RW_WP_SPPT");
                                String RT_WP_SPPT = dataObject.getString("RT_WP_SPPT");
                                String KOTA_WP_SPPT = dataObject.getString("KOTA_WP_SPPT");
                                String NJOP_SPPT = dataObject.getString("NJOP_SPPT");
                                String LUAS_BUMI_SPPT = dataObject.getString("LUAS_BUMI_SPPT");
                                String KD_KLS_TANAH = dataObject.getString("KD_KLS_TANAH");
                                String NJOP_BUMI_SPPT = dataObject.getString("NJOP_BUMI_SPPT");
                                String LUAS_BNG_SPPT = dataObject.getString("LUAS_BNG_SPPT");
                                String KD_KLS_BNG = dataObject.getString("KD_KLS_BNG");
                                String NJOP_BNG_SPPT = dataObject.getString("NJOP_BNG_SPPT");
                                String PBB_TERHUTANG_SPPT = dataObject.getString("PBB_TERHUTANG_SPPT");
                                String PBB_YG_HARUS_DIBAYAR_SPPT = dataObject.getString("PBB_YG_HARUS_DIBAYAR_SPPT");
                                String FAKTOR_PENGURANG_SPPT = dataObject.getString("FAKTOR_PENGURANG_SPPT");
                                String FAKTOR_PENGURANG_2 = dataObject.getString("FAKTOR_PENGURANG_2");

                                Calendar calendar = Calendar.getInstance();

                                int year = calendar.get(Calendar.YEAR);
                                /*
                                if (Integer.parseInt(tahun_pajak)==year) {
                                    int s = Integer.parseInt(FAKTOR_PENGURANG_SPPT) + Integer.parseInt(FAKTOR_PENGURANG_2);
                                    double d = ((Integer.parseInt(PBB_TERHUTANG_SPPT) - s) * 0.08);
                                    Log.e(tag_string_req, String.format("%.0f", d));
                                    stimulus = Integer.toString(s);
                                    diskon = String.format("%.0f", d);
                                }
                                */
                                String LOKASI = JLN_WP_SPPT + " RT. " + RT_WP_SPPT + " RW. " + RW_WP_SPPT + "\n" + KOTA_WP_SPPT;

                                Intent i = new Intent(BayarPerorangActivity.this, BayarPerorangHasilActivity.class);
                                i.putExtra("THN_PAJAK_SPPT", THN_PAJAK_SPPT);
                                i.putExtra("NM_WP_SPPT", NM_WP_SPPT);
                                i.putExtra("LOKASI", LOKASI);
                                i.putExtra("NJOP_SPPT", NJOP_SPPT);
                                i.putExtra("LUAS_BUMI_SPPT", LUAS_BUMI_SPPT);
                                i.putExtra("KD_KLS_TANAH", KD_KLS_TANAH);
                                i.putExtra("NJOP_BUMI_SPPT", NJOP_BUMI_SPPT);
                                i.putExtra("LUAS_BNG_SPPT", LUAS_BNG_SPPT);
                                i.putExtra("KD_KLS_BNG", KD_KLS_BNG);
                                i.putExtra("NJOP_BNG_SPPT", NJOP_BNG_SPPT);
                                i.putExtra("PBB_TERHUTANG_SPPT", PBB_TERHUTANG_SPPT);
                                i.putExtra("PBB_YG_HARUS_DIBAYAR_SPPT", PBB_YG_HARUS_DIBAYAR_SPPT);
                                i.putExtra("stimulus",stimulus );
                                i.putExtra("diskon",diskon );
                                i.putExtra("sanksi",sanksi );
                                i.putExtra("jumlah",jumlah );
                                i.putExtra("GET_PARAMETER", get_paramenters_bayar);

                                startActivity(i);


                            } else {

                                String errorMsg = jObj.getString("msg");
                                errorMsg = errorMsg.replace("<br>", "\n");
                                siPAD.getInstance().dialog_alert(BayarPerorangActivity.this, v, errorMsg);

                            }
                        } catch (final JSONException e) {
                            e.printStackTrace();
                            Log.e(tag_string_req, e.getMessage());


                        }

                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("opsi", "Cari");
                params.put("uslog[email]", email);
                params.put("usreg[TELPON]", telpon);
                params.put("sppt[THN_PAJAK_SPPT]", tahun_pajak);
                params.put("sppt[KD_PROPINSI]", s_KD_PROPINSI);
                params.put("sppt[KD_DATI2]", s_KD_DATI2);
                params.put("sppt[KD_KECAMATAN]", s_KD_KECAMATAN);
                params.put("sppt[KD_KELURAHAN]", s_KD_KELURAHAN);
                params.put("sppt[KD_BLOK]", s_KD_BLOK);
                params.put("sppt[NO_URUT]", s_NO_URUT);
                params.put("sppt[KD_JNS_OP]", s_KD_JNS_OP);
                params.put("sppt[STATUS_PEMBAYARAN_SPPT]","0");

                return params;
            }
        };
        siPAD.getInstance().addToRequestQueue(postRequest, tag_string_req);
    }


    @Override
    protected void onPause() {
        super.onPause();
        getInstance().hideDialog();

    }

    @Override
    protected void onResume() {
        super.onResume();
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarPerorangActivity.this, v);

    }


    private void init_spinner() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tahun_sppt);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_thnSPPT.setAdapter(adapter);
        spinner_thnSPPT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih tahun SPPT");
                } else {
                    s_thnSPPT = spinner_thnSPPT.getItemAtPosition(position).toString();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void init_edit_nop_old(){

        getInstance().txtwatcher(KD_PROPINSI, KD_DATI2, 2);
        getInstance().txtwatcher(KD_DATI2, KD_KECAMATAN, 2);
        getInstance().txtwatcher(KD_KECAMATAN, KD_KELURAHAN, 3);
        getInstance().txtwatcher(KD_KELURAHAN, KD_BLOK, 3);
        getInstance().txtwatcher(KD_BLOK, NO_URUT, 3);
        getInstance().txtwatcher(NO_URUT, KD_JNS_OP, 4);

    }


    private void init_edit_nop(){

        PatternedTextWatcher patternedTextWatcher = new PatternedTextWatcher.Builder("##.##.###.###.###-####.#")
                .fillExtraCharactersAutomatically(true)
                .deleteExtraCharactersAutomatically(true)
                .respectPatternLength(true)
                .saveAllInput(false)

                .build();
        edit_nop.addTextChangedListener(patternedTextWatcher);
        edit_nop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count==24)
                    siPAD.hideSoftKeyboard(BayarPerorangActivity.this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edit_nop.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isEditEmpty(edit_nop)){

                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        String ss = clipboard.getText().toString();
                        //Log.e(TAG, ss);
                        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

                        if ((ss.length()<18)||(ss.length()>18)) {
                            getInstance().toastError(BayarPerorangActivity.this,BayarPerorangActivity.this.getString(R.string.error_nop));
                            return false;
                        } else {

                            edit_nop.setText(ss);
                        }

                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

                        String ss = item.getText().toString();
                        //Log.e(TAG, ss);
                        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

                        if ((ss.length()<18)||(ss.length()>18)) {
                            getInstance().toastError(BayarPerorangActivity.this,"Yang Anda salin bukan text NOP");
                            return false;
                        } else {

                            edit_nop.setText(ss);
                        }
                    }

                }

                return false;
            }
        });
        edit_nop.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                menu.close();
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {

            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                menu.close();
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode,
                                               MenuItem item) {
                return false;
            }
        });

    }

}