package id.boyolali.sipad.activity.pembayaran;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import faranjit.currency.edittext.CurrencyEditText;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.bayar.airtanah.RefAirTanah;
import id.boyolali.sipad.util.CurrencyFormat;
import id.boyolali.sipad.util.JenisPajak;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import mehdi.sakout.fancybuttons.FancyButton;

import static id.boyolali.sipad.util.HitungPajak.cek_masa_pajak;
import static id.boyolali.sipad.util.URLConfig.ALLOWED_URI_CHARS;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;

public class BayarAirtanah extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private View v;
    @BindView(R.id.spinner_data_usaha)
    MaterialSpinner spinner_data_usaha;
    @BindView(R.id.l_jenisusaha)
    TextView l_jenisusaha;
    @BindView(R.id.l_usaha)
    TextView l_usaha;
    @BindView(R.id.txt_kelas)
    TextView txt_kelas;

    @BindView(R.id.edit_jml_pemakaian)
    CurrencyEditText edit_jml_pemakaian;
    @BindView(R.id.edit_masapajak)
    EditText edit_masapajak;
    @BindView(R.id.rl_map)
    RelativeLayout rl_map;
    @BindView(R.id.l_alamat)
    TextView l_alamat;

    @BindView(R.id.rl_get_lokasi)
    RelativeLayout rl_get_lokasi;
    @BindView(R.id.l_masapajak)
    TextView l_masapajak;

    @BindView(R.id.but_hitung)
    FancyButton but_hitung;

    private PrefManager session;

    private ArrayList<String> RefAirtanah;
    private ArrayList<RefAirTanah> RefAirtanahList;
    private String idUsaha, namaUsaha, title, jenisUsaha, latitude, longitude, jenis_objek, jenis_pajak, status_denda = "0", s_denda_pajak, s_besaran_pajak, nm_kelas;
    private String kd_kelas, masa_pajak, masa_pajak2, Kd_Rek_1 = "", Kd_Rek_2 = "", Kd_Rek_3 = "", Kd_Rek_4 = "", Kd_Rek_5 = "", address, parameter_bayar;
    private String Id_Zona, Kd_Lokasi, Lokasi, country, Area, lat, lng, administrative_area_level_1, administrative_area_level_2, administrative_area_level_3, administrative_area_level_4, postal_code;


    private LatLng latlong = new LatLng(-7.3870042, 110.3264428);
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 0;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private LayoutParams lp;
    private ArrayList<String> addressFragments;
    public static AppCompatActivity end;
    private int mLastSpinnerPosition = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new PrefManager(getApplicationContext());
        String languageToLoad = "id";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.airtanah_bayar);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarAirtanah.this, v);
        end=this;
        lp = (LayoutParams) l_masapajak.getLayoutParams();
        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        idUsaha = (String) bundle.get("idUsaha");
        namaUsaha = (String) bundle.get("namaUsaha");
        jenisUsaha = (String) bundle.get("jenisUsaha");
        jenis_pajak = (String) bundle.get("jenis_pajak");
        String titlee = (String) bundle.get("title");
        title = titlee.toLowerCase();

        l_usaha.setText(namaUsaha);
        l_jenisusaha.setText(jenisUsaha);

        RefAirtanah = new ArrayList<>();
        RefAirtanahList = new ArrayList<>();
        addressFragments = new ArrayList<String>();

        //getInstance().req_sub_jenis_usaha(getApplicationContext(),v,"airtanah");


        if (getInstance().fileExist(getApplication(), "airtanah")) {
            req_spinner_data();
        } else {
            getInstance().dialog_alert_finish(BayarAirtanah.this, v, getString(R.string.error_koneksi));
        }

        spinner_data_usaha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih kategori usaha");
                } else {
                    int pos = spinner_data_usaha.getSelectedItemPosition();
                    jenis_objek = RefAirtanahList.get(position).getjenis_usaha();
                    kd_kelas = RefAirtanahList.get(position).getkd_kelas();
                    req_kelas(kd_kelas);

                    //getInstance().toastSuccess(getApplicationContext(), jenis_objek);
                    edit_jml_pemakaian.setEnabled(true);
                    edit_jml_pemakaian.requestFocus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        rl_map.setVisibility(View.GONE);


        edit_jml_pemakaian.setEnabled(false);
        edit_masapajak.setEnabled(false);
        but_hitung.setVisibility(View.GONE);
        getInstance().edit_enable(edit_jml_pemakaian, edit_masapajak, 5);

        siPAD.getInstance().fabric_report("Bayar Air Tanah","","");

    }

    @OnClick(R.id.but_edit_lokasi)
    void edit_lokasi() {
        popup_placepicker();
    }

    @OnClick(R.id.rl_get_lokasi)
    void lokasi() {
        popup_placepicker();
    }

    @OnClick(R.id.edit_masapajak)
    void date_picker1() {
        datePicker_dialog();
    }

    private void datePicker_dialog() {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(this, new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                String month = "";
                String month2 = "";
                if (String.valueOf(selectedMonth + 1).length() < 2) {
                    month = "0" + String.valueOf(selectedMonth + 1);
                } else {
                    month = String.valueOf(selectedMonth + 1);
                }
                masa_pajak = String.valueOf(selectedYear) + " - " + month;
                edit_masapajak.setText(masa_pajak);
                but_hitung.setVisibility(View.VISIBLE);
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setTitle("Pilih Masa Pajak")
                .setMinYear(1990)
                .setMaxYear(2030)
                .build()
                .show();
    }

    private void popup_placepicker() {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(BayarAirtanah.this), PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void req_spinner_data() {

        String jsonString = getInstance().get_file_data(getApplicationContext(), "airtanah");

        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                RefAirTanah ref = new RefAirTanah();
                JSONObject jsnobject = jsonArray.getJSONObject(i);
                ref.setid_usaha(jsnobject.getString("id_usaha"));
                ref.setjenis_usaha(jsnobject.getString("jenis_usaha"));
                ref.setkd_kelas(jsnobject.getString("kd_kelas"));

                RefAirtanahList.add(ref);
                RefAirtanah.add(jsnobject.getString("jenis_usaha"));

            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, RefAirtanah);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_data_usaha.setAdapter(adapter);

        } catch (JSONException e) {


            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void req_kelas(String kd_kelas) {

        final String tag_string_req = "request kelas";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_AIRTANAH_KELAS + kd_kelas, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);

                try {
                    final JSONObject jObj = new JSONObject(response);
                    String a = jObj.getString("data");
                    JSONArray data_array = new JSONArray(a);
                    JSONObject jsnobject = data_array.getJSONObject(0);
                    txt_kelas.setText(jsnobject.getString("nm_kelas"));
                    nm_kelas = jsnobject.getString("nm_kelas");

                } catch (final JSONException e) {


                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                siPAD.getInstance().dialog_alert_finish(BayarAirtanah.this, v, BayarAirtanah.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ;

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
                latlong = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                l_alamat.setText(address);
                //l_alamat.setSelected(true);


        /*
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
          Bitmap bitmap;
          @Override
          public void onSnapshotReady(Bitmap snapshot) {
            bitmap = snapshot;
            try {
              File newfile = new File(Environment.getExternalStorageDirectory()+"/siPAD/");
              newfile.mkdir();
              String mPath = newfile.toString() +"/"+"Currentlocation.jpeg";

              FileOutputStream out = new FileOutputStream(mPath);

              File imageFile = new File(mPath);

              FileOutputStream outputStream = new FileOutputStream(imageFile);
              int quality = 100;
              bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
              outputStream.flush();
              outputStream.close();
            }catch (Exception e){
              e.printStackTrace();
            }

          }
        };
        mMap.snapshot(callback);
        */
                lp.addRule(RelativeLayout.BELOW, R.id.cv_map);
                rl_map.setVisibility(View.VISIBLE);
                rl_get_lokasi.setVisibility(View.GONE);
                mMap.addMarker(new MarkerOptions().position(latlong));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 18));

                getAddress();

            }
        }
    }

    private void getAddress() {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
            if (addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {

                    //Log.e("Addresssss", addresses.get(0).getLocality());

                }
                //String alamat = addresses.get(0).getAddressLine(0);
                //String[] separated = alamat.split(", ");


                //Log.e("Addresssss", separated[1]);
                String aal3 = addresses.get(0).getLocality();
                if (aal3.contains("Boyolali")) {
                    aal3 = "Kecamatan " + aal3;
                    get_address_data(aal3);
                } else {
                    get_address_data(aal3);
                }


            } else {

                //Log.e("Addresssss", "alamat tidak ditemukan");

            }

        } catch (IOException ioException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.addMarker(new MarkerOptions().position(latlong));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15));

    }


    private void get_address_data(String area) {

        final String tag_string_req = "address from server";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_ALAMAT_ZONA + area, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);

                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    String row = jObj.getString("row");
                    if (row.contains("0")) {

                        //Log.e(tag_string_req,"error");
                        getInstance().toastError(getApplicationContext(), "Lokasi Tidak Tersedia");
                        popup_placepicker();

                    } else {

                        JSONArray data_array = new JSONArray(data);
                        JSONObject jsnobject = data_array.getJSONObject(0);
                        Id_Zona = jsnobject.getString("Id_Zona");
                        Lokasi = jsnobject.getString("Lokasi");
                        Kd_Lokasi = jsnobject.getString("Kd_Lokasi");
                        lat = jsnobject.getString("lat");
                        lng = jsnobject.getString("lng");
                        country = jsnobject.getString("country");
                        administrative_area_level_1 = jsnobject.getString("administrative_area_level_1");
                        administrative_area_level_2 = jsnobject.getString("administrative_area_level_2");
                        administrative_area_level_3 = jsnobject.getString("administrative_area_level_3");
                        administrative_area_level_4 = jsnobject.getString("administrative_area_level_4");
                        postal_code = jsnobject.getString("postal_code");

                        String administrative_area_level_3 = jsnobject.getString("administrative_area_level_3");
                        edit_masapajak.setEnabled(true);
                        //Log.e(tag_string_req,Lokasi);

                    }

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(BayarAirtanah.this, v, BayarAirtanah.this.getString(R.string.error_koneksi));
                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    @OnClick(R.id.but_hitung)
    void hitung() {

        masa_pajak = edit_masapajak.getText().toString();

        if ((isEditEmpty(edit_jml_pemakaian)) || (isEditEmpty(edit_masapajak))) {

            getInstance().toastError(getApplicationContext(), "Lengkapi data yang dibutuhkan");

        } else {

            masa_pajak = masa_pajak.replace(" ", "");
            masa_pajak2 = masa_pajak.replace(" ", "");
            masa_pajak = masa_pajak.replace("-", "");
            String cek_masa  = cek_masa_pajak(Integer.parseInt(masa_pajak));

            if (cek_masa.equals("false")) {

                getInstance().dialog_alert(this, v, "Masa Pajak " + masa_pajak2 + " Belum wajib lapor");

            } else {

                String volume = edit_jml_pemakaian.getText().toString();
                volume = volume.replace(".", "");
                String param_hitung = Kd_Lokasi + "/" + nm_kelas + "/" + volume;
                String encoded = Uri.encode(param_hitung, ALLOWED_URI_CHARS);
                hitung_airtanah(encoded);


            }

        }

    }


    private void hitung_airtanah(String param) {

        final String tag_string_req = "hitung air tanah";
        //Log.e(tag_string_req,param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_AIRTANAH_HITUNG + param, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);

                String pajak = response;
                s_besaran_pajak = pajak.replace("\"", "");
                String param_denda = s_besaran_pajak + "/" + masa_pajak2 + "-01/" + jenis_pajak;
                hitung_denda(param_denda);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void hitung_denda(String param) {

        final String tag_string_req = "hitung denda";
        //Log.e(tag_string_req,param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_HITUNG_DENDA + param, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);

                try {
                    final JSONObject jObj = new JSONObject(response);
                    s_denda_pajak = jObj.getString("denda");
                    dialog_result(namaUsaha, jenisUsaha, jenis_objek, s_besaran_pajak, s_denda_pajak, edit_masapajak.getText().toString(), jenis_pajak);

                } catch (final JSONException e) {


                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void param_bayar() {
        String params = "";
        String Omzet = edit_jml_pemakaian.getText().toString();
        Omzet = Omzet.replace(".", "");
        masa_pajak = edit_masapajak.getText().toString();
        masa_pajak = masa_pajak.replace(" ", "");

        String ST_TEMPO = "ST_TEMPO[STATUS]=" + status_denda;

        String URAIAN = "&data[0][URAIAN]=" + jenis_objek + " " + masa_pajak;
        String VOLUME = "&data[0][VOLUME]=" + Omzet;
        String HARGA = "&data[0][HARGA]=" + Omzet;
        String TARIF1 = "&data[0][TARIF]=10";
        String TOTAL_HARGA = "&data[0][TOTAL_HARGA]=" + Omzet;
        String TOTAL_PAJAK = "&data[0][TOTAL_PAJAK]=" + s_besaran_pajak;
        String PAJAK = "&data[0][PAJAK]=" + s_besaran_pajak;
        String KD_Rek_1 = "&data[0][KD_Rek_1]=" + Kd_Rek_1;
        String KD_Rek_2 = "&data[0][KD_Rek_2]=" + Kd_Rek_2;
        String KD_Rek_3 = "&data[0][KD_Rek_3]=" + Kd_Rek_3;
        String KD_Rek_4 = "&data[0][KD_Rek_4]=" + Kd_Rek_4;
        String KD_Rek_5 = "&data[0][KD_Rek_4]=" + Kd_Rek_5;

        String URAIAN_TARIF = "&nota[URAIAN_TARIF]=Pajak Periode " + masa_pajak;
        String TGL_NOTA = "&nota[TGL_NOTA]=" + masa_pajak + "-01";
        String USAHA_ID = "&nota[USAHA_ID]=" + idUsaha;
        String JENIS_PAJAK = "&nota[JENIS_PAJAK]=" + jenis_pajak;
        String JUMLAH_HARGA = "&nota[JUMLAH_HARGA]=" + Omzet;
        String JUMLAH_PAJAK = "&nota[JUMLAH_PAJAK]=" + s_besaran_pajak;
        String TARIF2 = "&nota[TARIF]=10";
        String LOKASI = "&nota[LOKASI]=" + address;
        String LAT = "&nota[LAT]=" + latitude;
        String LNG = "&nota[LNG]=" + longitude;
        String JENIS_OBJEK = "&nota[JENIS_OBJEK]=" + jenis_objek;
        String COUNTRY = "&nota[COUNTRY]=" + country;
        String ADMINISTRATIVE_AREA_LEVEL_1 = "&nota[ADMINISTRATIVE_AREA_LEVEL_1]=" + administrative_area_level_1;
        String ADMINISTRATIVE_AREA_LEVEL_2 = "&nota[ADMINISTRATIVE_AREA_LEVEL_2]=" + administrative_area_level_2;
        String ADMINISTRATIVE_AREA_LEVEL_3 = "&nota[ADMINISTRATIVE_AREA_LEVEL_3]=" + administrative_area_level_3;
        String ADMINISTRATIVE_AREA_LEVEL_4 = "&nota[ADMINISTRATIVE_AREA_LEVEL_4]=" + administrative_area_level_4;
        String POSTAL_CODE = "&nota[POSTAL_CODE]=" + postal_code;
        String JENIS_PERMOHONAN = "&nota[JENIS_PERMOHONAN]=" + "0";

        parameter_bayar = ST_TEMPO + URAIAN + VOLUME + HARGA + TARIF1 + TOTAL_HARGA + TOTAL_PAJAK + PAJAK + KD_Rek_1 + KD_Rek_2 + KD_Rek_3 + KD_Rek_4 + KD_Rek_5
                + URAIAN_TARIF + TGL_NOTA + USAHA_ID + JENIS_PAJAK + JUMLAH_HARGA + JUMLAH_PAJAK + TARIF2 + LOKASI + LAT + LNG + JENIS_OBJEK + COUNTRY
                + ADMINISTRATIVE_AREA_LEVEL_1 + ADMINISTRATIVE_AREA_LEVEL_2 + ADMINISTRATIVE_AREA_LEVEL_3 + ADMINISTRATIVE_AREA_LEVEL_4
                + POSTAL_CODE + JENIS_PERMOHONAN;

    }


    public void dialog_result(final String namaUsaha, String jenisUsaha, String jenis_objek, String s_besaran_pajak, String s_denda_pajak, final String masa_pajak, String jenis_pajak) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(this).inflate(R.layout.activity_bayar_hasil_hitung,
                (ViewGroup) v.findViewById(android.R.id.content), false);

        final TextView txt_jns_pajak = viewInflated.findViewById(R.id.txt_jns_pajak);
        final TextView txt_nm_usaha = viewInflated.findViewById(R.id.txt_nm_usaha);
        final TextView txt_jns_usaha = viewInflated.findViewById(R.id.txt_jns_usaha);
        final TextView txt_kategori = viewInflated.findViewById(R.id.txt_kategori);
        final TextView txt_masapajak = viewInflated.findViewById(R.id.txt_masapajak);
        final TextView txt_besaran_pajak = viewInflated.findViewById(R.id.txt_besaran_pajak);
        final TextView txt_denda_pajak = viewInflated.findViewById(R.id.txt_denda_pajak);
        final TextView txt_terbilang = viewInflated.findViewById(R.id.txt_terbilang);
        final ImageView img_share = viewInflated.findViewById(R.id.img_share);
        final Button dialog_button_ok = viewInflated.findViewById(R.id.dialog_button_ok);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        final LinearLayout ll_2_button = viewInflated.findViewById(R.id.ll_2_button);

        dialog_button_ok.setText("OK");
        dialog_left.setText("Kembali");
        dialog_right.setText("Bayar");

        txt_jns_pajak.setText("Pajak " + JenisPajak.cek_pajak(jenis_pajak));
        txt_nm_usaha.setText(namaUsaha);
        txt_jns_usaha.setText(jenisUsaha);
        txt_kategori.setText(jenis_objek);
        txt_masapajak.setText(masa_pajak);
        txt_besaran_pajak.setText(CurrencyFormat.rupiah_format_string(s_besaran_pajak));
        int denda = Integer.parseInt(s_denda_pajak);
        if (denda > 0) {

            s_denda_pajak = CurrencyFormat.rupiah_format_string(s_denda_pajak);
            status_denda = "1";
        }

        txt_denda_pajak.setText(s_denda_pajak);
        txt_terbilang.setText("(" + CurrencyFormat.info_terbilang(Integer.parseInt(s_besaran_pajak)) + " rupiah)");


        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });

        if (session.isLoggedIn()) {
            dialog_button_ok.setVisibility(View.GONE);
            ll_2_button.setVisibility(View.VISIBLE);

        } else {
            dialog_button_ok.setVisibility(View.VISIBLE);
            ll_2_button.setVisibility(View.GONE);

        }


        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInstance().showDialog();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        getInstance().hideDialog();
                        show_dialog_persetujuan_bayar();


                    }
                }, 1000);

            }
        });
        dialog_button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_button.setVisibility(View.GONE);
                img_share.setVisibility(View.GONE);
                dialog_button_ok.setVisibility(View.GONE);
                getInstance().shareImage(getInstance().takeSS(viewInflated, namaUsaha + "_" + masa_pajak));
                dialog_button_ok.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        });


    }


    private void show_dialog_persetujuan_bayar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_persetujuan_bayar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.bt_accept)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                param_bayar();
                Intent i = new Intent(BayarAirtanah.this, BayarHasil.class);
                i.putExtra("parameter", parameter_bayar);
                i.putExtra("jenis_pajak", jenis_pajak);
                startActivity(i);
                dialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Button Accept Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((Button) dialog.findViewById(R.id.bt_decline)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "Button Decline Clicked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getInstance().init_p_dialog(BayarAirtanah.this, v);
    }
}
