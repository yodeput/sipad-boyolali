package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.shashank.sony.fancytoastlib.FancyToast;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.kalkulator.KalkulatorAirtanah;
import id.boyolali.sipad.activity.kalkulator.KalkulatorHiburan;
import id.boyolali.sipad.activity.kalkulator.KalkulatorMinerba;
import id.boyolali.sipad.activity.kalkulator.KalkulatorPenerangan;
import id.boyolali.sipad.activity.kalkulator.KalkulatorReklame;
import id.boyolali.sipad.activity.user.DatausahaActivity;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.MainActivity.viewPager;

public class MenuPopup extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = MenuPopup.class.getSimpleName();
    @BindView(R.id.but_pembayaran)
    ImageButton but_pembayaran;
    @BindView(R.id.but_kalkulator)
    ImageButton but_kalkulator;
    @BindView(R.id.but_close)
    ImageButton but_close;
    @BindView(R.id.icon_title)
    ImageView icon_title;
    @BindView(R.id.text_title)
    TextView text_title;
    @BindView(R.id.ln_background)
    LinearLayout ln_background;
    @BindView(R.id.div_menu_pajak)
    ImageView div_menu_pajak;
    private Class newAct;
    private String title, jenis_pajak;
    private PrefManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_pajak);
        ButterKnife.bind(this);
        session = new PrefManager(getApplicationContext());
        initView();

        but_pembayaran.setOnClickListener(this);
        but_kalkulator.setOnClickListener(this);
        but_close.setOnClickListener(this);
        but_close.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimaryDark));


        /*
        TextView menuName  = findViewById(R.id.textMenu);
        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        String j =(String) b.get("title");
        menuName.setText(j);

        if ((j.contains("RESTORAN"))||(j.contains("HOTEL"))||(j.contains("HIBURAN"))){
            menuName.setBackgroundColor(Color.parseColor("#a5d6a7"));
        } else if ((j.contains("AIR"))||(j.contains("PENERANGAN"))||(j.contains("PARKIR"))) {
            menuName.setBackgroundColor(Color.parseColor("#80deea"));
        } else {
            menuName.setBackgroundColor(Color.parseColor("#fff59d"));
        }*/
        String taptargetmenupopup = session.getPref("taptargetmenupopup");
        if (taptargetmenupopup.isEmpty()) {
            taptarget();
        }


    }

    private void taptarget() {
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.but_pembayaran),
                        "Pembayaran",
                        "Untuk melakukan pembayaran anda membutuhkan akun yang sudah aktif\n\n*)Menu PBB tidak membutuhkan akun.")
                        .titleTextColor(R.color.white)
                        .textColor(R.color.white)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.white)
                        .outerCircleColor(R.color.red_700)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        session.addPref("taptargetmenupopup", "false");
                        taptarget2();
                    }
                });
    }

    private void taptarget2() {
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.but_kalkulator),
                        "Kalkulator",
                        "Untuk mengetahui besaran pajak silahkan coba menu Kalkulator")
                        .titleTextColor(R.color.white)
                        .textColor(R.color.white)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.white)
                        .outerCircleColor(R.color.red_700)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                    }
                });
    }


    private void initView() {

        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        title = (String) bundle.get("title");

        switch (title) {
            case "PAJAK AIR TANAH": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.icon_airtanah));
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.airtanah));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.airtanah));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.airtanah));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.airtanah));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_airtanah));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_airtanah));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_airtanah);
                jenis_pajak = "2";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK BPHTB": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_bphtb);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.bphtb));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.bphtb));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.bphtb));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.bphtb));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_bphtb));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_bphtb));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_bphtb);
                jenis_pajak = "12";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK HIBURAN": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_hiburan);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.hiburan));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.hiburan));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.hiburan));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.hiburan));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_hiburan));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_hiburan));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_hiburan);
                jenis_pajak = "10";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK HOTEL": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_hotel);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.hotel));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.hotel));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.hotel));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.hotel));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_hotel);
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_hotel));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_hotel));
                jenis_pajak = "7";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK MINERBA": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_minerba);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.minerba));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.minerba));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.minerba));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.minerba));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_minerba));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_minerba));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_minerba);
                jenis_pajak = "3";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK PARKIR": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_parkir);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.parkir));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.parkir));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.parkir));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.parkir));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_parkir));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_parkir));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_parkir);
                jenis_pajak = "8";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK PBB": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_pbb);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.pbb));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.pbb));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.pbb));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.pbb));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_pbb));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_pbb));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_pbb);
                jenis_pajak = "9";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK PENERANGAN": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_penerangan);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.penerangan));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.penerangan));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.penerangan));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.penerangan));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_penerangan));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_penerangan));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_penerangan);
                jenis_pajak = "5";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK RESTORAN": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_restoran);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.restoran));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.restoran));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.restoran));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.restoran));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_restoran));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_restoran));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_restoran);
                jenis_pajak = "4";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK BURUNG WALET": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_walet);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.walet));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.walet));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.walet));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.walet));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_walet));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_walet));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_walet);
                jenis_pajak = "6";
                newAct = DatausahaActivity.class;
                return;
            }
            case "PAJAK REKLAME": {
                text_title.setText(title);
                siPAD.getInstance().fabric_report(TAG, title, title);
                icon_title.setImageResource(R.drawable.icon_reklame);
                icon_title.setColorFilter(ContextCompat.getColor(this, R.color.reklame));
                div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.reklame));
                but_pembayaran.setColorFilter(ContextCompat.getColor(this, R.color.reklame));
                but_kalkulator.setColorFilter(ContextCompat.getColor(this, R.color.reklame));
                but_pembayaran.setBackground(ContextCompat.getDrawable(this, R.drawable.button_reklame));
                but_kalkulator.setBackground(ContextCompat.getDrawable(this, R.drawable.button_reklame));
                ln_background.setBackgroundResource(R.drawable.rounded_corner_reklame);
                jenis_pajak = "1";
                newAct = DatausahaActivity.class;
                return;
            }


        }


    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but_pembayaran: {
                Intent i = new Intent(this, newAct);
                i.putExtra("title", "Bayar " + title);
                i.putExtra("jenis_pajak", jenis_pajak);
                if (session.isLoggedIn()) {

                    if (session.isActive()){
                        startActivity(i);
                    } else {

                        siPAD.getInstance().dialog_alert(this,v,"Akun Anda belum Aktif\nSilahkan tunggu verivfikasi data");
                    }

                } else {

                    FancyToast.makeText(this, "Anda harus Login untuk melakukan pembayaran", FancyToast.LENGTH_LONG, FancyToast.WARNING, false).show();


                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            finish();
                            viewPager.setCurrentItem(4);


                        }
                    }, 1000);


                }

                return;
            }
            case R.id.but_kalkulator: {


                if (jenis_pajak.contains("8") || jenis_pajak.contains("5") || jenis_pajak.contains("7") || jenis_pajak.contains("4")) {

                    String[] parts = title.split(" ");
                    String lastWord = parts[parts.length - 1];
                    Intent i = new Intent(this, KalkulatorPenerangan.class);
                    i.putExtra("jenisPajak", lastWord);
                    i.putExtra("title", lastWord);
                    i.putExtra("jenis_pajak", jenis_pajak);
                    startActivity(i);

                } else if (jenis_pajak.contains("2")) {

                    String[] parts = title.split(" ");
                    String lastWord = parts[parts.length - 1];
                    Intent i = new Intent(this, KalkulatorAirtanah.class);
                    i.putExtra("jenisPajak", lastWord);
                    i.putExtra("title", lastWord);
                    i.putExtra("jenis_pajak", jenis_pajak);
                    startActivity(i);


                } else if (jenis_pajak.contains("3")) {

                    String[] parts = title.split(" ");
                    String lastWord = parts[parts.length - 1];
                    Intent i = new Intent(this, KalkulatorMinerba.class);
                    i.putExtra("jenisPajak", lastWord);
                    i.putExtra("title", lastWord);
                    i.putExtra("jenis_pajak", jenis_pajak);
                    startActivity(i);

                } else if (jenis_pajak.contains("10")) {

                    String[] parts = title.split(" ");
                    String lastWord = parts[parts.length - 1];
                    Intent i = new Intent(this, KalkulatorHiburan.class);
                    i.putExtra("jenisPajak", lastWord);
                    i.putExtra("title", lastWord);
                    i.putExtra("jenis_pajak", jenis_pajak);
                    startActivity(i);

                } else if (jenis_pajak.contains("1")) {

                    String[] parts = title.split(" ");
                    String lastWord = parts[parts.length - 1];
                    Intent i = new Intent(this, KalkulatorReklame.class);
                    i.putExtra("jenisPajak", lastWord);
                    i.putExtra("title", lastWord);
                    i.putExtra("jenis_pajak", jenis_pajak);
                    startActivity(i);

                } else {

                    siPAD.getInstance().toastError(MenuPopup.this, "On progress");
                }


            }
            case R.id.but_close: {
                finish();
                return;
            }


        }
    }

}
