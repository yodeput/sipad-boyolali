package id.boyolali.sipad.activity.pbb;

import android.os.Bundle;
import android.os.Handler;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.pbb.TAGIHAN;
import id.boyolali.sipad.adapter.pbb.TAGIHANAdapter;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.CurrencyFormat.info_terbilang;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;
import static id.boyolali.sipad.util.siPAD.getInstance;


public class CekTagihanPBBHasilActivity extends AppCompatActivity implements TAGIHANAdapter.TAGIHANsAdapterListener {

    private static final String TAG = CekTagihanPBBHasilActivity.class.getSimpleName();
    private PrefManager session;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.cv_recycler)
    CardView cv_recycler;

    @BindView(R.id.txt_nama_wp)
    TextView txt_nama_wp;
    @BindView(R.id.txt_alamat)
    TextView txt_alamat;
    @BindView(R.id.txt_jumlah)
    TextView txt_jumlah;
    @BindView(R.id.txt_terbilang)
    TextView txt_terbilang;

    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_share)
    ImageView img_share;

    @BindView(R.id.main_content)CoordinatorLayout main_content;

    private View v;
    private List<TAGIHAN> TAGIHANList;
    private TAGIHANAdapter mAdapter;
    private AlertDialog dialog;
    private String NM_WP_SPPT,total;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.img_back)
    void back() {
        finish();
    }

    @OnClick(R.id.img_share)
    void share() {
        img_back.setVisibility(View.GONE);
        img_share.setVisibility(View.GONE);
        getInstance().showDialog();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getInstance().hideDialog();
                getInstance().shareImage(getInstance().takeSS(main_content, NM_WP_SPPT+"_"+total));
                img_back.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        }, 1000);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_cek_tagihan_hasil);
        ButterKnife.bind(this);

        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(this, v);
        TAGIHANList = new ArrayList<>();
        mAdapter = new TAGIHANAdapter(this, TAGIHANList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        read_json();

    }

    private void read_json() {
        final String tag_string_req = "read_hasil_cek_tagihan";
        String jsonString = siPAD.getInstance().get_file_data(this, "cekTagihan");

        try {
            final JSONObject jObj = new JSONObject(jsonString);
            String data = jObj.getString("data");
            total = jObj.getString("total");
            JSONArray data_array = new JSONArray(data);
            JSONObject dataObject = data_array.getJSONObject(0);
            NM_WP_SPPT = dataObject.getString("NM_WP_SPPT");
            String a = dataObject.getString("JLN_WP_SPPT");
            String b = dataObject.getString("BLOK_KAV_NO_WP_SPPT");
            String c = dataObject.getString("RT_WP_SPPT");
            String d = dataObject.getString("RW_WP_SPPT");
            String e = dataObject.getString("KELURAHAN_WP_SPPT");

            if (a.contains("null")){a="";}else if (b.contains("null")){b="";}
            else if (c.contains("null")){c="-";}else if (d.contains("null")){d="-";}
            else if (e.contains("null")){e="-";}

            String alamat = a+""+b+" RT. "+c+" RW. "+d+" KEL. "+e;
            txt_nama_wp.setText(NM_WP_SPPT);
            txt_alamat.setText(alamat);
            txt_jumlah.setText(rupiah_format_string(total));
            txt_terbilang.setText(info_terbilang(Integer.parseInt(total)) + " rupiah");
            List<TAGIHAN> items = new Gson().fromJson(data_array.toString(), new TypeToken<List<TAGIHAN>>() {
            }.getType());


            // adding contacts to contacts list
            TAGIHANList.clear();
            TAGIHANList.addAll(items);
        } catch (final JSONException e) {

            getInstance().hideDialog();
            e.printStackTrace();
            //Log.e(tag_string_req, e.getMessage());

        }
    }

    @Override
    public void onTAGIHANSelected(TAGIHAN TAGIHAN) {
        //Toast.makeText(this.getApplicationContext(), "Informasi: " + OPP.getOPP(), Toast.LENGTH_LONG).show();
    }

    public void clear() {
        TAGIHANList.clear();
        mAdapter.notifyDataSetChanged();
    }

}