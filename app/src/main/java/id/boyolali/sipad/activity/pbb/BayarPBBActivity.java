package id.boyolali.sipad.activity.pbb;

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;

import static id.boyolali.sipad.util.siPAD.getInstance;

public class BayarPBBActivity extends AppCompatActivity {

  private static final String TAG = BayarPBBActivity.class.getSimpleName();
  private View v;

 @BindView(R.id.but_perorangan)
 ImageButton but_perorangan;

  @BindView(R.id.but_kolektif)
  ImageButton but_kolektif;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_pbb_menu_bayar);
    ButterKnife.bind(this);
    v = getWindow().getDecorView().findViewById(android.R.id.content);
    getInstance().init_p_dialog(BayarPBBActivity.this,v);

    but_kolektif.setColorFilter(ContextCompat.getColor(this,R.color.hijau));
    but_perorangan.setColorFilter(ContextCompat.getColor(this,R.color.blue_700));

  }
  @OnClick(R.id.but_perorangan) void bayar_perorang(){

    startActivity(new Intent(this,BayarPerorangActivity.class));
  }
  @OnClick(R.id.but_kolektif) void bayar_kolektif(){
    startActivity(new Intent(this,BayarKolektifActivity.class));
  }



  @OnClick(R.id.but_close) void but_close(){

    finish();

  }
}
