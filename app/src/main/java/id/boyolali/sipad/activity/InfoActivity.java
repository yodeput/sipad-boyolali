package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;

public class InfoActivity extends AppCompatActivity implements View.OnClickListener {

    private String info;

    @BindView(R.id.img_back)
    ImageView btn_back;
    @BindView(R.id.txt_info)
    TextView txt_info;
    @BindView(R.id.txt_title_info)
    TextView txt_title_info;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);

        btn_back.setOnClickListener(this);

        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        info = (String) bundle.get("info");

        show_info(info);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {

                finish();

            }
        }
    }

    private void show_info(String info){

        switch (info) {
            case "Pajak Air Tanah": {
                txt_title_info.setText("Info Pajak Air Tanah");
                txt_info.setText(Html.fromHtml(getString(R.string.info_airtanah)));
                return;
            }
            case "BPHTB": {
                txt_title_info.setText("Info Pajak BPHTB");
                txt_info.setText(Html.fromHtml(getString(R.string.info_bph)));

                return;
            }
            case "Pajak Hiburan": {
                txt_title_info.setText("Info Pajak Hiburan");
                txt_info.setText(Html.fromHtml(getString(R.string.info_hiburan)));
                return;
            }
            case "Pajak Hotel": {
                txt_title_info.setText("Info Pajak Hotel");
                txt_info.setText(Html.fromHtml(getString(R.string.info_hotel)));
                return;
            }
            case "Pajak Minerba": {
                txt_title_info.setText("Info Pajak Minerba");
                txt_info.setText(Html.fromHtml(getString(R.string.info_minerba)));
                return;
            }
            case "Pajak Parkir": {
                txt_title_info.setText("Info Pajak Parkir");
                txt_info.setText(Html.fromHtml(getString(R.string.info_parkir)));
                return;
            }
            case "Pajak Bumi dan Bangunan": {
                txt_title_info.setText("Info Pajak Bumi dan Bangunan");
                txt_info.setText(Html.fromHtml(getString(R.string.info_pbb)));
                return;
            }
            case "Pajak Penerangan": {
                txt_title_info.setText("Info Pajak Penerangan");
            txt_info.setText(Html.fromHtml(getString(R.string.info_penerangan)));

                return;
            }
            case "Pajak Restoran": {
                txt_title_info.setText("Info Pajak Restoran");
                txt_info.setText(Html.fromHtml(getString(R.string.info_restoran)));
                return;
            }
            case "Pajak Burung Walet": {
                txt_title_info.setText("Info Burung Walet");
                txt_info.setText(Html.fromHtml(getString(R.string.info_walet)));
                return;
            }
            case "Pajak Reklame": {
                txt_title_info.setText("Info Pajak Reklame");
                txt_info.setText(Html.fromHtml(getString(R.string.info_reklame)));
                return;
            }


        }

    }
}
