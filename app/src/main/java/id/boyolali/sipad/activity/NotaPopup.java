package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import static id.boyolali.sipad.util.siPAD.getInstance;

import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.user.nota.Nota;
import id.boyolali.sipad.adapter.user.nota.NotaAdapter;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

public class NotaPopup extends AppCompatActivity implements NotaAdapter.NotasAdapterListener{
    private static final String TAG = NotaPopup.class.getSimpleName();

    private String nota_id, act,sharePath ;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.but_share)
    ImageButton but_share;

    @BindView(R.id.ln_screenshoot)
    LinearLayout ln_screenshoot;

    private List<Nota> notaList;
    private NotaAdapter mAdapter;
    private ImageView iv_pending, iv_bbayar;
    private View v;

    @OnClick(R.id.but_share) void share(){
      getInstance().showDialog();
      but_share.setVisibility(View.GONE);

      Handler handler = new Handler();
      handler.postDelayed(new Runnable() {
        public void run() {
          getInstance().hideDialog();
          getInstance().shareImage( getInstance().takeSS(ln_screenshoot,nota_id));
        }
      }, 1000);


    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nota);
        ButterKnife.bind(this);

        v = getWindow().getDecorView().findViewById(android.R.id.content);

        getInstance().init_p_dialog(this,v);
        iv_pending = findViewById(R.id.iv_pending);
        iv_bbayar = findViewById(R.id.iv_bbayar);

        nota_id = getIntent().getStringExtra("nota_id");
        act  = getIntent().getStringExtra("act");
        if (act.contains("kb")){

          iv_bbayar.setVisibility(View.VISIBLE);

        } else if (act.contains("opp")){

          iv_pending.setVisibility(View.VISIBLE);

        }


        notaList = new ArrayList<>();
        mAdapter = new NotaAdapter(this, notaList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        fetchNota();

    }

    private void fetchNota() {
        getInstance().showDialog();
        JsonArrayRequest request = new JsonArrayRequest(URLConfig.URL_USER_NOTA+nota_id,
            new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                  //Log.d(TAG,response.toString());
                    if (response == null) {
                        //Toast.makeText(getActivity().getApplicationContext(), "Couldn't fetch the contacts! Pleas try again.", Toast.LENGTH_LONG).show();
                        return;

                    }


                    List<Nota> items = new Gson().fromJson(response.toString(), new TypeToken<List<Nota>>() {
                    }.getType());

                    // adding contacts to contacts list
                    notaList.clear();
                    notaList.addAll(items);
                    getInstance().hideDialog();
                    //recyclerView.setVisibility(View.VISIBLE);
                    // refreshing recycler view


                    mAdapter.notifyDataSetChanged();
                }
            }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(NotaPopup.this, v, NotaPopup.this.getString(R.string.error_koneksi));

                //getInstance().fabric_report(TAG,"Fetch Data",error.getMessage());
                //Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

        getInstance().addToRequestQueue(request);
    }

    @Override
    public void onNotaSelected(Nota nota) {
        //Toast.makeText(getActivity().getApplicationContext(), "Informasi: " + berita.getTitle(), Toast.LENGTH_LONG).show();
    }

    public void clear() {
        notaList.clear();
        mAdapter.notifyDataSetChanged();
    }

    public void addAll(List<Nota> list) {
        notaList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

}
