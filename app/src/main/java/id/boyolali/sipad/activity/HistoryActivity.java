package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import id.boyolali.sipad.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.util.PrefManager;


public class HistoryActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = HistoryActivity.class.getSimpleName();
    private PrefManager session;
    private View v;
    private AlertDialog dialog;

    private SharedPreferences dataPref;
    private SharedPreferences.Editor editor;
    int pgNumber;

    @BindView(R.id.ln_tab_vp)
    LinearLayout ln_tab_vp;
    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    String dataLUNAS_len,dataOPP_len,dataKB_len;

    @OnClick(R.id.img_back) void close(){

        finish();

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);

        session = new PrefManager(this.getApplicationContext());
        //sessionCheck();

    }

    /*

    private void setupViewPager(ViewPager viewPager) {
        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new LunasActivity(), "Lunas ["+dataLUNAS_len+"]");
        adapter.addFragment(new KBActivity(), "Belum Bayar ["+dataOPP_len+"]");
        adapter.addFragment(new OPPActivity(), "Pending ["+dataKB_len+"]");
        viewPager.setAdapter(adapter);


        viewPager.setOnPageChangeListener(new OnPageChangeListener() {
            public void onPageSelected(int pageNumber) {
                // Just define a callback method in your fragment and call it like this!
                adapter.getItem(pageNumber);

                pgNumber = pageNumber;
            Log.d(TAG,""+pgNumber);
            }

            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

            }

            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {

            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }


    }

    private void sessionCheck(){


        session = new PrefManager(getApplicationContext());

        if (session.isLoggedIn()) {
            dataPref = getApplicationContext().getSharedPreferences("siPAD_data", 0);
            dataLUNAS_len = dataPref.getString("dataLUNAS_len",null);
            dataOPP_len = dataPref.getString("dataOPP_len",null);
            dataKB_len = dataPref.getString("dataKB_len",null);
            setupViewPager(viewPager);
            tabLayout.setupWithViewPager(viewPager);
            ln_tab_vp.setVisibility(View.VISIBLE);

        } else {

            ln_tab_vp.setVisibility(View.INVISIBLE);

        }
    }




    @Override
    public void onResume() {
        super.onResume();
        viewPager.setCurrentItem(pgNumber);

    }

    @Override
    public void onPause() {
        super.onPause();
        viewPager.setCurrentItem(pgNumber);

    }
*/

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_exit: {
                return;
            }
            case R.id.fab_refresh: {


            }
        }
    }
}