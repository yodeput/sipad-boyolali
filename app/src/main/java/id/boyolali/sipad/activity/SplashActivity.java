package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.RandomTransitionGenerator;
import com.google.firebase.analytics.FirebaseAnalytics;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.MainActivity;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

public class SplashActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = SplashActivity.class.getSimpleName();
    private FirebaseAnalytics mFirebaseAnalytics;
    final Handler handler = new Handler();
    private KenBurnsView mKenBurns;
    private ImageView mLogo,mTextLogo;
    private TextView welcomeText;
    private SharedPreferences dataPref;
    private SharedPreferences.Editor editor;
    private PrefManager session;
    private View v;


    @BindView(R.id.rl_pemkab)RelativeLayout rl_pemkab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);


        v = getWindow().getDecorView().findViewById(android.R.id.content);
        ButterKnife.bind(this);
        session = new PrefManager(this.getApplicationContext());


        boolean isConnected = ConnectivityReceiver.isOnline(SplashActivity.this);
        if (isConnected){
            siPAD.getInstance().req_slideshow_data_in_splash(SplashActivity.this, v);
            siPAD.getInstance().req_status_pajak(SplashActivity.this);
        }

        String wizard = session.getPref("wizard");
        if (wizard.contains("false")){
            session.addPref("wizard", "false");
        } else {
            session.addPref("wizard", "true");
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        initResources();
        animation1();
        animation2();
        animation3();

        final Intent intent = new Intent(SplashActivity.this, MainActivity.class);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                SplashActivity.this.finish();
            }
        }, 5000);
    }

    private void initResources() {
        mLogo = findViewById(R.id.icon);
        mTextLogo = findViewById(R.id.icon2);
        mKenBurns = findViewById(R.id.image);
        AccelerateDecelerateInterpolator ACCELERATE_DECELERATE = new AccelerateDecelerateInterpolator();

        RandomTransitionGenerator generator = new RandomTransitionGenerator(10000, ACCELERATE_DECELERATE);
        mKenBurns.setTransitionGenerator(generator); //set new transition on kenburns view

    }


    private void animation1() {
        mLogo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        mLogo.startAnimation(anim);
    }

    private void animation2() {
        mTextLogo.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.fade_in2);
        mTextLogo.startAnimation(anim);
    }

    private void animation3() {
        rl_pemkab.setAlpha(1.0F);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate_down_to_center);
        rl_pemkab.startAnimation(anim);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        siPAD.getInstance().setConnectivityListener(SplashActivity.this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        //showSnack(isConnected);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}