package id.boyolali.sipad.activity.kalkulator;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.layer_net.stepindicator.StepIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.pembayaran.BayarHasil;
import id.boyolali.sipad.adapter.bayar.reklame.RefReklame;
import id.boyolali.sipad.util.CurrencyFormat;
import id.boyolali.sipad.util.JenisPajak;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import mehdi.sakout.fancybuttons.FancyButton;

import static id.boyolali.sipad.util.URLConfig.ALLOWED_URI_CHARS;
import static id.boyolali.sipad.util.URLConfig.URL_REKLAME_DATA;
import static id.boyolali.sipad.util.URLConfig.produk_reklame;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;

public class KalkulatorReklame extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback {

    private View v;
    @BindView(R.id.txt_title)
    TextView txt_title;

    @BindView(R.id.rl_step1)
    RelativeLayout rl_step1;
    @BindView(R.id.l_jenisusaha)
    TextView l_jenisusaha;
    @BindView(R.id.l_usaha)
    TextView l_usaha;
    @BindView(R.id.spinner_data_usaha)
    MaterialSpinner spinner_data_usaha;
    @BindView(R.id.muka_reklame)
    RadioGroup muka_reklame;
    @BindView(R.id.status_reklame)
    RadioGroup status_reklame;
    @BindView(R.id.letak_reklame)
    RadioGroup letak_reklame;

    @BindView(R.id.rl_step2)
    RelativeLayout rl_step2;
    @BindView(R.id.spinner_produk)
    MaterialSpinner spinner_produk;
    @BindView(R.id.edit_materi)
    EditText edit_materi;
    @BindView(R.id.edit_luas)
    EditText edit_luas;
    @BindView(R.id.edit_jumlah)
    EditText edit_jumlah;

    @BindView(R.id.rl_step3)
    RelativeLayout rl_step3;
    @BindView(R.id.edit_masapajak1)
    EditText edit_masapajak1;
    @BindView(R.id.edit_masapajak2)
    EditText edit_masapajak2;
    @BindView(R.id.rl_map)
    RelativeLayout rl_map;
    @BindView(R.id.l_alamat)
    TextView l_alamat;
    @BindView(R.id.rl_get_lokasi)
    RelativeLayout rl_get_lokasi;
    @BindView(R.id.l_masapajak)
    TextView l_masapajak;


    @BindView(R.id.rl_next)
    RelativeLayout rl_next;
    @BindView(R.id.but_step)
    FancyButton but_step;
    @BindView(R.id.step_indicator)
    StepIndicator step_indicator;

    private PrefManager session;

    private ArrayList<String> RefReklame;
    private ArrayList<RefReklame> RefReklameList;
    private String idUsaha, namaUsaha, title, jenisUsaha, latitude, longitude, jenis_objek, jenis_pajak, status_denda = "0", s_denda_pajak, s_besaran_pajak, nm_kelas;
    private String kd_kelas, masa_pajak1, masa_pajak2, Kd_Rek_1 = "", Kd_Rek_2 = "", Kd_Rek_3 = "", Kd_Rek_4 = "", Kd_Rek_5 = "", address, parameter_bayar;
    private String Id_Zona, Kd_Lokasi, Lokasi, country, Area, lat, lng, administrative_area_level_1, administrative_area_level_2, administrative_area_level_3, administrative_area_level_4, postal_code;

    private String s_uraian,s_uraian2, s_muka, s_letak, s_status, s_produk, s_materi, s_luas, s_unit, s_jumlah;
    private String s_periode_year, s_periode_period, s_periode_week, s_periode_day;
    private String jenis_reklame,jenis_reklame2, s_produk_position;
    private int vhargatahun, vhargabulan, vhargaminggu,totalharga;

    private LatLng latlong = new LatLng(-7.3870042, 110.3264428);
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 0;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private LayoutParams lp;
    private ArrayList<String> addressFragments;
    public static AppCompatActivity end;
    private int mLastSpinnerPosition = -1;
    private Animation animShow, animHide, animbackShow, animbackHide;
    public static ArrayList<String> datePickerString;
    private DatePickerDialog dateDialog1, dateDialog2;
    private String URL_TARIF_TAHUN, URL_TARIF_BULAN, URL_TARIF_MINGGU;
    private int init_year, init_month, init_day;
    private int muka_selected_pos, letak_selected_pos, status_selected_pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new PrefManager(getApplicationContext());
        String languageToLoad = "id";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.kalkulator_reklame);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(KalkulatorReklame.this, v);
        end = this;
        lp = (LayoutParams) l_masapajak.getLayoutParams();
        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        idUsaha = (String) bundle.get("idUsaha");
        namaUsaha = (String) bundle.get("namaUsaha");
        jenisUsaha = (String) bundle.get("jenisUsaha");
        jenis_pajak = (String) bundle.get("jenis_pajak");
        String titlee = (String) bundle.get("title");
        title = titlee.toLowerCase();

        l_usaha.setText(namaUsaha);
        l_jenisusaha.setText(jenisUsaha);
        txt_title.setText("Kalkulator "+JenisPajak.cek_pajak(jenis_pajak).toUpperCase());
        RefReklame = new ArrayList<>();
        RefReklameList = new ArrayList<>();
        addressFragments = new ArrayList<String>();

        //getInstance().req_sub_jenis_usaha(getApplicationContext(),v,"airtanah");

        step1();
        init_animation();
        datePickerString = new ArrayList<>();

        if (getInstance().fileExist(getApplication(), title)) {
            req_spinner_data();
        } else {
            getInstance().dialog_alert_finish(KalkulatorReklame.this, v, getString(R.string.error_koneksi));
        }

        spinner_data_usaha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih kategori usaha");
                } else {
                    int pos = spinner_data_usaha.getSelectedItemPosition();
                    kd_kelas = RefReklameList.get(position).getkd_reklame();
                    jenis_reklame = RefReklameList.get(position).getnm_reklame();
                    jenis_reklame2 = RefReklameList.get(position).getnm_reklame();
                    Kd_Rek_1 = RefReklameList.get(position).getkd_rek_1();
                    Kd_Rek_2 = RefReklameList.get(position).getkd_rek_2();
                    Kd_Rek_3 = RefReklameList.get(position).getkd_rek_3();
                    Kd_Rek_4 = RefReklameList.get(position).getkd_rek_4();
                    Kd_Rek_5 = RefReklameList.get(position).getkd_rek_5();


                    muka_reklame.setEnabled(true);
                    letak_reklame.setEnabled(true);
                    status_reklame.setEnabled(true);
                    //getInstance().toastSuccess(getApplicationContext(), jenis_objek);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, produk_reklame);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_produk.setAdapter(adapter);

        spinner_produk.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih kategori produk");
                } else {
                    s_produk = "Produk : " + spinner_produk.getItemAtPosition(position).toString();
                    s_produk_position = Integer.toString(position + 1);

                    //getInstance().toastSuccess(getApplicationContext(), jenis_objek);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mGoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        rl_map.setVisibility(View.GONE);

        edit_masapajak1.setEnabled(true);


    }

    @OnClick(R.id.but_step)
    void next_step() {
        if (rl_step1.isShown()) {

            step2();

        } else if (rl_step2.isShown()) {

            step3();

        } else if ((isEditEmpty(edit_masapajak1)) || (isEditEmpty(edit_masapajak2))) {

            getInstance().toastError(getApplicationContext(), "Lengkapi data yang dibutuhkan");

        } else {

            masa_pajak1 = edit_masapajak1.getText().toString();
            masa_pajak2 = edit_masapajak2.getText().toString();
            String param = masa_pajak1 + "/" + masa_pajak2;
            hitung_periode(param);


        }

    }

    @Override
    public void onBackPressed() {

        if (rl_step3.isShown()) {
            back_step2();

        } else if (rl_step2.isShown()) {
            back_step1();

        } else if (rl_step1.isShown()) {

            //getInstance().dialog_alert_batal_proses(this, v, "Keluar dari kalkulator?");
            finish();
        }
    }

    @OnClick(R.id.but_edit_lokasi)
    void edit_lokasi() {
        popup_placepicker();
    }

    @OnClick(R.id.rl_get_lokasi)
    void lokasi() {
        popup_placepicker();
    }

    @OnClick(R.id.edit_masapajak1)
    void date_picker1() {
        showDatePicker1();
    }

    @OnClick(R.id.edit_masapajak2)
    void date_picker2() {
        showDatePicker2();
    }

    private void step1() {
        step_indicator.setCurrentStepPosition(0);
        rl_step1.setVisibility(View.VISIBLE);
        rl_step2.setVisibility(View.GONE);
        rl_step3.setVisibility(View.GONE);
        rl_next.setVisibility(View.GONE);
        muka_reklame.setEnabled(false);
        letak_reklame.setEnabled(false);
        status_reklame.setEnabled(false);

        status_reklame.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                rl_next.setVisibility(View.VISIBLE);
            }
        });


    }

    private void step2() {
        int mukaselectedId = muka_reklame.getCheckedRadioButtonId();
        RadioButton muka_selected = (RadioButton) findViewById(mukaselectedId);
        int letakselectedId = letak_reklame.getCheckedRadioButtonId();
        RadioButton letak_selected = (RadioButton) findViewById(letakselectedId);
        int statusselectedId = status_reklame.getCheckedRadioButtonId();
        RadioButton status_selected = (RadioButton) findViewById(statusselectedId);

        if ((muka_selected == null) || (letak_selected == null) || (status_selected == null)) {

            getInstance().dialog_alert(this, v, "Lengkapi data yang dibutuhkan");

        } else {
            rl_next.setVisibility(View.GONE);
            s_muka = muka_selected.getText().toString();
            s_letak = letak_selected.getText().toString();
            s_status = status_selected.getText().toString();

            int radioButtonID = muka_reklame.getCheckedRadioButtonId();
            View radioButton = muka_reklame.findViewById(radioButtonID);
            muka_selected_pos = muka_reklame.indexOfChild(radioButton)+1;

            int radioButtonID2 = letak_reklame.getCheckedRadioButtonId();
            View radioButton2 = letak_reklame.findViewById(radioButtonID2);
            letak_selected_pos = letak_reklame.indexOfChild(radioButton2)+1;

            int radioButtonID3 = status_reklame.getCheckedRadioButtonId();
            View radioButton3 = status_reklame.findViewById(radioButtonID3);
            status_selected_pos = status_reklame.indexOfChild(radioButton3)+1;


            step_indicator.setCurrentStepPosition(1);
            rl_step1.setVisibility(View.GONE);
            rl_step2.setVisibility(View.VISIBLE);
            rl_step3.setVisibility(View.GONE);

            rl_step1.startAnimation(animHide);
            rl_step2.startAnimation(animShow);

            edit_jumlah.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (s.length() != 0)
                        rl_next.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void step3() {
        if ((isEditEmpty(edit_materi)) || (isEditEmpty(edit_luas)) || (isEditEmpty(edit_jumlah))) {

            getInstance().dialog_alert(this, v, "Lengkapi data yang dibutuhkan");

        } else {
            rl_next.setVisibility(View.GONE);
            s_materi = edit_materi.getText().toString();
            s_luas = edit_luas.getText().toString();
            s_jumlah = edit_jumlah.getText().toString();

            step_indicator.setCurrentStepPosition(2);
            rl_step1.setVisibility(View.GONE);
            rl_step2.setVisibility(View.GONE);
            rl_step3.setVisibility(View.VISIBLE);

            rl_step2.startAnimation(animHide);
            rl_step3.startAnimation(animShow);

            edit_masapajak2.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                                              int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start,
                                          int before, int count) {
                    if (s.length() != 0)
                        but_step.setText("Hitung");
                    rl_next.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    private void back_step1() {
        step_indicator.setCurrentStepPosition(0);
        rl_step1.setVisibility(View.VISIBLE);
        rl_step2.setVisibility(View.GONE);
        rl_step3.setVisibility(View.GONE);
        rl_next.setVisibility(View.VISIBLE);

        rl_step2.startAnimation(animbackHide);
        rl_step1.startAnimation(animbackShow);

    }

    private void back_step2() {

        step_indicator.setCurrentStepPosition(1);

        rl_step1.setVisibility(View.GONE);
        rl_step2.setVisibility(View.VISIBLE);
        rl_step3.setVisibility(View.GONE);
        rl_next.setVisibility(View.VISIBLE);

        rl_step3.startAnimation(animbackHide);
        rl_step2.startAnimation(animbackShow);
    }

    private void back_step3() {
        step_indicator.setCurrentStepPosition(2);
        rl_step1.setVisibility(View.GONE);
        rl_step2.setVisibility(View.GONE);
        rl_step3.setVisibility(View.VISIBLE);

        rl_step3.startAnimation(animbackHide);
        rl_step3.startAnimation(animbackShow);
    }

    private void init_animation() {
        animShow = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_right);
        animHide = AnimationUtils.loadAnimation(this, R.anim.anim_slide_out_left);
        animbackShow = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_in_right);
        animbackHide = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_out_left);
    }

    public void showDatePicker1() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        dateDialog1 = new DatePickerDialog(KalkulatorReklame.this, R.style.MyDatePickerDialogStyle,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        init_year = year;
                        init_month = month;
                        init_day = day;
                        edit_masapajak1.setText(year + "-" + month + "-" + day);
                        if (isEditEmpty(edit_masapajak2)) {
                            getInstance().showDialog();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    getInstance().hideDialog();
                                    showDatePicker2();
                                }
                            }, 500);
                        }

                    }
                }, year, month, dayOfMonth);
        dateDialog1.show();
    }

    public void showDatePicker2() {
        int year = init_year + 1;
        int month = init_month;
        int dayOfMonth = init_day;
        dateDialog2 = new DatePickerDialog(KalkulatorReklame.this, R.style.MyDatePickerDialogStyle,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                        edit_masapajak2.setText(year + "-" + month + "-" + day);

                    }
                }, year, month, dayOfMonth);
        dateDialog2.show();
    }

    private void popup_placepicker() {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(KalkulatorReklame.this), PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    public void req_spinner_data() {

        String jsonString = getInstance().get_file_data(getApplicationContext(), title);

        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                RefReklame ref = new RefReklame();
                JSONObject jsnobject = jsonArray.getJSONObject(i);
                ref.setkd_reklame(jsnobject.getString("kd_reklame"));
                ref.setnm_reklame(jsnobject.getString("nm_reklame"));
                ref.setkd_rek_1(jsnobject.getString("kd_rek_1"));
                ref.setkd_rek_2(jsnobject.getString("kd_rek_2"));
                ref.setkd_rek_3(jsnobject.getString("kd_rek_3"));
                ref.setkd_rek_4(jsnobject.getString("kd_rek_4"));
                ref.setkd_rek_5(jsnobject.getString("kd_rek_5"));

                RefReklameList.add(ref);
                RefReklame.add(jsnobject.getString("nm_reklame"));

            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, RefReklame);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_data_usaha.setAdapter(adapter);

        } catch (JSONException e) {


            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void getAddress() {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
            if (addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {

                    //Log.e("Addresssss", addresses.get(0).getLocality());

                }
                //String alamat = addresses.get(0).getAddressLine(0);
                //String[] separated = alamat.split(", ");


                //Log.e("Addresssss", separated[1]);
                String aal3 = addresses.get(0).getLocality();
                if (aal3.contains("Boyolali")) {
                    aal3 = "Kecamatan " + aal3;
                    get_address_data(aal3);
                } else {
                    get_address_data(aal3);
                }


            } else {

                //Log.e("Addresssss", "alamat tidak ditemukan");

            }

        } catch (IOException ioException) {
        } catch (IllegalArgumentException illegalArgumentException) {
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.addMarker(new MarkerOptions().position(latlong));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 15));

    }

    private void get_address_data(String area) {

        final String tag_string_req = "address from server";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_ALAMAT_AREA + area, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);

                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    String row = jObj.getString("row");
                    if (row.contains("0")) {

                        //Log.e(tag_string_req,"error");
                        getInstance().toastError(getApplicationContext(), "Lokasi Tidak Tersedia");

                    } else {

                        JSONArray data_array = new JSONArray(data);
                        JSONObject jsnobject = data_array.getJSONObject(0);
                        Id_Zona = jsnobject.getString("Id_Area");
                        Lokasi = jsnobject.getString("Area");
                        Kd_Lokasi = jsnobject.getString("Kd_Lokasi");
                        lat = jsnobject.getString("lat");
                        lng = jsnobject.getString("lng");
                        country = jsnobject.getString("Lokasi");
                        administrative_area_level_1 = jsnobject.getString("administrative_area_level_1");
                        administrative_area_level_2 = jsnobject.getString("administrative_area_level_2");
                        administrative_area_level_3 = jsnobject.getString("administrative_area_level_3");
                        administrative_area_level_4 = jsnobject.getString("administrative_area_level_4");
                        postal_code = jsnobject.getString("postal_code");

                        String administrative_area_level_3 = jsnobject.getString("administrative_area_level_3");

                        //Log.e(tag_string_req,Lokasi);

                    }

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(KalkulatorReklame.this, v, KalkulatorReklame.this.getString(R.string.error_koneksi));
                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void hitung_periode(String param) {

        final String tag_string_req = "hitung denda";
        //Log.e(tag_string_req,param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_REKLAME_PERIODE + param, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(tag_string_req, response);


                try {
                    final JSONObject jObj = new JSONObject(response);
                    s_periode_year = jObj.getString("year");
                    s_periode_period = jObj.getString("period");
                    s_periode_week = jObj.getString("week");
                    s_periode_day = jObj.getString("day");

                    jenis_reklame = jenis_reklame.replace("/", "|");

                    String param_tahun = Kd_Lokasi + jenis_reklame + "Tahun/";
                    URL_TARIF_TAHUN = URL_REKLAME_DATA + param_tahun;
                    get_tarif_tahun get_tarif_tahun = new get_tarif_tahun();
                    get_tarif_tahun.execute();

                    String param_bulan = Kd_Lokasi + jenis_reklame + "Bulan/";
                    URL_TARIF_BULAN = URL_REKLAME_DATA + param_bulan;
                    get_tarif_bulan get_tarif_bulan = new get_tarif_bulan();
                    get_tarif_bulan.execute();


                } catch (final JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();
                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public class get_tarif_tahun extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(Uri.encode(URL_TARIF_TAHUN, ALLOWED_URI_CHARS));

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                        System.out.print(current);

                    }
                    // return the data to onPostExecute method
                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                final JSONObject jObj = new JSONObject(s);
                final String row = jObj.getString("row");
                final String data = jObj.getString("data");
                if (row.equals("1")) {

                    JSONArray dataArr = new JSONArray(data);
                    JSONObject dataObj = dataArr.getJSONObject(0);
                    String jangka_waktu = dataObj.getString("jangka_waktu");
                    vhargatahun = Integer.parseInt(dataObj.getString("tarif"));


                    Log.e("tahun", dataObj.toString());

                } else {

                    //siPAD.getInstance().dialog_alert(BayarReklame.this, v, getString(R.string.error_koneksi));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    public class get_tarif_bulan extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(Uri.encode(URL_TARIF_BULAN, ALLOWED_URI_CHARS));

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                        System.out.print(current);

                    }
                    // return the data to onPostExecute method
                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                final JSONObject jObj = new JSONObject(s);
                final String row = jObj.getString("row");
                final String data = jObj.getString("data");
                if (row.equals("1")) {

                    JSONArray dataArr = new JSONArray(data);
                    JSONObject dataObj = dataArr.getJSONObject(0);
                    String jangka_waktu = dataObj.getString("jangka_waktu");
                    vhargabulan = Integer.parseInt(dataObj.getString("tarif"));
                    Log.e("bulan", dataObj.toString());
                    hitung_pajak();

                } else {

                    jenis_reklame = jenis_reklame.replace("/", "|");
                    String param_minggu = "/Menyeluruh" + jenis_reklame + "Minggu/";
                    URL_TARIF_MINGGU = URL_REKLAME_DATA + param_minggu;
                    get_tarif_minggu get_tarif_minggu = new get_tarif_minggu();
                    get_tarif_minggu.execute();

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    public class get_tarif_minggu extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(Uri.encode(URL_TARIF_MINGGU, ALLOWED_URI_CHARS));
                    Log.e("wkkwwkkwkwkw", URL_TARIF_MINGGU);
                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                        System.out.print(current);

                    }
                    // return the data to onPostExecute method
                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {


            try {
                final JSONObject jObj = new JSONObject(s);
                final String row = jObj.getString("row");
                final String data = jObj.getString("data");
                if (row.equals("1")) {

                    JSONArray dataArr = new JSONArray(data);
                    JSONObject dataObj = dataArr.getJSONObject(0);
                    String jangka_waktu = dataObj.getString("jangka_waktu");
                    vhargaminggu = Integer.parseInt(dataObj.getString("tarif"));
                    Log.e("minggu", dataObj.toString());

                } else {

                    siPAD.getInstance().dialog_alert(KalkulatorReklame.this, v, getString(R.string.error_koneksi));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }

    private void hitung_pajak() {

        double hargabulanya = 0, hargatahunya = 0;
        int jml_bln = Integer.parseInt(s_periode_period);
        s_luas = edit_luas.getText().toString();
        int luas = Integer.parseInt(s_luas);
        s_uraian = Kd_Lokasi;
        s_uraian2="";

        if (jml_bln < 12) {

            if (s_produk_position.equals("1")) {

                hargatahunya = 0;
                double a = (luas * 0.25);
                hargabulanya = (vhargabulan * jml_bln) * a;
                s_uraian = s_uraian + " Produk : Produk Bukan Rokok";
                s_uraian = s_uraian + " Luas : " + s_luas;
                s_uraian2 = s_uraian2 + "Produk : Produk Bukan Rokok\n";
                s_uraian2 = s_uraian2 + "Luas : " + s_luas+" meter\n";


            } else if ((s_produk_position.equals("2")) || (s_produk_position.equals("3"))) {

                hargatahunya = 0;
                double a = (luas * 0.25);
                double ghargasewa = (vhargabulan * jml_bln) * a;
                hargabulanya = ghargasewa + (ghargasewa * 25 / 100);
                s_uraian = s_uraian + " Produk : Rokok/ Miras";
                s_uraian = s_uraian + " Luas : " + s_luas;
                s_uraian2 = s_uraian2 + "Produk : Rokok/ Miras\n";
                s_uraian2 = s_uraian2 + "Luas : " + s_luas+" meter\n";

            } else if (s_produk_position.equals("4")) {

                hargatahunya = 0;
                double a = (luas * 0.25);
                double ghargasewa = vhargabulan * jml_bln * a;
                hargabulanya = ghargasewa * 0.5;
                s_uraian = s_uraian + " Produk : Bukan Produk";
                s_uraian = s_uraian + " Luas : " + s_luas;
                s_uraian2 = s_uraian2 + "Produk : Bukan Produk\n";
                s_uraian2 = s_uraian2 + "Luas : " + s_luas+" meter\n";

            }


        } else if (jml_bln >= 12) {
            int jml_tahun = (int) Math.floor(jml_bln / 12);
            int sisa_bulan = jml_bln % 12;
            if (s_produk_position.equals("1")) {
                double a = (luas * 0.25);
                hargatahunya = (vhargatahun * jml_tahun) * a;
                ;
                hargabulanya = (vhargabulan * sisa_bulan) * a;
                s_uraian = s_uraian + " Produk : Produk Bukan Rokok";
                s_uraian = s_uraian + " Luas : " + s_luas;
                s_uraian2 = s_uraian2 + "Produk : Produk Bukan Rokok\n";
                s_uraian2 = s_uraian2 + "Luas : " + s_luas+" meter\n";

            } else if ((s_produk_position.equals("2")) || (s_produk_position.equals("3"))) {
                double a = (luas * 0.25);

                double gaawaltahun = (vhargatahun * jml_tahun) * a;
                hargatahunya = (gaawaltahun + (gaawaltahun * 25 / 100));

                double gaawalbulan = (vhargabulan * sisa_bulan) * a;
                hargabulanya = (gaawalbulan + (gaawalbulan * 25 / 100));

                s_uraian = s_uraian + " Produk : Rokok/ Miras";
                s_uraian = s_uraian + " Luas : " + s_luas;
                s_uraian2 = s_uraian2 + "Produk : Rokok/ Miras\n";
                s_uraian2 = s_uraian2 + "Luas : " + s_luas+" meter\n";

            } else if (s_produk_position.equals("4")) {
                double a = (luas * 0.25);
                double gaawaltahun = (vhargatahun * jml_tahun) * a;
                hargatahunya = (gaawaltahun * 0.5);

                double gaawalbulan = (vhargabulan * sisa_bulan) * a;
                hargabulanya = (gaawalbulan * 0.5);
                s_uraian = s_uraian + " Produk : Bukan Produk";
                s_uraian = s_uraian + " Luas : " + s_luas;
                s_uraian2 = s_uraian2 + "Produk : Bukan Produk\n";
                s_uraian2 = s_uraian2 + "Luas : " + s_luas+" meter\n";

            }


        }

        int aa = (int)hargatahunya;
        int bb = (int)hargabulanya;
        int cc = 0;
        Log.e(s_uraian, "----Harga Tahun " + CurrencyFormat.currency_format_integer(aa)+ "----Harga Bulan " + CurrencyFormat.currency_format_integer(bb));
        harga_sewa(aa,bb,cc);
    }

    private void harga_sewa(int hargat,int hargab,int hargam) {
        int jmlreklame = Integer.parseInt(s_jumlah);
        int pajak, total_pajak;
        int tarif_t=0,tarif_b=0,tarif_m=0;

        if ((letak_selected_pos == 1) && status_selected_pos == 1) {

            if  ((jenis_reklame2.contains( "Reklame Papan/Bill Board") ||
                    jenis_reklame2 .contains("Reklame Megatron/Videotron/LED"))) {

                double a = (double) hargat * 0.8;
                double b = (double) hargab * 0.8;
                double c = (double) hargam * 0.8;
                tarif_t = (int)a;
                tarif_b= (int)b;
                tarif_m = (int)c;


            }else {


                double a = (double) hargat;
                double b = (double) hargab;
                double c = (double) hargam;
                tarif_t = (int)a;
                tarif_b= (int)b;
                tarif_m = (int)c;


            }

            s_uraian = s_uraian +  " Status Tanah : Milik Sendiri" ;
            s_uraian = s_uraian +  " Letak : Indoor";
            s_uraian2 = s_uraian2 +  "Status Tanah : Milik Sendiri\n" ;
            s_uraian2 = s_uraian2 +  "Letak : Indoor - ";

        } else if (status_selected_pos == 1) {
            if  ((jenis_reklame2.contains("Reklame Papan/Bill Board") ||
                    jenis_reklame2 .contains("Reklame Megatron/Videotron/LED"))) {

                double a = (double) hargat * 0.8;
                double b = (double) hargab * 0.8;
                double c = (double) hargam * 0.8;
                tarif_t = (int)a;
                tarif_b= (int)b;
                tarif_m = (int)c;

            }else {

                double a = (double) hargat;
                double b = (double) hargab;
                double c = (double) hargam;
                tarif_t = (int)a;
                tarif_b= (int)b;
                tarif_m = (int)c;


            }

            s_uraian = s_uraian +  " Status Tanah : Milik Sendiri" ;
            s_uraian = s_uraian +  " Letak : Outdoor";
            s_uraian2 = s_uraian2 +  "Status Tanah : Milik Sendiri\n" ;
            s_uraian2 = s_uraian2 +  "Letak : Outdoor - ";
        } else {

            double a = (double) hargat;
            double b = (double) hargab;
            double c = (double) hargam;
            tarif_t = (int)a;
            tarif_b= (int)b;
            tarif_m = (int)c;

            s_uraian = s_uraian +  " Status Tanah : Milik Negara" ;
            s_uraian2 = s_uraian2 +  "Status Tanah : Milik Negara\n";
            if (letak_selected_pos == 1) {
                s_uraian = s_uraian +  " Letak : Indoor";
                s_uraian2 = s_uraian2 +  "Letak : Indoor - ";
            } else {
                s_uraian = s_uraian +  " Letak : Outdoor";
                s_uraian2 = s_uraian2 +  "Letak : Outdoor - ";
            }

        }

        if (muka_selected_pos == 2) {

            pajak = ((tarif_t + tarif_b + tarif_m) + (tarif_t + tarif_b + tarif_m) * 50 / 100)*jmlreklame;
            s_uraian = s_uraian +  " Muka : 2";
            s_uraian2 = s_uraian2 +  "Muka : 2 sisi\n";
        } else {

            pajak = (tarif_t + tarif_b + tarif_m) * jmlreklame;
            s_uraian = s_uraian +  " Muka : 1";
            s_uraian2 = s_uraian2 +  "Muka : 1 sisi\n";
        }

        total_pajak = pajak;
        s_besaran_pajak = Integer.toString(total_pajak);
        totalharga = vhargatahun*jmlreklame;
        s_uraian = s_uraian +  " Jumlah Reklame : "+jmlreklame;
        s_uraian2 = s_uraian2 +  "Jumlah Reklame : "+jmlreklame;


        String param = Integer.toString(total_pajak)+"/"+masa_pajak1+"/1/" +"2";
        hitung_denda(param);

    }

    private void hitung_denda(String param) {
        getInstance().showDialog();
        final String tag_string_req = "hitung denda";
        //Log.e(tag_string_req,param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_HITUNG_DENDA + param, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    s_denda_pajak = jObj.getString("denda");
                    String masa = masa_pajak1+ " / "+masa_pajak2;
                    dialog_result(namaUsaha, jenisUsaha, jenis_reklame2, s_besaran_pajak, s_denda_pajak,s_uraian2 ,masa, jenis_pajak);

                } catch (final JSONException e) {
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        });

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void param_bayar() {
        String params = "";
        String Omzet = edit_jumlah.getText().toString();
        Omzet = Omzet.replace(".", "");
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        String date_now = year+"-"+month+"-"+dayOfMonth;

        String ST_TEMPO = "ST_TEMPO[STATUS]=" + status_denda;

        String URAIAN = "&data[0][URAIAN]=Reklame "+jenis_reklame2;
        String VOLUME = "&data[0][VOLUME]=" + s_jumlah;
        String URAIAN_TARIF = "&data[0][URAIAN_TARIF]=" + s_uraian;
        String HARGA = "&data[0][HARGA]=" + vhargatahun;
        String TOTAL_HARGA = "&data[0][TOTAL_HARGA]=" + totalharga;
        String PAJAK = "&data[0][PAJAK]=" + s_besaran_pajak;
        String TOTAL_PAJAK = "&data[0][TOTAL_PAJAK]=" + s_besaran_pajak;
        String KD_Rek_1 = "&data[0][kd_rek_1]=" + Kd_Rek_1;
        String KD_Rek_2 = "&data[0][Kd_Rek_2]=" + Kd_Rek_2;
        String KD_Rek_3 = "&data[0][Kd_Rek_3]=" + Kd_Rek_3;
        String KD_Rek_4 = "&data[0][Kd_Rek_4]=" + Kd_Rek_4;
        String KD_Rek_5 = "&data[0][Kd_Rek_5]=" + Kd_Rek_5;

        String URAIAN_TARIF2 = "&nota[URAIAN_TARIF]=Reklame "+jenis_reklame2;
        String TGL_NOTA = "&nota[TGL_NOTA]=" + date_now;
        String USAHA_ID = "&nota[USAHA_ID]=" + idUsaha;
        String JENIS_PAJAK = "&nota[JENIS_PAJAK]=" + jenis_pajak;
        String JUMLAH_HARGA = "&nota[JUMLAH_HARGA]=" + totalharga;
        String JUMLAH_PAJAK = "&nota[JUMLAH_PAJAK]=" + s_besaran_pajak;
        String TGL_MASA_PAJAK = "&nota[TGL_MASA_PAJAK]="+masa_pajak1;
        String TGL_JATUH_TEMPO_MASA_PAJAK = "&nota[TGL_JATUH_TEMPO_MASA_PAJAK]="+masa_pajak2;
        String LOKASI = "&nota[LOKASI]=" + address;
        String LAT = "&nota[LAT]=" + latitude;
        String LNG = "&nota[LNG]=" + longitude;
        String COUNTRY = "&nota[COUNTRY]=" + country;
        String ADMINISTRATIVE_AREA_LEVEL_1 = "&nota[ADMINISTRATIVE_AREA_LEVEL_1]=" + administrative_area_level_1;
        String ADMINISTRATIVE_AREA_LEVEL_2 = "&nota[ADMINISTRATIVE_AREA_LEVEL_2]=" + administrative_area_level_2;
        String ADMINISTRATIVE_AREA_LEVEL_3 = "&nota[ADMINISTRATIVE_AREA_LEVEL_3]=" + administrative_area_level_3;
        String ADMINISTRATIVE_AREA_LEVEL_4 = "&nota[ADMINISTRATIVE_AREA_LEVEL_4]=" + administrative_area_level_4;
        String POSTAL_CODE = "&nota[POSTAL_CODE]=" + postal_code;
        String JENIS_PERMOHONAN = "&nota[JENIS_PERMOHONAN]=" + "0";
        String JENIS_OBJEK = "&nota[JENIS_OBJEK]=" + jenis_objek;
        String URAIAN_TARIF3 = "&nota[URAIAN_TARIF]=Reklame "+s_uraian;

        parameter_bayar = ST_TEMPO + URAIAN + VOLUME + URAIAN_TARIF + HARGA + TOTAL_HARGA  + PAJAK + TOTAL_PAJAK + KD_Rek_1 + KD_Rek_2 + KD_Rek_3 + KD_Rek_4 + KD_Rek_5
                + URAIAN_TARIF2 + TGL_NOTA + USAHA_ID + JENIS_PAJAK + JUMLAH_HARGA + JUMLAH_PAJAK + TGL_MASA_PAJAK + TGL_JATUH_TEMPO_MASA_PAJAK + LOKASI + LAT + LNG + JENIS_OBJEK + COUNTRY
                + ADMINISTRATIVE_AREA_LEVEL_1 + ADMINISTRATIVE_AREA_LEVEL_2 + ADMINISTRATIVE_AREA_LEVEL_3 + ADMINISTRATIVE_AREA_LEVEL_4
                + POSTAL_CODE + JENIS_PERMOHONAN+URAIAN_TARIF3;

    }

    public void dialog_result(final String namaUsaha, String jenisUsaha, String jenis_objek, String s_besaran_pajak, String s_denda_pajak,final String keterangan, final String masa_pajak, String jenis_pajak) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(this).inflate(R.layout.activity_bayar_hasil_hitung_reklame,
                (ViewGroup) v.findViewById(android.R.id.content), false);

        final TextView txt_jns_pajak = viewInflated.findViewById(R.id.txt_jns_pajak);
        final TextView txt_nm_usaha = viewInflated.findViewById(R.id.txt_nm_usaha);
        final TextView L_1 = viewInflated.findViewById(R.id.L_1);
        final TextView L_2 = viewInflated.findViewById(R.id.L_2);
        final TextView txt_jns_usaha = viewInflated.findViewById(R.id.txt_jns_usaha);
        final TextView txt_kategori = viewInflated.findViewById(R.id.txt_kategori);
        final TextView txt_keterangan = viewInflated.findViewById(R.id.txt_keterangan);
        final TextView txt_masapajak = viewInflated.findViewById(R.id.txt_masapajak);
        final TextView txt_besaran_pajak = viewInflated.findViewById(R.id.txt_besaran_pajak);
        final TextView txt_denda_pajak = viewInflated.findViewById(R.id.txt_denda_pajak);
        final TextView txt_terbilang = viewInflated.findViewById(R.id.txt_terbilang);
        final ImageView img_share = viewInflated.findViewById(R.id.img_share);
        final Button dialog_button_ok = viewInflated.findViewById(R.id.dialog_button_ok);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        final LinearLayout ll_2_button = viewInflated.findViewById(R.id.ll_2_button);

        ll_2_button.setVisibility(View.GONE);
        txt_jns_usaha.setVisibility(View.GONE);
        txt_nm_usaha.setVisibility(View.GONE);
        L_1.setVisibility(View.INVISIBLE);
        L_2.setVisibility(View.GONE);

        dialog_button_ok.setText("OK");
        dialog_left.setText("Kembali");
        dialog_right.setText("Bayar");

        txt_jns_pajak.setText("Pajak " + JenisPajak.cek_pajak(jenis_pajak));
        txt_nm_usaha.setText(namaUsaha);
        txt_jns_usaha.setText(jenisUsaha);
        txt_kategori.setText(jenis_objek);
        txt_keterangan.setText(keterangan);
        txt_masapajak.setText(masa_pajak);



        txt_besaran_pajak.setText(CurrencyFormat.rupiah_format_string(s_besaran_pajak));
        int denda = Integer.parseInt(s_denda_pajak);
        if (denda > 0) {

            s_denda_pajak = CurrencyFormat.rupiah_format_string(s_denda_pajak);
            status_denda = "1";
        }

        txt_denda_pajak.setText(s_denda_pajak);
        txt_terbilang.setText("(" + CurrencyFormat.info_terbilang(Integer.parseInt(s_besaran_pajak)) + " rupiah)");


        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });


        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInstance().showDialog();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        getInstance().hideDialog();
                        show_dialog_persetujuan_bayar();


                    }
                }, 1000);

            }
        });
        dialog_button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_button.setVisibility(View.GONE);
                img_share.setVisibility(View.GONE);
                dialog_button_ok.setVisibility(View.GONE);
                getInstance().shareImage(getInstance().takeSS(viewInflated, namaUsaha + "_" + masa_pajak));
                dialog_button_ok.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        });


    }

    private void show_dialog_persetujuan_bayar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_persetujuan_bayar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.bt_accept)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                param_bayar();
                Intent i = new Intent(KalkulatorReklame.this, BayarHasil.class);
                i.putExtra("parameter", parameter_bayar);
                i.putExtra("jenis_pajak", jenis_pajak);
                startActivity(i);
                dialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Button Accept Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((Button) dialog.findViewById(R.id.bt_decline)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "Button Decline Clicked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getInstance().init_p_dialog(KalkulatorReklame.this, v);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        ;

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                StringBuilder stBuilder = new StringBuilder();
                String placename = String.format("%s", place.getName());
                latitude = String.valueOf(place.getLatLng().latitude);
                longitude = String.valueOf(place.getLatLng().longitude);
                latlong = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                address = String.format("%s", place.getAddress());
                stBuilder.append("Name: ");
                stBuilder.append(placename);
                stBuilder.append("\n");
                stBuilder.append("Latitude: ");
                stBuilder.append(latitude);
                stBuilder.append("\n");
                stBuilder.append("Logitude: ");
                stBuilder.append(longitude);
                stBuilder.append("\n");
                stBuilder.append("Address: ");
                stBuilder.append(address);
                l_alamat.setText(address);
                //l_alamat.setSelected(true);


        /*
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
          Bitmap bitmap;
          @Override
          public void onSnapshotReady(Bitmap snapshot) {
            bitmap = snapshot;
            try {
              File newfile = new File(Environment.getExternalStorageDirectory()+"/siPAD/");
              newfile.mkdir();
              String mPath = newfile.toString() +"/"+"Currentlocation.jpeg";

              FileOutputStream out = new FileOutputStream(mPath);

              File imageFile = new File(mPath);

              FileOutputStream outputStream = new FileOutputStream(imageFile);
              int quality = 100;
              bitmap.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
              outputStream.flush();
              outputStream.close();
            }catch (Exception e){
              e.printStackTrace();
            }

          }
        };
        mMap.snapshot(callback);
        */
                lp.addRule(RelativeLayout.BELOW, R.id.cv_map);
                rl_map.setVisibility(View.VISIBLE);
                rl_get_lokasi.setVisibility(View.GONE);
                mMap.addMarker(new MarkerOptions().position(latlong));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latlong));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latlong, 18));

                getAddress();

            }
        }
    }
}
