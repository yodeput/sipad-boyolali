package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.IpCons;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.esafirm.rximagepicker.RxImagePicker;
import com.layer_net.stepindicator.StepIndicator;
import com.tsongkha.spinnerdatepicker.DatePicker;
import com.tsongkha.spinnerdatepicker.DatePickerDialog;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.Domisili;
import id.boyolali.sipad.util.Tools;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import rx.Observable;

import static id.boyolali.sipad.util.URLConfig.REGISTER_AKUN;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;
import static id.boyolali.sipad.util.siPAD.isValidEmail;
import static id.boyolali.sipad.util.siPAD.isValidPhone;

public class DaftarAkunActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener{

    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.step_indicator)
    StepIndicator step_indicator;

    @BindView(R.id.spinner_provinsi)
    MaterialSpinner spinner_provinsi;
    @BindView(R.id.spinner_kotakab)
    MaterialSpinner spinner_kotakab;
    @BindView(R.id.spinner_kecamatan)
    MaterialSpinner spinner_kecamatan;
    @BindView(R.id.spinner_desa)
    MaterialSpinner spinner_desa;

    @BindView(R.id.ll_main)
    LinearLayout ll_main;
    @BindView(R.id.ll_next)
    LinearLayout ll_next;

    @BindView(R.id.step_1)
    ScrollView step_1;
    @BindView(R.id.edit_nama_wp)
    EditText edit_nama_wp;
    @BindView(R.id.edit_email)
    EditText edit_email;
    @BindView(R.id.edit_telepon)
    EditText edit_telepon;
    @BindView(R.id.edit_alamat)
    EditText edit_alamat;
    @BindView(R.id.edit_rt)
    EditText edit_rt;
    @BindView(R.id.edit_rw)
    EditText edit_rw;

    @BindView(R.id.step_2)
    ScrollView step_2;
    @BindView(R.id.edit_nama_penanggung)
    EditText edit_nama_penanggung;
    @BindView(R.id.edit_alamat_penanggung)
    EditText edit_alamat_penanggung;
    @BindView(R.id.edit_nik_penanggung)
    EditText edit_nik_penanggung;
    @BindView(R.id.edit_tempat_lahir)
    EditText edit_tempat_lahir;
    @BindView(R.id.edit_tanggal_lahir)
    EditText edit_tanggal_lahir;
    @BindView(R.id.bt_pilih_tanggal)
    Button bt_pilih_tanggal;

    @BindView(R.id.step_3)
    ScrollView step_3;
    @BindView(R.id.bt_foto_ktp)
    Button bt_foto_ktp;
    @BindView(R.id.img_ktp)
    ImageView img_ktp;
    @BindView(R.id.textView24)TextView txt_img;

    @BindView(R.id.bt_next)
    Button bt_next;

    private ArrayList<Domisili> ProvArrayList,KabArrayList,KecArrayList,DesaArrayList;
    private ArrayList<String> ProvArray,KabArray,KecArray,DesaArray;
    private SimpleDateFormat simpleDateFormat;
    private static final int RC_CAMERA = 3000;
    private ArrayList<Image> images = new ArrayList<>();
    private int mLastSpinnerPosition = -1, counter = 0;
    private Animation animShow, animHide, animbackShow, animbackHide;
    private View v;

    private String NAMA_NASABAH,JENIS_ID,TELPON,ALAMAT,RT,RW,ID_PROV,ID_DATI,ID_KEC,ID_DESA,
            NAMA_PENANGGUNG_JAWAB, ALAMAT_PENANGGUNG_JAWAB,NO_ID,TEMPATLAHIR,TGLLAHIR,KTP,
            PPAT,EMAIL;

    @OnClick(R.id.bt_next) void next(){
        if (step_1.isShown()){
            step2();
        } else if (step_2.isShown()){
            step3();
        } else if (step_3.isShown()) {
          if (!KTP.isEmpty()){
            get_string();
            post_daftar();
          }
        }
    }

    @OnClick(R.id.bt_foto_ktp) void pilih_ktp(){
        dialog_pilih_sumber();
    }
    @OnClick(R.id.bt_pilih_tanggal) void pilih_tanggal(){
        datePicker_dialog();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_akun);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(DaftarAkunActivity.this,v);
        ProvArrayList = new ArrayList<>();ProvArray= new ArrayList<>();
        KabArrayList = new ArrayList<>();KabArray= new ArrayList<>();
        KecArrayList = new ArrayList<>();KecArray= new ArrayList<>();
        DesaArrayList = new ArrayList<>();DesaArray= new ArrayList<>();

        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        ll_root.setVisibility(View.GONE);
        show_dialog_persetujuan_daftar();
    }
    private void init_layout(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daftar Akun");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
        ll_root.setVisibility(View.VISIBLE);
        step_indicator.setCurrentStepPosition(0);
        edit_nama_wp.requestFocus();
        step1();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id  = item.getItemId();
        if (id == android.R.id.home) {
            getInstance().dialog_alert_batal_proses(DaftarAkunActivity.this,v,"Akan membatalkan Pendaftaran?");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void show_next(){
     ll_next.setVisibility(View.VISIBLE);
    }
    private void step1(){
        step_1.setVisibility(View.VISIBLE);
        step_2.setVisibility(View.GONE);
        step_3.setVisibility(View.GONE);
        ll_next.setVisibility(View.GONE);
    }
    private void step2(){
        if ((isEditEmpty(edit_nama_wp))||(isEditEmpty(edit_email))||(isEditEmpty(edit_telepon))
                ||(isEditEmpty(edit_alamat))||(isEditEmpty(edit_rt))||(isEditEmpty(edit_rw))){
            getInstance().toastError(DaftarAkunActivity.this,"Lengkapi data Pendaftaran");
            edit_nama_wp.requestFocus();
        } else if (!isValidEmail(edit_email.getText().toString())){
            getInstance().toastError(DaftarAkunActivity.this,"Format email salah");
            edit_email.requestFocus();
        }else if (!isValidPhone(edit_telepon.getText().toString())){
            getInstance().toastError(DaftarAkunActivity.this,"Format nomor telepon Salah");
            edit_telepon.requestFocus();
        } else {
            step_1.setVisibility(View.GONE);
            step_2.setVisibility(View.VISIBLE);
            step_1.startAnimation(animHide);
            step_2.startAnimation(animShow);
            step_3.setVisibility(View.GONE);
            ll_next.setVisibility(View.GONE);
            step_indicator.setCurrentStepPosition(1);
        }
    }
    private void step3(){
        if ((isEditEmpty(edit_nama_penanggung))||(isEditEmpty(edit_alamat_penanggung))||(isEditEmpty(edit_nik_penanggung))
                ||(isEditEmpty(edit_tempat_lahir))||(isEditEmpty(edit_tanggal_lahir))){
            getInstance().toastError(DaftarAkunActivity.this,"Lengkapi data Pendaftaran");
            edit_nama_wp.requestFocus();
        } else {
            step_1.setVisibility(View.GONE);
            step_2.setVisibility(View.GONE);
            step_3.setVisibility(View.VISIBLE);
            step_2.startAnimation(animHide);
            step_3.startAnimation(animShow);
            ll_next.setVisibility(View.GONE);
            step_indicator.setCurrentStepPosition(2);

        }
    }

    private void get_string(){
        NAMA_NASABAH = edit_nama_wp.getText().toString();
        EMAIL = edit_email.getText().toString();
        TELPON = edit_telepon.getText().toString();
        ALAMAT = edit_alamat.getText().toString();
        RT = edit_rt.getText().toString();
        RW = edit_rw.getText().toString();

        NAMA_PENANGGUNG_JAWAB = edit_nama_penanggung.getText().toString();
        ALAMAT_PENANGGUNG_JAWAB = edit_alamat_penanggung.getText().toString();
        NO_ID = edit_nik_penanggung.getText().toString();
        TEMPATLAHIR = edit_tempat_lahir.getText().toString();
        TGLLAHIR = edit_tanggal_lahir.getText().toString();

        JENIS_ID = "2";
        PPAT = "ppat";
        Log.e("daftar_akun_activity",KTP);
    }


    private void init_spinner(){

        spinner_provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KabArrayList.clear();
                KabArray.clear();
                KecArrayList.clear();
                KecArray.clear();
                DesaArrayList.clear();
                DesaArray.clear();
                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Provinsi");
                } else {
                    String kd_prov = ProvArrayList.get(position).getid();
                    ID_PROV = kd_prov;
                    get_kab(kd_prov);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_kotakab.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Kota/Kabupaten");
                } else {
                    String kd_kab = KabArrayList.get(position).getid();
                    ID_DATI= kd_kab;
                    get_kec(kd_kab);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Kecamatan");
                } else {
                    String kd_kec = KecArrayList.get(position).getid();
                    ID_KEC = kd_kec;
                    get_desa(kd_kec);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_desa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Desa/Kelurahan");
                } else {
                    String kd_desa = DesaArrayList.get(position).getid();
                    ID_DESA = kd_desa;
                    show_next();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void get_prov() {
        getInstance().showDialog();
        final String tag_string_req = "req_prov";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_PROVINSI, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setname(jsnobject.getString("name"));

                        ProvArrayList.add(dom);
                        ProvArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarAkunActivity.this, android.R.layout.simple_spinner_item, ProvArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_provinsi.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarAkunActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_kab(String kd_prov) {
        getInstance().showDialog();
        final String tag_string_req = "req_kab";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_KOTAKAB + kd_prov, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setid_provinsi(jsnobject.getString("id_prov"));
                        dom.setname(jsnobject.getString("name"));

                        KabArrayList.add(dom);
                        KabArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarAkunActivity.this, android.R.layout.simple_spinner_item, KabArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_kotakab.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarAkunActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_kec(String kd_kab) {
        getInstance().showDialog();
        final String tag_string_req = "req_desa";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_KEC + kd_kab, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setid_kab(jsnobject.getString("id_kab"));
                        dom.setname(jsnobject.getString("name"));

                        KecArrayList.add(dom);
                        KecArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarAkunActivity.this, android.R.layout.simple_spinner_item, KecArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_kecamatan.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarAkunActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_desa(String kd_kecamatan) {
        getInstance().showDialog();
        final String tag_string_req = "req_desa";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_DESA + kd_kecamatan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setid_kec(jsnobject.getString("id_kec"));
                        dom.setname(jsnobject.getString("name"));

                        DesaArrayList.add(dom);
                        DesaArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarAkunActivity.this, android.R.layout.simple_spinner_item, DesaArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_desa.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarAkunActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void datePicker_dialog() {
        final Calendar today = Calendar.getInstance();
        new SpinnerDatePickerDialogBuilder()
                .context(DaftarAkunActivity.this)
                .callback(DaftarAkunActivity.this)
                .spinnerTheme(R.style.NumberPickerStyle)
                .showTitle(true)
                .showDaySpinner(true)
                .defaultDate(today.get(Calendar.YEAR),today.get(Calendar.MONTH),today.get(Calendar.DATE))
                .maxDate(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DATE))
                .minDate(1935, 0, 1)
                .build()
                .show();
    }
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        edit_tanggal_lahir.setText(simpleDateFormat.format(calendar.getTime()));
        edit_nik_penanggung.clearFocus();
        show_next();
    }

    private void init_animation() {
        animShow = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_right);
        animHide = AnimationUtils.loadAnimation(this, R.anim.anim_slide_out_left);
        animbackShow = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_in_right);
        animbackHide = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_out_left);
    }

    @Override
    public void onBackPressed() {
        if (step_3.isShown()){
            step_3.startAnimation(animbackHide);
            step_2.startAnimation(animbackShow);
            step_1.setVisibility(View.GONE);
            step_2.setVisibility(View.VISIBLE);
            step_3.setVisibility(View.GONE);
            step_indicator.setCurrentStepPosition(1);
            show_next();
        } else if (step_2.isShown()){
            step_2.startAnimation(animbackHide);
            step_1.startAnimation(animbackShow);
            step_1.setVisibility(View.VISIBLE);
            step_2.setVisibility(View.GONE);
            step_3.setVisibility(View.GONE);
            step_indicator.setCurrentStepPosition(0);
            show_next();
        } else if (step_1.isShown()){
            getInstance().dialog_alert_batal_proses(DaftarAkunActivity.this,v,"Akan membatalkan Pendaftaran?");
        }
    }


    private Observable<List<Image>> getImagePickerObservable() {
        return RxImagePicker.getInstance()
                .start(this, ImagePicker.create(this));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_CAMERA) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImage();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private ImagePicker getImagePicker() {

        ImagePicker imagePicker = ImagePicker.create(this)
                .language("id")
                .returnMode(true
                        ? ReturnMode.ALL
                        : ReturnMode.NONE)
                .folderMode(true)
                .includeVideo(false)
                .toolbarArrowColor(Color.RED)
                .toolbarFolderTitle("Dokumen KTP")
                .toolbarImageTitle("Pilih untuk memilih")
                .toolbarDoneButtonText("Selesai");
        imagePicker.single();

        return imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()); // can be full path
    }

    private void startWithIntent() {
        startActivityForResult(getImagePicker().getIntent(this), IpCons.RC_IMAGE_PICKER);
    }

    private void start() {
        getImagePicker().start(); // start image picker activity with request code
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }
    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);
            printImages(images);
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void printImages(List<Image> images) {
        if (images == null) return;

        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0, l = images.size(); i < l; i++) {

            stringBuffer.append(images.get(i).getPath());
        }
        txt_img.setText(stringBuffer.toString());
        File file = new File(stringBuffer.toString());
        imgToBase64(stringBuffer.toString());
        Glide.with(this)
        .load(file)
        .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
        .into(img_ktp);
        show_next();
        bt_next.setText("Kirim Data");
        bt_foto_ktp.setText("Ambil Ulang");
    }

    public void dialog_pilih_sumber() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_pilih_ktp);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.d_galeri)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.d_kamera)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        captureImage();
                        dialog.dismiss();
                    }
                });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void imgToBase64(String path){
        Bitmap bm = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream); //bm is the bitmap object
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        KTP = "data:image/jpeg;base64,"+Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void post_daftar(){
        getInstance().showDialog();
        StringRequest postRequest = new StringRequest(Request.Method.POST, REGISTER_AKUN,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        getInstance().hideDialog();
                        Log.e("Response", response);
                        try {
                            final JSONObject jObj = new JSONObject(response);
                            final Boolean error = jObj.getBoolean("error");

                            if (!error){
                                show_dialog_sukses_daftar();
                            } else {

                                String msg = jObj.getString("msg");
                                step_3.startAnimation(animbackHide);
                                step_1.startAnimation(animbackShow);
                                step_1.setVisibility(View.VISIBLE);
                                step_2.setVisibility(View.GONE);
                                step_3.setVisibility(View.GONE);
                                step_indicator.setCurrentStepPosition(0);
                                show_next();
                                getInstance().dialog_alert(DaftarAkunActivity.this,v,"Alamat Email sudah terdaftar\nSilakan cek kembali");
                            }
                        }catch (final JSONException e) {
                            getInstance().hideDialog();
                            e.printStackTrace();
                            //Log.e(tag_string_req, e.getMessage());


                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getInstance().hideDialog();
                        getInstance().dialog_alert_finish(DaftarAkunActivity.this,v,getString(R.string.error_koneksi));
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                 params.put("usreg[NAMA_NASABAH]",NAMA_NASABAH);
                 params.put("usreg[JENIS_ID]", JENIS_ID);
                 params.put("usreg[TELPON]", TELPON);
                 params.put("usreg[ALAMAT]",ALAMAT );
                 params.put("usreg[RT]", RT);
                 params.put("usreg[RW]", RW);
                 params.put("usreg[ID_PROV]", ID_PROV);
                 params.put("usreg[ID_DATI]", ID_DATI);
                 params.put("usreg[ID_KEC]", ID_KEC );
                 params.put("usreg[ID_DESA]", ID_DESA);
                 params.put("usreg[NAMA_PENANGGUNG_JAWAB]",NAMA_PENANGGUNG_JAWAB);
                 params.put("usreg[ALAMAT_PENANGGUNG_JAWAB]", ALAMAT_PENANGGUNG_JAWAB);
                 params.put("usreg[NO_ID]", NO_ID);
                 params.put("usreg[TEMPATLAHIR]", TEMPATLAHIR);
                 params.put("usreg[TGLLAHIR]", TGLLAHIR);
                 params.put("usreg[KTP]", KTP);
                 params.put("usreg[STATUS_USER]", "1");
                 params.put("uslog[PPAT]", PPAT);
                 params.put("uslog[email]", EMAIL);

                return params;
            }
        };
        siPAD.getInstance().addToRequestQueue(postRequest);
    }

    private void show_dialog_persetujuan_daftar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_persetujuan_daftar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });

        dialog.getWindow().setAttributes(lp);
        ((Button) dialog.findViewById(R.id.bt_accept)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                get_prov();
                init_spinner();
                init_layout();
                init_animation();
                dialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Button Accept Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((Button) dialog.findViewById(R.id.bt_decline)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "Button Decline Clicked", Toast.LENGTH_SHORT).show();
                finish();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void show_dialog_sukses_daftar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_sukses_daftar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });

        dialog.getWindow().setAttributes(lp);
        ((Button) dialog.findViewById(R.id.bt_accept)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                //Toast.makeText(getApplicationContext(), "Button Accept Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }
}
