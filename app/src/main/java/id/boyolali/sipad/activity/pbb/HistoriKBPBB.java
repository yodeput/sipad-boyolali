package id.boyolali.sipad.activity.pbb;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.miguelcatalan.materialsearchview.MaterialSearchView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.pbb.KodeBayarDB.KodeBayarDB;
import id.boyolali.sipad.adapter.pbb.KodeBayarDB.KodeBayarDBAdapter;
import id.boyolali.sipad.adapter.pbb.KodeBayarDB.RealmKBAdapter;

import id.boyolali.sipad.realmdb.RealmController;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;
import io.realm.Realm;
import io.realm.RealmResults;


public class HistoriKBPBB extends AppCompatActivity {

    private static final String TAG = HistoriKBPBB.class.getSimpleName();
    private PrefManager session;
    @BindView(R.id.recycler_view)
    RecyclerView recycler;
    @BindView(R.id.txt_datakosong)
    TextView txt_datakosong;
    @BindView(R.id.cv_recycler)
    CardView cv_recycler;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;

    private View v;
    private Realm realm;
    private LayoutInflater inflater;
    private KodeBayarDBAdapter adapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setTitle("History Kode Bayar PBB");


        this.realm = RealmController.with(this).getRealm();

        setupRecycler();

        RealmController.with(this).refresh();


        setRealmAdapter(RealmController.with(this).getAllKB());
        siPAD.getInstance().fabric_report("PBB History","","");

    }

    public void setRealmAdapter(RealmResults<KodeBayarDB> db) {

        RealmKBAdapter realmAdapter = new RealmKBAdapter(this.getApplicationContext(), db, true);

        adapter.setRealmAdapter(realmAdapter);
        adapter.notifyDataSetChanged();
    }



    private void setupRecycler() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler.setHasFixedSize(true);

        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        // create an empty adapter and add it to the recycler view
        adapter = new KodeBayarDBAdapter(this);
        recycler.setAdapter(adapter);

        if (RealmController.with(this).getCountKB() > 0){

            recycler.setAdapter(adapter);

        } else {

            txt_datakosong.setVisibility(View.VISIBLE);
            txt_datakosong.setText(this.getString(R.string.data_kb_pbb_kosong));
            txt_datakosong.setTextSize(13);
            cv_recycler.setVisibility(View.GONE);

        }

    }


}