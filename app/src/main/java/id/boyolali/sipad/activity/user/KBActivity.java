package id.boyolali.sipad.activity.user;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.miguelcatalan.materialsearchview.MaterialSearchView;
import id.boyolali.sipad.util.PrefManager;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.user.KB;
import id.boyolali.sipad.adapter.user.KBAdapter;
import id.boyolali.sipad.util.siPAD;


public class KBActivity extends AppCompatActivity implements KBAdapter.KBsAdapterListener {

    private static final String TAG = KBActivity.class.getSimpleName();
  private PrefManager session;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txt_datakosong)
    TextView txt_datakosong;
    @BindView(R.id.cv_recycler)
    CardView cv_recycler;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;


    private View v;
    private List<KB> KBList;
    private KBAdapter mAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
  @Override
  public void onBackPressed() {
    if (searchView.isSearchOpen()) {
      searchView.closeSearch();
    } else {
      super.onBackPressed();
    }
  }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        ButterKnife.bind(this);

      session = new PrefManager(this.getApplicationContext());
      Toolbar toolbar = findViewById(R.id.toolbar);
      toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back);
      toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setTitle("Kode Bayar");

        KBList = new ArrayList<>();
        mAdapter = new KBAdapter(this, KBList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        String dataKB_len = session.getPref("dataKB_len");
        if (dataKB_len.equals("0")){

            txt_datakosong.setVisibility(View.VISIBLE);
            cv_recycler.setVisibility(View.GONE);

        } else {

            read_json();

        }
      searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
          mAdapter.getFilter().filter(query);
          return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
          mAdapter.getFilter().filter(newText);
          return false;
        }
      });

        siPAD.getInstance().fabric_report("User Data Kode Bayar","","");
    }

    private void read_json() {

        String jsonString = siPAD.getInstance().get_file_data(this,"dataKB");

        List<KB> items = new Gson().fromJson(jsonString, new TypeToken<List<KB>>() {
        }.getType());


        // adding contacts to contacts list
        KBList.clear();
        KBList.addAll(items);

    }

    @Override
    public void onKBSelected(KB KB) {
        //Toast.makeText(this.getApplicationContext(), "Informasi: " + KB.getKB(), Toast.LENGTH_LONG).show();
    }

    public void clear() {
        KBList.clear();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        clear();
        read_json();

        super.onResume();
    }
}