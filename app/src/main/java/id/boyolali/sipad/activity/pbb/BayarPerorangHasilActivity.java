package id.boyolali.sipad.activity.pbb;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.pbb.KodeBayarDB.KodeBayarDB;
import id.boyolali.sipad.realmdb.RealmController;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import io.realm.Realm;

import static id.boyolali.sipad.util.CurrencyFormat.info_terbilang;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;
import static id.boyolali.sipad.util.URLConfig.URL_PBB_PERORANGAN;
import static id.boyolali.sipad.util.siPAD.getInstance;

public class BayarPerorangHasilActivity extends AppCompatActivity {

    private static final String TAG = BayarPerorangHasilActivity.class.getSimpleName();
    private View v;

    private String kd_bayar;
    private Realm realm;
    private String[] PARAMETER;

    @BindView(R.id.txt_NM_WP_SPPT)
    TextView txt_NM_WP_SPPT;
    @BindView(R.id.txt_LOKASI)
    TextView txt_LOKASI;
    @BindView(R.id.txt_LUAS_BUMI_SPPT)
    TextView txt_LUAS_BUMI_SPPT;
    @BindView(R.id.txt_KD_KLS_TANAH)
    TextView txt_KD_KLS_TANAH;
    @BindView(R.id.txt_NJOP_BUMI_SPPT)
    TextView txt_NJOP_BUMI_SPPT;
    @BindView(R.id.txt_LUAS_BNG_SPPT)
    TextView txt_LUAS_BNG_SPPT;
    @BindView(R.id.txt_KD_KLS_BNG)
    TextView txt_KD_KLS_BNG;
    @BindView(R.id.txt_NJOP_BNG_SPPT)
    TextView txt_NJOP_BNG_SPPT;
    @BindView(R.id.txt_jumlah_NJOP)
    TextView txt_jumlah_NJOP;
    @BindView(R.id.txt_PBB_YG_HARUS_DIBAYAR_SPPT)
    TextView txt_PBB_YG_HARUS_DIBAYAR_SPPT;

    @BindView(R.id.txt_kodebayar)
    TextView txt_kodebayar;

    @BindView(R.id.img_logo)
    ImageView img_logo;
    @BindView(R.id.img_share)
    ImageView img_share;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_qrcode)
    ImageView img_qrcode;

    @BindView(R.id.rl_all)
    RelativeLayout rl_all;
    @BindView(R.id.cv_kd_bayar)
    CardView cv_kd_bayar;
    @BindView(R.id.sv_bawah)
    ScrollView sv_bawah;
    @BindView(R.id.rl_pemkab)
    RelativeLayout rl_pemkab;

    @BindView(R.id.txt_nmwp)
    TextView txt_nmwp;
    @BindView(R.id.txt_nop)
    TextView txt_nop;
    @BindView(R.id.txt_jttempo)
    TextView txt_jttempo;
    @BindView(R.id.txt_terhutangsppt)
    TextView txt_terhutangsppt;
    @BindView(R.id.txt_sanksi)
    TextView txt_sanksi;
    @BindView(R.id.txt_diskon)
    TextView txt_diskon;
    @BindView(R.id.txt_jumlah)
    TextView txt_jumlah;
    @BindView(R.id.txt_t_pembayaran)
    TextView txt_t_pembayaran;
    @BindView(R.id.txt_ket)
    TextView txt_ket;
    @BindView(R.id.txt_terbilang)
    TextView txt_terbilang;

    @BindView(R.id.txt_stimulus)
    TextView txt_stimulus;
    @BindView(R.id.txt_diskon2)
    TextView txt_diskon2;
    @BindView(R.id.l_6)
    TextView l_6;
    @BindView(R.id.txt_sanksi2)
    TextView txt_sanksi2;
    @BindView(R.id.txt_jumlah2)
    TextView txt_jumlah2;


    @BindView(R.id.btn_id)
    CircularProgressButton btn_id;
    @BindView(R.id.ll_diskon)
    LinearLayout ll_diskon;


    @OnClick(R.id.img_share)
    void img_share() {
        img_back.setVisibility(View.GONE);
        img_share.setVisibility(View.GONE);
        getInstance().showDialog();
        img_logo.setVisibility(View.VISIBLE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getInstance().hideDialog();
                getInstance().shareImage(getInstance().takeSS(rl_all, kd_bayar));
                img_logo.setVisibility(View.GONE);
                img_back.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }
    @OnClick(R.id.img_back) void back(){
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_perorangan_hasil);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarPerorangHasilActivity.this, v);

        this.realm = RealmController.with(this).getRealm();
        RealmController.with(this).refresh();
        sv_bawah.setVisibility(View.VISIBLE);

        Intent intent = getIntent();
        String NM_WP_SPPT = intent.getStringExtra("NM_WP_SPPT");
        String LOKASI = intent.getStringExtra("LOKASI");
        String NJOP_SPPT = intent.getStringExtra("NJOP_SPPT");
        String LUAS_BUMI_SPPT = intent.getStringExtra("LUAS_BUMI_SPPT");
        String KD_KLS_TANAH = intent.getStringExtra("KD_KLS_TANAH");
        String NJOP_BUMI_SPPT = intent.getStringExtra("NJOP_BUMI_SPPT");
        String LUAS_BNG_SPPT = intent.getStringExtra("LUAS_BNG_SPPT");
        String KD_KLS_BNG = intent.getStringExtra("KD_KLS_BNG");
        String NJOP_BNG_SPPT = intent.getStringExtra("NJOP_BNG_SPPT");
        String PBB_TERHUTANG_SPPT = intent.getStringExtra("PBB_TERHUTANG_SPPT");
        String stimulus = intent.getStringExtra("stimulus");
        String diskon = intent.getStringExtra("diskon");

        String sanksi = intent.getStringExtra("sanksi");
        String jumlah = intent.getStringExtra("jumlah");

        String PBB_YG_HARUS_DIBAYAR_SPPT = intent.getStringExtra("PBB_YG_HARUS_DIBAYAR_SPPT");
        ll_diskon.setVisibility(View.GONE);
        if (!diskon.equals("0")){
            ll_diskon.setVisibility(View.VISIBLE);
            txt_stimulus.setText(rupiah_format_string(stimulus));
            txt_diskon2.setText(rupiah_format_string(diskon));
            txt_sanksi2.setVisibility(View.GONE);
            l_6.setVisibility(View.GONE);
        }
        PARAMETER = intent.getStringArrayExtra("GET_PARAMETER");
        txt_NM_WP_SPPT.setText(NM_WP_SPPT);
        txt_LOKASI.setText(LOKASI);
        txt_LUAS_BUMI_SPPT.setText(LUAS_BUMI_SPPT);
        txt_KD_KLS_TANAH.setText(KD_KLS_TANAH);
        txt_NJOP_BUMI_SPPT.setText(NJOP_BUMI_SPPT);
        txt_LUAS_BNG_SPPT.setText(LUAS_BNG_SPPT);
        txt_KD_KLS_BNG.setText(KD_KLS_BNG);
        txt_NJOP_BNG_SPPT.setText(NJOP_BNG_SPPT);
        txt_jumlah_NJOP.setText(rupiah_format_string(NJOP_SPPT));


        txt_PBB_YG_HARUS_DIBAYAR_SPPT.setText(rupiah_format_string(PBB_YG_HARUS_DIBAYAR_SPPT));
        txt_sanksi2.setText(rupiah_format_string(sanksi));
        txt_jumlah2.setText(rupiah_format_string(jumlah));
        txt_terbilang.setText( info_terbilang(Integer.parseInt(jumlah))+" rupiah");

        btn_id.setText("Buat Kode Bayar");
        cv_kd_bayar.setVisibility(View.GONE);
        img_share.setVisibility(View.GONE);
        txt_ket.setVisibility(View.GONE);
        rl_pemkab.setVisibility(View.GONE);
        //Log.e("parararararam",URLConfig.URL_PBB_PERORANGAN + GET_PARAMETER);

    }


    @OnClick(R.id.btn_id)
    void but_kode_bayar() {

        bayar_perorangan();

    }


    private void bayar_perorangan(){

        final String tag_string_req = "req_cek_njop";
        StringRequest postRequest = new StringRequest(Request.Method.POST, URL_PBB_PERORANGAN, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                try {
                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {

                        //String data = jObj.getString("data");
                        String link = jObj.getString("link");
                        String email = jObj.getString("sendto");
                        kd_bayar = jObj.getString("kd_bayar");
                        String jatuh_tempo = jObj.getString("jatuh_tempo");
                        String namawp = jObj.getString("namawp");
                        String nop = jObj.getString("nop");
                        String yhd = jObj.getString("yhd");
                        String sanksi = jObj.getString("sanksi");
                        String diskon = jObj.getString("diskon");
                        String total = jObj.getString("total");


                        Number id = realm.where(KodeBayarDB.class).max("id");
                        int nextid = (id == null) ? 1 : id.intValue()+1;
                        KodeBayarDB db = new KodeBayarDB();
                        db.setId(nextid);
                        db.setkode_bayar(kd_bayar);
                        db.setjumlah(total);
                        db.setjenis_bayar("Bayar Perorangan");
                        realm.beginTransaction();
                        realm.copyToRealm(db);
                        realm.commitTransaction();


                        txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                        txt_nmwp.setText(namawp);
                        txt_nop.setText(nop);
                        txt_jttempo.setText(jatuh_tempo);
                        txt_terhutangsppt.setText(yhd);
                        txt_sanksi.setText(sanksi);
                        txt_diskon.setText(diskon);
                        txt_jumlah.setText(total);
                        txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                        Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                        img_qrcode.setImageBitmap(qrcode_Bitmap);

                        //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                        //txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+email);

                        sv_bawah.setVisibility(View.GONE);
                        btn_id.setProgress(100);
                        btn_id.doneLoadingAnimation(getResources().getColor(R.color.blue_700), BitmapFactory.decodeResource(getResources(), R.drawable.ic_check));
                        img_back.setVisibility(View.GONE);
                        img_share.setVisibility(View.GONE);
                        btn_id.animate().alpha(0.0f).setDuration(1000);
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                cv_kd_bayar.setVisibility(View.VISIBLE);
                                txt_ket.setVisibility(View.VISIBLE);
                                rl_pemkab.setVisibility(View.VISIBLE);
                            }
                        }, 1000);

                        Handler handler2 = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                getInstance().saveSS(rl_all, kd_bayar);
                                img_back.setVisibility(View.VISIBLE);
                                img_share.setVisibility(View.VISIBLE);
                            }
                        }, 1200);

                    } else {

                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert_finish(BayarPerorangHasilActivity.this, v, errorMsg);

                    }
                } catch (final JSONException e) {
                    e.printStackTrace();
                    Log.e(tag_string_req, e.getMessage());


                }

            }
        },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("opsi", "Bentuk Kode Bayar");
                params.put("sppt[KD_PROPINSI]", PARAMETER[0]);
                params.put("sppt[KD_DATI2]", PARAMETER[1]);
                params.put("sppt[KD_KECAMATAN]", PARAMETER[2]);
                params.put("sppt[KD_KELURAHAN]", PARAMETER[3]);
                params.put("sppt[KD_BLOK]", PARAMETER[4]);
                params.put("sppt[NO_URUT]", PARAMETER[5]);
                params.put("sppt[KD_JNS_OP]", PARAMETER[6]);
                params.put("sppt[THN_PAJAK_SPPT]", PARAMETER[7]);
                params.put("uslog[email]", PARAMETER[8]);
                params.put("usreg[TELPON]", PARAMETER[9]);
                params.put("sppt[STATUS_PEMBAYARAN_SPPT]","0");
                return params;
            }
        };
        siPAD.getInstance().addToRequestQueue(postRequest, tag_string_req);
    }

}