package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {

    @BindView(R.id.img_back)
    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);

        img_back.setOnClickListener(this);

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back: {

                finish();

            }
        }
    }

    @Override
    public void onBackPressed() {

        finish();
    }
}
