package id.boyolali.sipad.activity.pembayaran;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.user.DatausahaActivity;
import id.boyolali.sipad.util.JenisPajak;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.siPAD.getInstance;

public class BayarHasil extends AppCompatActivity {

    private static final String TAG = BayarHasil.class.getSimpleName();
    private View v;
    private String parameter,jenis_pajak,URL,kd_bayar;

    @BindView(R.id.cv_kd_bayar)
    CardView cv_kd_bayar;

    @BindView(R.id.txt_jenis_pajak)
    TextView txt_jenis_pajak;
    @BindView(R.id.txt_kodebayar)
    TextView txt_kodebayar;

    @BindView(R.id.img_share)
    ImageView img_share;
    @BindView(R.id.img_back)
    ImageView img_back;
    @BindView(R.id.img_qrcode)
    ImageView img_qrcode;

    @BindView(R.id.txt_nmwp)
    TextView txt_nmwp;
    @BindView(R.id.txt_npwpd)
    TextView txt_npwpd;
    @BindView(R.id.txt_nama_usaha)
    TextView txt_nama_usaha;
    @BindView(R.id.txt_pajak)
    TextView txt_pajak;
    @BindView(R.id.txt_sanksi)
    TextView txt_sanksi;
    @BindView(R.id.txt_jumlah)
    TextView txt_jumlah;
    @BindView(R.id.txt_t_pembayaran)
    TextView txt_t_pembayaran;

    @BindView(R.id.txt_ket)
    TextView txt_ket;
    @BindView(R.id.rl_kb)
    RelativeLayout rl_kb;
    @BindView(R.id.cv_bottom)
    CardView cv_bottom;

    @BindView(R.id.rl_all)RelativeLayout rl_all;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayar_hasil);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarHasil.this, v);

        Intent i = getIntent();
        Bundle b = i.getExtras();
        parameter = b.getString("parameter");
        //String encoded = Uri.encode(parameter, ALLOWED_URI_CHARS);
        jenis_pajak = b.getString("jenis_pajak");
        txt_jenis_pajak.setText("Pajak "+JenisPajak.cek_pajak(jenis_pajak));
        txt_jenis_pajak.setTextColor(JenisPajak.color_pajak(getApplicationContext(),jenis_pajak));
        cv_kd_bayar.setCardBackgroundColor(JenisPajak.color_pajak(getApplicationContext(),jenis_pajak));
        //parameter = encoded;
        URL=JenisPajak.url_bayar(jenis_pajak)+parameter;
        //Log.e("URLLLLLLL",URL);
        txt_ket.setVisibility(View.GONE);
        cv_bottom.setVisibility(View.GONE);

        get_kode_bayar(URL);
    }


    @OnClick(R.id.img_back)
    void back() {
        finish();
    }

    @OnClick(R.id.img_share)
    void img_share() {
        img_back.setVisibility(View.GONE);
        img_share.setVisibility(View.GONE);
        getInstance().showDialog();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                getInstance().hideDialog();
                getInstance().shareImage(getInstance().takeSS(rl_all,kd_bayar ));
                img_back.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }

    private void get_kode_bayar(String url) {
        getInstance().showDialog();
        final String tag_string_req = "get_kode_bayar";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();

                try {
                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");
                    final String msg = jObj.getString("msg");
                    if (!error) {
                        String pajakk = jObj.getString("pajak");
                        if (pajakk.equals("reklame")) {

                            getInstance().dialog_alert_finish(BayarHasil.this,v,"Pembayaran Pajak Reklame selesai\nSilahkan tunggu verifikasi data");
                            DatausahaActivity.end.finish();
                            JenisPajak.finish(jenis_pajak).finish();

                        }else if (pajakk.equals("airtanah")){

                            getInstance().dialog_alert_finish(BayarHasil.this,v,"Pembayaran Pajak Air Tanah selesai\nSilahkan tunggu verifikasi data");
                            DatausahaActivity.end.finish();
                            JenisPajak.finish(jenis_pajak).finish();

                        }else {
                            txt_ket.setVisibility(View.VISIBLE);
                            cv_bottom.setVisibility(View.VISIBLE);
                            getInstance().hideDialog();
                            String data = jObj.getString("data");
                            kd_bayar = jObj.getString("kode_bayar");
                            String tgl_pembentukan = jObj.getString("tgl_pembentukan");
                            String tgl_jatuh_tempo = jObj.getString("tgl_jatuh_tempo");
                            String namawp = jObj.getString("nama_nasabah");
                            String npwpd = jObj.getString("npwpd");
                            String nama_usaha = jObj.getString("nama_usaha");
                            String pajak = jObj.getString("pajak");
                            String sanksi = jObj.getString("sanksi");
                            String jumlah = jObj.getString("jumlah");

                            txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                            txt_nmwp.setText(namawp);
                            txt_npwpd.setText(npwpd);
                            txt_nama_usaha.setText(nama_usaha);
                            txt_pajak.setText(pajak);
                            txt_sanksi.setText(sanksi);
                            txt_jumlah.setText(jumlah);
                            txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                            Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                            img_qrcode.setImageBitmap(qrcode_Bitmap);
                            DatausahaActivity.end.finish();
                            JenisPajak.finish(jenis_pajak).finish();

                            //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                            //txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+email);

                        }
                    } else if ((error)&&(msg.equals("Denda"))){

                        cv_bottom.setVisibility(View.GONE);
                        DatausahaActivity.end.finish();
                        JenisPajak.finish(jenis_pajak).finish();
                        show_dialog_denda();

                    } else if ((error)&&(msg.equals("nonaktif"))){

                        siPAD.getInstance().dialog_alert(BayarHasil.this, v, getString(R.string.nonaktif));

                    }
                } catch (final JSONException e) {
                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(BayarHasil.this, v, BayarHasil.this.getString(R.string.error_koneksi));
                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

        };

        siPAD.getInstance().addToRequestQueue(strReq);


    }

    private void show_dialog_denda() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_info_denda);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.bt_ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                dialog.dismiss();

            }
        });


        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

}
