package id.boyolali.sipad.activity.pbb;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.pbb.KodeBayar;
import id.boyolali.sipad.adapter.pbb.KodeBayarPBBAdapter;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.siPAD.getInstance;


public class CariKodeBayarPBBHasilActivity extends AppCompatActivity implements KodeBayarPBBAdapter.KodeBayarsAdapterListener {

    private static final String TAG = CariKodeBayarPBBHasilActivity.class.getSimpleName();
    private PrefManager session;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txt_datakosong)
    TextView txt_datakosong;
    @BindView(R.id.cv_recycler)
    CardView cv_recycler;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;

    private View v;
    private List<KodeBayar> KodeBayarList;
    private KodeBayarPBBAdapter mAdapter;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        ButterKnife.bind(this);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        this.setSupportActionBar(toolbar);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setTitle("Kode Bayar PBB");

        KodeBayarList = new ArrayList<>();
        mAdapter = new KodeBayarPBBAdapter(this, KodeBayarList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        read_json();

        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

    }

    private void read_json() {
        final String tag_string_req = "read_hasil_cari_kb_pbb";
        String jsonString = siPAD.getInstance().get_file_data(this, "cari_kb_pbb");

        try {
            final JSONObject jObj = new JSONObject(jsonString);
            String data = jObj.getString("data");
            JSONArray data_array = new JSONArray(data);

            List<KodeBayar> items = new Gson().fromJson(data_array.toString(), new TypeToken<List<KodeBayar>>() {
            }.getType());

            KodeBayarList.clear();
            KodeBayarList.addAll(items);
        } catch (final JSONException e) {

            getInstance().hideDialog();
            e.printStackTrace();
            //Log.e(tag_string_req, e.getMessage());


        }
    }

    @Override
    public void onKodeBayarSelected(KodeBayar KodeBayar) {
        //Toast.makeText(this.getApplicationContext(), "Informasi: " + OPP.getOPP(), Toast.LENGTH_LONG).show();
    }

    public void clear() {
        KodeBayarList.clear();
        mAdapter.notifyDataSetChanged();
    }

}