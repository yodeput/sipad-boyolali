package id.boyolali.sipad.activity.pembayaran;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.bayar.minerba.ItemMinerba;
import id.boyolali.sipad.adapter.bayar.minerba.ItemMinerbaDB;
import id.boyolali.sipad.adapter.bayar.minerba.ItemMinerbaDBAdapter;
import id.boyolali.sipad.adapter.bayar.minerba.RealmItemMinerbaAdapter;
import id.boyolali.sipad.realmdb.RealmController;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.CurrencyFormat;
import id.boyolali.sipad.util.JenisPajak;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;
import io.realm.Realm;
import io.realm.RealmResults;
import mehdi.sakout.fancybuttons.FancyButton;

import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_integer;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;

public class BayarMinerba extends AppCompatActivity implements ItemMinerbaDBAdapter.ItemMinerbaAdapterListener {

    private View v;
    @BindView(R.id.l_jenisusaha)
    TextView l_jenisusaha;
    @BindView(R.id.l_usaha)
    TextView l_usaha;

    @BindView(R.id.edit_masapajak)
    EditText edit_masapajak;

    @BindView(R.id.rl_jml_pajak)
    RelativeLayout rl_jml_pajak;

    @BindView(R.id.recycler_view)
    RecyclerView recycler;
    @BindView(R.id.txt_datakosong)
    TextView txt_datakosong;
    @BindView(R.id.txt_jumlah_pajak)
    TextView txt_jumlah_pajak;

    @BindView(R.id.but_add_item)
    FancyButton but_add_item;

    private PrefManager session;

    private ArrayList<String> Minerbajenis;
    private ArrayList<ItemMinerba> itemMinerbaList;
    private ArrayList<ItemMinerbaDB> HitungPajakList;
    private ArrayAdapter<String> adapter_spinner;
    private List<ItemMinerbaDB> itemList;
    private ItemMinerbaDBAdapter adapter;

    private String idUsaha, namaUsaha, title, jenisUsaha, latitude, longitude, jenis_objek, jenis_pajak, status_denda = "0", s_denda_pajak, s_besaran_pajak, s_jenis_bahan, s_ukuran_jenis, s_tariff_jenis;
    private String kd_kelas, masa_pajak, masa_pajak2, kd_Rek_1 = "", kd_Rek_2 = "", kd_Rek_3 = "", kd_Rek_4 = "", kd_Rek_5 = "", address, parameter_bayar;
    private String Id_Zona, Kd_Lokasi, Lokasi, country, Area, lat, lng, administrative_area_level_1, administrative_area_level_2, administrative_area_level_3, administrative_area_level_4, postal_code;


    private int PLACE_PICKER_REQUEST = 0;
    private Realm realm;
    private LayoutInflater inflater;
    private int mLastSpinnerPosition = -1;
    private long item_hitung,item_hitung2;
    private int[] jumlah_pajak, jumlah_harga,jumlah_pajak2;
    private int total_pajak, total_harga;
    public static AppCompatActivity end;
    private boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.minerba_bayar);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarMinerba.this, v);
        session = new PrefManager(getApplicationContext());
        end = this;
        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        idUsaha = (String) bundle.get("idUsaha");
        namaUsaha = (String) bundle.get("namaUsaha");
        jenisUsaha = (String) bundle.get("jenisUsaha");
        jenis_pajak = (String) bundle.get("jenis_pajak");
        String titlee = (String) bundle.get("title");
        title = titlee.toLowerCase();

        Minerbajenis = new ArrayList<>();
        itemMinerbaList = new ArrayList<>();
        HitungPajakList = new ArrayList<>();
        itemList=new ArrayList<>();
        l_usaha.setText(namaUsaha);
        l_jenisusaha.setText(jenisUsaha);


        this.realm = RealmController.with(this).getRealm();
        RealmController.with(this).refresh();
        isConnected = ConnectivityReceiver.isOnline(this);

        getInstance().req_jenis_bahan_minerba("jenis_bahan_minerba", getApplicationContext());


        setupRecycler();
        RealmController.with(this).refresh();
        RealmController.with(this).clearPerhitunganMinerba();
        item_hitung2 = RealmController.with(this).getCountPerhitunganMinerba();
        setRealmAdapter(RealmController.with(this).getAllPerhitunganMinerba());


        adapter.setOnClickListener(new ItemMinerbaDBAdapter.OnClickListener(){
            @Override
            public void onItemClick(View view, ItemMinerbaDB obj, int pos) {
                RealmResults<ItemMinerbaDB> result = realm.where(ItemMinerbaDB.class).findAll();
                realm.beginTransaction();
                result.remove(pos);
                realm.commitTransaction();
                adapter.notifyDataSetChanged();
                siPAD.getInstance().toastSuccess(BayarMinerba.this,"Berhasil dihapus");
                HitungPajakList.remove(pos);
                item_hitung2 = HitungPajakList.size();
                int a = (int) item_hitung2;
                //Log.e("wkwkwkwkwkwk",Long.toString(item_hitung2));
                jumlah_pajak2 = new int[a];
                int total_pajak2 = 0;
                if (item_hitung2 > 0) {
                    for (int i = 0; i < a; i++) {
                        jumlah_pajak2[i] = Integer.parseInt(HitungPajakList.get(i).gettotal_pajak());
                        //Log.e("wkwkwkwkwkwk",Integer.toString(jumlah_pajak2[i]));
                    }
                    for (int i : jumlah_pajak2) {
                        total_pajak2 += i;
                    }

                    txt_datakosong.setVisibility(View.GONE);
                    recycler.setVisibility(View.VISIBLE);
                    rl_jml_pajak.setVisibility(View.VISIBLE);
                    txt_jumlah_pajak.setText(rupiah_format_integer(total_pajak2));

                } else {

                    txt_datakosong.setVisibility(View.VISIBLE);
                    txt_datakosong.setText("Silahkan tambah item Pajak Minerba");
                    txt_datakosong.setTextSize(14);
                    txt_jumlah_pajak.setText("0");
                    recycler.setVisibility(View.GONE);
                    rl_jml_pajak.setVisibility(View.GONE);
                }

            }
        });

        siPAD.getInstance().fabric_report("Bayar Minerba","","");
    }

    @OnClick(R.id.edit_masapajak)
    void date_picker1() {
        datePicker_dialog();
    }

    private void datePicker_dialog() {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(this, new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                String month = "";
                String month2 = "";
                if (String.valueOf(selectedMonth + 1).length() < 2) {
                    month = "0" + String.valueOf(selectedMonth + 1);
                } else {
                    month = String.valueOf(selectedMonth + 1);
                }
                masa_pajak = String.valueOf(selectedYear) + " - " + month;
                edit_masapajak.setText(masa_pajak);
                but_add_item.setVisibility(View.VISIBLE);
                edit_masapajak.setEnabled(false);
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setTitle("Pilih Masa Pajak")
                .setMinYear(1990)
                .setMaxYear(2030)
                .build()
                .show();
    }

    @OnClick(R.id.but_add_item)
    void tambah() {
        req_spinner_data();
        show_dialog_tambah_pajak();
        //showCustomDialog();

    }

    @OnClick(R.id.but_bayar)
    void bayar() {

        if (isConnected) {
            s_besaran_pajak=Integer.toString(total_pajak);
            s_denda_pajak="0";
            jenis_objek=Long.toString(item_hitung)+" item";
            dialog_result(namaUsaha,jenisUsaha,jenis_objek,s_besaran_pajak,s_denda_pajak, edit_masapajak.getText().toString(),jenis_pajak);

        } else {

            getInstance().dialog_alert_finish(BayarMinerba.this, v, getString(R.string.error_koneksi));

        }


    }


    public void onItemSelected(ItemMinerbaDB itemMinerba) {

    }

    public void req_spinner_data() {

        String jsonString = getInstance().get_file_data(getApplicationContext(), "jenis_bahan_minerba");
        //Log.e("zzzzzzzzzzzz", jsonString);
        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                ItemMinerba sub = new ItemMinerba();
                JSONObject jsnobject = jsonArray.getJSONObject(i);
                sub.setjn_bahan(jsnobject.getString("jn_bahan"));
                sub.setukuran(jsnobject.getString("ukuran"));
                sub.settarif(jsnobject.getString("tarif"));
                sub.setkd_rek_1(jsnobject.getString("kd_rek_1"));
                sub.setkd_rek_2(jsnobject.getString("kd_rek_2"));
                sub.setkd_rek_3(jsnobject.getString("kd_rek_3"));
                sub.setkd_rek_4(jsnobject.getString("kd_rek_4"));
                sub.setkd_rek_5(jsnobject.getString("kd_rek_5"));

                itemMinerbaList.add(sub);
                Minerbajenis.add(jsnobject.getString("jn_bahan"));

            }

            adapter_spinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Minerbajenis);
            adapter_spinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        } catch (JSONException e) {


            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void show_dialog_tambah_pajak() {
        /*
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        LayoutInflater layoutInflater = LayoutInflater.from(this);

        final View dialogView = layoutInflater.inflate(R.layout.minerba_dialog_tambah_pajak, null);
        */

        final Dialog dialogView = new Dialog(this);
        dialogView.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialogView.setContentView(R.layout.minerba_dialog_tambah_pajak);
        dialogView.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialogView.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;


        final Button dialog_left = dialogView.findViewById(R.id.dialog_left);
        final Button dialog_right = dialogView.findViewById(R.id.dialog_right);
        final MaterialSpinner spinner_jenis_bahan = dialogView.findViewById(R.id.spinner_jenis_bahan);
        final EditText edit_volume = (EditText) dialogView.findViewById(R.id.edit_volume);
        final EditText edit_jml_pajak = (EditText) dialogView.findViewById(R.id.edit_jml_pajak);
        spinner_jenis_bahan.setAdapter(adapter_spinner);
        edit_volume.setEnabled(false);

        edit_jml_pajak.setEnabled(false);
        //edit_volume.setRawInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        //builder.setView(dialogView);
        //final AlertDialog dialog = builder.create();


        dialogView.getWindow().setAttributes(lp);
        dialogView.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialogView.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;
        dialogView.show();

        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogView.dismiss();
            }
        });

        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih Jenis Bahan");
                } else if (isEditEmpty(edit_volume)) {
                    getInstance().toastError(getApplicationContext(), "Silahkan isi volume pemakaian");
                } else {
                    String volume = edit_volume.getText().toString();

                    final int v = Integer.parseInt(volume);
                    final int ukuran = Integer.parseInt(s_ukuran_jenis);
                    final int tarif = Integer.parseInt(s_tariff_jenis);
                    final int ttl_harga = ukuran * v;
                    final int ttl_pajak = tarif * v;

                    Number id = realm.where(ItemMinerbaDB.class).max("id");
                    int nextid = (id == null) ? 1 : id.intValue() + 1;
                    ItemMinerbaDB db = new ItemMinerbaDB();
                    db.setId(nextid);
                    db.setjenis_bahan(s_jenis_bahan);
                    db.setvolume(volume);
                    db.setharga(s_ukuran_jenis);
                    db.settotal_harga(Integer.toString(ttl_harga));
                    db.settotal_pajak(Integer.toString(ttl_pajak));
                    db.setkd_rek_1(kd_Rek_1);
                    db.setkd_rek_2(kd_Rek_2);
                    db.setkd_rek_3(kd_Rek_3);
                    db.setkd_rek_4(kd_Rek_4);
                    db.setkd_rek_5(kd_Rek_5);

                    HitungPajakList.add(db);
                    realm.beginTransaction();
                    realm.copyToRealm(db);
                    realm.commitTransaction();
                    realm.refresh();
                    adapter.notifyDataSetChanged();

                    item_hitung = RealmController.with(BayarMinerba.this).getCountPerhitunganMinerba();
                    setRealmAdapter(RealmController.with(BayarMinerba.this).getAllPerhitunganMinerba());
                    dialogView.dismiss();
                    int a = (int) item_hitung;
                    jumlah_pajak = new int[a];
                    total_pajak = 0;
                    if (item_hitung > 0) {

                        for (int i = 0; i < a; i++) {
                            jumlah_pajak[i] = Integer.parseInt(HitungPajakList.get(i).gettotal_pajak());

                        }


                        for (int i : jumlah_pajak) {
                            total_pajak += i;
                        }

                        txt_datakosong.setVisibility(View.GONE);
                        recycler.setVisibility(View.VISIBLE);
                        rl_jml_pajak.setVisibility(View.VISIBLE);
                        txt_jumlah_pajak.setText(rupiah_format_integer(total_pajak));
                    }
                }

            }
        });

        spinner_jenis_bahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    //getInstance().toastError(getApplicationContext(), "Anda harus memilih Jenis Bahan");
                } else {
                    int pos = spinner_jenis_bahan.getSelectedItemPosition();
                    s_jenis_bahan = itemMinerbaList.get(position).getjn_bahan();
                    s_ukuran_jenis = itemMinerbaList.get(position).getukuran();
                    s_tariff_jenis = itemMinerbaList.get(position).gettarif();
                    kd_Rek_1 = itemMinerbaList.get(position).getkd_rek_1();
                    kd_Rek_2 = itemMinerbaList.get(position).getkd_rek_2();
                    kd_Rek_3 = itemMinerbaList.get(position).getkd_rek_3();
                    kd_Rek_4 = itemMinerbaList.get(position).getkd_rek_4();
                    kd_Rek_5 = itemMinerbaList.get(position).getkd_rek_5();

                    //Log.e("jenis_objek",jenis_objek);
                    edit_volume.setEnabled(true);
                    edit_volume.requestFocus();


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edit_jml_pajak.setText(rupiah_format_integer(0));
        edit_volume.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (count > 0) {
                    int v = Integer.parseInt(edit_volume.getText().toString());
                    int ukuran = Integer.parseInt(s_ukuran_jenis);
                    int harga = v * ukuran;

                    edit_jml_pajak.setText(rupiah_format_integer(harga));
                } else {
                    edit_jml_pajak.setText(rupiah_format_integer(0));

                }


            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setRealmAdapter(RealmResults<ItemMinerbaDB> db) {

        RealmItemMinerbaAdapter realmAdapter = new RealmItemMinerbaAdapter(this.getApplicationContext(), db, true);

        adapter.setRealmAdapter(realmAdapter);
        adapter.notifyDataSetChanged();

    }

    private void setupRecycler() {
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recycler.setHasFixedSize(true);

        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler.setLayoutManager(layoutManager);

        // create an empty adapter and add it to the recycler view
        adapter = new ItemMinerbaDBAdapter(this, itemList,this);
        recycler.setAdapter(adapter);

        if (item_hitung > 0) {

            recycler.setAdapter(adapter);

        } else {

            txt_datakosong.setVisibility(View.VISIBLE);
            txt_datakosong.setText("Silahkan tambah item Pajak Minerba");
            txt_datakosong.setTextSize(14);
            recycler.setVisibility(View.GONE);

        }

    }

    private void param_bayar() {

        String params = "";
        String Omzet = "";
        Omzet = Omzet.replace("Rp", "");
        Omzet = Omzet.replace(".", "");
        masa_pajak = edit_masapajak.getText().toString();
        masa_pajak = masa_pajak.replace(" ", "");

        String ST_TEMPO = "ST_TEMPO[STATUS]=" + status_denda;

        item_hitung = RealmController.with(BayarMinerba.this).getCountPerhitunganMinerba();
        setRealmAdapter(RealmController.with(BayarMinerba.this).getAllPerhitunganMinerba());
        int a = (int) item_hitung;
        String[] dataArray = new String[a];
        jumlah_pajak = new int[a];
        jumlah_harga = new int[a];

        for (int i = 0; i < a; i++) {
            jumlah_pajak[i] = Integer.parseInt(HitungPajakList.get(i).gettotal_pajak());
            jumlah_harga[i] = Integer.parseInt(HitungPajakList.get(i).gettotal_harga());
            String URAIAN = "data[" + i + "][URAIAN]=" + HitungPajakList.get(i).getjenis_bahan();
            String VOLUME = "data[" + i + "][VOLUME]=" + HitungPajakList.get(i).getvolume();
            String HARGA = "&data[" + i + "][HARGA]=" + HitungPajakList.get(i).getharga();
            String TOTAL_HARGA = "&data[" + i + "][TOTAL_HARGA]=" + HitungPajakList.get(i).gettotal_harga();
            String PAJAK = "&data[" + i + "][PAJAK]=" + HitungPajakList.get(i).gettotal_pajak();
            String TOTAL_PAJAK = "&data[" + i + "][TOTAL_PAJAK]=" + HitungPajakList.get(i).gettotal_pajak();
            String KD_Rek_1 = "&data[" + i + "][kd_rek_1]=" + HitungPajakList.get(i).getkd_rek_1();
            String KD_Rek_2 = "&data[" + i + "][kd_rek_2]=" + HitungPajakList.get(i).getkd_rek_2();
            String KD_Rek_3 = "&data[" + i + "][kd_rek_3]=" + HitungPajakList.get(i).getkd_rek_3();
            String KD_Rek_4 = "&data[" + i + "][kd_rek_4]=" + HitungPajakList.get(i).getkd_rek_4();
            String KD_Rek_5 = "&data[" + i + "][kd_rek_5]=" + HitungPajakList.get(i).getkd_rek_5() + "d";

            dataArray[i] = URAIAN + VOLUME + HARGA + TOTAL_HARGA + PAJAK + TOTAL_PAJAK + KD_Rek_1 + KD_Rek_2 + KD_Rek_3 + KD_Rek_4 + KD_Rek_5;

        }

        total_harga = 0;
        for (int i : jumlah_harga) {
            total_harga += i;
        }

        String URAIAN_TARIF = "&nota[URAIAN_TARIF]=Pajak Minerba " + masa_pajak;
        String TGL_NOTA = "&nota[TGL_NOTA]=" + masa_pajak + "-01";
        String USAHA_ID = "&nota[USAHA_ID]=" + idUsaha;
        String JENIS_PAJAK = "&nota[JENIS_PAJAK]=" + jenis_pajak;
        String JUMLAH_HARGA = "&nota[JUMLAH_HARGA]=" + total_harga;
        String JUMLAH_PAJAK = "&nota[JUMLAH_PAJAK]=" + total_pajak;
        String JENIS_PERMOHONAN = "&nota[JENIS_PERMOHONAN]=" + "0";


        String s_param1 = Arrays.toString(dataArray);
        String s_param2 = URAIAN_TARIF + TGL_NOTA + USAHA_ID + JENIS_PAJAK + JUMLAH_HARGA + JUMLAH_PAJAK + JENIS_PERMOHONAN;
        s_param1 = s_param1.replace(", ", "&");
        s_param1 = s_param1.replace("[d", "d");
        s_param1 = s_param1.replace("d]", "");
        s_param1 = s_param1.replace("d,", "");
        parameter_bayar = s_param1 + s_param2;

        show_dialog_persetujuan_bayar(parameter_bayar);
    }


    public void dialog_result(final String namaUsaha, String jenisUsaha, String jenis_objek, String s_besaran_pajak, String s_denda_pajak, final String masa_pajak, String jenis_pajak) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(this).inflate(R.layout.activity_bayar_hasil_hitung,
                (ViewGroup) v.findViewById(android.R.id.content), false);

        final TextView txt_jns_pajak = viewInflated.findViewById(R.id.txt_jns_pajak);
        final TextView txt_nm_usaha = viewInflated.findViewById(R.id.txt_nm_usaha);
        final TextView txt_jns_usaha = viewInflated.findViewById(R.id.txt_jns_usaha);
        final TextView L_7 = viewInflated.findViewById(R.id.L_7);
        final TextView txt_kategori = viewInflated.findViewById(R.id.txt_kategori);
        final TextView txt_masapajak = viewInflated.findViewById(R.id.txt_masapajak);
        final TextView txt_besaran_pajak = viewInflated.findViewById(R.id.txt_besaran_pajak);
        final TextView txt_denda_pajak = viewInflated.findViewById(R.id.txt_denda_pajak);
        final TextView txt_terbilang = viewInflated.findViewById(R.id.txt_terbilang);
        final ImageView img_share = viewInflated.findViewById(R.id.img_share);
        final Button dialog_button_ok = viewInflated.findViewById(R.id.dialog_button_ok);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        final LinearLayout ll_2_button = viewInflated.findViewById(R.id.ll_2_button);

        dialog_button_ok.setText("OK");
        dialog_left.setText("Kembali");
        dialog_right.setText("Bayar");

        txt_jns_pajak.setText("Pajak " + JenisPajak.cek_pajak(jenis_pajak));
        txt_nm_usaha.setText(namaUsaha);
        txt_jns_usaha.setText(jenisUsaha);
        txt_kategori.setText(jenis_objek);
        txt_masapajak.setText(masa_pajak);
        L_7.setText("Item Pajak");

        txt_besaran_pajak.setText(CurrencyFormat.rupiah_format_string(s_besaran_pajak));
        int denda = Integer.parseInt(s_denda_pajak);
        if (denda == 0) {
            status_denda = "0";
            txt_denda_pajak.setText(s_denda_pajak);
        } else {
            s_denda_pajak = CurrencyFormat.rupiah_format_string(s_denda_pajak);
            txt_denda_pajak.setText(s_denda_pajak);
            status_denda = "1";
        }


        txt_terbilang.setText("(" + CurrencyFormat.info_terbilang(Integer.parseInt(s_besaran_pajak)) + " rupiah)");


        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });

        if (session.isLoggedIn()) {
            dialog_button_ok.setVisibility(View.GONE);
            ll_2_button.setVisibility(View.VISIBLE);

        } else {
            dialog_button_ok.setVisibility(View.VISIBLE);
            ll_2_button.setVisibility(View.GONE);

        }

        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInstance().showDialog();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        getInstance().hideDialog();
                        param_bayar();
                        dialog.dismiss();

                    }
                }, 1000);

            }
        });
        dialog_button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_button.setVisibility(View.GONE);
                img_share.setVisibility(View.GONE);
                dialog_button_ok.setVisibility(View.GONE);
                getInstance().shareImage(getInstance().takeSS(viewInflated, namaUsaha + "_" + masa_pajak));
                dialog_button_ok.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        });


    }


    private void show_dialog_persetujuan_bayar(final String param) {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_persetujuan_bayar);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.show();
        dialog.getWindow().setAttributes(lp);

        ((Button) dialog.findViewById(R.id.bt_accept)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent i = new Intent(BayarMinerba.this, BayarHasil.class);
                i.putExtra("parameter", param);
                i.putExtra("jenis_pajak", jenis_pajak);
                startActivity(i);

                //Toast.makeText(getApplicationContext(), "Button Accept Clicked", Toast.LENGTH_SHORT).show();
            }
        });

        ((Button) dialog.findViewById(R.id.bt_decline)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "Button Decline Clicked", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });


    }


    @Override
    public void onBackPressed() {
        if (!isEditEmpty(edit_masapajak)) {

            getInstance().dialog_alert_batal_proses(BayarMinerba.this, v, BayarMinerba.this.getString(R.string.batal_bayar));
            return;
        }
        super.onBackPressed();
    }

}
