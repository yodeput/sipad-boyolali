package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.content.Intent;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.pbb.BayarPBBActivity;
import id.boyolali.sipad.activity.pbb.CariKodeBayarPBBActivity;
import id.boyolali.sipad.activity.pbb.CekNJOPActivity;
import id.boyolali.sipad.activity.pbb.CekTagihanPBBActivity;
import id.boyolali.sipad.activity.pbb.HistoriKBPBB;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

public class MenuPopupPBB extends AppCompatActivity{
    private static final String TAG = MenuPopupPBB.class.getSimpleName();
    @BindView(R.id.but_pembayaran)
    ImageButton but_pembayaran;
    @BindView(R.id.but_infotagihan)
    ImageButton but_infotagihan;
    @BindView(R.id.but_njop)
    ImageButton but_njop;
    @BindView(R.id.but_cari_kb)
    ImageButton but_cari_kb;
    @BindView(R.id.but_history_kb)
    ImageButton but_history_kb;


    @BindView(R.id.but_close)
    Button but_close;
    @BindView(R.id.icon_title)
    ImageView icon_title;
    @BindView(R.id.text_title)
    TextView text_title;
    @BindView(R.id.ln_background)
    LinearLayout ln_background;
    @BindView(R.id.div_menu_pajak)
    ImageView div_menu_pajak;
    private Class newAct;
    private String title;
    private PrefManager session;
    private View v;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_menu);
        ButterKnife.bind(this);
        session = new PrefManager(getApplicationContext());
        initView();
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        String tap_pbb = session.getPref("tap_pbb");
        if (tap_pbb.isEmpty()){
            taptarget_but_njop();
        }

    }

    private void initView() {
        icon_title.setColorFilter(ContextCompat.getColor(this, R.color.hijau));
        div_menu_pajak.setBackgroundColor(ContextCompat.getColor(this, R.color.hijau));
        ln_background.setBackgroundResource(R.drawable.rounded_corner_pbb);

    }

    @OnClick(R.id.but_close) void close(){
        finish();
    }

    @OnClick(R.id.but_pembayaran)void bayar(){


        Intent i = new Intent(this,BayarPBBActivity.class);
        cek("PBB",i);

    }
    @OnClick(R.id.but_infotagihan)void infotagihan(){

        startActivity(new Intent(this,CekTagihanPBBActivity.class));

    }
    @OnClick(R.id.but_njop)void cekNJOP(){
        startActivity(new Intent(this, CekNJOPActivity.class));
    }

    @OnClick(R.id.but_cari_kb) void but_cari_kb(){

        startActivity(new Intent(this, CariKodeBayarPBBActivity.class));


    }

    @OnClick(R.id.but_history_kb) void but_history_kb(){

        startActivity(new Intent(this, HistoriKBPBB.class));


    }

    @OnClick(R.id.rl_background) void rl_background(){


    }

    private void taptarget_but_njop(){
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.but_njop),
                        "CEK NJOP",
                        "Untuk melihat data Nilai Jual Objek Pajak")
                        .titleTextColor(R.color.black)
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.black)
                        .outerCircleColor(R.color.green_500)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        session.addPref("tap_pbb","false");
                        taptarget_but_infotagihan();
                    }
                });
    }

    private void taptarget_but_infotagihan(){
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.but_infotagihan),
                        "Tagihan",
                        "Pilih menu ini untuk melihat tagihan Pajak yang belum dibayar")
                        .titleTextColor(R.color.black)
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.black)
                        .outerCircleColor(R.color.blue_500)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        session.addPref("tap_pbb","false");
                        taptarget_but_bayar();
                    }
                });
    }


    private void taptarget_but_bayar(){
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.but_pembayaran),
                        "Pembayaran",
                        "Akan muncul 2 opsi Pembayaran.\nPembayaran Perorangan dan Pembayaran Kolektif")
                        .titleTextColor(R.color.black)
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.black)
                        .outerCircleColor(R.color.orange_500)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        session.addPref("tap_pbb","false");
                        taptarget_but_history();
                    }
                });
    }

    private void taptarget_but_history(){
        TapTargetView.showFor(this,
                TapTarget.forView(findViewById(R.id.but_history_kb),
                        "Riwayat Pembayaran",
                        "Setiap proses bayar Pajak PBB yang dilakukan diaplikasi ini akan muncul di menu ini")
                        .titleTextColor(R.color.black)
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.black)
                        .outerCircleColor(R.color.pink_500)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        session.addPref("tap_pbb","false");

                    }
                });
    }

    private void cek(String jenis_pajak, Intent i){

        String status = siPAD.getInstance().see_status_pajak(jenis_pajak);
        if (status.equals("0")){
            siPAD.getInstance().toastInfo(MenuPopupPBB.this,"Layanan sedang dalam perbaikan");
        } else if (status.equals("1")){
            startActivity(i);
        }

    }
}
