package id.boyolali.sipad.activity.pbb;

import static id.boyolali.sipad.util.URLConfig.tahun_sppt;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_integer;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class CekNJOPActivity extends AppCompatActivity {

    private static final String TAG = CekNJOPActivity.class.getSimpleName();
    private View v;

    @BindView(R.id.spinner_thnSPPT)
    MaterialSpinner spinner_thnSPPT;
    @BindView(R.id.KD_PROPINSI)
    EditText KD_PROPINSI;
    @BindView(R.id.KD_DATI2)
    EditText KD_DATI2;
    @BindView(R.id.KD_KECAMATAN)
    EditText KD_KECAMATAN;
    @BindView(R.id.KD_KELURAHAN)
    EditText KD_KELURAHAN;
    @BindView(R.id.KD_BLOK)
    EditText KD_BLOK;
    @BindView(R.id.NO_URUT)
    EditText NO_URUT;
    @BindView(R.id.KD_JNS_OP)
    EditText KD_JNS_OP;

    @BindView(R.id.edit_nop)
    EditText edit_nop;

    public static String s_thnSPPT = "SPPT", s_KD_PROPINSI, s_KD_DATI2, s_KD_KECAMATAN, s_KD_KELURAHAN, s_KD_BLOK, s_NO_URUT, s_KD_JNS_OP;
    private String get_paramenters;
    private int mLastSpinnerPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_cek_njop);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(CekNJOPActivity.this, v);

        init_spinner();
        init_edit_nop();


        siPAD.getInstance().fabric_report("PBB Cek NJOP","","");
    }


    @OnClick(R.id.img_back)
    void back() {
        finish();
    }

    @OnClick(R.id.but_cek)
    void cek() {


        if (s_thnSPPT.contains("SPPT")) {

            getInstance().toastError(getApplicationContext(), "Anda harus pilih tahun SPPT");
            spinner_thnSPPT.requestFocus();
            spinner_thnSPPT.setError("Anda harus pilih tahun SPPT");


        } else if (isEditEmpty(edit_nop)) {

            getInstance().toastError(getApplicationContext(), "NOP harus diisi");
            edit_nop.requestFocus();

        } else {


            parameter();



        }
    }


    private void cari_data() {
        final String tag_string_req = "req_cek_njop";
        getInstance().showDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_CEKNJOP + get_paramenters, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                Log.e(tag_string_req, response);
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {

                        String data = jObj.getString("data");
                        JSONArray data_array = new JSONArray(data);
                        JSONObject dataObject = data_array.getJSONObject(0);
                        String NM_WP_SPPT = dataObject.getString("NM_WP_SPPT");
                        String JLN_WP_SPPT = dataObject.getString("JLN_WP_SPPT");
                        String RW_WP_SPPT = dataObject.getString("RW_WP_SPPT");
                        String RT_WP_SPPT = dataObject.getString("RT_WP_SPPT");
                        String KOTA_WP_SPPT = dataObject.getString("KOTA_WP_SPPT");
                        String NJOP_BUMI_SPPT = dataObject.getString("NJOP_BUMI_SPPT");
                        String LUAS_BUMI_SPPT = dataObject.getString("LUAS_BUMI_SPPT");
                        String NJOP_BNG_SPPT = dataObject.getString("NJOP_BNG_SPPT");
                        String LUAS_BNG_SPPT = dataObject.getString("LUAS_BNG_SPPT");
                        String THN_PAJAK_SPPT = dataObject.getString("THN_PAJAK_SPPT");

                        int njop_bumi = 0;
                        int njop_bangunan = 0;
                        String s_njop_bumi = "";
                        String s_njop_bangunan = "";
                        String a = dataObject.getString("JLN_WP_SPPT");
                        String b = dataObject.getString("BLOK_KAV_NO_WP_SPPT");
                        String c = dataObject.getString("RT_WP_SPPT");
                        String d = dataObject.getString("RW_WP_SPPT");
                        String e = dataObject.getString("KELURAHAN_WP_SPPT");

                        if (a.contains("null")){a="";}else if (b.contains("null")){b="";}
                        else if (c.contains("null")){c="-";}else if (d.contains("null")){d="-";}
                        else if (e.contains("null")){e="-";}

                        String LOKASI = a+""+b+" RT. "+c+" RW. "+d+" KEL. "+e+"\n" + KOTA_WP_SPPT;

                        int luas_bumi = Integer.parseInt(LUAS_BUMI_SPPT);
                        int luas_bng = Integer.parseInt(LUAS_BNG_SPPT);
                        if ((luas_bng>0)&&(luas_bumi>0)){

                            njop_bangunan = Integer.parseInt(NJOP_BNG_SPPT) / Integer.parseInt(LUAS_BNG_SPPT);
                            s_njop_bangunan = rupiah_format_integer(njop_bangunan);
                            NJOP_BNG_SPPT = rupiah_format_string(NJOP_BNG_SPPT);

                            njop_bumi = Integer.parseInt(NJOP_BUMI_SPPT) / Integer.parseInt(LUAS_BUMI_SPPT);
                            s_njop_bumi = rupiah_format_integer(njop_bumi);
                            NJOP_BUMI_SPPT = rupiah_format_string(NJOP_BUMI_SPPT);
                        } else if ((luas_bng==0)&&(luas_bumi>0)){
                            s_njop_bangunan = "-";
                            NJOP_BNG_SPPT = "-";
                            njop_bumi = Integer.parseInt(NJOP_BUMI_SPPT) / Integer.parseInt(LUAS_BUMI_SPPT);
                            s_njop_bumi = rupiah_format_integer(njop_bumi);
                            NJOP_BUMI_SPPT = rupiah_format_string(NJOP_BUMI_SPPT);

                        } else if ((luas_bng>0)&&(luas_bumi==0)){

                            s_njop_bumi = "-";
                            NJOP_BUMI_SPPT = "-";
                            njop_bangunan = Integer.parseInt(NJOP_BNG_SPPT) / Integer.parseInt(LUAS_BNG_SPPT);
                            s_njop_bangunan = rupiah_format_integer(njop_bangunan);
                            NJOP_BNG_SPPT = rupiah_format_string(NJOP_BNG_SPPT);
                        }  else if ((luas_bng==0)&&(luas_bumi==0)){

                            s_njop_bumi = "-";
                            NJOP_BUMI_SPPT = "-";
                            s_njop_bangunan = "-";
                            NJOP_BNG_SPPT = "-";

                        }



                        dialog_result(NM_WP_SPPT, LOKASI, NJOP_BUMI_SPPT, s_njop_bumi, NJOP_BNG_SPPT, s_njop_bangunan, THN_PAJAK_SPPT);


                    } else {

                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert(CekNJOPActivity.this, v, errorMsg);

                    }
                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(CekNJOPActivity.this, v, CekNJOPActivity.this.getString(R.string.error_koneksi));
                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("KD_PROPINSI", s_KD_PROPINSI);
                params.put("KD_DATI2", s_KD_DATI2);
                params.put("KD_KECAMATAN", s_KD_KECAMATAN);
                params.put("KD_KELURAHAN", s_KD_KELURAHAN);
                params.put("KD_BLOK", s_KD_BLOK);
                params.put("NO_URUT", s_NO_URUT);
                params.put("KD_JNS_OP", s_KD_JNS_OP);
                params.put("THN_PAJAK_SPPT", s_thnSPPT);
                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);

    }

    public void dialog_result(final String nm_wp, String lokasi, String total_bumi, String bumi, String total_bangunan, String bangunan, final String tahun_pajak) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(this).inflate(R.layout.activity_pbb_cek_njop_hasil,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView txt_nm_wp = viewInflated.findViewById(R.id.txt_nm_wp);
        final TextView txt_lokasi = viewInflated.findViewById(R.id.txt_jns_usaha);
        final TextView txt_tahun_pajak = viewInflated.findViewById(R.id.txt_kategori);
        final TextView txt_total_bumi = viewInflated.findViewById(R.id.txt_besaran_pajak);
        final TextView txt_bumi = viewInflated.findViewById(R.id.txt_denda_pajak);
        final TextView txt_total_bangunan = viewInflated.findViewById(R.id.txt_total_bangunan);
        final TextView txt_bangunan = viewInflated.findViewById(R.id.txt_bangunan);
        final ImageView img_share = viewInflated.findViewById(R.id.img_share);
        final Button dialog_button1 = viewInflated.findViewById(R.id.dialog_button1);
        txt_nm_wp.setText(nm_wp);
        txt_lokasi.setText(lokasi);
        txt_tahun_pajak.setText(tahun_pajak);
        txt_total_bumi.setText(total_bumi);
        txt_bumi.setText(bumi);
        txt_total_bangunan.setText(total_bangunan);
        txt_bangunan.setText(bangunan);

        dialog_button1.setText("OK");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });
        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_share.setVisibility(View.GONE);
                getInstance().shareImage(getInstance().takeSS(viewInflated, nm_wp + "_" + tahun_pajak));
                img_share.setVisibility(View.VISIBLE);
            }
        });


    }

    private void parameter() {

        String ss = edit_nop.getText().toString();
        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

        if ((ss.length()<18)||(ss.length()>18)){

            getInstance().toastError(CekNJOPActivity.this,"Format NOP salah, cek kembali");
            edit_nop.requestFocus();

        } else {

            s_KD_PROPINSI = ss.substring(0,2);
            s_KD_DATI2 = ss.substring(2,4);
            s_KD_KECAMATAN = ss.substring(4,7);
            s_KD_KELURAHAN = ss.substring(7,10);
            s_KD_BLOK = ss.substring(10,13);
            s_NO_URUT =ss.substring(13,17);
            s_KD_JNS_OP = ss.substring(17);

            get_paramenters =
                    "KD_PROPINSI=" + s_KD_PROPINSI + "&KD_DATI2=" + s_KD_DATI2 + "&KD_KECAMATAN="
                            + s_KD_KECAMATAN + "&KD_KELURAHAN=" + s_KD_KELURAHAN + "&KD_BLOK=" + s_KD_BLOK
                            + "&NO_URUT=" + s_NO_URUT + "&KD_JNS_OP=" + s_KD_JNS_OP + "&THN_PAJAK_SPPT="
                            + s_thnSPPT;
            cari_data();

        }



    }

    private void init_spinner() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tahun_sppt);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_thnSPPT.setAdapter(adapter);
        spinner_thnSPPT.requestFocus();
        edit_nop.clearFocus();


        spinner_thnSPPT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()

        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih tahun SPPT");
                } else {
                    s_thnSPPT = spinner_thnSPPT.getItemAtPosition(position).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    private void init_edit_nop_old(){
        getInstance().txtwatcher(KD_PROPINSI, KD_DATI2, 2);
        getInstance().txtwatcher(KD_DATI2, KD_KECAMATAN, 2);
        getInstance().txtwatcher(KD_KECAMATAN, KD_KELURAHAN, 3);
        getInstance().txtwatcher(KD_KELURAHAN, KD_BLOK, 3);
        getInstance().txtwatcher(KD_BLOK, NO_URUT, 3);
        getInstance().txtwatcher(NO_URUT, KD_JNS_OP, 4);
    }

    private void init_edit_nop(){

        PatternedTextWatcher patternedTextWatcher = new PatternedTextWatcher.Builder("##.##.###.###.###-####.#")
                .fillExtraCharactersAutomatically(true)
                .deleteExtraCharactersAutomatically(true)
                .respectPatternLength(true)
                .saveAllInput(false)

                .build();
        edit_nop.addTextChangedListener(patternedTextWatcher);
        edit_nop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count==24)
                    siPAD.hideSoftKeyboard(CekNJOPActivity.this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edit_nop.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isEditEmpty(edit_nop)) {
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        String ss = clipboard.getText().toString();
                        //Log.e(TAG, ss);
                        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

                        if ((ss.length() < 18) || (ss.length() > 18)) {
                            getInstance().toastError(CekNJOPActivity.this, CekNJOPActivity.this.getString(R.string.error_nop));
                            return false;
                        } else {

                            edit_nop.setText(ss);
                        }

                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

                        String ss = item.getText().toString();
                        //Log.e(TAG, ss);
                        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

                        if ((ss.length() < 18) || (ss.length() > 18)) {
                            getInstance().toastError(CekNJOPActivity.this, "Yang Anda salin bukan text NOP");
                            return false;
                        } else {

                            edit_nop.setText(ss);
                        }
                    }
                }
                return false;
            }
        });

        edit_nop.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                menu.close();
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {

            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                menu.close();
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode,
                                               MenuItem item) {
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getInstance().init_p_dialog(CekNJOPActivity.this,v);
    }
}
