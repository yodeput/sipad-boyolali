package id.boyolali.sipad.activity.pbb;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.szagurskii.patternedtextwatcher.PatternedTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;

public class CekTagihanPBBActivity extends AppCompatActivity {

    private static final String TAG = CekTagihanPBBActivity.class.getSimpleName();
    private View v;

    @BindView(R.id.KD_PROPINSI)
    EditText KD_PROPINSI;
    @BindView(R.id.KD_DATI2)
    EditText KD_DATI2;
    @BindView(R.id.KD_KECAMATAN)
    EditText KD_KECAMATAN;
    @BindView(R.id.KD_KELURAHAN)
    EditText KD_KELURAHAN;
    @BindView(R.id.KD_BLOK)
    EditText KD_BLOK;
    @BindView(R.id.NO_URUT)
    EditText NO_URUT;
    @BindView(R.id.KD_JNS_OP)
    EditText KD_JNS_OP;

    @BindView(R.id.edit_nop)
    EditText edit_nop;
    @BindView(R.id.txt_title)
    TextView txt_title;
    public static String s_thnSPPT = "SPPT", s_KD_PROPINSI, s_KD_DATI2, s_KD_KECAMATAN, s_KD_KELURAHAN, s_KD_BLOK, s_NO_URUT, s_KD_JNS_OP;
    private String get_paramenters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_cek_tagihan);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(CekTagihanPBBActivity.this, v);

        txt_title.setText("Cek Tagihan");
        init_edit_nop();

        siPAD.getInstance().fabric_report("PBB Cek Tagihan ","","");
    }

    private void init_edit_nop_old() {
        getInstance().txtwatcher(KD_PROPINSI, KD_DATI2, 2);
        getInstance().txtwatcher(KD_DATI2, KD_KECAMATAN, 2);
        getInstance().txtwatcher(KD_KECAMATAN, KD_KELURAHAN, 3);
        getInstance().txtwatcher(KD_KELURAHAN, KD_BLOK, 3);
        getInstance().txtwatcher(KD_BLOK, NO_URUT, 3);
        getInstance().txtwatcher(NO_URUT, KD_JNS_OP, 4);
    }

    private void init_edit_nop() {

        PatternedTextWatcher patternedTextWatcher = new PatternedTextWatcher.Builder("##.##.###.###.###-####.#")
                .fillExtraCharactersAutomatically(true)
                .deleteExtraCharactersAutomatically(true)
                .respectPatternLength(true)
                .saveAllInput(false)

                .build();
        edit_nop.addTextChangedListener(patternedTextWatcher);edit_nop.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count==24)
                    siPAD.hideSoftKeyboard(CekTagihanPBBActivity.this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edit_nop.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (isEditEmpty(edit_nop)) {
                    int sdk = android.os.Build.VERSION.SDK_INT;
                    if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                        android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        String ss = clipboard.getText().toString();
                        //Log.e(TAG, ss);
                        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

                        if ((ss.length() < 18) || (ss.length() > 18)) {
                            getInstance().toastError(CekTagihanPBBActivity.this, CekTagihanPBBActivity.this.getString(R.string.error_nop));
                            return false;
                        } else {

                            edit_nop.setText(ss);
                        }

                    } else {
                        android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        android.content.ClipData.Item item = clipboard.getPrimaryClip().getItemAt(0);

                        String ss = item.getText().toString();
                        //Log.e(TAG, ss);
                        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");

                        if ((ss.length() < 18) || (ss.length() > 18)) {
                            getInstance().toastError(CekTagihanPBBActivity.this, "Yang Anda salin bukan text NOP");
                            return false;
                        } else {

                            edit_nop.setText(ss);
                        }
                    }
                }
                return false;
            }
        });

        edit_nop.setCustomSelectionActionModeCallback(new ActionMode.Callback() {
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                menu.close();
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {

            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                menu.clear();
                menu.close();
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode,
                                               MenuItem item) {
                return false;
            }
        });

    }


    @OnClick(R.id.img_back)
    void back() {
        finish();
    }

    @OnClick(R.id.but_cek)
    void cek() {

        if (isEditEmpty(edit_nop)) {

            getInstance().toastError(getApplicationContext(), "NOP harus diisi");
            edit_nop.requestFocus();

        } else {


            parameter();


        }
    }


    private void cari_data() {
        final String tag_string_req = "req_cek_njop";
        getInstance().showDialog();
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_CEKTAGIHAN + get_paramenters, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");
                    //Log.e("testtttttt",response);

                    if (!error) {

                        getInstance().make_file_data(getApplicationContext(), "cekTagihan", response);
                        startActivity(new Intent(getApplication(), CekTagihanPBBHasilActivity.class));


                    } else {

                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert(CekTagihanPBBActivity.this, v, errorMsg);

                    }
                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(CekTagihanPBBActivity.this, v, CekTagihanPBBActivity.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("KD_PROPINSI", s_KD_PROPINSI);
                params.put("KD_DATI2", s_KD_DATI2);
                params.put("KD_KECAMATAN", s_KD_KECAMATAN);
                params.put("KD_KELURAHAN", s_KD_KELURAHAN);
                params.put("KD_BLOK", s_KD_BLOK);
                params.put("NO_URUT", s_NO_URUT);
                params.put("KD_JNS_OP", s_KD_JNS_OP);
                params.put("THN_PAJAK_SPPT", s_thnSPPT);
                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);

    }


    private void parameter() {

        String ss = edit_nop.getText().toString();
        ss = ss.replaceAll("[-\\[\\]^/,'*:.!><~@#$%+=?|\"\\\\()]+", "");
        if ((ss.length() < 18) || (ss.length() > 18)) {

            getInstance().toastError(CekTagihanPBBActivity.this, "Format NOP salah, cek kembali");
            edit_nop.requestFocus();
        } else {
            s_KD_PROPINSI = ss.substring(0,2);
            s_KD_DATI2 = ss.substring(2,4);
            s_KD_KECAMATAN = ss.substring(4,7);
            s_KD_KELURAHAN = ss.substring(7,10);
            s_KD_BLOK = ss.substring(10,13);
            s_NO_URUT =ss.substring(13,17);
            s_KD_JNS_OP = ss.substring(17);

            get_paramenters =
                    "KD_PROPINSI=" + s_KD_PROPINSI + "&KD_DATI2=" + s_KD_DATI2 + "&KD_KECAMATAN="
                            + s_KD_KECAMATAN + "&KD_KELURAHAN=" + s_KD_KELURAHAN + "&KD_BLOK=" + s_KD_BLOK
                            + "&NO_URUT=" + s_NO_URUT + "&KD_JNS_OP=" + s_KD_JNS_OP + "&STATUS_PEMBAYARAN_SPPT=0";
            cari_data();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getInstance().init_p_dialog(CekTagihanPBBActivity.this, v);
    }
}
