package id.boyolali.sipad.activity.pbb;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.layer_net.stepindicator.StepIndicator;

import net.glxn.qrgen.android.QRCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.keckel.kecamatan;
import id.boyolali.sipad.adapter.keckel.kelurahan;
import id.boyolali.sipad.adapter.pbb.KodeBayarDB.KodeBayarDB;
import id.boyolali.sipad.adapter.pbb.NOP;
import id.boyolali.sipad.adapter.pbb.NOPAdapter;
import id.boyolali.sipad.realmdb.RealmController;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import io.realm.Realm;
import mehdi.sakout.fancybuttons.FancyButton;

import static id.boyolali.sipad.adapter.pbb.NOPAdapter.selectedCount;
import static id.boyolali.sipad.adapter.pbb.NOPAdapter.selected_nop;
import static id.boyolali.sipad.util.CurrencyFormat.info_terbilang;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_integer;
import static id.boyolali.sipad.util.CurrencyFormat.rupiah_format_string;
import static id.boyolali.sipad.util.URLConfig.URL_PBB_KOLEKTIF;
import static id.boyolali.sipad.util.URLConfig.URL_PBB_KOLEKTIF2;
import static id.boyolali.sipad.util.URLConfig.URL_PBB_PERORANGAN;
import static id.boyolali.sipad.util.URLConfig.tahun_sppt;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;
import static id.boyolali.sipad.util.siPAD.isValidEmail;
import static id.boyolali.sipad.util.siPAD.isValidPhone;

public class BayarKolektifActivity extends AppCompatActivity implements NOPAdapter.NOPsAdapterListener {

    private static final String TAG = BayarKolektifActivity.class.getSimpleName();
    @BindView(R.id.edit_nm_pemohon)
    EditText edit_nm_pemohon;
    @BindView(R.id.edit_email)
    EditText edit_email;
    @BindView(R.id.edit_telepon)
    EditText edit_telepon;
    @BindView(R.id.search_view)
    SearchView search_view;
    @BindView(R.id.spinner_thnSPPT)
    MaterialSpinner spinner_thnSPPT;
    @BindView(R.id.spinner_kecamatan)
    MaterialSpinner spinner_kecamatan;
    @BindView(R.id.spinner_kelurahan)
    MaterialSpinner spinner_kelurahan;
    @BindView(R.id.but_pilih_nop)
    FancyButton but_pilih_nop;
    @BindView(R.id.but_selanjutnya)
    FancyButton but_selanjutnya;
    @BindView(R.id.rl_success)
    RelativeLayout rl_success;
    @BindView(R.id.rl_error)
    RelativeLayout rl_error;
    @BindView(R.id.rl_step1)
    RelativeLayout rl_step1;
    @BindView(R.id.rl_step2)
    RelativeLayout rl_step2;
    @BindView(R.id.rl_step3)
    RelativeLayout rl_step3;
    @BindView(R.id.txt_step)
    TextView txt_step;
    @BindView(R.id.step_indicator)
    StepIndicator step_indicator;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.rl_selanjutnya)
    RelativeLayout rl_selanjutnya;
    @BindView(R.id.rl_margin_rv)
    RelativeLayout rl_margin_rv;
    @BindView(R.id.txt_sukses)
    TextView txt_sukses;
    @BindView(R.id.txt_jumlahnop)
    TextView txt_jumlahnop;
    @BindView(R.id.rl_count2)
    RelativeLayout rl_count2;
    @BindView(R.id.rl_count1)
    RelativeLayout rl_count1;
    @BindView(R.id.txt_countnop)
    TextView txt_countnop;
    @BindView(R.id.txt_pbb_terhutang)
    TextView txt_pbb_terhutang;
    @BindView(R.id.txt_stimulus)
    TextView txt_stimulus;
    @BindView(R.id.txt_diskon2)
    TextView txt_diskon2;
    @BindView(R.id.txt_pbb_sanksi)
    TextView txt_pbb_sanksi;
    @BindView(R.id.txt_harus_dibayar)
    TextView txt_harus_dibayar;
    @BindView(R.id.txt_terbilang)
    TextView txt_terbilang;
    @BindView(R.id.btn_id)
    CircularProgressButton btn_id;
    @BindView(R.id.logo_sipad)
    ImageView logo_sipad;
    @BindView(R.id.logo_side)
    ImageView logo_side;
    @BindView(R.id.txt_title)
    TextView txt_titlel;
    @BindView(R.id.rl_all)
    RelativeLayout rl_all;
    @BindView(R.id.cv_bottom_kdbayar)
    CardView cv_bottom_kdbayar;
    @BindView(R.id.txt_kodebayar)
    TextView txt_kodebayar;
    @BindView(R.id.txt_nmwp)
    TextView txt_nmwp;
    @BindView(R.id.txt_jttempo)
    TextView txt_jttempo;
    @BindView(R.id.txt_terhutangsppt)
    TextView txt_terhutangsppt;
    @BindView(R.id.txt_sanksi)
    TextView txt_sanksi;
    @BindView(R.id.txt_diskon)
    TextView txt_diskon;
    @BindView(R.id.txt_jumlah)
    TextView txt_jumlah;
    @BindView(R.id.txt_t_pembayaran)
    TextView txt_t_pembayaran;
    @BindView(R.id.txt_ket)
    TextView txt_ket;
    @BindView(R.id.img_qrcode)
    ImageView img_qrcode;
    @BindView(R.id.img_back)
    ImageView img_back;
    private View v;
    private Realm realm;
    private ArrayList<String> kecamatanArray;
    private ArrayList<kecamatan> kecamatanArrayList;
    private ArrayList<String> kelurahanArray;
    private ArrayList<kelurahan> kelurahanArrayList;
    private String kode_desa, s_thnSPPT, param, s_nop, kd_bayar, param_kd_bayar;
    private RelativeLayout.LayoutParams lp;

    private ArrayList<NOP> nop_selected_list = new ArrayList<>();
    private List<NOP> NOPList;
    private NOPAdapter mAdapter;

    private Animation animShow, animHide, animbackShow, animbackHide;
    private int mLastSpinnerPosition = -1, counter = 0;

    private ActionMode actionMode;
    private String full_url,s_nm_pemohon,s_email,s_telepon;
    private String[] NOParray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pbb_kolektif);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(BayarKolektifActivity.this, v);

        this.realm = RealmController.with(this).getRealm();
        RealmController.with(this).refresh();

        lp = (RelativeLayout.LayoutParams) but_pilih_nop.getLayoutParams();

        kecamatanArray = new ArrayList<>();
        kecamatanArrayList = new ArrayList<>();
        kelurahanArray = new ArrayList<>();
        kelurahanArrayList = new ArrayList<>();
        NOPList = new ArrayList<>();
        selected_nop.clear();

        step1();
        init_animation();
        init_spinner();
        init_searchview();
        btn_id.setText("Buat Kode Bayar");

        but_pilih_nop.setVisibility(View.GONE);
        cv_bottom_kdbayar.setVisibility(View.GONE);
        txt_ket.setVisibility(View.GONE);
        rl_count1.setVisibility(View.INVISIBLE);
        rl_selanjutnya.setVisibility(View.GONE);
        btn_id.setVisibility(View.GONE);

        //edit_email.setText("yodeput@gmail.com");
        //edit_telepon.setText("081976324776");
        //edit_nm_pemohon.setText("Yogi Dewansyah");


        //actionModeCallback = new ActionModeCallback();
        siPAD.getInstance().fabric_report("Bayar PBB Kolektif", "", "");
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            //actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        counter = mAdapter.getSelectedItemCount();

        if (counter == 0) {
            rl_count1.setVisibility(View.INVISIBLE);
            txt_countnop.setText(Integer.toString(counter));

            rl_margin_rv.setVisibility(View.GONE);
            rl_selanjutnya.setVisibility(View.GONE);
        } else if (counter == 1) {
            rl_count1.setVisibility(View.VISIBLE);
            txt_countnop.setText(Integer.toString(counter));

            rl_margin_rv.setVisibility(View.GONE);
            rl_selanjutnya.setVisibility(View.GONE);
        } else if (counter > 1) {
            rl_count1.setVisibility(View.VISIBLE);
            txt_countnop.setText(Integer.toString(counter));

            rl_margin_rv.setVisibility(View.VISIBLE);
            rl_selanjutnya.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.img_back)
    void back() {
        if (rl_step3.isShown()) {

            batal_step3();


        } else if (rl_step2.isShown()) {

            rl_step1.setVisibility(View.VISIBLE);
            rl_step2.setVisibility(View.GONE);
            rl_step3.setVisibility(View.GONE);
            txt_step.setText("Langkah 1");
            step_indicator.setCurrentStepPosition(0);
            rl_step1.startAnimation(animbackShow);
            rl_step2.startAnimation(animbackHide);
            selected_nop.clear();
            selectedCount = 0;
            counter = 0;
            rl_count1.setVisibility(View.GONE);
            rl_margin_rv.setVisibility(View.GONE);

        } else if (rl_step1.isShown()) {

            getInstance().dialog_exit(this, v, "Akan membatalkan proses bayar?");

        } else if (cv_bottom_kdbayar.isShown()) {

            finish();

        }


    }

    @OnClick(R.id.but_pilih_nop)
    void pilih() {

        if (s_thnSPPT.contains("SPPT")) {

            getInstance().toastError(getApplicationContext(), "Anda harus pilih tahun SPPT");
            spinner_thnSPPT.requestFocus();

        } else {
            selected_nop.clear();
            step2();


        }

    }


    @OnClick(R.id.but_selanjutnya)
    void next() {

        step3();

        Object list = selected_nop.toArray();
        int a = ((Object[]) list).length;
        NOParray = null;
        NOParray = new String[a];


        int[] jumlah_harus_dibayar = new int[a];
        int total_harus_dibayar = 0;

        int[] jumlah_pbb_terhutang = new int[a];
        int total_pbb_terhutang = 0;

        int[] jumlah_pbb_sanksi = new int[a];
        int total_pbb_sanksi = 0;

        int[] diskon = new int[a];
        int total_diskon = 0;

        int[] stimulus = new int[a];
        int total_stimulus = 0;

        for (int i = 0; i < a; i++) {

            String KD_PROPINSI = selected_nop.get(i).getKD_PROPINSI();
            String KD_DATI2 = selected_nop.get(i).getKD_DATI2();
            String KD_KECAMATAN = selected_nop.get(i).getKD_KECAMATAN();
            String KD_KELURAHAN = selected_nop.get(i).getKD_KELURAHAN();
            String KD_BLOK = selected_nop.get(i).getKD_BLOK();
            String NO_URUT = selected_nop.get(i).getNO_URUT();
            String KD_JNS_OP = selected_nop.get(i).getKD_JNS_OP();

            //NOParray[i] = "NOP[]="+selected_nop.get(i).getKD_PROPINSI() + selected_nop.get(i).getKD_DATI2() + selected_nop.get(i).getKD_KECAMATAN() + selected_nop.get(i).getKD_KELURAHAN() + selected_nop.get(i).getKD_BLOK() + selected_nop.get(i).getNO_URUT() + selected_nop.get(i).getKD_JNS_OP();
            NOParray[i] = selected_nop.get(i).getKD_PROPINSI() + selected_nop.get(i).getKD_DATI2() + selected_nop.get(i).getKD_KECAMATAN() + selected_nop.get(i).getKD_KELURAHAN() + selected_nop.get(i).getKD_BLOK() + selected_nop.get(i).getNO_URUT() + selected_nop.get(i).getKD_JNS_OP();

            jumlah_pbb_terhutang[i] = Integer.parseInt(selected_nop.get(i).getPBB_TERHUTANG_SPPT());
            jumlah_harus_dibayar[i] = Integer.parseInt(selected_nop.get(i).getPBB_YG_HARUS_DIBAYAR_SPPT());
            jumlah_pbb_sanksi[i] = Integer.parseInt(selected_nop.get(i).getSANKSI());
            diskon[i] = Integer.parseInt(selected_nop.get(i).getDISKON());
            stimulus[i] = Integer.parseInt(selected_nop.get(i).getSTIMULUS());
            //Log.e("wkwkwkwkwk", "................"+diskon[i]);

        }
        /*
        s_nop = Arrays.toString(NOParray);
        s_nop = s_nop.replace(",", "&");
        s_nop = s_nop.replace("[N", "N");
        s_nop = s_nop.replace("0]", "0");
        s_nop = s_nop.replace(" ", "");
        Log.e("wkwkwkwkwkwk",s_nop);
*/
        for (int i : jumlah_pbb_terhutang) {
            total_pbb_terhutang += i;
        }
        for (int i : jumlah_pbb_sanksi) {
            total_pbb_sanksi += i;
        }
        for (int i : jumlah_harus_dibayar) {
            total_harus_dibayar += i;
        }
        for (int i : diskon) {
            total_diskon += i;
        }
        for (int i : stimulus) {
            total_stimulus += i;
        }

        txt_jumlahnop.setText("Jumlah NOP " + counter);
        txt_pbb_terhutang.setText(rupiah_format_integer(total_harus_dibayar));

        int jumlah = total_harus_dibayar+total_pbb_sanksi-total_diskon;
        txt_harus_dibayar.setText(rupiah_format_integer(jumlah));
        txt_pbb_sanksi.setText(rupiah_format_integer(total_pbb_sanksi));
        txt_terbilang.setText(info_terbilang(jumlah) + " rupiah");
        txt_stimulus.setText(rupiah_format_integer(total_stimulus));
        txt_diskon2.setText(rupiah_format_integer(total_diskon));

    }

    @OnClick(R.id.btn_id)
    void but_kode_bayar() {

        s_email = edit_email.getText().toString();
        s_telepon = edit_telepon.getText().toString();
        s_nm_pemohon = edit_nm_pemohon.getText().toString();
        if ((s_nm_pemohon == "") || (s_email == "") || (s_telepon == "")) {

            getInstance().toastError(getApplicationContext(), "Isi Form dengan Benar");
            edit_nm_pemohon.requestFocus();

        } else if (isEditEmpty(edit_nm_pemohon) || isEditEmpty(edit_email) || isEditEmpty(edit_telepon)) {

            getInstance().toastError(getApplicationContext(), "Isi Form dengan Benar");
            edit_nm_pemohon.requestFocus();

        } else if (!isValidEmail(s_email)) {

            getInstance().toastError(getApplicationContext(), "Format email salah");
            edit_email.requestFocus();

        } else if (!isValidPhone(s_telepon)) {

            getInstance().toastError(getApplicationContext(), "Format nomor telepon salah");
            edit_telepon.requestFocus();

        } else {
            param_kd_bayar = s_nop + "&THN_PAJAK_SPPT=" + s_thnSPPT + "&user[telepon]=" + s_telepon + "&user[email]=" + s_email + "&user[nm_pemohon]=" + s_nm_pemohon;
            full_url = URL_PBB_KOLEKTIF2 + param_kd_bayar;
            Log.e("wkwkwkwkwk",full_url);

            //old methode
            //get_kode_bayar(param_kd_bayar); bayar_kolektif();
            //


            //kode_bayar kode_bayar = new kode_bayar();
            //kode_bayar.execute();

            JSONObject data = new JSONObject();
            try {

                JSONObject opsi = new JSONObject();
                JSONObject THN_PAJAK_SPPT = new JSONObject();
                JSONObject ID_DESA = new JSONObject();
                JSONObject uslog = new JSONObject();
                JSONObject usreg = new JSONObject();
                JSONObject uslog_child = new JSONObject();
                JSONObject usreg_child = new JSONObject();

                uslog_child.put("NAMA_PEMOHON", s_nm_pemohon);
                uslog_child.put("email", s_email);
                usreg_child.put("TELPON", s_telepon);

                opsi.put("opsi", "Bentuk Kode Bayar");
                THN_PAJAK_SPPT.put("THN_PAJAK_SPPT", s_thnSPPT);
                ID_DESA.put("ID_DESA", kode_desa );
                uslog.put("uslog", uslog_child.toString());
                usreg.put("usreg", usreg_child.toString());

                data.put("NOP[]", NOParray);
                data.put("data", opsi.toString());
                data.put("THN_PAJAK_SPPT", THN_PAJAK_SPPT.toString());
                data.put("ID_DESA", ID_DESA.toString());
                data.put("uslog", uslog.toString());
                data.put("usreg", usreg.toString());

            }catch (JSONException e) {
                e.printStackTrace();
            }

            Log.e("wkwkwkwkwk", ""+data);
            //postData(data);
            bayar_kolektif();
        }




    }

    private void bayar_kolektif(){
        btn_id.startAnimation();
        final String tag_string_req = "req_cek_njop";
        StringRequest postRequest = new StringRequest(Request.Method.POST, URL_PBB_KOLEKTIF, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response) {
                Log.d("Response", response);
                try {

                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {
                        rl_step3.setVisibility(View.GONE);
                        step_indicator.setVisibility(View.GONE);
                        btn_id.setProgress(100);
                        btn_id.setGravity(Gravity.CENTER_HORIZONTAL);
                        btn_id.doneLoadingAnimation(getResources().getColor(R.color.pbb), BitmapFactory.decodeResource(getResources(), R.drawable.ic_check));
                        btn_id.animate().alpha(0.0f).setDuration(1000);

                        String link = jObj.getString("link");
                        String email = jObj.getString("sendto");
                        kd_bayar = jObj.getString("kd_bayar");
                        String terbit = jObj.getString("terbit");
                        String jatuh_tempo = jObj.getString("jatuh_tempo");
                        String namawp = jObj.getString("namawp");
                        String terhutang = jObj.getString("terhutang");
                        String sanksi = jObj.getString("sanksi");
                        String diskon = jObj.getString("diskon");
                        String total = jObj.getString("total");

                        Number id = realm.where(KodeBayarDB.class).max("id");
                        int nextid = (id == null) ? 1 : id.intValue() + 1;
                        KodeBayarDB db = new KodeBayarDB();
                        db.setId(nextid);
                        db.setkode_bayar(kd_bayar);
                        db.setjumlah(total);
                        db.setjenis_bayar("Bayar Kolektif");
                        realm.beginTransaction();
                        realm.copyToRealm(db);
                        realm.commitTransaction();

                        txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                        txt_nmwp.setText(namawp);
                        //txt_nop.setText(namawp);
                        txt_jttempo.setText(jatuh_tempo);
                        txt_terhutangsppt.setText(terhutang);
                        txt_sanksi.setText(sanksi);
                        txt_diskon.setText(diskon);
                        txt_jumlah.setText(total);
                        txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                        Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                        img_qrcode.setImageBitmap(qrcode_Bitmap);

                        //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                        //txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+email);

                        img_back.setVisibility(View.GONE);
                        logo_side.setVisibility(View.GONE);
                        txt_titlel.setVisibility(View.GONE);
                        logo_sipad.setVisibility(View.VISIBLE);
                        txt_step.setVisibility(View.GONE);

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                cv_bottom_kdbayar.setVisibility(View.VISIBLE);
                                txt_ket.setVisibility(View.VISIBLE);
                            }
                        }, 1000);

                        Handler handler2 = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                getInstance().saveSS(rl_all, kd_bayar);
                                img_back.setVisibility(View.VISIBLE);
                                logo_side.setVisibility(View.VISIBLE);
                                txt_titlel.setVisibility(View.VISIBLE);
                                logo_sipad.setVisibility(View.GONE);
                            }
                        }, 1200);

                    } else {

                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert_finish(BayarKolektifActivity.this, v, errorMsg);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Log.d("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                try {
                    for (int i = 0; i <  NOParray.length; i++) {
                        params.put("NOP["+i+"]",NOParray[i]);
                    }
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                    return null;
                }
                params.put("opsi", "Bentuk Kode Bayar");
                params.put("ID_DESA",kode_desa);
                params.put("THN_PAJAK_SPPT", s_thnSPPT);
                params.put("uslog[NAMA_PEMOHON]",s_nm_pemohon );
                params.put("uslog[email]", s_email);
                params.put("usreg[TELPON]", s_telepon);
                Log.e("wkwkwkwkwk", params.toString());
                return params;
            }
        };
        siPAD.getInstance().addToRequestQueue(postRequest, tag_string_req);
    }

    public void postData(JSONObject data){
        final String tag_string_req = "req_cek_njop";

        JsonObjectRequest jsonobj = new JsonObjectRequest(Request.Method.POST, URL_PBB_KOLEKTIF,data,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e(tag_string_req, response.toString());
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(tag_string_req, error.toString());
                    }
                }
        );
        siPAD.getInstance().addToRequestQueue(jsonobj, tag_string_req);

    }

    @Override
    public void onNOPSelected(NOP NOP) {


    }

    public void clear() {
        NOPList.clear();
        mAdapter.notifyDataSetChanged();
    }


    private void step1() {

        rl_step1.setVisibility(View.VISIBLE);
        rl_step2.setVisibility(View.GONE);
        rl_step3.setVisibility(View.GONE);
        txt_step.setText("Langkah 1");
        step_indicator.setCurrentStepPosition(0);

    }

    private void step2() {

        rl_step1.setVisibility(View.GONE);
        rl_step2.setVisibility(View.VISIBLE);
        rl_step3.setVisibility(View.GONE);
        rl_selanjutnya.setVisibility(View.GONE);
        txt_step.setText("Langkah 2");
        step_indicator.setCurrentStepPosition(1);

        rl_step1.startAnimation(animHide);
        rl_step2.startAnimation(animShow);

        mAdapter = new NOPAdapter(this, NOPList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mAdapter);
        get_nop(param);
        mAdapter.setOnClickListener(new NOPAdapter.OnClickListener() {
            @Override
            public void onItemClick(View view, NOP obj, int pos) {
                if (mAdapter.getSelectedItemCount() > 0) {
                    enableActionMode(pos);
                } else {
                    // read the inbox which removes bold from the row
                    NOP nop = mAdapter.getItem(pos);
                    getInstance().toastInfo(getApplicationContext(), "Tekan dan tahan untuk memilih");

                }
            }

            @Override
            public void onItemLongClick(View view, NOP obj, int pos) {
                enableActionMode(pos);
            }
        });


    }

    private void step3() {

        rl_step1.setVisibility(View.GONE);
        rl_step2.setVisibility(View.GONE);
        rl_step3.setVisibility(View.VISIBLE);
        btn_id.setVisibility(View.VISIBLE);
        txt_step.setText("Langkah 3");
        step_indicator.setCurrentStepPosition(2);

        rl_step2.startAnimation(animHide);
        rl_step3.startAnimation(animShow);

    }

    private void init_animation() {
        animShow = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_right);
        animHide = AnimationUtils.loadAnimation(this, R.anim.anim_slide_out_left);
        animbackShow = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_in_right);
        animbackHide = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_out_left);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (rl_step3.isShown()) {

            batal_step3();
            return true;

        } else if (rl_step2.isShown()) {

            rl_step1.setVisibility(View.VISIBLE);
            rl_step2.setVisibility(View.GONE);
            rl_step3.setVisibility(View.GONE);
            txt_step.setText("Langkah 1");
            step_indicator.setCurrentStepPosition(0);
            rl_step1.startAnimation(animbackShow);
            rl_step2.startAnimation(animbackHide);
            selected_nop.clear();
            selectedCount = 0;
            counter = 0;
            rl_count1.setVisibility(View.GONE);
            rl_margin_rv.setVisibility(View.GONE);
            return true;

        } else if (rl_step1.isShown()) {

            getInstance().dialog_exit(this, v, "Keluar dari menu bayar?");

        }


        return super.onKeyDown(keyCode, event);
    }


    private void get_kecamatan() {
        getInstance().showDialog();
        final String tag_string_req = "req_kecamatan";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_KECAMATAN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("kecamatan");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        kecamatan kec = new kecamatan();
                        kec.setNM_KECAMATAN(jsnobject.getString("NM_KECAMATAN"));
                        kec.setKD_KECAMATAN(jsnobject.getString("KD_KECAMATAN"));

                        kecamatanArrayList.add(kec);
                        kecamatanArray.add(jsnobject.getString("NM_KECAMATAN"));

                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BayarKolektifActivity.this, android.R.layout.simple_spinner_item, kecamatanArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_kecamatan.setAdapter(adapter);


                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(BayarKolektifActivity.this, v, BayarKolektifActivity.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void get_kelurahan(String kd_kecamatan) {
        getInstance().showDialog();
        final String tag_string_req = "req_kelurahan";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_KELURAHAN + kd_kecamatan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("kelurahan");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        kelurahan kel = new kelurahan();
                        kel.setNM_KELURAHAN(jsnobject.getString("NM_KELURAHAN"));
                        kel.setKD_KELURAHAN(jsnobject.getString("KD_KECAMATAN"));
                        kel.setID(jsnobject.getString("ID"));

                        kelurahanArrayList.add(kel);
                        kelurahanArray.add(jsnobject.getString("NM_KELURAHAN"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BayarKolektifActivity.this, android.R.layout.simple_spinner_item, kelurahanArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_kelurahan.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(BayarKolektifActivity.this, v, BayarKolektifActivity.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_nop(String param) {

        getInstance().showDialog();
        final String tag_string_req = "req_nop_perdesa";
        //Log.e(tag_string_req, param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_NOP_PERDESA + param, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {

                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    if (data_array.length() > 0) {
                        List<NOP> items = new Gson().fromJson(data, new TypeToken<List<NOP>>() {
                        }.getType());
                        NOPList.clear();
                        NOPList.addAll(items);

                        txt_sukses.setText("Terdapat " + data_array.length() + " NOP");
                        rl_error.setVisibility(View.GONE);
                        rl_success.setVisibility(View.VISIBLE);
                        rl_success.animate().alpha(1.0f).setDuration(1000);
                        lp.addRule(RelativeLayout.BELOW, R.id.rl_success);
                        but_pilih_nop.setVisibility(View.VISIBLE);
                        but_pilih_nop.animate().alpha(1.0f).setDuration(2000);


                    } else {
                        rl_success.setVisibility(View.GONE);
                        rl_error.setVisibility(View.VISIBLE);
                        rl_error.animate().alpha(1.0f).setDuration(1000);
                        lp.addRule(RelativeLayout.BELOW, R.id.rl_error);
                        but_pilih_nop.setVisibility(View.GONE);
                        but_pilih_nop.animate().alpha(0.0f).setDuration(2000);


                    }


                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert_finish(BayarKolektifActivity.this, v, BayarKolektifActivity.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_kode_bayar(final String param) {
        btn_id.startAnimation();
        final String tag_string_req = "req_post_paramp";
        Log.e(tag_string_req, param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_PBB_KOLEKTIF + param, new Response.Listener<String>() {


            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req, response);

                try {
                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {

                        String link = jObj.getString("link");
                        String email = jObj.getString("sendto");
                        kd_bayar = jObj.getString("kd_bayar");
                        String terbit = jObj.getString("terbit");
                        String jatuh_tempo = jObj.getString("jatuh_tempo");
                        String namawp = jObj.getString("namawp");
                        String terhutang = jObj.getString("terhutang");
                        String sanksi = jObj.getString("sanksi");
                        String total = jObj.getString("total");

                        Number id = realm.where(KodeBayarDB.class).max("id");
                        int nextid = (id == null) ? 1 : id.intValue() + 1;
                        KodeBayarDB db = new KodeBayarDB();
                        db.setId(nextid);
                        db.setkode_bayar(kd_bayar);
                        db.setjumlah(total);
                        db.setjenis_bayar("Bayar Kolektif");
                        realm.beginTransaction();
                        realm.copyToRealm(db);
                        realm.commitTransaction();

                        txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                        txt_nmwp.setText(namawp);
                        //txt_nop.setText(namawp);
                        txt_jttempo.setText(jatuh_tempo);
                        txt_terhutangsppt.setText(terhutang);
                        txt_sanksi.setText(sanksi);
                        txt_jumlah.setText(total);
                        txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                        Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                        img_qrcode.setImageBitmap(qrcode_Bitmap);

                        //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                        //txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+email);

                        rl_step3.setVisibility(View.GONE);
                        btn_id.setProgress(100);
                        btn_id.doneLoadingAnimation(getResources().getColor(R.color.colorPrimary), BitmapFactory.decodeResource(getResources(), R.drawable.ic_check));

                        btn_id.animate().alpha(0.0f).setDuration(1000);

                        img_back.setVisibility(View.GONE);
                        logo_side.setVisibility(View.GONE);
                        txt_titlel.setVisibility(View.GONE);
                        logo_sipad.setVisibility(View.VISIBLE);
                        txt_step.setVisibility(View.GONE);

                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {
                                cv_bottom_kdbayar.setVisibility(View.VISIBLE);
                                txt_ket.setVisibility(View.VISIBLE);
                            }
                        }, 1000);

                        Handler handler2 = new Handler();
                        handler.postDelayed(new Runnable() {
                            public void run() {

                                getInstance().saveSS(rl_all, kd_bayar);
                                img_back.setVisibility(View.VISIBLE);
                                logo_side.setVisibility(View.VISIBLE);
                                txt_titlel.setVisibility(View.VISIBLE);
                                logo_sipad.setVisibility(View.GONE);
                            }
                        }, 1200);

                    } else {

                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert(BayarKolektifActivity.this, v, errorMsg);

                    }
                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                siPAD.getInstance().dialog_alert_finish(BayarKolektifActivity.this, v, BayarKolektifActivity.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);


    }

    private void batal_step3() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(this).inflate(R.layout.dialog_with_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText("Akan membatalkan proses bayar?");
        dialog_left.setText("Tidak");
        dialog_right.setText("Ya");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                rl_step1.setVisibility(View.GONE);
                rl_step2.setVisibility(View.VISIBLE);
                rl_step3.setVisibility(View.GONE);
                btn_id.setVisibility(View.GONE);
                txt_step.setText("Langkah 2");
                step_indicator.setCurrentStepPosition(1);
                rl_step2.startAnimation(animbackShow);
                rl_step3.startAnimation(animbackHide);

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });


    }

    private void init_spinner() {

        spinner_kecamatan.setEnabled(false);
        spinner_kelurahan.setEnabled(false);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tahun_sppt);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_thnSPPT.setAdapter(adapter);

        spinner_thnSPPT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih tahun SPPT");
                } else {
                    s_thnSPPT = spinner_thnSPPT.getItemAtPosition(position).toString();
                    get_kecamatan();
                    spinner_kecamatan.setEnabled(true);
                    but_pilih_nop.setVisibility(View.GONE);
                    rl_success.setVisibility(View.GONE);
                    rl_error.setVisibility(View.GONE);
                    spinner_kelurahan.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        spinner_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                kelurahanArray.clear();
                kelurahanArrayList.clear();

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Kecamatan");
                } else {
                    get_kelurahan(kecamatanArrayList.get(position).getKD_KECAMATAN());
                    spinner_kelurahan.setEnabled(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_kelurahan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Kelurahan/Desa");
                } else {
                    kode_desa = kelurahanArrayList.get(position).getID();
                    param = "id=" + kode_desa + "&THN_PAJAK_SPPT=" + s_thnSPPT;
                    get_nop(param);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void init_searchview() {

        search_view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });

    }

    public class kode_bayar extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            btn_id.startAnimation();

        }

        @Override
        protected String doInBackground(String... params) {

            // implement API in background and store the response in current variable
            String current = "";
            try {
                URL url;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(full_url);

                    urlConnection = (HttpURLConnection) url
                            .openConnection();

                    InputStream in = urlConnection.getInputStream();

                    InputStreamReader isw = new InputStreamReader(in);

                    int data = isw.read();
                    while (data != -1) {
                        current += (char) data;
                        data = isw.read();
                        System.out.print(current);

                    }
                    // return the data to onPostExecute method
                    return current;

                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return "Exception: " + e.getMessage();
            }
            return current;
        }

        @Override
        protected void onPostExecute(String s) {

            Log.e("Response",s);
            try {

                final JSONObject jObj = new JSONObject(s);
                final Boolean error = jObj.getBoolean("error");

                if (!error) {
                    rl_step3.setVisibility(View.GONE);
                    step_indicator.setVisibility(View.GONE);
                    btn_id.setProgress(100);
                    btn_id.setGravity(Gravity.CENTER_HORIZONTAL);
                    btn_id.doneLoadingAnimation(getResources().getColor(R.color.pbb), BitmapFactory.decodeResource(getResources(), R.drawable.ic_check));
                    btn_id.animate().alpha(0.0f).setDuration(1000);

                    String link = jObj.getString("link");
                    String email = jObj.getString("sendto");
                    kd_bayar = jObj.getString("kd_bayar");
                    String terbit = jObj.getString("terbit");
                    String jatuh_tempo = jObj.getString("jatuh_tempo");
                    String namawp = jObj.getString("namawp");
                    String terhutang = jObj.getString("terhutang");
                    String sanksi = jObj.getString("sanksi");
                    String diskon = jObj.getString("diskon");
                    String total = jObj.getString("total");

                    Number id = realm.where(KodeBayarDB.class).max("id");
                    int nextid = (id == null) ? 1 : id.intValue() + 1;
                    KodeBayarDB db = new KodeBayarDB();
                    db.setId(nextid);
                    db.setkode_bayar(kd_bayar);
                    db.setjumlah(total);
                    db.setjenis_bayar("Bayar Kolektif");
                    realm.beginTransaction();
                    realm.copyToRealm(db);
                    realm.commitTransaction();

                    txt_kodebayar.setText("KODEBAYAR\n" + kd_bayar);
                    txt_nmwp.setText(namawp);
                    //txt_nop.setText(namawp);
                    txt_jttempo.setText(jatuh_tempo);
                    txt_terhutangsppt.setText(terhutang);
                    txt_sanksi.setText(sanksi);
                    txt_diskon.setText(diskon);
                    txt_jumlah.setText(total);
                    txt_t_pembayaran.setText(getString(R.string.bank_bayar));
                    Bitmap qrcode_Bitmap = QRCode.from(kd_bayar).withSize(250, 250).bitmap();
                    img_qrcode.setImageBitmap(qrcode_Bitmap);

                    //getInstance().toastSuccess(getApplicationContext(),"Surat Pengantar sudah dikirim ke "+email);
                    //txt_ket.setText("*)Surat Pengantar Pembayaran SPPT sudah dikirim ke "+email);

                    img_back.setVisibility(View.GONE);
                    logo_side.setVisibility(View.GONE);
                    txt_titlel.setVisibility(View.GONE);
                    logo_sipad.setVisibility(View.VISIBLE);
                    txt_step.setVisibility(View.GONE);

                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {
                            cv_bottom_kdbayar.setVisibility(View.VISIBLE);
                            txt_ket.setVisibility(View.VISIBLE);
                        }
                    }, 1000);

                    Handler handler2 = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            getInstance().saveSS(rl_all, kd_bayar);
                            img_back.setVisibility(View.VISIBLE);
                            logo_side.setVisibility(View.VISIBLE);
                            txt_titlel.setVisibility(View.VISIBLE);
                            logo_sipad.setVisibility(View.GONE);
                        }
                    }, 1200);

                } else {
                    btn_id.stopAnimation();
                    String errorMsg = jObj.getString("msg");
                    errorMsg = errorMsg.replace("<br>", "\n");
                    siPAD.getInstance().dialog_alert_finish(BayarKolektifActivity.this, v, errorMsg);

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }
}
