package id.boyolali.sipad.activity;

/*******************************************************************************
 * Double Dots Dynamic ~we connect the dots~
 * Copyright (c) 2019. All Rights Reserved
 * W: http://doubledots.id/
 * E: admin@doubledots.id
 *
 ******************************************************************************/

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.model.Image;
import com.layer_net.stepindicator.StepIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.Domisili;
import id.boyolali.sipad.adapter.ListBadanUsaha;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.Tools;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.URLConfig.REGISTER_AKUN;
import static id.boyolali.sipad.util.URLConfig.REGISTER_USAHA;
import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;
import static id.boyolali.sipad.util.siPAD.isValidEmail;
import static id.boyolali.sipad.util.siPAD.isValidPhone;

public class DaftarUsahaActivity extends AppCompatActivity  {

    @BindView(R.id.ll_root)
    LinearLayout ll_root;
    @BindView(R.id.step_indicator)
    StepIndicator step_indicator;

    @BindView(R.id.spinner_badan_usaha)
    MaterialSpinner spinner_badan_usaha;
    @BindView(R.id.spinner_provinsi)
    MaterialSpinner spinner_provinsi;
    @BindView(R.id.spinner_kotakab)
    MaterialSpinner spinner_kotakab;
    @BindView(R.id.spinner_kecamatan)
    MaterialSpinner spinner_kecamatan;
    @BindView(R.id.spinner_desa)
    MaterialSpinner spinner_desa;

    @BindView(R.id.ll_main)
    LinearLayout ll_main;
    @BindView(R.id.ll_next)
    LinearLayout ll_next;

    @BindView(R.id.step_1)
    ScrollView step_1;
    @BindView(R.id.edit_nama_usaha)
    EditText edit_nama_usaha;
    @BindView(R.id.edit_telepon)
    EditText edit_telepon;
    @BindView(R.id.edit_alamat)
    EditText edit_alamat;
    @BindView(R.id.edit_rt)
    EditText edit_rt;
    @BindView(R.id.edit_rw)
    EditText edit_rw;

    @BindView(R.id.step_2)
    ScrollView step_2;
    @BindView(R.id.bt_foto_ktp)
    Button bt_foto_ktp;
    @BindView(R.id.img_siup)
    ImageView img_siup;
    @BindView(R.id.textView24)
    TextView txt_img;

    @BindView(R.id.bt_next)
    Button bt_next;

    private ArrayList<Domisili> ProvArrayList,KabArrayList,KecArrayList,DesaArrayList;
    private ArrayList<ListBadanUsaha> BadanUsahaList;
    private ArrayList<String> ProvArray,KabArray,KecArray,DesaArray,BadanUsahaArray;
    private SimpleDateFormat simpleDateFormat;
    private static final int RC_CAMERA = 3000;
    private ArrayList<Image> images = new ArrayList<>();
    private int mLastSpinnerPosition = -1, counter = 0;
    private Animation animShow, animHide, animbackShow, animbackHide;
    private View v;
    private PrefManager session;
    private  Calendar cal;

    private String NASABAH_ID,NPWPD,KODE_BADAN_USAHA,DESKRIPSI_BADAN_USAHA,NAMA_USAHA,ALAMAT,
            RT,RW,ID_DESA,DESA,ID_KEC,KEC,ID_DATI,DATI,ID_PROV,PROPINSI,TELPON,
            TGL_REGISTER,SIUP_DOC,USERID,USERNAMECREATE;;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_usaha);
        ButterKnife.bind(this);

        v = getWindow().getDecorView().findViewById(android.R.id.content);
        session = new PrefManager(getApplicationContext());
        getInstance().init_p_dialog(DaftarUsahaActivity.this,v);
        ProvArrayList = new ArrayList<>();ProvArray= new ArrayList<>();
        KabArrayList = new ArrayList<>();KabArray= new ArrayList<>();
        KecArrayList = new ArrayList<>();KecArray= new ArrayList<>();
        DesaArrayList = new ArrayList<>();DesaArray= new ArrayList<>();
        BadanUsahaList = new ArrayList<>();BadanUsahaArray= new ArrayList<>();
        cal = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.US);
        get_badan_usaha();
        get_prov();
        init_spinner();
        init_layout();
        init_animation();
    }

    private void init_layout(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Tambah Data Usaha");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Tools.setSystemBarColor(this);
        ll_root.setVisibility(View.VISIBLE);
        step_indicator.setCurrentStepPosition(0);
        edit_nama_usaha.requestFocus();
        step1();

    }
    private void init_animation() {
        animShow = AnimationUtils.loadAnimation(this, R.anim.anim_slide_in_right);
        animHide = AnimationUtils.loadAnimation(this, R.anim.anim_slide_out_left);
        animbackShow = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_in_right);
        animbackHide = AnimationUtils.loadAnimation(this, R.anim.anim_back_slide_out_left);
    }
    private void show_next(){
        ll_next.setVisibility(View.VISIBLE);
    }
    private void step1(){
        step_1.setVisibility(View.VISIBLE);
        step_2.setVisibility(View.GONE);
        ll_next.setVisibility(View.GONE);
    }

    private void step2(){
        if ((isEditEmpty(edit_nama_usaha))||(isEditEmpty(edit_telepon))
                ||(isEditEmpty(edit_alamat))||(isEditEmpty(edit_rt))||(isEditEmpty(edit_rw))){
            getInstance().toastError(DaftarUsahaActivity.this,"Lengkapi data Pendaftaran");
            edit_nama_usaha.requestFocus();
        } else if (!isValidPhone(edit_telepon.getText().toString())){
            getInstance().toastError(DaftarUsahaActivity.this,"Format nomor telepon Salah");
            edit_telepon.requestFocus();
        } else {
            step_1.setVisibility(View.GONE);
            step_2.setVisibility(View.VISIBLE);
            step_1.startAnimation(animHide);
            step_2.startAnimation(animShow);
            ll_next.setVisibility(View.GONE);
            step_indicator.setCurrentStepPosition(1);
        }
    }

    @OnClick(R.id.bt_next) void next(){
        if (step_1.isShown()){
            step2();
            Log.e("daftar_akun_activity",simpleDateFormat.format(cal.getTime()));
        } else if (step_2.isShown()) {
            if (!SIUP_DOC.isEmpty()){
                get_string();
            }
        }
    }

    @OnClick(R.id.bt_foto_ktp) void pilih_ktp(){
        dialog_pilih_sumber();
    }

    private void get_string(){
        NAMA_USAHA = edit_nama_usaha.getText().toString();
        TELPON = edit_telepon.getText().toString();
        ALAMAT = edit_alamat.getText().toString();
        RT = edit_rt.getText().toString();
        RW = edit_rw.getText().toString();

        NASABAH_ID = session.getPref("nasabah_id");
        NPWPD = session.getPref("npwpd");
        USERID = NPWPD;
        USERNAMECREATE = NPWPD;
        TGL_REGISTER = simpleDateFormat.format(cal.getTime());


        post_daftar();
    }

    private void post_daftar(){
        getInstance().showDialog();
        StringRequest postRequest = new StringRequest(Request.Method.POST, REGISTER_USAHA,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        getInstance().hideDialog();

                        try {
                            final JSONObject jObj = new JSONObject(response);
                            final Boolean error = jObj.getBoolean("error");
                            getInstance().req_user_data(NPWPD,DaftarUsahaActivity.this);
                            if (!error){
                                show_dialog_sukses_daftar();
                            } else {

                            }
                        }catch (final JSONException e) {
                            getInstance().hideDialog();
                            e.printStackTrace();
                            //Log.e(tag_string_req, e.getMessage());


                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        getInstance().hideDialog();
                        getInstance().dialog_alert_finish(DaftarUsahaActivity.this,v,getString(R.string.error_koneksi));
                        Log.e("Error.Response", error.toString());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String>  params = new HashMap<String, String>();
                params.put("NASABAH_ID",NASABAH_ID);
                params.put("NPWPD",NPWPD);
                params.put("KODE_BADAN_USAHA",KODE_BADAN_USAHA);
                params.put("DESKRIPSI_BADAN_USAHA",DESKRIPSI_BADAN_USAHA);
                params.put("NAMA_USAHA",NAMA_USAHA);
                params.put("ALAMAT",ALAMAT);
                params.put("RT",RT);
                params.put("RW",RW);
                params.put("ID_DESA",ID_DESA);
                params.put("DESA",DESA);
                params.put("ID_KEC",ID_KEC);
                params.put("KEC",KEC);
                params.put("ID_DATI",ID_DATI);
                params.put("DATI",DATI);
                params.put("ID_PROV",ID_PROV);
                params.put("PROPINSI",PROPINSI);
                params.put("TELPON",TELPON);
                params.put("TGL_REGISTER",TGL_REGISTER);
                params.put("SIUP_DOC",SIUP_DOC);
                params.put("USERID",USERID);
                params.put("USERNAMECREATE",USERNAMECREATE);
                return params;
            }
        };
        siPAD.getInstance().addToRequestQueue(postRequest);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id  = item.getItemId();
        if (id == android.R.id.home) {
            getInstance().dialog_alert_batal_proses(DaftarUsahaActivity.this,v,"Akan membatalkan Pendaftaran?");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {

        finish();
    }

    private void init_spinner(){

        spinner_provinsi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KabArrayList.clear();
                KabArray.clear();
                KecArrayList.clear();
                KecArray.clear();
                DesaArrayList.clear();
                DesaArray.clear();
                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Provinsi");
                } else {
                    String kd_prov = ProvArrayList.get(position).getid();
                    ID_PROV = kd_prov;
                    PROPINSI = ProvArrayList.get(position).getname();
                    get_kab(kd_prov);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_kotakab.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Kota/Kabupaten");
                } else {
                    String kd_kab = KabArrayList.get(position).getid();
                    ID_DATI = KabArrayList.get(position).getid();;
                    DATI = KabArrayList.get(position).getname();
                    get_kec(kd_kab);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_kecamatan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Kecamatan");
                } else {
                    String kd_kec = KecArrayList.get(position).getid();
                    ID_KEC = kd_kec;
                    KEC = KecArrayList.get(position).getname();
                    get_desa(kd_kec);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_desa.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Desa/Kelurahan");
                } else {
                    String kd_desa = DesaArrayList.get(position).getid();
                    ID_DESA = kd_desa;
                    DESA = DesaArrayList.get(position).getname();
                    show_next();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_badan_usaha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Pilih Badan Usaha");
                } else {
                    KODE_BADAN_USAHA = BadanUsahaList.get(position).getKODE_BADAN_USAHA();
                    DESKRIPSI_BADAN_USAHA= BadanUsahaList.get(position).getDESKRIPSI_BADAN_USAHA();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void get_prov() {
        getInstance().showDialog();
        final String tag_string_req = "req_prov";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_PROVINSI, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setname(jsnobject.getString("name"));

                        ProvArrayList.add(dom);
                        ProvArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarUsahaActivity.this, android.R.layout.simple_spinner_item, ProvArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_provinsi.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarUsahaActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_kab(String kd_prov) {
        getInstance().showDialog();
        final String tag_string_req = "req_kab";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_KOTAKAB + kd_prov, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setid_provinsi(jsnobject.getString("id_prov"));
                        dom.setname(jsnobject.getString("name"));

                        KabArrayList.add(dom);
                        KabArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarUsahaActivity.this, android.R.layout.simple_spinner_item, KabArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_kotakab.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarUsahaActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_kec(String kd_kab) {
        getInstance().showDialog();
        final String tag_string_req = "req_desa";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_KEC + kd_kab, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setid_kab(jsnobject.getString("id_kab"));
                        dom.setname(jsnobject.getString("name"));

                        KecArrayList.add(dom);
                        KecArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarUsahaActivity.this, android.R.layout.simple_spinner_item, KecArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_kecamatan.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarUsahaActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_desa(String kd_kecamatan) {
        getInstance().showDialog();
        final String tag_string_req = "req_desa";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_DESA + kd_kecamatan, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        Domisili dom = new Domisili();
                        dom.setid(jsnobject.getString("id"));
                        dom.setid_kec(jsnobject.getString("id_kec"));
                        dom.setname(jsnobject.getString("name"));

                        DesaArrayList.add(dom);
                        DesaArray.add(jsnobject.getString("name"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarUsahaActivity.this, android.R.layout.simple_spinner_item, DesaArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_desa.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarUsahaActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void get_badan_usaha() {
        getInstance().showDialog();
        final String tag_string_req = "req_badan_usaha";
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.GET_BADAN_USAHA, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(tag_string_req,response);
                getInstance().hideDialog();
                try {
                    final JSONObject jObj = new JSONObject(response);
                    String data = jObj.getString("data");
                    JSONArray data_array = new JSONArray(data);

                    for (int i = 0; i < data_array.length(); i++) {

                        JSONObject jsnobject = data_array.getJSONObject(i);
                        ListBadanUsaha bu = new ListBadanUsaha();
                        bu.setKODE_BADAN_USAHA(jsnobject.getString("KODE_BADAN_USAHA"));
                        bu.setDESKRIPSI_BADAN_USAHA(jsnobject.getString("DESKRIPSI_BADAN_USAHA"));

                        BadanUsahaList.add(bu);
                        BadanUsahaArray.add(jsnobject.getString("DESKRIPSI_BADAN_USAHA"));

                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(DaftarUsahaActivity.this, android.R.layout.simple_spinner_item, BadanUsahaArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner_badan_usaha.setAdapter(adapter);

                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    //Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                //siPAD.getInstance().dialog_alert_finish(DaftarUsahaActivity.this, v, D.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    public void dialog_pilih_sumber() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_pilih_ktp);
        dialog.setCancelable(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        ((Button) dialog.findViewById(R.id.d_galeri)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start();
                dialog.dismiss();
            }
        });
        ((Button) dialog.findViewById(R.id.d_kamera)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureImage();
                dialog.dismiss();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RC_CAMERA) {
            if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                captureImage();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
    private ImagePicker getImagePicker() {

        ImagePicker imagePicker = ImagePicker.create(this)
                .language("id")
                .returnMode(true
                        ? ReturnMode.ALL
                        : ReturnMode.NONE)
                .folderMode(true)
                .includeVideo(false)
                .toolbarArrowColor(Color.RED)
                .toolbarFolderTitle("Dokumen KTP")
                .toolbarImageTitle("Pilih untuk memilih")
                .toolbarDoneButtonText("Selesai");
        imagePicker.single();

        return imagePicker.limit(1) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera")   // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(Environment.getExternalStorageDirectory().getPath()); // can be full path
    }

    private void start() {
        getImagePicker().start(); // start image picker activity with request code
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, final int resultCode, Intent data) {
        if (ImagePicker.shouldHandle(requestCode, resultCode, data)) {
            images = (ArrayList<Image>) ImagePicker.getImages(data);
            printImages(images);
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void imgToBase64(String path){
        Bitmap bm = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream); //bm is the bitmap object
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        SIUP_DOC = "data:image/jpeg;base64,"+ Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void printImages(List<Image> images) {
        if (images == null) return;

        StringBuilder stringBuffer = new StringBuilder();
        for (int i = 0, l = images.size(); i < l; i++) {

            stringBuffer.append(images.get(i).getPath());
        }
        txt_img.setText(stringBuffer.toString());
        File file = new File(stringBuffer.toString());
        imgToBase64(stringBuffer.toString());
        Glide.with(this)
                .load(file)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(img_siup);
        show_next();
        bt_next.setText("Kirim Data");
        bt_foto_ktp.setText("Ambil Ulang");
    }

    private void show_dialog_sukses_daftar() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_sukses_daftar_usaha);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });

        dialog.getWindow().setAttributes(lp);
        ((Button) dialog.findViewById(R.id.bt_accept)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();
                //Toast.makeText(getApplicationContext(), "Button Accept Clicked", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

}
