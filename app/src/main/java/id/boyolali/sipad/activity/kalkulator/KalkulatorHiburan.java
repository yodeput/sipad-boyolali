package id.boyolali.sipad.activity.kalkulator;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import faranjit.currency.edittext.CurrencyEditText;
import fr.ganfra.materialspinner.MaterialSpinner;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.bayar.SubJenisUsaha;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.CurrencyFormat;
import id.boyolali.sipad.util.JenisPajak;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import mehdi.sakout.fancybuttons.FancyButton;

import static id.boyolali.sipad.util.siPAD.getInstance;
import static id.boyolali.sipad.util.siPAD.isEditEmpty;

public class KalkulatorHiburan extends AppCompatActivity {

    private View v;
    @BindView(R.id.spinner_data_usaha)
    MaterialSpinner spinner_data_usaha;

    @BindView(R.id.edit_omzet)
    CurrencyEditText edit_omzet;
    @BindView(R.id.edit_masapajak)
    EditText edit_masapajak;

    @BindView(R.id.but_hitung)
    FancyButton but_hitung;


    @BindView(R.id.txt_title)
    TextView txt_title;

    private PrefManager session;

    private ArrayList<String> SubUsaha;
    private ArrayList<SubJenisUsaha> SubJenisList;
    private String idUsaha, namaUsaha, kd_hiburan, title, jenisUsaha, latitude, longitude, jenis_objek, jenis_pajak, status_denda = "0", s_denda_pajak, s_besaran_pajak, tarif;
    private String masa_pajak, masa_pajak2, Kd_Rek_1 = "", Kd_Rek_2 = "", Kd_Rek_3 = "", Kd_Rek_4 = "", Kd_Rek_5 = "", address, parameter_bayar;
    private String Id_Zona, Kd_Lokasi, Lokasi, country, Area, lat, lng, administrative_area_level_1, administrative_area_level_2, administrative_area_level_3, administrative_area_level_4, postal_code;


    private LatLng latlong = new LatLng(-7.3870042, 110.3264428);
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 0;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private LayoutParams lp;
    private ArrayList<String> addressFragments;

    private int mLastSpinnerPosition = -1;
    public static AppCompatActivity end;
    private boolean isConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        session = new PrefManager(getApplicationContext());
        String languageToLoad = "id";
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        this.setContentView(R.layout.kalkulator_hiburan);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        getInstance().init_p_dialog(KalkulatorHiburan.this, v);
        end = this;
        isConnected = ConnectivityReceiver.isOnline(this);

        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        idUsaha = (String) bundle.get("idUsaha");
        namaUsaha = (String) bundle.get("namaUsaha");
        jenisUsaha = (String) bundle.get("jenisUsaha");
        jenis_pajak = (String) bundle.get("jenis_pajak");
        String titlee = (String) bundle.get("title");
        title = titlee.toLowerCase();


        txt_title.setText(JenisPajak.cek_pajak(jenis_pajak).toUpperCase());
        SubUsaha = new ArrayList<>();
        SubJenisList = new ArrayList<>();
        addressFragments = new ArrayList<String>();

        if (isConnected) {
            getInstance().req_sub_jenis_usaha(this, v, title);
            getInstance().showDialog();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {


                    if (getInstance().fileExist(getApplication(), title)) {
                        req_spinner_data();
                        getInstance().hideDialog();

                    } else {
                        getInstance().dialog_alert_finish(KalkulatorHiburan.this, v, getString(R.string.error_koneksi));
                        getInstance().hideDialog();
                    }

                }
            }, 2000);


        } else {
            getInstance().dialog_alert_finish(KalkulatorHiburan.this, v, getString(R.string.error_koneksi));
        }


        spinner_data_usaha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (mLastSpinnerPosition == position) {
                    return; //do nothing
                }
                mLastSpinnerPosition = position;
                if (mLastSpinnerPosition == -1) {
                    getInstance().toastError(getApplicationContext(), "Anda harus memilih kategori usaha");
                } else {
                    int pos = spinner_data_usaha.getSelectedItemPosition();
                    jenis_objek = SubJenisList.get(position).getnm_hiburan();
                    tarif = SubJenisList.get(position).gettarif();
                    kd_hiburan = SubJenisList.get(position).getkd_hiburan();
                    Kd_Rek_1 = SubJenisList.get(position).getkd_rek_1();
                    Kd_Rek_2 = SubJenisList.get(position).getkd_rek_2();
                    Kd_Rek_3 = SubJenisList.get(position).getkd_rek_3();
                    Kd_Rek_4 = SubJenisList.get(position).getkd_rek_4();
                    Kd_Rek_5 = SubJenisList.get(position).getkd_rek_5();

                    //getInstance().toastSuccess(getApplicationContext(), jenis_objek);
                    edit_omzet.setEnabled(true);
                    edit_omzet.requestFocus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        edit_omzet.setEnabled(false);
        edit_masapajak.setEnabled(false);
        but_hitung.setVisibility(View.GONE);
        getInstance().edit_enable(edit_omzet, edit_masapajak, 5);

    }


    @OnClick(R.id.edit_masapajak)
    void date_picker1() {
        datePicker_dialog();
    }

    private void datePicker_dialog() {
        final Calendar today = Calendar.getInstance();
        MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(this, new MonthPickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(int selectedMonth, int selectedYear) {
                String month = "";
                String month2 = "";
                if (String.valueOf(selectedMonth + 1).length() < 2) {
                    month = "0" + String.valueOf(selectedMonth + 1);
                } else {
                    month = String.valueOf(selectedMonth + 1);
                }
                masa_pajak = String.valueOf(selectedYear) + " - " + month;
                edit_masapajak.setText(masa_pajak);
                but_hitung.setVisibility(View.VISIBLE);
            }
        }, today.get(Calendar.YEAR), today.get(Calendar.MONTH));

        builder.setTitle("Pilih Masa Pajak")
                .setMinYear(1990)
                .setMaxYear(2030)
                .build()
                .show();
    }


    public void req_spinner_data() {

        String jsonString = getInstance().get_file_data(getApplicationContext(), title);

        try {
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); i++) {
                SubJenisUsaha sub = new SubJenisUsaha();
                JSONObject jsnobject = jsonArray.getJSONObject(i);
                sub.setnm_hiburan(jsnobject.getString("nm_hiburan"));
                sub.setkd_hiburan(jsnobject.getString("kd_hiburan"));
                sub.settarif(jsnobject.getString("tarif"));
                sub.setkd_rek_1(jsnobject.getString("kd_rek_1"));
                sub.setkd_rek_2(jsnobject.getString("kd_rek_2"));
                sub.setkd_rek_3(jsnobject.getString("kd_rek_3"));
                sub.setkd_rek_4(jsnobject.getString("kd_rek_4"));
                sub.setkd_rek_5(jsnobject.getString("kd_rek_5"));

                SubJenisList.add(sub);
                SubUsaha.add(jsnobject.getString("nm_hiburan"));

            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, SubUsaha);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner_data_usaha.setAdapter(adapter);

        } catch (JSONException e) {


            e.printStackTrace();
            Toast.makeText(getApplicationContext(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.but_hitung)
    void hitung() {

        masa_pajak = edit_masapajak.getText().toString();

        if ((isEditEmpty(edit_omzet)) || (isEditEmpty(edit_masapajak))) {

            getInstance().toastError(getApplicationContext(), "Lengkapi data yang dibutuhkan");

        } else {


            masa_pajak = masa_pajak.replace(" ", "");
            masa_pajak2 = masa_pajak.replace(" ", "");
            masa_pajak = masa_pajak.replace("-", "");


            String Omzet = edit_omzet.getText().toString();
            Omzet = Omzet.replace("Rp", "");
            Omzet = Omzet.replace(".", "");
            int a = Integer.parseInt(Omzet) * Integer.parseInt(tarif) / 100;
            s_besaran_pajak = Integer.toString(a);
            String param = s_besaran_pajak + "/" + masa_pajak2 + "-01/" + jenis_pajak;

            hitung_denda(param);

        }

    }

    private void hitung_denda(String param) {

        final String tag_string_req = "hitung denda";
        //Log.e(tag_string_req,param);
        StringRequest strReq = new StringRequest(Request.Method.GET,
                URLConfig.URL_HITUNG_DENDA + param, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                //Log.e(tag_string_req,response);

                try {
                    final JSONObject jObj = new JSONObject(response);
                    s_denda_pajak = jObj.getString("denda");
                    dialog_result(namaUsaha, jenisUsaha, jenis_objek, s_besaran_pajak, s_denda_pajak, edit_masapajak.getText().toString(), jenis_pajak);

                } catch (final JSONException e) {


                    e.printStackTrace();
                    Log.e(tag_string_req, e.getMessage());


                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                siPAD.getInstance().dialog_alert_finish(KalkulatorHiburan.this, v, KalkulatorHiburan.this.getString(R.string.error_koneksi));

                //Log.e(tag_string_req, error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();

                return params;
            }

        };

        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }


    private void param_bayar() {
        String params = "";
        String Omzet = edit_omzet.getText().toString();
        Omzet = Omzet.replace("Rp", "");
        Omzet = Omzet.replace(".", "");
        masa_pajak = edit_masapajak.getText().toString();
        masa_pajak = masa_pajak.replace(" ", "");

        String ST_TEMPO = "ST_TEMPO[STATUS]=" + status_denda;

        String URAIAN = "&data[URAIAN]=" + jenis_objek + " " + masa_pajak;
        String VOLUME = "&data[VOLUME]=" + "1";
        String HARGA = "&data[HARGA]=" + Omzet;
        String TARIF1 = "&data[TARIF]=" + tarif;
        String TOTAL_HARGA = "&data[TOTAL_HARGA]=" + Omzet;
        String TOTAL_PAJAK = "&data[TOTAL_PAJAK]=" + s_besaran_pajak;
        String PAJAK = "&data[PAJAK]=" + s_besaran_pajak;
        String KD_Rek_1 = "&data[KD_Rek_1]=" + Kd_Rek_1;
        String KD_Rek_2 = "&data[KD_Rek_2]=" + Kd_Rek_2;
        String KD_Rek_3 = "&data[KD_Rek_3]=" + Kd_Rek_3;
        String KD_Rek_4 = "&data[KD_Rek_4]=" + Kd_Rek_4;
        String KD_Rek_5 = "&data[KD_Rek_4]=" + Kd_Rek_5;

        String URAIAN_TARIF = "&nota[URAIAN_TARIF]=Pajak Periode " + masa_pajak;
        String TGL_NOTA = "&nota[TGL_NOTA]=" + masa_pajak + "-01";
        String USAHA_ID = "&nota[USAHA_ID]=" + idUsaha;
        String JENIS_PAJAK = "&nota[JENIS_PAJAK]=" + jenis_pajak;
        String JUMLAH_HARGA = "&nota[JUMLAH_HARGA]=" + Omzet;
        String JUMLAH_PAJAK = "&nota[JUMLAH_PAJAK]=" + s_besaran_pajak;
        String TARIF2 = "&nota[TARIF]=" + tarif;
        String LOKASI = "&nota[LOKASI]=" + address;
        String LAT = "&nota[LAT]=" + latitude;
        String LNG = "&nota[LNG]=" + longitude;
        String JENIS_OBJEK = "&nota[JENIS_OBJEK]=" + jenis_objek;
        String COUNTRY = "&nota[COUNTRY]=" + country;
        String ADMINISTRATIVE_AREA_LEVEL_1 = "&nota[ADMINISTRATIVE_AREA_LEVEL_1]=" + administrative_area_level_1;
        String ADMINISTRATIVE_AREA_LEVEL_2 = "&nota[ADMINISTRATIVE_AREA_LEVEL_2]=" + administrative_area_level_2;
        String ADMINISTRATIVE_AREA_LEVEL_3 = "&nota[ADMINISTRATIVE_AREA_LEVEL_3]=" + administrative_area_level_3;
        String ADMINISTRATIVE_AREA_LEVEL_4 = "&nota[ADMINISTRATIVE_AREA_LEVEL_4]=" + administrative_area_level_4;
        String POSTAL_CODE = "&nota[POSTAL_CODE]=" + postal_code;
        String JENIS_PERMOHONAN = "&nota[JENIS_PERMOHONAN]=" + "0";

        parameter_bayar = ST_TEMPO + URAIAN + VOLUME + HARGA + TARIF1 + TOTAL_HARGA + TOTAL_PAJAK + PAJAK + KD_Rek_1 + KD_Rek_2 + KD_Rek_3 + KD_Rek_4 + KD_Rek_5
                + URAIAN_TARIF + TGL_NOTA + USAHA_ID + JENIS_PAJAK + JUMLAH_HARGA + JUMLAH_PAJAK + TARIF2 + LOKASI + LAT + LNG + JENIS_OBJEK + COUNTRY
                + ADMINISTRATIVE_AREA_LEVEL_1 + ADMINISTRATIVE_AREA_LEVEL_2 + ADMINISTRATIVE_AREA_LEVEL_3 + ADMINISTRATIVE_AREA_LEVEL_4
                + POSTAL_CODE + JENIS_PERMOHONAN;


    }


    public void dialog_result(final String namaUsaha, String jenisUsaha, String jenis_objek, String s_besaran_pajak, String s_denda_pajak, final String masa_pajak, String jenis_pajak) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(this).inflate(R.layout.kalkulator_hasil_hitung,
                (ViewGroup) v.findViewById(android.R.id.content), false);

        final TextView txt_jns_pajak = viewInflated.findViewById(R.id.txt_jns_pajak);
        final TextView txt_nm_usaha = viewInflated.findViewById(R.id.txt_nm_usaha);
        final TextView L_7 = viewInflated.findViewById(R.id.L_7);
        final TextView txt_jns_usaha = viewInflated.findViewById(R.id.txt_jns_usaha);
        final TextView txt_kategori = viewInflated.findViewById(R.id.txt_kategori);
        final TextView txt_masapajak = viewInflated.findViewById(R.id.txt_masapajak);
        final TextView txt_besaran_pajak = viewInflated.findViewById(R.id.txt_besaran_pajak);
        final TextView txt_denda_pajak = viewInflated.findViewById(R.id.txt_denda_pajak);
        final TextView txt_terbilang = viewInflated.findViewById(R.id.txt_terbilang);
        final ImageView img_share = viewInflated.findViewById(R.id.img_share);
        final Button dialog_button_ok = viewInflated.findViewById(R.id.dialog_button_ok);
        final LinearLayout ll_2_button = viewInflated.findViewById(R.id.ll_2_button);

        dialog_button_ok.setText("OK");


        txt_jns_pajak.setText("Pajak " + JenisPajak.cek_pajak(jenis_pajak));
        txt_nm_usaha.setText(namaUsaha);
        txt_jns_usaha.setText(jenisUsaha);
        txt_kategori.setText(jenis_objek);
        txt_masapajak.setText(masa_pajak);
        txt_besaran_pajak.setText(CurrencyFormat.rupiah_format_string(s_besaran_pajak));
        int denda = Integer.parseInt(s_denda_pajak);
        if (denda == 0) {
            status_denda = "0";
            txt_denda_pajak.setText(s_denda_pajak);
        } else {
            s_denda_pajak = CurrencyFormat.rupiah_format_string(s_denda_pajak);
            txt_denda_pajak.setText(s_denda_pajak);
            status_denda = "1";
        }


        txt_terbilang.setText("(" + CurrencyFormat.info_terbilang(Integer.parseInt(s_besaran_pajak)) + " rupiah)");


        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });


        dialog_button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

        img_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_2_button.setVisibility(View.GONE);
                img_share.setVisibility(View.GONE);
                dialog_button_ok.setVisibility(View.GONE);
                getInstance().shareImage(getInstance().takeSS(viewInflated, namaUsaha + "_" + masa_pajak));
                dialog_button_ok.setVisibility(View.VISIBLE);
                img_share.setVisibility(View.VISIBLE);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        //getInstance().init_p_dialog(KalkulatorHiburan.this,v);
    }

}
