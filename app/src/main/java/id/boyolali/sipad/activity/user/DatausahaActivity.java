package id.boyolali.sipad.activity.user;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.DaftarUsahaActivity;
import id.boyolali.sipad.activity.pembayaran.BayarAirtanah;
import id.boyolali.sipad.activity.pembayaran.BayarHiburan;
import id.boyolali.sipad.activity.pembayaran.BayarMinerba;
import id.boyolali.sipad.activity.pembayaran.BayarPenerangan;
import id.boyolali.sipad.activity.pembayaran.BayarReklame;
import id.boyolali.sipad.adapter.user.USAHA;
import id.boyolali.sipad.adapter.user.USAHAAdapter;
import id.boyolali.sipad.util.JenisPajak;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.siPAD.getInstance;


public class DatausahaActivity extends AppCompatActivity implements USAHAAdapter.USAHAsAdapterListener {

    private static final String TAG = DatausahaActivity.class.getSimpleName();

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txt_datakosong)
    TextView txt_datakosong;
    @BindView(R.id.cv_recycler)
    CardView cv_recycler;
    @BindView(R.id.search_view)
    MaterialSearchView searchView;
    @BindView(R.id.l_silahkan_pilih)
    TextView l_silahkan_pilih;

    private View v;
    private List<USAHA> USAHAList;
    private USAHAAdapter mAdapter;
    private AlertDialog dialog;
    private PrefManager session;
    private String title, jenis_pajak;
    private Toolbar toolbar;
    public static AppCompatActivity end;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_usaha);
        ButterKnife.bind(this);
        v = getWindow().getDecorView().findViewById(android.R.id.content);
        session = new PrefManager(this.getApplicationContext());
        end = this;
        getInstance().init_p_dialog(this, v);

        Intent in = getIntent();
        Bundle bundle = in.getExtras();
        String from = (String) bundle.get("title");
        jenis_pajak = (String) bundle.get("jenis_pajak");

        if (from.contains("Usaha")) {

            title = "Data Usaha";
            Toolbar toolbar = findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back);
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
            this.setSupportActionBar(toolbar);
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            this.getSupportActionBar().setTitle(title);
            siPAD.getInstance().fabric_report("User Data Usaha","","");

        } else if (from.equals("PAJAK AIR TANAH")) {

            l_silahkan_pilih.setVisibility(View.VISIBLE);
            title = from;
            toolbar = findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back);
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
            this.setSupportActionBar(toolbar);
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            this.getSupportActionBar().setTitle(title);
            init_title();
            getInstance().req_sub_jenis_usaha(this, v, "airtanah");

        } else {

            l_silahkan_pilih.setVisibility(View.VISIBLE);
            title = from;
            toolbar = findViewById(R.id.toolbar);
            toolbar.setNavigationIcon(R.drawable.ic_action_arrow_back);
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
            this.setSupportActionBar(toolbar);
            this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            this.getSupportActionBar().setTitle(title);
            init_title();

            String fileName = JenisPajak.cek_pajak(jenis_pajak);
            getInstance().req_sub_jenis_usaha(this, v, fileName.toLowerCase());
        }


        init_data();
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                mAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });



    }

    private void init_data(){
        USAHAList = new ArrayList<>();
        mAdapter = new USAHAAdapter(this, USAHAList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        String dataUSAHA_len = session.getPref("dataUSAHA_len");
        if (dataUSAHA_len.equals("0")) {
            txt_datakosong.setVisibility(View.VISIBLE);
            cv_recycler.setVisibility(View.GONE);
        } else {
            read_json();
        }
    }

    @OnClick(R.id.fab_add) void add_usaha(){
        startActivity(new Intent(DatausahaActivity.this, DaftarUsahaActivity.class));
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            if (title.contains("Data Usaha")) {
                super.onBackPressed();
            } else {
                finish();
                //siPAD.getInstance().dialog_cancel_bayar(this,v);

            }


        }
    }

    private void read_json() {

        String jsonString = siPAD.getInstance().get_file_data(this, "dataUSAHA");

        List<USAHA> items = new Gson().fromJson(jsonString, new TypeToken<List<USAHA>>() {
        }.getType());

        // adding contacts to contacts list
        USAHAList.clear();
        USAHAList.addAll(items);

    }


    @Override
    public void onUSAHASelected(USAHA USAHA) {
        String caseTitle = title.toLowerCase();
        if (caseTitle.contains("usaha")) {


        } else if (jenis_pajak.contains("8") || jenis_pajak.contains("5") || jenis_pajak.contains("7") || jenis_pajak.contains("4")) {

            String[] parts = title.split(" ");
            String lastWord = parts[parts.length - 1];
            Intent i = new Intent(this, BayarPenerangan.class);
            i.putExtra("idUsaha", USAHA.getUSAHA_ID());
            i.putExtra("namaUsaha", USAHA.getNAMA_USAHA());
            i.putExtra("jenisUsaha", USAHA.getDESKRIPSI_BADAN_USAHA());
            i.putExtra("jenisPajak", lastWord);
            i.putExtra("title", lastWord);
            i.putExtra("jenis_pajak", jenis_pajak);
            startActivity(i);

        } else if (jenis_pajak.contains("2")) {

            String[] parts = title.split(" ");
            String lastWord = parts[parts.length - 1];
            Intent i = new Intent(this, BayarAirtanah.class);
            i.putExtra("idUsaha", USAHA.getUSAHA_ID());
            i.putExtra("namaUsaha", USAHA.getNAMA_USAHA());
            i.putExtra("jenisUsaha", USAHA.getDESKRIPSI_BADAN_USAHA());
            i.putExtra("jenisPajak", lastWord);
            i.putExtra("title", lastWord);
            i.putExtra("jenis_pajak", jenis_pajak);
            startActivity(i);


        } else if (jenis_pajak.contains("3")) {

            String[] parts = title.split(" ");
            String lastWord = parts[parts.length - 1];
            Intent i = new Intent(this, BayarMinerba.class);
            i.putExtra("idUsaha", USAHA.getUSAHA_ID());
            i.putExtra("namaUsaha", USAHA.getNAMA_USAHA());
            i.putExtra("jenisUsaha", USAHA.getDESKRIPSI_BADAN_USAHA());
            i.putExtra("jenisPajak", lastWord);
            i.putExtra("title", lastWord);
            i.putExtra("jenis_pajak", jenis_pajak);
            startActivity(i);

        } else if (jenis_pajak.contains("10")) {

            String[] parts = title.split(" ");
            String lastWord = parts[parts.length - 1];
            Intent i = new Intent(this, BayarHiburan.class);
            i.putExtra("idUsaha", USAHA.getUSAHA_ID());
            i.putExtra("namaUsaha", USAHA.getNAMA_USAHA());
            i.putExtra("jenisUsaha", USAHA.getDESKRIPSI_BADAN_USAHA());
            i.putExtra("jenisPajak", lastWord);
            i.putExtra("title", lastWord);
            i.putExtra("jenis_pajak", jenis_pajak);
            startActivity(i);

        } else if (jenis_pajak.contains("1")) {

            String[] parts = title.split(" ");
            String lastWord = parts[parts.length - 1];
            Intent i = new Intent(this, BayarReklame.class);
            i.putExtra("idUsaha", USAHA.getUSAHA_ID());
            i.putExtra("namaUsaha", USAHA.getNAMA_USAHA());
            i.putExtra("jenisUsaha", USAHA.getDESKRIPSI_BADAN_USAHA());
            i.putExtra("jenisPajak", lastWord);
            i.putExtra("title", lastWord);
            i.putExtra("jenis_pajak", jenis_pajak);
            startActivity(i);
        } else {

            siPAD.getInstance().toastError(DatausahaActivity.this, "On progress");
        }
    }

    public void clear() {
        USAHAList.clear();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        clear();
        init_data();

        super.onResume();
    }

    private void init_title() {

        ActionBar ab = this.getSupportActionBar();
        TextView tv = new TextView(getApplicationContext());
        LayoutParams lp = new RelativeLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT);
        Typeface face = ResourcesCompat.getFont(getApplicationContext(), R.font.coda);
        tv.setTypeface(face);
        tv.setLayoutParams(lp);
        tv.setText(ab.getTitle());
        tv.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(tv);
    }



}