package id.boyolali.sipad.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.jsibbold.zoomage.ZoomageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.DaftarAkunActivity;
import id.boyolali.sipad.activity.DaftarUsahaActivity;


public class SliderDialogFragment extends DialogFragment {

    private int request_code = 0;
    private View root_view;
    private TextView txt_judul,txt_tanggal,txt_isi;
    private ZoomageView zoomageView;
    @BindView(R.id.bt_close)ImageButton bt_close;

    @OnClick (R.id.bt_close) void close(){
        dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.dialog_slider, container, false);
        ButterKnife.bind(this,root_view);
        String judul = getArguments().getString("judul");
        String gambar = getArguments().getString("gambar");
        String date = getArguments().getString("date");
        txt_judul = (TextView) root_view.findViewById(R.id.txt_judul);
        zoomageView = (ZoomageView) root_view.findViewById(R.id.myZoomageView);

        txt_judul.setText(judul);
        if(gambar.contains("slide_daftar.png")){
            startActivity(new Intent(getActivity(), DaftarAkunActivity.class));
            dismiss();

        } else if(gambar.contains("usaha")) {
            startActivity(new Intent(getActivity(), DaftarUsahaActivity.class));
            dismiss();

        } else {

        Glide.with(getActivity())
                .load(gambar)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(zoomageView);

        root_view.setFocusableInTouchMode(true);
        root_view.requestFocus();
        root_view.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    dismiss();
                    return true;
                }
                return false;
            }
        });
        }
        return root_view;
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    public void onBackPressed() {
      dismiss();
    }


}