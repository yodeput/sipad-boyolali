package id.boyolali.sipad.fragment.info;

import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.readystatesoftware.systembartint.SystemBarTintManager;

import id.boyolali.sipad.R;


public class InfoBayarFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = InfoBayarFragment.class.getSimpleName();
    Button firstFragment, secondFragment;
    private View v;

    public static InfoBayarFragment newInstance(String text) {

        Bundle args = new Bundle();
        args.putString("msg", text);

        InfoBayarFragment fragment = new InfoBayarFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_info_bayar, container, false);
        initStatusBar();


        //setHasOptionsMenu(true);
        //toolbar = v.findViewById(R.id.toolbar);
        //((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        //((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.toolbar_title_info);


        return v;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.firstFragment: {
                loadFragment(new InfoPajakFragment());
                //Toast.makeText(getActivity(),"firstFragment",Toast.LENGTH_LONG).show();

                return;
            }
            case R.id.secondFragment: {
                loadFragment(new InfoBayarFragment());
                //Toast.makeText(getActivity(),"secondFragment",Toast.LENGTH_LONG).show();
            }
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.commit();
    }

    private void initStatusBar() {
        Window window = getActivity().getWindow();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            SystemBarTintManager tintManager = new SystemBarTintManager(getActivity());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        }

    }
}
