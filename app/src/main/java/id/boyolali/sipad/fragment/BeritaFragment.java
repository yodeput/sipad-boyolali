package id.boyolali.sipad.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.Berita;
import id.boyolali.sipad.adapter.BeritaAdapter;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;


public class BeritaFragment extends Fragment implements BeritaAdapter.BeritasAdapterListener, View.OnClickListener {
    private static final String TAG = BeritaFragment.class.getSimpleName();
    private final static String TAG_FRAGMENT = "FRAGMENT_BERITA";
    public static AlertDialog pDialog;
    @BindView(R.id.img_exit)
    ImageView img_exit;
    @BindView(R.id.fab_refresh)
    FloatingActionButton fab_refresh;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.txt_keterangan)TextView txt_keterangan;
    @BindView(R.id.shimmer_view_container)ShimmerFrameLayout mShimmerViewContainer;
    private View v;
    private List<Berita> beritaList;
    private BeritaAdapter mAdapter;

    public static BeritaFragment newInstance(String text) {

        Bundle args = new Bundle();
        args.putString("msg", text);

        BeritaFragment fragment = new BeritaFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_berita, container, false);
        ButterKnife.bind(this, v);
        initStatusBar();
        init_p_dialog();


        TextView textView = v.findViewById(R.id.btn_horizontal_ntb);
        //      textView.setText(getArguments().getString("msg"));

        img_exit.setOnClickListener(this);
        fab_refresh.setOnClickListener(this);

        startAnim();

        beritaList = new ArrayList<>();
        mAdapter = new BeritaAdapter(getActivity(), beritaList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        fetchBeritaNoDialog();
        return v;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_exit: {
                siPAD.getInstance().dialog_exit(getActivity(), v,"Akan keluar Aplikasi?");
                return;
            }
            case R.id.fab_refresh: {

                fetchBerita();
            }
        }
    }

    private void startAnim(){

        mShimmerViewContainer.startShimmer();
        mShimmerViewContainer.setVisibility(View.VISIBLE);
        txt_keterangan.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);

    }

    private void stopAnim(){

        mShimmerViewContainer.stopShimmer();
        mShimmerViewContainer.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);

    }

    private void fetchBerita() {
        startAnim();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(URLConfig.URL_BERITA,null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        stopAnim();
                        try {
                            JSONArray jsonArray = response.getJSONArray("berita");
                            String row = response.getString("row");
                            if (row.equals("0")){
                                txt_keterangan.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                fab_refresh.setVisibility(View.GONE);
                            } else {
                                fab_refresh.setVisibility(View.VISIBLE);
                                txt_keterangan.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);

                            List<Berita> items = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Berita>>() {
                            }.getType());
                            beritaList.clear();
                            beritaList.addAll(items);
                            hideDialog();
                            mAdapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error: " + e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                hideDialog();
                //siPAD.getInstance().fabric_report(TAG,"Fetch Data",error.getMessage());
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void fetchBeritaNoDialog() {
    startAnim();
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(URLConfig.URL_BERITA,null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        stopAnim();
                        try {
                            JSONArray jsonArray = response.getJSONArray("berita");
                            String row = response.getString("row");
                            if (row.equals("0")){
                                txt_keterangan.setVisibility(View.VISIBLE);
                                recyclerView.setVisibility(View.GONE);
                                fab_refresh.setVisibility(View.GONE);
                            } else {
                                fab_refresh.setVisibility(View.VISIBLE);
                                txt_keterangan.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);


                                List<Berita> items = new Gson().fromJson(jsonArray.toString(), new TypeToken<List<Berita>>() {
                                }.getType());
                                beritaList.clear();
                                beritaList.addAll(items);
                                hideDialog();
                                mAdapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e) {
                            e.printStackTrace();
                            e.printStackTrace();
                            Log.e(TAG, "Error: " + e.getMessage());
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // error in getting json
                hideDialog();
                //siPAD.getInstance().fabric_report(TAG,"Fetch Data",error.getMessage());
                Log.e(TAG, "Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(), "Error: " + error.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

        siPAD.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    public void onBeritaSelected(Berita berita) {
        FragmentManager fragmentManager = getFragmentManager();
        BeritaDialogFragment newFragment = new BeritaDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("judul", berita.getjudul());
        bundle.putString("isi", berita.getisi());
        bundle.putString("gambar", berita.getgambar());
        bundle.putString("date", berita.gettanggal());

        newFragment.setArguments(bundle);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();


        //Toast.makeText(getActivity().getApplicationContext(), "Informasi: " + berita.getjudul(), Toast.LENGTH_LONG).show();
    }

    public void clear() {
        beritaList.clear();
        mAdapter.notifyDataSetChanged();
    }

    public void addAll(List<Berita> list) {
        beritaList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    private void initStatusBar() {
        Window window = getActivity().getWindow();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            SystemBarTintManager tintManager = new SystemBarTintManager(getActivity());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        }

    }

    public void init_p_dialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View viewInflated = LayoutInflater.from(getActivity()).inflate(R.layout.p_dialog,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        AVLoadingIndicatorView avi = viewInflated.findViewById(R.id.avi);
        builder.setView(viewInflated);
        pDialog = builder.create();
        avi.show();
        pDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = pDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;

    }

    public void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    public void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        mShimmerViewContainer.stopShimmer();
        super.onDestroy();
    }

    @Override
    public void onResume() {
       startAnim();
        super.onResume();
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmer();
        super.onPause();
    }
}
