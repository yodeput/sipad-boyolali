package id.boyolali.sipad.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.skydoves.powermenu.OnDismissedListener;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import id.boyolali.sipad.activity.DaftarAkunActivity;
import id.boyolali.sipad.activity.user.DatausahaActivity;
import id.boyolali.sipad.activity.user.KBActivity;
import id.boyolali.sipad.activity.user.LunasActivity;
import id.boyolali.sipad.activity.user.OPPActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.boyolali.sipad.activity.AboutActivity;
import id.boyolali.sipad.R;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.URLConfig;
import id.boyolali.sipad.util.siPAD;
import id.boyolali.sipad.util.SQLiteHandler;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import mehdi.sakout.fancybuttons.FancyButton;

import static id.boyolali.sipad.util.siPAD.getInstance;


public class UserFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = UserFragment.class.getSimpleName();
    public static LinearLayout ll_login;
    private View v;
    private PrefManager session;
    private SQLiteHandler db;
    private AlertDialog dialog;
    private PowerMenu hamburgerMenu;

    @BindView(R.id.email)
    EditText inputEmail;
    @BindView(R.id.password)
    EditText inputPassword;
    @BindView(R.id.btnLogin)
    FancyButton btnLogin;
    @BindView(R.id.cv_login)
    CardView cv_login;
    @BindView(R.id.rl_profile)
    RelativeLayout rl_profile;
    @BindView(R.id.img_profile)
    CircleImageView imgProfile;
    @BindView(R.id.txt_name)
    TextView txt_name;
    @BindView(R.id.txt_npwpd)
    TextView txt_npwpd;
    @BindView(R.id.txt_email)
    TextView txt_email;
    @BindView(R.id.txt_phone)
    TextView txt_phone;
    @BindView(R.id.txt_status)
    TextView txt_status;
    @BindView(R.id.txt_kb)
    TextView txt_kb;
    @BindView(R.id.txt_pp)
    TextView txt_pp;
    @BindView(R.id.txt_lunas)
    TextView txt_lunas;
    @BindView(R.id.txt_datausaha)
    TextView txt_datausaha;
    @BindView(R.id.progressBar)
    ProgressBar pg;
    @BindView(R.id.but_menu)
    ImageView but_menu;
    @BindView(R.id.but_edit_profile)
    ImageView but_edit_profile;
    @BindView(R.id.ll_menu_user)
    LinearLayout ll_menu_user;

    private String packageName, status_akun;
    private String contain_npwpd;
    private boolean isConnected;

    @OnClick(R.id.ll_kb)
    void button_kb() {
        Intent i = new Intent(getActivity(), KBActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.ll_opp)
    void button_opp() {
        Intent i = new Intent(getActivity(), OPPActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.ll_lunas)
    void button_lunas() {
        Intent i = new Intent(getActivity(), LunasActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.ll_du)
    void button_du() {
        Intent i = new Intent(getActivity(), DatausahaActivity.class);
        i.putExtra("title", "Data Usaha");
        startActivity(i);
    }


    @OnClick(R.id.img_cp_npwpd)
    void copy_npwpd() {

        String npwpd_to_copy = txt_npwpd.getText().toString();
        getInstance().copyText(getActivity(), npwpd_to_copy);

    }

    @OnClick(R.id.bt_daftar) void daftar(){
        Intent i = new Intent(getActivity(), DaftarAkunActivity.class);
        startActivity(i);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, v);

        packageName = getActivity().getPackageName();
        isConnected = ConnectivityReceiver.isOnline(getActivity());
        inputEmail.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        getInstance().init_p_dialog(getActivity(),v);

        sessionCheck();

        init_hamburgermenu();

        btnLogin.setOnClickListener(this);
        but_menu.setOnClickListener(this);
        but_edit_profile.setOnClickListener(this);

        //inputEmail.setText("usep.senks@gmail.com");
        //inputPassword.setText("12345");

        return v;
    }


    private void taptarget1() {
        TapTargetView.showFor(getActivity(),
                TapTarget.forView(v.findViewById(R.id.ll_kb),
                        "Data Akun",
                        "Ini adalah salah satu menu yang memuat seluruh data yang dimiliki oleh Anda")
                        .titleTextColor(R.color.black)
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.white)
                        .outerCircleColor(R.color.teal_300)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        taptarget2();
                        session.addPref("taptarget_user", "false");

                    }
                });
    }

    private void taptarget2() {
        HashMap<String, String> user = db.getUserDetails();
        status_akun = user.get("npwpd_status");
        String a;
        if (status_akun.equals("1")) {
            a="Akun anda sudah aktif dan bisa melakukan transaksi";

        } else {
            a="Akun anda belum aktif, mohon tunggu verifikasi dari kami";

        }

        TapTargetView.showFor(getActivity(),
                TapTarget.forView(v.findViewById(R.id.txt_status),
                        "Status Akun",
                        a)
                        .titleTextColor(R.color.black)
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.white)
                        .outerCircleColor(R.color.teal_300)
                        .outerCircleAlpha(0.96f)
                        .cancelable(false),
                new TapTargetView.Listener() {
                    @Override
                    public void onTargetClick(TapTargetView view) {
                        super.onTargetClick(view);
                        view.dismiss(true);
                        session.addPref("taptarget_user", "false");

                    }
                });
    }
    private void sessionCheck() {

        db = new SQLiteHandler(getActivity().getApplicationContext());
        session = new PrefManager(getActivity().getApplicationContext());

        if (session.isLoggedIn()) {

            if (isConnected) {
                siPAD.getInstance().req_slideshow_data(getActivity(), v);
                view_data();
            }
            rl_profile.setVisibility(View.VISIBLE);
            cv_login.setVisibility(View.GONE);

        } else {

            rl_profile.setVisibility(View.GONE);
            cv_login.setVisibility(View.VISIBLE);

        }
    }

    private void view_data() {
        //siPAD.getInstance().req_slideshow_data(getActivity(),v);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                String a1 = session.getPref("dataLUNAS_len");
                String a2 = session.getPref("dataKB_len");
                String a3 = session.getPref("dataOPP_len");
                String a4 = session.getPref("dataUSAHA_len");

                //Log.v(TAG,"aaaaaaaaaaaaaaaaaaaaaaaaaaa"+a1+a2+a3+a4);

                txt_lunas.setText(a1);
                txt_kb.setText(a2);
                txt_pp.setText(a3);
                txt_datausaha.setText(a4);
            }
        }, 1500);


        HashMap<String, String> user = db.getUserDetails();
        txt_name.setText(user.get("nama_lengkap"));
        txt_npwpd.setText(user.get("npwpd"));
        txt_email.setText(user.get("email"));
        txt_phone.setText(user.get("no_telp"));

        contain_npwpd = txt_npwpd.getText().toString();
        status_akun = user.get("npwpd_status");

        if (status_akun.contains("1")) {
            session.setActive(true);
            txt_status.setText("     Aktif     ");
            txt_status.setTextColor(getResources().getColor(R.color.white));
            txt_status.setBackgroundColor(getResources().getColor(R.color.hijau));
        } else {
            session.setActive(false);
            txt_status.setText("  Belum Aktif  ");
            txt_status.setTextColor(getResources().getColor(R.color.white));
            txt_status.setBackgroundColor(getResources().getColor(R.color.merah));
        }

        loadPP();
        String taptarget_user = session.getPref("taptarget_user");
        if (taptarget_user.isEmpty()) {

            Handler handlerr = new Handler();
            handlerr.postDelayed(new Runnable() {
                public void run() {
                    taptarget1();
                }
            }, 1500);

        }
    }

    private void init_hamburgermenu() {

        hamburgerMenu = new PowerMenu.Builder(getActivity())
                .addItem(new PowerMenuItem("Refresh Data", false))
                .addItem(new PowerMenuItem("Keluar Akun", false))
                .addItem(new PowerMenuItem("Tentang siPAD", false))
                .setLifecycleOwner(this)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setSelectedEffect(true)
                .setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark))
                .setSelectedTextColor(Color.WHITE)
                .setMenuColor(Color.WHITE)
                .setSelectedMenuColor(getActivity().getResources().getColor(R.color.colorPrimary))
                .setOnMenuItemClickListener(onMenuItemClickListener)
                .setOnDismissListener(onDismissedListener)
                .build();
    }

    private OnDismissedListener onDismissedListener = new OnDismissedListener() {
        @Override
        public void onDismissed() {
        }
    };

    private OnMenuItemClickListener<PowerMenuItem> onMenuItemClickListener = new OnMenuItemClickListener<PowerMenuItem>() {
        @Override
        public void onItemClick(int position, PowerMenuItem item) {
            String menu = item.getTitle();
            switch (menu) {
                case "Refresh Data": {


                    getInstance().showDialog();
                    if (isConnected) {
                        getInstance().refresh_data_user(getActivity());
                    }

                    view_data();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        public void run() {

                            getInstance().hideDialog();

                        }
                    }, 1500);


                    hamburgerMenu.dismiss();
                    return;
                }
                case "Tentang siPAD": {

                    Intent i = new Intent(getActivity(), AboutActivity.class);
                    startActivity(i);

                    hamburgerMenu.dismiss();
                    return;
                }
                case "Keluar Akun": {

                    dialog_logout();

                    hamburgerMenu.dismiss();
                }
            }


        }
    };

    public static UserFragment newInstance(String text) {

        Bundle args = new Bundle();
        args.putString("msg", text);

        UserFragment fragment = new UserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but_menu: {

                hamburgerMenu.showAsDropDown(but_menu, 0, 0);
                return;

            }
            case R.id.but_edit_profile: {

                //dialog_password();

                return;

            }
            case R.id.btnLogin: {

                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                siPAD.hideSoftKeyboard(getActivity());

                if (!email.isEmpty() && !password.isEmpty()) {

                    if (isConnected){
                        checkLogin(email, password);
                    }


                } else {

                    FancyToast.makeText(getActivity(), "Username / Password masih kosong", FancyToast.LENGTH_LONG, FancyToast.ERROR, false).show();

                }
            }
        }
    }

    private void checkLogin(final String username, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        getInstance().showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                URLConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Login Response: " + response.toString());
                getInstance().hideDialog();

                try {
                    final JSONObject jObj = new JSONObject(response);
                    final Boolean error = jObj.getBoolean("error");

                    if (!error) {

                        session.setLogin(true);
                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("nama_lengkap");
                        String npwpd = user.getString("npwpd");
                        String email = user.getString("email");
                        String no_telp = user.getString("no_telp");
                        String foto = user.getString("foto");
                        String status = user.getString("npwpd_status");
                        String nasabah_id = user.getString("nasabah_id");

                        contain_npwpd = npwpd;
                        rl_profile.setVisibility(View.VISIBLE);
                        cv_login.setVisibility(View.GONE);
                        inputEmail.setText("");
                        inputPassword.setText("");


                        db.addUser(name, npwpd, nasabah_id, email, no_telp, foto, status);
                        session.addPref("name", name);
                        session.addPref("name", name);
                        session.addPref("npwpd", npwpd);
                        session.addPref("nasabah_id", nasabah_id);

                        getInstance().req_user_data(npwpd, getActivity());
                        getInstance().req_user_timeline(nasabah_id, getActivity());

                        getInstance().hideDialog();
                        view_data();



                    } else {

                        String errorMsg = jObj.getString("msg");
                        errorMsg = errorMsg.replace("<br>", "\n");
                        siPAD.getInstance().dialog_alert(getActivity(), v, errorMsg);

                    }
                } catch (final JSONException e) {

                    getInstance().hideDialog();
                    e.printStackTrace();
                    Log.e("Json Login error: ", e.getMessage());

                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(final VolleyError error) {
                getInstance().hideDialog();
                siPAD.getInstance().dialog_alert(getActivity(), v, getActivity().getResources().getString(R.string.error_koneksi));
                Log.e(TAG, "Login Error: " + error.getMessage());
                //Toast.makeText(getActivity().getApplicationContext(),error.getMessage()+"sdhfjdsfjlsdjflsdjf", Toast.LENGTH_LONG).show();
                //siPAD.getInstance().fabric_report(TAG, "Check Login", error.getMessage());

                //dialog_alert(error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }

        };

        // Adding request to request queue
        siPAD.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void logoutUser() {
        getInstance().showDialog();
        siPAD.getInstance().delete_file_data(getContext());

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                session.setLogin(false);
                db.deleteUsers();
                rl_profile.setVisibility(View.GONE);
                cv_login.setVisibility(View.VISIBLE);
                getInstance().hideDialog();
            }
        }, 1500);


    }

    private void loadPP() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                File imgDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "Android" + File.separator + "data" + File.separator + packageName + File.separator + "img");
                File picUser = new File(imgDir + "/" + contain_npwpd + ".png");
                imgProfile.setVisibility(View.VISIBLE);
                pg.setVisibility(View.INVISIBLE);
                Picasso.get()
                        .load(R.drawable.img_profile)
                        .error(R.drawable.img_profile)
                        .memoryPolicy(MemoryPolicy.NO_STORE)
                        .transform(new CropCircleTransformation())
                        .noFade()
                        .into(imgProfile);

            }
        }, 800);

    }

    private void dialog_password() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_with_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText("Anda akan merubah data akun?");
        dialog_left.setText("Tidak");
        dialog_right.setText("Ya");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getInstance().toastInfo(getActivity(), "Layanan belum tersedia");

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });


    }

    public void dialog_logout() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogSlideAnim);
        final View viewInflated = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_with_button,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        final TextView titletxt = viewInflated.findViewById(R.id.title_txt);
        final TextView messagetxt = viewInflated.findViewById(R.id.message_txt);
        final Button dialog_left = viewInflated.findViewById(R.id.dialog_left);
        final Button dialog_right = viewInflated.findViewById(R.id.dialog_right);
        titletxt.setText("");
        messagetxt.setText("Akun anda akan keluar??");
        dialog_left.setText("Tidak");
        dialog_right.setText("Ya");
        builder.setView(viewInflated);
        final AlertDialog dialog = builder.create();

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;


        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                }
                return true;
            }
        });
        dialog_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                logoutUser();

            }
        });
        dialog_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        getInstance().init_p_dialog(getActivity(), v);
        sessionCheck();
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

}