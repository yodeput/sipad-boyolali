package id.boyolali.sipad.fragment.info;


import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.wang.avi.AVLoadingIndicatorView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.R;


public class InfoPajakFragment extends Fragment  implements View.OnClickListener {
    private View v;
    private androidx.appcompat.app.AlertDialog dialog;

    @BindView(R.id.but_airtanah)
    ImageButton but_airtanah;
    @BindView(R.id.but_bphtb)
    ImageButton but_bphtb;
    @BindView(R.id.but_hiburan)
    ImageButton but_hiburan;
    @BindView(R.id.but_hotel)
    ImageButton but_hotel;
    @BindView(R.id.but_minerba)
    ImageButton but_minerba;
    @BindView(R.id.but_parkir)
    ImageButton but_parkir;
    @BindView(R.id.but_pbb)
    ImageButton but_pbb;
    @BindView(R.id.but_penerangan)
    ImageButton but_penerangan;
    @BindView(R.id.but_restoran)
    ImageButton but_restoran;
    @BindView(R.id.but_walet)
    ImageButton but_walet;
    @BindView(R.id.but_reklame)
    ImageButton but_reklame;
    @BindView(R.id.txt_info)
    TextView txt_info;
    @BindView(R.id.img_logo)ImageView img_logo;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_info_pajak, container, false);
        ButterKnife.bind(this, v);

        initStatusBar();
        init_p_dialog();

        but_airtanah.setOnClickListener(this);
        but_bphtb.setOnClickListener(this);
        but_hiburan.setOnClickListener(this);
        but_hotel.setOnClickListener(this);
        but_minerba.setOnClickListener(this);
        but_parkir.setOnClickListener(this);
        but_pbb.setOnClickListener(this);
        but_penerangan.setOnClickListener(this);
        but_restoran.setOnClickListener(this);
        but_walet.setOnClickListener(this);
        but_reklame.setOnClickListener(this);
        txt_info.setVisibility(View.GONE);

        but_airtanah.setColorFilter(ContextCompat.getColor(getActivity(),R.color.airtanah));
        but_bphtb.setColorFilter(ContextCompat.getColor(getActivity(),R.color.bphtb));
        but_hiburan.setColorFilter(ContextCompat.getColor(getActivity(),R.color.hiburan));
        but_hotel.setColorFilter(ContextCompat.getColor(getActivity(),R.color.hotel));
        but_minerba.setColorFilter(ContextCompat.getColor(getActivity(),R.color.minerba));
        but_parkir.setColorFilter(ContextCompat.getColor(getActivity(),R.color.parkir));
        but_pbb.setColorFilter(ContextCompat.getColor(getActivity(),R.color.pbb));
        but_penerangan.setColorFilter(ContextCompat.getColor(getActivity(),R.color.penerangan));
        but_restoran.setColorFilter(ContextCompat.getColor(getActivity(),R.color.restoran));
        but_walet.setColorFilter(ContextCompat.getColor(getActivity(),R.color.walet));
        but_reklame.setColorFilter(ContextCompat.getColor(getActivity(),R.color.reklame));
        //setHasOptionsMenu(true);
        //toolbar = v.findViewById(R.id.toolbar);
        //((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        //((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.toolbar_title_info);

        return v;

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.but_airtanah: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_airtanah));

                return;
            }
            case R.id.but_bphtb: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_bph));

                return;
            }
            case R.id.but_hiburan: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_hiburan));

                return;
            }
            case R.id.but_hotel: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_hotel));

                return;
            }
            case R.id.but_minerba: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_minerba));
                return;
            }
            case R.id.but_parkir: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_parkir));
                return;
            }
            case R.id.but_pbb: {
                showInfo(getString(R.string.info_pbb));
                txt_info.setVisibility(View.GONE);
                return;
            }
            case R.id.but_penerangan: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_penerangan));

                return;
            }
            case R.id.but_restoran: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_restoran));

                return;
            }
            case R.id.but_walet: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_walet));
                return;
            }
            case R.id.but_reklame: {
                txt_info.setVisibility(View.GONE);
                showInfo(getString(R.string.info_reklame));

            }

        }
    }

    private void showInfo(final String info) {
        showDialog();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                img_logo.setVisibility(View.GONE);
                txt_info.setVisibility(View.VISIBLE);
                txt_info.setText(Html.fromHtml(info));
                hideDialog();


            }
        }, 500);


    }

    private void init_p_dialog() {

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        View viewInflated = LayoutInflater.from(getActivity()).inflate(R.layout.p_dialog,
                (ViewGroup) v.findViewById(android.R.id.content), false);
        AVLoadingIndicatorView avi = viewInflated.findViewById(R.id.avi);
        builder.setView(viewInflated);
        dialog = builder.create();
        avi.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER_HORIZONTAL;

    }

    private void showDialog() {
        if (!dialog.isShowing())
            dialog.show();
    }

    private void hideDialog() {
        if (dialog.isShowing())
            dialog.dismiss();
    }

    private void initStatusBar() {
        Window window = getActivity().getWindow();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            SystemBarTintManager tintManager = new SystemBarTintManager(getActivity());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        }

    }

    public static InfoPajakFragment newInstance(String text) {

        Bundle args = new Bundle();
        args.putString("msg", text);

        InfoPajakFragment fragment = new InfoPajakFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
