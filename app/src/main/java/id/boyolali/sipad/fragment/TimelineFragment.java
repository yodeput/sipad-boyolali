package id.boyolali.sipad.fragment;

import android.os.Bundle;
import android.os.Handler;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;
import id.boyolali.sipad.adapter.Timeline;
import id.boyolali.sipad.adapter.TimelineAdapter;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.siPAD.getInstance;


public class TimelineFragment extends Fragment implements TimelineAdapter.TimelinesAdapterListener {
    private static final String TAG = TimelineFragment.class.getSimpleName();
    public static AlertDialog pDialog;

    private PrefManager session;
    private String nama, npwpd, nasabah_id;
    @BindView(R.id.txt_require_login)
    TextView txt_require_login;
    @BindView(R.id.cv_recycler)
    CardView cv_recycler;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.img_refresh)
    ImageView img_refresh;

    private View v;
    private List<Timeline> TimelineList;
    private TimelineAdapter mAdapter;
    private AlertDialog dialog;

    @OnClick(R.id.img_exit)
    void close() {

        siPAD.getInstance().dialog_exit(getActivity(), v, "Akan keluar Aplikasi?");

    }

    @OnClick(R.id.img_refresh)
    void refresh() {

        clear();
        siPAD.getInstance().showDialog();
        boolean isConnected = ConnectivityReceiver.isOnline(getActivity());
        if (isConnected) {
            getInstance().refresh_data_user(getActivity());
        }
        read_json_Timeline();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                siPAD.getInstance().hideDialog();
            }
        }, 1500);


    }

    public static TimelineFragment newInstance(String text) {

        Bundle args = new Bundle();
        args.putString("msg", text);

        TimelineFragment fragment = new TimelineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_timeline, container, false);
        ButterKnife.bind(this, v);
        session = new PrefManager(getActivity().getApplicationContext());
        TimelineList = new ArrayList<>();
        mAdapter = new TimelineAdapter(getActivity(), TimelineList, this);
        siPAD.getInstance().init_p_dialog(getActivity(), v);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);


        boolean isConnected = ConnectivityReceiver.isOnline(getActivity());
        if (isConnected) {
            sessionCheck();
        }

        return v;
    }

    private void sessionCheck() {


        session = new PrefManager(getActivity().getApplicationContext());

        if (session.isLoggedIn()) {

            txt_require_login.setVisibility(View.GONE);
            img_refresh.setVisibility(View.VISIBLE);
            read_json_Timeline();

        } else {

            cv_recycler.setVisibility(View.GONE);

        }
    }


    private void read_json_Timeline() {

        String jsonString = siPAD.getInstance().get_file_data(getActivity(), "timeLine");

        List<Timeline> items = new Gson().fromJson(jsonString, new TypeToken<List<Timeline>>() {
        }.getType());


        // adding contacts to contacts list

        TimelineList.addAll(items);

    }

    @Override
    public void onTimelineSelected(Timeline Timeline) {
        //Toast.makeText(getActivity().getApplicationContext(), "Informasi: " + SubJenisUsaha.getTimeline(), Toast.LENGTH_LONG).show();
    }

    public void clear() {
        TimelineList.clear();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        clear();
        boolean isConnected = ConnectivityReceiver.isOnline(getActivity());
        if (isConnected) {
            sessionCheck();
        }

        super.onResume();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}