package id.boyolali.sipad.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetView;
import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.smarteist.autoimageslider.SliderLayout;
import com.smarteist.autoimageslider.SliderView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.boyolali.sipad.activity.WizardActivity;
import id.boyolali.sipad.activity.MenuPopup;
import id.boyolali.sipad.R;
import id.boyolali.sipad.activity.MenuPopupPBB;
import id.boyolali.sipad.util.ConnectivityReceiver;
import id.boyolali.sipad.util.PrefManager;
import id.boyolali.sipad.util.siPAD;

import static id.boyolali.sipad.util.JenisPajak.cek_pajak;


public class HomeFragment extends Fragment implements View.OnClickListener,ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = HomeFragment.class.getSimpleName();
    private View v;
    private ImageView img_exit;
    private PrefManager session;
    private int i;

    SliderLayout sliderLayout;
    @BindView(R.id.but_airtanah)
    ImageButton but_airtanah;
    @BindView(R.id.but_bphtb)
    ImageButton but_bphtb;
    @BindView(R.id.but_hiburan)
    ImageButton but_hiburan;
    @BindView(R.id.but_hotel)
    ImageButton but_hotel;
    @BindView(R.id.but_minerba)
    ImageButton but_minerba;
    @BindView(R.id.but_parkir)
    ImageButton but_parkir;
    @BindView(R.id.but_pbb)
    ImageButton but_pbb;
    @BindView(R.id.but_penerangan)
    ImageButton but_penerangan;
    @BindView(R.id.but_restoran)
    ImageButton but_restoran;
    @BindView(R.id.but_walet)
    ImageButton but_walet;
    @BindView(R.id.but_reklame)
    ImageButton but_reklame;

    @BindView(R.id.txt_error)TextView txt_error;

    private Class newAct;

    public static HomeFragment newInstance(String text) {

        Bundle args = new Bundle();
        args.putString("msg", text);

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, v);
        initStatusBar();
        session = new PrefManager(getActivity().getApplicationContext());
        img_exit = v.findViewById(R.id.img_exit);
        sliderLayout = v.findViewById(R.id.imageSlider);
        sliderLayout.setIndicatorAnimation(SliderLayout.Animations.FILL); //set indicator animation by using SliderLayout.Animations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderLayout.setScrollTimeInSec(8);

            if (session.isLoggedIn()){
                initImageSlider_logedin(sliderLayout);
            } else {
                initImageSlider(sliderLayout);
            }

            siPAD.getInstance().req_slideshow_data(getActivity(),v);
            sliderLayout.setVisibility(View.VISIBLE);
            txt_error.setVisibility(View.GONE);


        img_exit.setOnClickListener(this);
        but_airtanah.setOnClickListener(this);
        but_bphtb.setOnClickListener(this);
        but_hiburan.setOnClickListener(this);
        but_hotel.setOnClickListener(this);
        but_minerba.setOnClickListener(this);
        but_parkir.setOnClickListener(this);
        but_pbb.setOnClickListener(this);
        but_penerangan.setOnClickListener(this);
        but_restoran.setOnClickListener(this);
        but_walet.setOnClickListener(this);
        but_reklame.setOnClickListener(this);


        but_airtanah.setColorFilter(ContextCompat.getColor(getActivity(),R.color.airtanah));
        but_bphtb.setColorFilter(ContextCompat.getColor(getActivity(),R.color.bphtb));
        but_hiburan.setColorFilter(ContextCompat.getColor(getActivity(),R.color.hiburan));
        but_hotel.setColorFilter(ContextCompat.getColor(getActivity(),R.color.hotel));
        but_minerba.setColorFilter(ContextCompat.getColor(getActivity(),R.color.minerba));
        but_parkir.setColorFilter(ContextCompat.getColor(getActivity(),R.color.parkir));
        but_pbb.setColorFilter(ContextCompat.getColor(getActivity(),R.color.pbb));
        but_penerangan.setColorFilter(ContextCompat.getColor(getActivity(),R.color.penerangan));
        but_restoran.setColorFilter(ContextCompat.getColor(getActivity(),R.color.restoran));
        but_walet.setColorFilter(ContextCompat.getColor(getActivity(),R.color.walet));
        but_reklame.setColorFilter(ContextCompat.getColor(getActivity(),R.color.reklame));

        String wizard = session.getPref("wizard");
        if (wizard.contains("true")){
            startActivity(new Intent(getActivity(),WizardActivity.class));
        }

        String taptarget = session.getPref("taptarget");
        if (taptarget.isEmpty()){
            taptarget();
        }

        return v;
    }

    private void taptarget(){
        TapTargetView.showFor(getActivity(),
                TapTarget.forView(v.findViewById(R.id.but_parkir),
                        "Menu Pajak",
                        "Ini adalah salah satu menu pembayaran pajak\nSilahkan tekan untuk mencoba")
                        .textColor(R.color.black)
                        .transparentTarget(false)
                        .tintTarget(false)
                        .targetCircleColor(R.color.white)
                        .outerCircleColor(R.color.colorAccent)
                        .cancelable(false),
        new TapTargetView.Listener() {
            @Override
            public void onTargetClick(TapTargetView view) {
                super.onTargetClick(view);
               view.dismiss(true);
               session.addPref("taptarget","false");
                Class a = MenuPopup.class;
                Intent i = new Intent(getActivity(), a);
                i.putExtra("title", "PAJAK PARKIR");
                i.putExtra("jenis_pajak", "8");
                startActivity(i);
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_exit: {

                siPAD.getInstance().dialog_exit(getActivity(), v,"Akan keluar Aplikasi?");

                return;
            }
            case R.id.but_airtanah: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK AIR TANAH");
                i.putExtra("jenis_pajak", "2");
                cek("2",i);
                return;
            }
            case R.id.but_bphtb: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK BPHTB");
                i.putExtra("jenis_pajak", "12");
                cek("12",i);

                return;
            }
            case R.id.but_hiburan: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK HIBURAN");
                i.putExtra("jenis_pajak", "10");
                //startActivity(i);
                cek("10",i);
                return;
            }
            case R.id.but_hotel: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK HOTEL");
                i.putExtra("jenis_pajak", "7");
                cek("7",i);

                return;
            }
            case R.id.but_minerba: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK MINERBA");
                i.putExtra("jenis_pajak", "3");
                cek("3",i);

                return;
            }
            case R.id.but_parkir: {
                Class a = MenuPopup.class;
                Intent i = new Intent(getActivity(), a);
                i.putExtra("title", "PAJAK PARKIR");
                i.putExtra("jenis_pajak", "8");
                cek("8",i);

                return;
            }
            case R.id.but_pbb: {

                Intent i = new Intent(getActivity(), MenuPopupPBB.class);
                i.putExtra("title", "PAJAK PBB");
                i.putExtra("jenis_pajak", "9");
                startActivity(i);

                return;
            }
            case R.id.but_penerangan: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK PENERANGAN");
                i.putExtra("jenis_pajak", "5");
                cek("5",i);

                return;
            }
            case R.id.but_restoran: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK RESTORAN");
                i.putExtra("jenis_pajak", "4");
                cek("4",i);
                return;
            }
            case R.id.but_walet: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK BURUNG WALET");
                i.putExtra("jenis_pajak", "6");
                cek("6",i);
                return;
            }
            case R.id.but_reklame: {

                Intent i = new Intent(getActivity(), MenuPopup.class);
                i.putExtra("title", "PAJAK REKLAME");
                i.putExtra("jenis_pajak", "1");
                cek("1",i);


            }

        }
    }

    private void cek(String jenis_pajak, Intent i){

        String status = siPAD.getInstance().see_status_pajak(cek_pajak(jenis_pajak));
        if (status.equals("0")){
            siPAD.getInstance().dialog_alert(getActivity(),v,"Layanan sedang dalam perbaikan");
        } else if (status.equals("1")){
            startActivity(i);
        }  else if (status.equals("2")){
            siPAD.getInstance().toastInfo(getActivity(),"Layanan belum tersedia");
        }

    }




    private void initImageSlider(SliderLayout sliderLayout) {

        final String[] img = {"","","",""};
        final String[] desc = {"","","",""};

        /*
        String img_url0 = prefManager.getPref("img_url0");
        String img_url1 = prefManager.getPref("img_url1");
        String img_url2 = prefManager.getPref("img_url2");
        String img_url3 = prefManager.getPref("img_url3");
        String deskipsi0 = prefManager.getPref("deskipsi0");
        String deskipsi1 = prefManager.getPref("deskipsi1");
        String deskipsi2 = prefManager.getPref("deskipsi2");
        String deskipsi3 = prefManager.getPref("deskipsi3");
        */
        for (i=0; i <= 3; i++) {
            img[i] =  session.getPref("img_url"+i);
            desc[i] =  session.getPref("deskipsi"+i);
            SliderView sliderView = new SliderView(getContext());

            switch (i) {
                case 0:
                    sliderView.setImageUrl(img[0]);
                    sliderView.setDescription(desc[0]);
                    break;
                case 1:
                        sliderView.setImageUrl(img[1]);
                        sliderView.setDescription(desc[1]);
                    break;
                case 2:
                    sliderView.setImageUrl(img[2]);
                    sliderView.setDescription(desc[2]);
                    break;
                case 3:
                    sliderView.setImageUrl(img[3]);
                    sliderView.setDescription(desc[3]);
                    break;
            }

            final int finalI = i;
            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    FragmentManager fragmentManager = getFragmentManager();
                    SliderDialogFragment newFragment = new SliderDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("judul", desc[finalI]);
                    bundle.putString("gambar", img[finalI]);

                    newFragment.setArguments(bundle);

                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
                }
            });
            sliderLayout.addSliderView(sliderView);
        }
    }

    private void initImageSlider_logedin(SliderLayout sliderLayout) {



        String img_url0 = session.getPref("img_url0");
        String img_url1 = session.getPref("img_url5");
        String img_url2 = session.getPref("img_url2");
        String img_url3 = session.getPref("img_url3");
        String deskipsi0 = session.getPref("deskipsi0");
        String deskipsi1 = session.getPref("deskipsi5");
        String deskipsi2 = session.getPref("deskipsi2");
        String deskipsi3 = session.getPref("deskipsi3");


        final String[] img = {img_url0,img_url1,img_url2,img_url3};
        final String[] desc = {deskipsi0,deskipsi1,deskipsi2,deskipsi3};
        /*
        for (int a=0; a <= 4; a++) {
            img[a] = session.getPref("img_url" + a);
            desc[a] = session.getPref("deskipsi" + a);
        }*/

        for (i=0; i <= 3; i++) {

            SliderView sliderView = new SliderView(getContext());

            switch (i) {
                case 0:
                    sliderView.setImageUrl(img[0]);
                    sliderView.setDescription(desc[0]);
                    break;
                case 1:
                    sliderView.setImageUrl(img[1]);
                    sliderView.setDescription(desc[1]);
                    break;
                case 2:
                    sliderView.setImageUrl(img[2]);
                    sliderView.setDescription(desc[2]);
                    break;
                case 3:
                    sliderView.setImageUrl(img[3]);
                    sliderView.setDescription(desc[3]);
                    break;

            }
            final int finalI = i;
            sliderView.setImageScaleType(ImageView.ScaleType.FIT_XY);
            sliderView.setOnSliderClickListener(new SliderView.OnSliderClickListener() {
                @Override
                public void onSliderClick(SliderView sliderView) {
                    FragmentManager fragmentManager = getFragmentManager();
                    SliderDialogFragment newFragment = new SliderDialogFragment();
                    Bundle bundle = new Bundle();
                        bundle.putString("judul", desc[finalI]);
                        bundle.putString("gambar", img[finalI]);

                    newFragment.setArguments(bundle);

                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
                }
            });
            sliderLayout.addSliderView(sliderView);
        }
    }

    private void initStatusBar() {
        Window window = getActivity().getWindow();

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            SystemBarTintManager tintManager = new SystemBarTintManager(getActivity());
            tintManager.setStatusBarTintEnabled(true);
            tintManager.setTintColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        // register connection status listener
        siPAD.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        //siPAD.getInstance().showalert(getActivity(), isConnected);
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }
}
