package id.boyolali.sipad.fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.boyolali.sipad.R;

public class BeritaDialogFragment extends DialogFragment {

    private int request_code = 0;
    private View root_view;
    private TextView txt_judul,txt_tanggal,txt_isi;
    private ImageView img_berita;
    @BindView(R.id.bt_close)ImageButton bt_close;

    @OnClick (R.id.bt_close) void close(){
        dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.dialog_berita, container, false);
        ButterKnife.bind(this,root_view);
        String judul = getArguments().getString("judul");
        String isi = getArguments().getString("isi");
        String gambar = getArguments().getString("gambar");
        String date = getArguments().getString("date");
        txt_judul = (TextView) root_view.findViewById(R.id.txt_judul);
        txt_tanggal = (TextView) root_view.findViewById(R.id.txt_tanggal);
        txt_isi = (TextView) root_view.findViewById(R.id.txt_isi);
        img_berita = (ImageView) root_view.findViewById(R.id.img_berita);

        txt_judul.setText(judul);
        txt_tanggal.setText(date);
        txt_isi.setText(Html.fromHtml(isi));

        Glide.with(getActivity())
                .load(gambar)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL))
                .into(img_berita);

        root_view.setFocusableInTouchMode(true);
        root_view.requestFocus();
        root_view.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {

                    dismiss();
                    return true;
                }
                return false;
            }
        });
        return root_view;
    }



    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }


    public void onBackPressed() {
      dismiss();
    }


}